<?php

/*
  Plugin Name: Sodexo Testimonials
  Description: Plugin for manage Testimonials custom post type
  Version: 0.1.1
  Author: Digitas LBI
  Author URI: https://www.digitaslbi.com/
  License: GPLv2 or later
  Text Domain: dlbi-sodexo-testimonials
 */

// Plugin Consts
define('SOD_TES_VERSION', '1.0');
define('SOD_TES_URL', plugins_url('', __FILE__));
define('SOD_TES_DIR', dirname(__FILE__));
define('SOD_TES_PTYPE', 'testimonial');

//Load class
require( SOD_TES_DIR . '/inc/class-client.php' );

//Init
function sodexo_testimonial_init() {
    new SodexoTestimonials_Client();

    // Load plugin textdomain
    load_plugin_textdomain( 'dlbi-sodexo-testimonials', false, dirname(plugin_basename(__FILE__)) . '/languages/');
}

add_action('plugins_loaded', 'sodexo_testimonial_init', 11);
