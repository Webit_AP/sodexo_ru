<?php

class SodexoTestimonials_Client {

    public function __construct() {
        add_action('init', array($this, 'sodexo_testimonial_register_cpt'));
        add_action('pre_get_posts', array($this, 'sodexo_alter_main_query'));
        add_filter('acf/settings/save_json', array($this, 'sodexo_testimonials_acf_json_save_point'));
        add_filter('acf/settings/load_json', array($this, 'sodexo_testimonials_acf_json_load_point'));
        
    }

    /**
     * Register the testimonial post type
     *
     * @return boolean
     * @author Digitas LBI
     */
    public static function sodexo_testimonial_register_cpt() {

        $slug_testimonials = get_field('url_testimonials', 'option') ? get_field('url_testimonials', 'option') : SOD_TES_PTYPE;

        register_post_type(SOD_TES_PTYPE, array(
            'labels' => array(
                'name' => __('Testimonial', 'dlbi-sodexo-testimonials'),
                'singular_name' => __('Testimonial', 'dlbi-sodexo-testimonials'),
                'add_new' => __('Add new', 'dlbi-sodexo-testimonials'),
                'add_new_item' => __('Add new Testimonial', 'dlbi-sodexo-testimonials'),
                'edit_item' => __('Edit Testimonial', 'dlbi-sodexo-testimonials'),
                'new_item' => __('New Testimonial', 'dlbi-sodexo-testimonials'),
                'view_item' => __('Show Testimonial', 'dlbi-sodexo-testimonials'),
                'search_items' => __('Search Testimonials', 'dlbi-sodexo-testimonials'),
                'not_found' => __('No Testimonials found', 'dlbi-sodexo-testimonials'),
                'not_found_in_trash' => __('No Testimonials found in trash', 'dlbi-sodexo-testimonials'),
                'parent_item_colon' => __('Parent Testimonial', 'dlbi-sodexo-testimonials'),
            ),
            'description' => '',
            'publicly_queryable' => true,
            'exclude_from_search' => false,
            'map_meta_cap' => true,
            'capability_type' => 'post',
            'public' => true,
            'hierarchical' => false,
            'rewrite' => array(
                'slug' => $slug_testimonials,
                'with_front' => true,
                'pages' => true,
                'feeds' => true,
            ),
            'has_archive' => $slug_testimonials,
            'query_var' => SOD_TES_PTYPE,
            'supports' => array(
                0 => 'title',
                1 => 'editor',
                2 => 'thumbnail',
                3 => 'excerpt',
                4 => 'page-attributes'
            ),
            'taxonomies' => array('category', 'post_tag'),
            'show_ui' => true,
            'menu_position' => 25,
            'menu_icon' => 'dashicons-format-quote',
            'can_export' => true,
            'show_in_nav_menus' => true,
            'show_in_menu' => true,
        ));


        return true;
    }

    /**
     * Register the faq taxonomies
     *
     * @param WP_Query $query
     * @return boolean
     * @author Digitas LBI
     */
    public function sodexo_alter_main_query($query) {
        if ($query->is_post_type_archive(SOD_TES_PTYPE) && $query->is_main_query() && !is_admin()) {
            $query->set('posts_per_page', 16);
        }
    }

    public function sodexo_testimonials_acf_json_save_point($path) {

        // update path
        $path = SOD_TES_DIR . '/acf-json';

        // return
        return $path;
    }

    public function sodexo_testimonials_acf_json_load_point($paths) {
        // remove original path (optional)
        unset($paths[0]);

        // append path
        $paths[] = SOD_TES_DIR . '/acf-json';

        // return
        return $paths;
    }

}
