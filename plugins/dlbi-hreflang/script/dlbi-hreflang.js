jQuery(function ($) {
    console.log("SOXINT-914");

    var hrefLangMetabox = $("#acf-group_5c599c8e01c6b");
    var permalink = $("#sample-permalink a").attr("href");
    var postId = $("#post_ID").val();
    if (hrefLangMetabox.length) {
        hrefLangMetabox.addClass("loading").find(".inside").append("<div class='overlay'>" +
            "<svg class=\"yoast-svg-icon yoast-svg-icon-loading-spinner SvgIcon__StyledSvg-jBzRth mPAyu\" aria-hidden=\"true\" role=\"img\" focusable=\"false\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 66 66\" fill=\"#64a60a\">" +
            "<circle class=\"path\" fill=\"none\" stroke-width=\"6\" stroke-linecap=\"round\" cx=\"33\" cy=\"33\" r=\"30\"></circle>" +
            "</svg>" +
            "</div>");
        var hasValue = false;
        var hasCurrent = false;
        var definedByHreflangPlugin = [];
        var index = 0;
        hrefLangMetabox.find('[data-name=\'dlbi_hreflang\'] input').each(function () {
            var $my = $(this);
            var val = $my.val();
            if ($my.val()) {
                hasValue = true;
                definedByHreflangPlugin[index] = {"hreflang": val};
                ++index;
            }
        });
        index = 0;
        hrefLangMetabox.find('[data-name=\'dlbi_href\'] input').each(function () {
            var $my = $(this);
            var val = $my.val();
            if ($my.val()) {
                hasValue = true;
                definedByHreflangPlugin[index]["href"] = val;
                ++index;
            }
        });
        console.log(definedByHreflangPlugin);
        $.ajax({
            url: ajaxurl,
            timeout: 5000,
            data: {
                action: "get_current_href_lang",
                post_id: postId,
                url: permalink
            },
            success: function (data) {
                var parsed = JSON.parse(data);
                if (parsed['items'] && parsed['items'].length) {
                    hrefLangMetabox.find(".acf-label").append("<div class='already-defined'><h4><em>Already defined by plugin</em></h4><ul></ul></div>");
                    var $alreadyDefined = hrefLangMetabox.find(".already-defined ul");
                    // console.log(parsed['items']);
                    $.each(parsed['items'], function (index, item) {
                        var inPlugin = false;
                        $.each(definedByHreflangPlugin, function (ind, defined) {
                            if ((item["hreflang"] === defined["hreflang"]) && (item["href"] === defined["href"])) {
                                inPlugin = true;
                            }
                        });
                        if (!inPlugin) {
                            $alreadyDefined.append("<li><span class='hreflang'>" + item["hreflang"] + "</span><span class='href'>" + item["href"] + "</span></li>")
                        }
                    });
                }
                hrefLangMetabox.removeClass("loading");
            },
            error: function () {
                hrefLangMetabox.find(".acf-label").append("<div class='already-defined'><h4><em>Already defined by plugin</em></h4><ul></ul></div>");
                var $alreadyDefined = hrefLangMetabox.find(".already-defined ul");
                $alreadyDefined.append("<p>Unable to join <a target='_blank' href='view-source:" + permalink + "'>" + permalink + "</a>. <strong>Please check</strong> hreflang already set by yourself.</p>");

                hrefLangMetabox.removeClass("loading");
            }
        });


        console.log("hrefLangMetabox", hasValue, hasCurrent);

    }
});