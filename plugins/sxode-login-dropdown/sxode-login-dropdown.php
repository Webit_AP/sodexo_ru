<?php

/*
  Plugin Name: [sodexo_de] Login dropdown
  Description: Plugin for integrating a dropdown menu for the login button

  Version: 1.0
  Author: Henrik Heil, b.relevant - Agile Digital Marketing Agency GmbH
  Author URI: http://www.b-relevant.agency
  Text Domain: sxode-login-dropdown
 */
// Plugin Consts
define('SXODE_LOGIN_DROPDOWN_DIR', dirname(__FILE__));

// Load class
require(SXODE_LOGIN_DROPDOWN_DIR . '/classes/sxode_login_dropdown.php');

// Init
function sxode_login_dropdown_init()
{
    new Sxode_Login_Dropdown();
}

add_action('plugins_loaded', 'sxode_login_dropdown_init', 11);
