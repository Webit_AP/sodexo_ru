<?php

class Sxode_Login_Dropdown
{
    public function __construct() {
        add_filter('init', [$this, 'add_options_sub_page']);

        //  Set load/save points for ACF Pro fields
        add_filter('acf/settings/save_json', [$this, 'acf_json_save_point']);
        add_filter('acf/settings/load_json', [$this, 'acf_json_load_point']);
    }

    /**
     * Add an ACF sub options page in "Theme Settings"
     */
    public function add_options_sub_page()
    {
        acf_add_options_sub_page(array(
            'page_title' => 'Login Dropdown',
            'menu_title' => 'Login Dropdown',
            'parent_slug' => 'acf-options-theme-settings',
        ));        
    }

    /**
     * Set save point for plugin-specific ACF pro fields
     *
     * @return string
     */
    public function acf_json_save_point($path)
    {
        $path = SXODE_LOGIN_DROPDOWN_DIR . '/acf-json';

        return $path;
    }

    /**
     * Set load point for plugin-specific ACF pro fields
     *
     * @return string
     */    
    public function acf_json_load_point($paths)
    {
        unset($paths[0]);
        $paths[] = SXODE_LOGIN_DROPDOWN_DIR . '/acf-json';

        return $paths;
    }

    public static function render_for_top_menu()
    {
        $menu_type = 'top';
        include(SXODE_LOGIN_DROPDOWN_DIR . '/templates/login-dropdown.php');
    }

    public static function render_for_combined_menu()
    {
        $menu_type = 'combined';
        include(SXODE_LOGIN_DROPDOWN_DIR . '/templates/login-dropdown.php');
    }    
}
