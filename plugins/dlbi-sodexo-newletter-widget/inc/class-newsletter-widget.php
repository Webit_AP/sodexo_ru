<?php

class SodexoWidget_Newsletter extends WP_Widget {

    /**
     * construct
     */
    public function __construct() {

	parent::__construct('newsletter', 'Newsletter', array(
	    'classname' => '',
	    'description' => "Newsletter subscription form."
		)
	);
    }

    /**
     * widget
     *
     * @param array $args
     * @param array $instance
     * @return
     */
    public function widget($args, $instance) {
	global $wp;
	$current_url = home_url(add_query_arg(array(), $wp->request));
	?>

	<form class="form_insc_news" action="<?php echo get_stylesheet_directory_uri() ?>/ajax/newsletter.php" method="post">
	    <div class="emailfooternewsletter">

		<?php
		// Display in footer
		if (isset($instance['location'])) {
		    ?>
	    	<p class="mt-3"><?php echo apply_filters('widget_title', $instance['title']); ?></p>
	    	<div class="form-group">
	    	    <div class="input-group">
	    		<label for="emailnewsletterfooter" class="sr-only"><?php echo __('Email address', 'lbi-sodexo-theme'); ?></label>
	    		<input type="email" name="newsletter_email" data-layer-position="footer" class="form-control" id="emailnewsletterfooter" aria-describedby="emailfooterNewsletteraria" placeholder="<?php echo $instance['placeholder']; ?>" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$">
	    		<span class="input-group-btn">
	    		    <button class="btn btn-secondary" type="submit"><?php _e('OK', 'dlbi-sodexo-newletter-widget'); ?></button>
	    		</span>
	    		<small id="emailfooterNewsletteraria" class="form-text sr-only"><?php echo __('Enter your email to subscribe', 'lbi-sodexo-theme'); ?></small>
	    	    </div>
			<?php if ($instance['label_checkbox']) { ?>
			    <input required id="label-checkbox-footer" name="newsletter_checkbox_footer" value="" type="checkbox">
			    <label for="label-checkbox-footer">
				<?php
				$widget_id = $args['widget_id'];
				if (get_field('terms_acceptance_show_block', 'widget_' . $widget_id)):
				    ?>
		    		<a href="<?php echo get_field('terms_acceptance_link', 'widget_' . $widget_id); ?>" <?php if (get_field('terms_acceptance_link_target', 'widget_' . $widget_id)): ?> target="_blank" <?php endif ?>><?php echo $instance['label_checkbox']; ?></a>
				<?php else: ?>
				    <?php echo $instance['label_checkbox']; ?>
				<?php endif; ?>
			    </label>
			<?php } ?>
	    	</div>
		    <?php
		} else {
		    echo $args['before_widget'];
		    echo $args['before_title'];
		    echo apply_filters('widget_title', $instance['title']);
		    echo $args['after_title'];
		    ?>
	    	<div class="col-md-4">
	    	    <label for="newsletter_email"><?php echo $instance['description']; ?></label>
	    	</div>
	    	<div class="col-md-4">
	    	    <label for="emailnewsletterwidget" class="sr-only"><?php echo __('Email address', 'lbi-sodexo-theme'); ?></label>
	    	    <input id="emailnewsletterwidget" data-layer-position="blog" name="newsletter_email" type="email" placeholder="<?php echo $instance['placeholder']; ?>" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"/>
	    	</div>
	    	<div class="col-md-4">
			<?php if ($instance['label_checkbox']) { ?>
			    <input required  id="label-checkbox-widget" name="newsletter_checkbox_widget" value="" type="checkbox">
			    <label for="label-checkbox-widget">
				<?php
				if (in_array('widget_id', $args)):
				    $widget_id = $args['widget_id'];
				    if (get_field('terms_acceptance_show_block', 'widget_' . $widget_id)):
					?>
					<a class="label-checkbox-widget-link" href="<?php echo get_field('terms_acceptance_link', 'widget_' . $widget_id); ?>" <?php if (get_field('terms_acceptance_link_target', 'widget_' . $widget_id)): ?> target="_blank" <?php endif ?>><?php echo $instance['label_checkbox']; ?></a>
				    <?php endif; ?>
				<?php else: ?>
				    <a class="label-checkbox-widget-link" href="<?php echo get_field('terms_acceptance_link', 'widget_' . $args['widget_id']); ?>" <?php if (get_field('terms_acceptance_link_target', 'widget_' . $args['widget_id'])): ?> target="_blank" <?php endif ?>><?php echo $instance['label_checkbox']; ?></a>
				<?php endif;
				?>
			    </label>
			<?php } ?>
	    	</div>
	    	<input type="submit" value="<?php echo __('Subscribe', 'dlbi-sodexo-newletter-widget'); ?>" name="newsletter_submit" />
		    <?php
		    echo $args['after_widget'];
		}
		?>

	    </div>
	</form>

	<?php
    }

    /**
     * update
     *
     * @param string $new
     * @param string $old
     * @return
     */
    public function update($new, $old) {
	return $new;
    }

    /**
     * form
     *
     * @param array $instance
     * @return
     */
    public function form($instance) {
	$title = isset($instance['title']) ? $instance['title'] : '';
	$location = isset($instance['location']) ? $instance['location'] : '';
	$description = isset($instance['description']) ? $instance['description'] : '';
	$placeholder = isset($instance['placeholder']) ? $instance['placeholder'] : '';
	$label_checkbox = isset($instance['label_checkbox']) ? $instance['label_checkbox'] : '';
	$label_checkbox = isset($instance['label_checkbox']) ? $instance['label_checkbox'] : '';

	$checked = "";
	if (isset($instance['location'])) {
	    $checked = " checked";
	}
	?>
	<p>
	    <label for="<?php echo $this->get_field_name('title'); ?>"><?php _e('Title:', 'dlbi-sodexo-newletter-widget'); ?></label>
	    <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
	</p>
	<p>
	    <label for="<?php echo $this->get_field_name('location'); ?>"><?php _e('Location footer:', 'dlbi-sodexo-newletter-widget'); ?></label>
	    <input class="widefat"  id="<?php echo $this->get_field_id('location'); ?>" type="checkbox" name="<?php echo $this->get_field_name('location'); ?>" value="<?php echo $location; ?>" <?php echo $checked; ?> >
	</p>
	<p>
	    <label for="<?php echo $this->get_field_name('description'); ?>"><?php _e('Description:', 'dlbi-sodexo-newletter-widget'); ?></label>
	    <textarea class="widefat" id="<?php echo $this->get_field_id('description'); ?>" name="<?php echo $this->get_field_name('description'); ?>" rows="1" cols="40"><?php echo $description; ?></textarea>
	</p>

	<p>
	    <label for="<?php echo $this->get_field_name('placeholder'); ?>"><?php _e('Placeholder:', 'dlbi-sodexo-newletter-widget'); ?></label>
	    <input class="widefat" id="<?php echo $this->get_field_id('placeholder'); ?>" name="<?php echo $this->get_field_name('placeholder'); ?>" type="text" value="<?php echo $placeholder; ?>" />
	</p>

	<p>
	    <label for="<?php echo $this->get_field_name('label_checkbox'); ?>"><?php _e('Label checkbox:', 'dlbi-sodexo-newletter-widget'); ?></label>
	    <input class="widefat" id="<?php echo $this->get_field_id('label_checkbox'); ?>" name="<?php echo $this->get_field_name('label_checkbox'); ?>" type="text" value="<?php echo $label_checkbox; ?>" />
	</p>
	<?php
    }

}
