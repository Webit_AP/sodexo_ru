<?php

/*
  Plugin Name: Widget Customized For The Newsletter
  Description: Widget customized for the newsletter
  Version: 1.0
  Author: Digitas LBI
  Author URI: https://www.digitaslbi.com/
  License: GPLv2 or later
  Text Domain: dlbi-sodexo-newletter-widget
 */

// Plugin Consts
define('SOD_NEW_VERSION', '1.0');
define('SOD_NEW_URL', plugins_url('', __FILE__));
define('SOD_NEW_DIR', dirname(__FILE__));

//Load class
require( SOD_NEW_DIR . '/inc/class-newsletter-widget.php' );

//Init widget newletter
function sodexo_widget_init() {
    register_widget('SodexoWidget_Newsletter');

    // Load plugin textdomain
    load_plugin_textdomain( 'dlbi-sodexo-newletter-widget', false, dirname(plugin_basename(__FILE__)) . '/languages/');
}

add_action('widgets_init', 'sodexo_widget_init');
