<?php
/*
Plugin Name: DLBI DQE Plugin
Description: Plugin wich allow to use DQE platform
Version: 1.0
*/

define( 'GF_SIMPLE_FIELD_ADDON_VERSION', '1.0' );
define('SOD_DQE_URL', plugins_url('', __FILE__));
define('SOD_DQE_DIR', dirname(__FILE__));
define('SOD_DQE_PTYPE', 'dqe');

add_action( 'gform_loaded', array( 'GF_Simple_Field_AddOn_Bootstrap', 'load' ), 5 );

class GF_Simple_Field_AddOn_Bootstrap {

    public static function load() {

        if ( ! method_exists( 'GFForms', 'include_addon_framework' ) ) {
            return;
        }

        require_once( 'class-gfsimplefieldaddon.php' );

        GFAddOn::register( 'GFSimpleFieldAddOn' );
    }

}