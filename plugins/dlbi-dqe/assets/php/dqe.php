<?php

function getData($url, $timeout = 15) {
    $session = curl_init($url);
    curl_setopt($session, CURLOPT_TIMEOUT, $timeout);
    curl_setopt($session, CURLOPT_HEADER, false);
    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($session, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:51.0) Gecko/20100101 Firefox/51.0');
    curl_setopt($session, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($session, CURLOPT_SSL_VERIFYPEER, 0);

    $response = curl_exec($session);
    if (!$response) {
        echo curl_error($session);
        return false;
    }
    curl_close($session);
    return $response;
}

$license   = 'O4X_GwtZ.X-U23A4A51hICTiBi';
$url = $_POST['url'] . $license;
//Vérification nécessaire pour éviter toute injection d'URL non valide
$parts = explode('.com', $url);
if ($parts[0] != 'https://prod2.dqe-software' && $parts[0] != 'https://preprod.dqe-software' && substr($url, 0, 21) != 'http://37.187.165.132' && substr($url, 0, 21) != 'http://192.99.201.129' && substr($url, 0, 14) != 'http://192.168') die();

// die(var_dump($url));

echo getData($url);
