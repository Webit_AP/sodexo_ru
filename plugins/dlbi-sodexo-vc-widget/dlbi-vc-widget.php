<?php
/**
 *  Plugin Name: Custom widgets for Visual Composer Plugin
 *  Description: Extend Visual Composer with your own set of widgets.
 *  Version: 1.0
 *  Author: Digitas LBI
 *  Author URI: https://www.digitaslbi.com/
 *  License: GPLv2 or later
 */

// Plugin Consts
define( 'SOD_VCWID_VERSION', '1.0' );
define( 'SOD_VCWID_URL', plugins_url( '', __FILE__ ) );
define( 'SOD_VCWID_DIR', dirname( __FILE__ ) );

// Load class
require SOD_VCWID_DIR . '/inc/class-client.php';
require SOD_VCWID_DIR . '/inc/class-admin.php';
require SOD_VCWID_DIR . '/inc/widgets/class-carousel-italia.php';
require SOD_VCWID_DIR . '/inc/widgets/class-store-locator.php';
require SOD_VCWID_DIR . '/inc/widgets/class-title-subtitle.php';
require SOD_VCWID_DIR . '/inc/widgets/class-documents-needed.php';
require SOD_VCWID_DIR . '/inc/widgets/class-carousel-they-trust-us.php';
require SOD_VCWID_DIR . '/inc/widgets/class-boxes.php';
require SOD_VCWID_DIR . '/inc/widgets/class-iframe-store-locator.php';
require SOD_VCWID_DIR . '/inc/widgets/class-carousel.php';
require SOD_VCWID_DIR . '/inc/widgets/class-carousel-italia-affiliate.php';
require SOD_VCWID_DIR . '/inc/widgets/class-carousel-italia-consumers.php';
require SOD_VCWID_DIR . '/inc/widgets/class-carousel-customers.php';
require SOD_VCWID_DIR . '/inc/widgets/class-carousel-affiliate.php';

//if ( is_admin() ) {

	$current_url = ( isset( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] === 'on' ? 'https' : 'http' ) . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	$url         = parse_url( $current_url );
	//if ( array_key_exists( 'path', $url ) && $url['path'] == '/wp-admin/post.php' ) {
		require SOD_VCWID_DIR . '/inc/widgets/class-blog-component.php';
		require SOD_VCWID_DIR . '/inc/widgets/class-choose-sodexo.php';
		require SOD_VCWID_DIR . '/inc/widgets/class-cross-sell.php';
		require SOD_VCWID_DIR . '/inc/widgets/class-discover-more.php';
		require SOD_VCWID_DIR . '/inc/widgets/class-discover-more-three-blocks.php';
		require SOD_VCWID_DIR . '/inc/widgets/class-employee-component.php';
		require SOD_VCWID_DIR . '/inc/widgets/class-prod-ctas.php';
		require SOD_VCWID_DIR . '/inc/widgets/class-product-page-info.php';
		require SOD_VCWID_DIR . '/inc/widgets/class-product-page-2nd-ctas.php';
		require SOD_VCWID_DIR . '/inc/widgets/class-single-carousel.php';
		require SOD_VCWID_DIR . '/inc/widgets/class-test.php';
		require SOD_VCWID_DIR . '/inc/widgets/class-testimonials-popin.php';
		require SOD_VCWID_DIR . '/inc/widgets/class-why-become-partner.php';
		require SOD_VCWID_DIR . '/inc/widgets/class-bloc-link.php';
		require SOD_VCWID_DIR . '/inc/widgets/class-affiliate-info.php';
		require SOD_VCWID_DIR . '/inc/widgets/class-faq.php';
		require SOD_VCWID_DIR . '/inc/widgets/class-discover-more-one-block.php';
		require SOD_VCWID_DIR . '/inc/widgets/class-contact-our-adviser.php';
		require SOD_VCWID_DIR . '/inc/widgets/class-online-accreditation-form.php';
		require SOD_VCWID_DIR . '/inc/widgets/class-life-quality-mobility.php';
		require SOD_VCWID_DIR . '/inc/widgets/class-poll.php';
	//}
//}
// Don't load directly
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

// Check if Visual Composer is installed
if ( ! defined( 'WPB_VC_VERSION' ) ) {
	// Display notice that Visual Compser is required
	add_action( 'admin_notices', 'showVcVersionNotice' );
	add_action( 'plugins_loaded', 'sodexo_load_textdomain', 12 );
	return;
}

// Show notice if your plugin is activated but Visual Composer is not
function showVcVersionNotice() {
	$plugin_data = get_plugin_data( __FILE__ );
	echo '
        <div class="updated">
          <p>' . sprintf( __( '<strong>%s</strong> requires <strong><a href="http://bit.ly/vcomposer" target="_blank">Visual Composer</a></strong> plugin to be installed and activated on your site.', 'vc_widget' ), $plugin_data['Name'] ) . '</p>
        </div>';
}


// Init
function sodexo_vcwid_init() {
	new SodexoVcwid_Client();

	if ( is_admin() ) {
		new SodexoVcwid_Admin();
	}

}

add_action( 'plugins_loaded', 'sodexo_vcwid_init', 11 );

/**
 * Load plugin textdomain.
 *
 * @since 1.5.2
 */
function sodexo_load_textdomain() {
	load_plugin_textdomain( 'dlbi-sodexo-vc-widget', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
}
