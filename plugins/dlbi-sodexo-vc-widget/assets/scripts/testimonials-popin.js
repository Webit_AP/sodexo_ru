/**
 * Carousel scripts
 */


(function($) {

    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });

    $('#document-needed-selector').change(function(){
        $('.type-contents').hide();
        $('#' + $(this).val()).show();
    });


})(jQuery);
