<?php
//List of attributs
$titles = vc_param_group_parse_atts( $atts['titles'] );

$mode = "buttonInText";
//$mode = "buttonNotInText";

?>
<section class="single-carousel">
    <div class="container">
		<?php if ( array_key_exists( 'titles', $atts ) ) : ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="single-carousel-slick">
						<?php foreach ( $titles as $key => $title ) : ?>
							<?php
							$value = $unit = $description = "";
							extract( $title );

							$valueLength       = strlen( $value );
							$unitLength        = strlen( $unit );
							$descriptionLength = strlen( $description );
							$button = $buttonInText = $buttonNotInText = $class_single_carousel ="";
							if ( array_key_exists( 'link', $title ) ) {
								$linkField       = vc_build_link( $title['link'] );
								$linkTitle       = $linkField['title'];
								$linkTitleLength = strlen( $linkTitle );
								$target          = "";
								if ( $linkField['target'] ) {
									$target = "target=\"_blank\"";
								}

								if ( ! empty( $linkField['url'] ) && ! empty( $linkField['title'] ) ) {
									$class_single_carousel = 'hasbutton';
									$button = "<div class=\"single-carousel--container_button\" data-length=\"$descriptionLength\">
                                                <div class=\"empty-space\"></div>
                                                <div class=\"single-carousel--container_button-wrapper\">
                                                    <a class=\"btn-sodexo btn-sodexo-white\" data-length=\"$linkTitleLength\" href=\"" . $linkField['url'] . "\" title=\"" . $linkField['title'] . "\" $target>
														" . $linkField['title'] . "
                                                    </a>
                                                </div>
                                            </div>";
									$$mode  = $button;
								}
							}

							?>
                            <div>
                                <div class="single-carousel--container <?php echo $mode." ".$class_single_carousel ?>">
                                    <div class="single-carousel--container_number-text">
                                        <div class="single-carousel--container_number">
                                            <p class="number_value" data-length="<?php echo $valueLength ?>"><?php echo $value; ?> <span class="number_unit" data-length="<?php echo $unitLength ?>"><?php echo $unit; ?> </span></p>

                                        </div>
                                        <div class="single-carousel--container_text" data-length="<?php echo $descriptionLength ?>">
                                            <p><?php echo $description ?><?php echo $buttonInText ?></p>

                                        </div>
                                    </div>
									<?php echo $buttonNotInText ?>
                                </div>
                            </div>
						<?php endforeach; ?>
                    </div><!-- .single-carousel-slick -->
                </div><!-- #single-carousel -->
            </div>
		<?php endif; ?>
    </div>
</section>