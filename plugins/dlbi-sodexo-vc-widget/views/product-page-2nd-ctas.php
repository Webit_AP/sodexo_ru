<?php
//List of attributs
$links = array();
if (array_key_exists('links', $atts)) {
    $links = vc_param_group_parse_atts($atts['links']);
}

$simplelink = 'simplelink';
$typeform = 'typeform';
$youtube='youtube';

?>

<section class="prod-2nd-cta">
  <div class="container">
		<div class="prod-2nd-ctas--desktop">
			<?php if ($links) : ?>
					<!-- LIGNE EMPLOYES -->
					<div class="row">

						<?php $classCollSM = 12 / count($links); ?>
						<?php foreach ($links as $link) :
                            $typelink = $link['type_link'];
                            $linkField = vc_build_link($link['link']); ?>

							<div class="col-12 col-sm-<?= $classCollSM; ?>">

                                <?php if (array_key_exists('title', $link)):?>
                                    <div class="prod-2nd-ctas--container_subtitle sameheight">
                                        <p><?php echo $link['title']; ?></p>
                                    </div>
                                <?php endif; ?>
                                <div class="prod-2nd-ctas--cta">
                                    <?php if ($typelink == $simplelink):?>
                                        <a class="btn-sodexo btn-sodexo-white" href="<?php echo $linkField['url']; ?>" title="<?php echo $linkField['title']; ?>" <?php if ($linkField['target']) : ?>target="_blank"<?php endif; ?>>
                                            <?php echo $linkField['title']; ?>
                                        </a>
                                    <?php elseif ($typelink == $typeform) : ?>
                                        <a class="btn-sodexo btn-sodexo-white" href="javascript:;" data-toggle="modal" data-target="#typeformModal" data-typeform="<?php echo $linkField['url']; ?>">
                                            <?php echo $linkField['title']; ?>
                                        </a>
                                    <?php elseif ($typelink == $youtube) : ?>
                                        <a class="btn-sodexo btn-sodexo-white" data-fancybox href="<?php echo $linkField['url']; ?>">
                                            <?php echo $linkField['title']; ?>
                                        </a>
                                    <?php endif; ?>
								</div>
							</div>
						<?php endforeach; ?>

					</div>

			<?php endif; ?>
		</div>
  </div>
</section>
