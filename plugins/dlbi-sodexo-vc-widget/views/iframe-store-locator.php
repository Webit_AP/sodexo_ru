<section class="iframe-store-locator">
     <?php
     if (array_key_exists('iframe_store_locator_widget_title', $atts)) : ?>
        <p class="sodexo-pretitle"><?php echo $atts['iframe_store_locator_widget_title']; ?></p>
     <?php endif; ?>
     <?php if (array_key_exists('iframe_store_locator_code', $atts)) : ?>
         <div class="row">
            <div class="col-sm-12">
               <?php echo urldecode(base64_decode(($atts['iframe_store_locator_code'])));?>
            </div>
         </div>
     <?php endif; ?>
</section>
