<?php
$items = vc_param_group_parse_atts( $atts['key-figures'] );
?>

<section class="prod-ctas">
	<div class="col-md-12">
		<?php if ( array_key_exists( 'widget-title', $atts ) ) : ?>
			<h2 class="sodexo-title"><?php echo $atts['widget-title']; ?></h2>
		<?php endif; ?>
		<?php if ( array_key_exists( 'text', $atts ) ) : ?>
			<div class="prod-ctas--text row no-gutters justify-content-center">
				<div class="col-md col-lg-9 "><?php echo $atts['text']; ?></div>
			</div>
		<?php endif; ?>
		<div class="prod-ctas--block container">
			<?php if ( array_key_exists( 'key-figures', $atts ) ) : ?>
				<div class="row">
					<div class="col">
						<div class="row no-gutters justify-content-center">
							<?php
							foreach ( $items as $item ) :
								// Get image, title, label and link : 'READ MORE'
								$image  = $item['icon'];
								$target = '';
								if ( isset( $item['target'] ) ) {
									$target = "target='" . $item['target'] . "'";
								}
								?>
								<div class="col-lg col-lg-3">
									<div class="prod-ctas--container pc">
										<div class="prod-ctas--container_image" style="background-image: url('<?php echo wp_get_attachment_image_url( $image ); ?>')"></div>
										<p class="prod-ctas--container_number"><?php echo $item['number']; ?></p>
										<p class="prod-ctas--container_title"><?php echo $item['title']; ?></p>
										<a href="<?php echo $item['read-more-link']; ?>" class="prod-ctas--container_read-more">
											<i class="fa fa-arrow-right fa-lg" aria-hidden="true"></i><span>Read more</span>
										</a>
										<a class="btn-sodexo btn-sodexo-white" href="<?php echo $item['url']; ?>" <?php echo $target; ?>><?php echo $item['cta']; ?></a>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</section>
