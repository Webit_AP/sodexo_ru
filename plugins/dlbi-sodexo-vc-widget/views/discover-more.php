<section class="discover-more">

	<?php /* no container-fluid because the widget is full width */ ?>

    <div class="row no-gutters">

        <div class="col-md-12">

			<?php
			$posts_id = vc_param_group_parse_atts($atts['titles']);
			$count    = count($posts_id);
			?>
            <!-- Get widget fields -->
			<?php if(array_key_exists('widget_title', $atts)) : ?>
                <h2 class="sodexo-title"><?php echo $atts['widget_title']; ?></h2>
			<?php endif; ?>

        </div>

    </div>

	<?php if(array_key_exists('titles', $atts)) : ?>

        <div class="row no-gutters">

            <div class="col-md-12">

                <div class="discover-more-blocks discover-more-<?php echo $count; ?>">

					<?php foreach($posts_id as $post_id){ ?>

						<?php
						// Get image, title, label and link : 'READ MORE'
						$image = $post_id['discovermore_image'];
						$style = "";
						if(!empty($image)):
							$imageurl = wp_get_attachment_image_src($image, 'large');
							$style    = apply_filters("lazyload-style", "background-image: url('" . $imageurl[0] . "');'");
						endif;
						?>

                        <div class="discover-more-content" <?php echo $style ?>>
                            <div class="discover-more-content_inside">

								<?php if(array_key_exists('discovermore_title', $post_id)): ?>
                                    <h3><?php echo $post_id['discovermore_title']; ?></h3>
								<?php endif ?>
								<?php if(array_key_exists('discovermore_description', $post_id)): ?>
                                    <p><?php echo $post_id['discovermore_description']; ?></p>
								<?php endif ?>

								<?php
								$linktype = $post_id['discovermore_linktype'];
								$postid   = $post_id['post_id_link'];
								?>
								<?php if($linktype == '1') : ?>
                                    <a href="<?php echo get_permalink($postid); ?>" class="btn-sodexo btn-sodexo-red btn-sodexo-plus"><i class="fa fa-plus" aria-hidden="true"></i></a>
								<?php elseif($linktype == '2') : ?>
                                    <a href="<?php echo get_permalink($postid); ?>" class="btn-sodexo btn-sodexo-red"><?php echo __('Discover more', 'dlbi-sodexo-vc-widget'); ?></a>
								<?php endif; ?>

                            </div><!--.discover-more-content_inside-->

                        </div><!--.discover-more-content-->

					<?php } ?>

                </div><!--discover-more-blocks-->

            </div>

        </div><!--.row-->

	<?php endif; ?>

</section><!--.discover-more-->
