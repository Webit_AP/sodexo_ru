<section class="carousel-theytrustus">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
				<?php if(array_key_exists('widget_title', $atts)) : ?>
                    <h2 class="sodexo-title"><?php echo $atts['widget_title']; ?></h2>
				<?php endif; ?>
            </div>
        </div>
		<?php if(array_key_exists('logos', $atts)){
			$logos   = explode(',', $atts['logos']);
			$targets = array_key_exists('targets', $atts) ? explode(',', $atts['targets']) : '';
			$links   = array_key_exists('links', $atts) ? explode(',', $atts['links']) : array();
			?>
            <div class="row">
                <div class="col-md-12">
                    <div class="carousel-theytrustus-slick">
						<?php
						$index = 0;
						foreach($logos as $logo){
							if(!empty($logo)){
								$link    = "";
								$tag     = "<div class=\"carousel-theytrustus--container_image\">";
								$end_tag = "</div>";
								if(isset($links[$index]) && strpos($links[$index], "://") !== false){
									$target = '';
									if(isset($targets[$index])){
										$target = $targets[$index];
									}
									//									dlbi_display_debug($links[$index], 0, "orangered");
									$link = "href='" . $links[$index] . "'";
									if($target){
										$link .= " target='$target'";
									}

									$tag     = "<a class=\"carousel-theytrustus--container_image\" $link>";
									$end_tag = "</a>";
								}
								?>
                                <div>
                                    <div class="carousel-theytrustus--container">
										<?php echo $tag; ?>
										<?php echo wp_get_attachment_image($logo, 'thumbnail'); ?>
										<?php echo $end_tag; ?>
                                    </div>
                                </div>
							<?php }
							++ $index;
						} ?>
                    </div>
                </div>
            </div>
		<?php } ?>
    </div>
</section>
