
<section class="container alt" id="wrapper-title-subtitle">
    <div class="col-12">

    <?php
    if (array_key_exists('affiliate_widget_title', $atts)) : ?>
        <h2 class="sodexo-title"><?php echo $atts['affiliate_widget_title']; ?></h2>
    <?php endif; ?>

    <?php
    if (array_key_exists('affiliate_subtitle', $atts)) : ?>
        <p class="sodexo-pretitle"><?php echo $atts['affiliate_subtitle']; ?></p>
    <?php endif; ?>

    </div>

</section>
