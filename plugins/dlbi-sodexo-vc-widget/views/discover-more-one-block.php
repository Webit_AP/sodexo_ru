<section class="discover-three-more">
    <div class="col-md-12 padding-zero">

		<div class="discover-three-more--desktop padding-zero">

	    	    <div class="row no-gutters">

						<div class="col-md-4 discover-three-more--block">
						    <div class="discover-three-more--container">
                                <?php if (array_key_exists('cta_one_block_image',  $atts)): ?>
                                    <div class="choose-sodexo--container_image">
                                        <?php
                                        // Get image, title, label and link : 'READ MORE'
                                        $image = $atts['cta_one_block_image'];

                                        if (!empty($image)):
                                            echo wp_get_attachment_image($image);
                                        endif;
                                        ?>
                                    </div>
                                <?php endif ?>
								<div class="discover-three-more--container_content">

                                    <?php if (array_key_exists('cta_one_block_title', $atts)) : ?>
                                        <p class="sodexo-pretitle"><?php echo $atts['cta_one_block_title']; ?></p>
                                    <?php endif; ?>

                                    <?php if (array_key_exists('cta_one_block_description', $atts)) : ?>
                                        <p class="sodexo-pretitle"><?php echo $atts['cta_one_block_description']; ?></p>
                                    <?php endif; ?>


                                    <?php
                                    $simplelink = 'simplelink';
                                    $typeform = 'typeform';
                                    $youtube='youtube';
                                    if (array_key_exists('cta_one_block_link', $atts)) :
                                        $typelink = '';
                                        if (array_key_exists('type_link', $atts)):
                                            $typelink = $atts["type_link"];
                                        endif;
                                        $linkField = vc_build_link($atts['cta_one_block_link']);
                                        ?>
                                        <?php if ($typelink == $simplelink):?>
                                            <a class="btn-sodexo btn-sodexo-transparent content_link" href="<?php echo $linkField['url']; ?>" <?php if ($linkField['target']) : ?>target="_blank"<?php endif; ?>><?php echo $linkField['title']; ?></a>
                                        <?php elseif ($typelink == $typeform) : ?>
                                            <a class="btn-sodexo btn-sodexo-transparent content_link" href="javascript:;" data-toggle="modal" data-target="#typeformModal" data-typeform="<?php echo $linkField['url']; ?>"><?php echo $linkField['title']; ?></a>
                                        <?php elseif ($typelink == $youtube) : ?>
                                            <a class="btn-sodexo btn-sodexo-transparent content_link" data-fancybox href="<?php echo $linkField['url']; ?>"><?php echo $linkField['title']; ?></a>
                                        <?php endif; ?>
                                    <?php endif; ?>

								</div>
						    </div>
						</div>

	    	    </div>

		</div>
    </div>
</section>
