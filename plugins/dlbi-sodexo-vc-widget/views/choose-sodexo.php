<?php
//List of attributs
$titles = vc_param_group_parse_atts($atts['titles']);
$uniqid = uniqid(rand());
?>

<section class="choose-sodexo">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <?php if(array_key_exists('widget_title', $atts)) : ?>
          <h2 class="sodexo-title"><?php echo $atts['widget_title']; ?></h2>
        <?php endif; ?>
      </div>
    </div>

    <?php if(array_key_exists('titles', $atts)){
      $count = count($titles);

      $col = "col-sm";

      if($count == 3 || $count == 5 || $count == 6){
        $col = "col-4";
      }

      if($count == 4){
        $col = "col-6";
      }
      ?>

      <div class="choose-sodexo--desktop">
        <div class="row no-gutters align-items-stretch">
          <?php
          foreach($titles as $title){
            $url = $target = $cta_title = "";
            if(array_key_exists('link', $title)){
              $link = vc_build_link($title['link']);
              if(isset($link["url"])){
                $url = $link["url"];
              }
              if(isset($link["target"])){
                $target = trim($link["target"]);
              }
              if(isset($link["title"])){
                $cta_title = trim($link["title"]);
              }
            }


            $attrs = "";
            $class = "choose-sodexo--container";
            $cta   = "";
            if($url){
              $attrs .= "href='$url'";
              if($target){
                $attrs .= "target='$target'";
              }
              if($cta_title){

                $cta = "<button class='btn-sodexo btn-sodexo-red' >$cta_title</button>";
              }
            }
            ?>
            <div class="<?php echo $col ?> mt-2" data-mh="mh-group-<?php echo $uniqid; ?>">
              <a class="<?php echo $class ?>" <?php echo $attrs; ?>>
                <?php if(array_key_exists('image', $title)){ ?>
                  <div class="choose-sodexo--container_image">
                    <?php
                    // Get image, title, label and link : 'READ MORE'
                    $image = $title['image'];
                    if(!empty($image)){
                      echo wp_get_attachment_image($image);
                    }
                    ?>
                  </div>
                <?php } ?>
                <div class="choose-sodexo--container_subtitle">
                  <?php if(array_key_exists('subtitle', $title)): ?>
                    <p><?php echo $title['subtitle']; ?></p>
                  <?php endif; ?>
                </div>
                <?php
                echo $cta;
                ?>
              </a><!-- .choose-sodexo--container -->
            </div><!-- .col- -->
          <?php } ?>

        </div><!-- .row -->

      </div><!-- .choose-sodexo--desktop -->

      <div class="choose-sodexo--mobile lazy-container">

        <div class="choose-sodexo-slick">

          <?php foreach($titles as $key => $title){
            $url = $target = $cta_title = "";
            if(array_key_exists('link', $title)){
              $link = vc_build_link($title['link']);
              if(isset($link["url"])){
                $url = $link["url"];
              }
              if(isset($link["target"])){
                $target = trim($link["target"]);
              }
              if(isset($link["title"])){
                $cta_title = trim($link["title"]);
              }
            }


            $attrs = "";
            $class = "choose-sodexo--container";
            $cta   = "";
            if($url){
              $attrs .= "href='$url'";
              if($target){
                $attrs .= "target='$target'";
              }
              if($cta_title){

                $cta = "<button class='btn-sodexo btn-sodexo-red' >$cta_title</button>";
              }
            }
            ?>
            
            <?php
            // Get image, title, label and link : 'READ MORE'
            $image = array_key_exists('image', $title) ? $title['image'] : "";
            ?>
            <a class="choose-sodexo--container" <?php echo $attrs; ?> >
            <?php if(!empty($image)): ?>
              <div class="choose-sodexo--container_image">
                <?php echo wp_get_attachment_image($image); ?>
              </div>
            <?php	endif;?>
            <div class="choose-sodexo--container_subtitle">
              <?php if(array_key_exists('subtitle', $title)): ?>
                <p><?php echo $title['subtitle']; ?></p>
              <?php endif; ?>
            </div>
            <div class="choose-sodexo--container_button">
              <?php echo $cta; ?>
            </div>

          </a>

        <?php } ?>

      </div>

    </div><!-- .choose-sodexo--mobile -->

  <?php } ?>


  <?php if(array_key_exists('pre-text-link', $atts)): ?>
    <div class="choose-sodexo--cta">
      <?php
      $simplelink = 'simplelink';
      $typeform   = 'typeform';
      $youtube    = 'youtube';

      $typelink = '';
      if(array_key_exists('type_link', $atts)):
        $typelink = $atts["type_link"];
      endif;

      $linkField = vc_build_link($atts['link']); ?>

      <span><?php echo $atts['pre-text-link']; ?></span>
      <?php if($typelink == $simplelink): ?>
        <a class="btn-sodexo btn-sodexo-red" href="<?php echo $linkField['url']; ?>" title="<?php echo $linkField['title']; ?>" <?php if($linkField['target']) : ?>target="_blank"<?php endif; ?>><?php echo $linkField['title']; ?></a>
      <?php elseif($typelink == $typeform) : ?>
        <a class="btn-sodexo btn-sodexo-red" href="javascript:;" data-toggle="modal" data-target="#typeformModal" data-typeform="<?php echo $linkField['url']; ?>"><?php echo $linkField['title']; ?></a>
      <?php elseif($typelink == $youtube) : ?>
        <a class="btn-sodexo btn-sodexo-red" data-fancybox href="<?php echo $linkField['url']; ?>"><?php echo $linkField['title']; ?></a>
      <?php endif; ?>
    </div>
  <?php endif; ?>

</div>

</section>
