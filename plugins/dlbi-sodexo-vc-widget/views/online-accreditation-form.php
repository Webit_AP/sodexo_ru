<section class="online-accreditation-form alt choose-sodexo container">
    <div class="col-12 choose-sodexo--two_blocks">
        <div class="choose-sodexo--container bg-grey info-blocks">
			<?php if (array_key_exists('title', $atts)) : ?>
			<h3 class="sodexo-title"><?php echo $atts['title']; ?></h3>
			<?php endif; ?>

			<?php if (array_key_exists('description', $atts)) : ?>
			<p class="sodexo-subtitle"><?php echo $atts['description']; ?></p>
			<?php endif; ?>

			<?php if (array_key_exists('typeform-url', $atts) && array_key_exists('typeform-height', $atts) && array_key_exists('typeform-width', $atts)) : ?>
			<div class="form-iframe"><?php echo do_shortcode( '[typeform_embed height="' . $atts['typeform-height'] . '" url="' . $atts['typeform-url'] . '" width="' . $atts['typeform-width'] . '"]' ); ?></div>
			<?php endif; ?>
			</div>
		</div>
	</div>
</section>