<section class="testimonials-popin">

    <div class="container">

        <div class="row">

            <div class="col-md-12">

				<?php
				$posts_id = vc_param_group_parse_atts($atts['posts_id']);
				$count    = count($posts_id);
				?>

				<?php if(array_key_exists('widget_title', $atts)) : ?>
                    <h2 class="sodexo-title"><?php echo $atts['widget_title']; ?></h2>
				<?php endif; ?>

            </div>

        </div>

		<?php if(array_key_exists('posts_id', $atts)) : ?>

            <div class="row no-gutters">

                <div class="col-sm-12">

                    <div class="testimonials testimonialscount-<?php echo $count; ?>">

						<?php foreach($posts_id as $post_id){ ?>

                            <div class="testimonial-container">

								<?php
								// Type: Picture ou Video
								$type = get_field("testimonial_type", $post_id['post_id']);
								$id   = $post_id['post_id'];

								// Get image
								$image    = get_field('testimonial_image', $id);
								$size     = "soxo-medium";
								$imageurl = wp_get_attachment_image_src($image, $size);
								if(!empty($image)) :
									$style = apply_filters("lazyload-style", "background-image: url('" . $imageurl[0] . "');'");
									?>
                                    <div class="testimonial-image" <?php echo $style ?>></div>
								<?php endif; ?>

                                <div class="testimonial-content">

                                    <div class="testimonial-quote">
										<?php echo get_field("testimonial_quote", $id); ?>
                                    </div>
									<?php if(get_field("testimonial_author", $id)){ ?>
                                        <p class="testimonial-author"><?php echo get_field("testimonial_author", $id); ?></p>
									<?php } ?>

									<?php if($type == 'Picture') : ?>
										<?php
										$simplelink = 'Simple link';
										$typeform   = 'Typeform';
										$youtube    = 'Youtube';
										if(is_array(get_field("testimonial_link", $id))):
											$testimonial_link = get_field("testimonial_link", $id);
											if($testimonial_link['url']): ?>
                                                <a <?php if(get_field("testimonial_select_a_link_type", $id) == $simplelink): ?>
                                                    class="btn-sodexo btn-sodexo-white" href="<?php echo $testimonial_link["url"]; ?>" <?php if($testimonial_link["target"]): ?> target="_blank" <?php endif ?> title="<?php echo $testimonial_link["title"]; ?>"
												<?php elseif(get_field("testimonial_select_a_link_type", $id) == $typeform) : ?>
                                                    class="btn-sodexo btn-sodexo-white" href="javascript:;" data-toggle="modal" data-target="#typeformModal" data-typeform="<?php echo $testimonial_link["url"]; ?>"
												<?php elseif(get_field("testimonial_select_a_link_type", $id) == $youtube) : ?>
                                                    class="btn-sodexo btn-sodexo-white" data-fancybox href="<?php echo $testimonial_link["url"]; ?>"
												<?php endif; ?>
                                                ><?php echo $testimonial_link["title"]; ?></a>
											<?php endif; ?>
										<?php endif; ?>

									<?php elseif($type == 'Video') : ?>

                                        <a data-fancybox class="btn-play-video" href="<?php echo get_field('testimonial_video_link', $id, false, false); ?>" onclick="dataLayer.push({'event': 'video-view','name':'<?php echo addslashes(get_field("testimonial_author", $id)); ?>' });"
                                           title="<?php echo get_field("testimonial_author", $id); ?>"></a>

									<?php endif; ?>

                                </div><!--.testimonial-content-->

                            </div><!--.testimonial-container-->

						<?php } ?>

                    </div><!--.testimonials-->

                </div><!--.col-sm-12-->

            </div><!--.row-->

		<?php endif; ?>

		<?php
		$simplelink = 'simplelink';
		$typeform   = 'typeform';
		$youtube    = 'youtube';

		if(array_key_exists('link', $atts)):
			$typelink = '';
			if(array_key_exists('type_link', $atts)):
				$typelink = $atts["type_link"];
			endif; ?>
            <div class="testimonials-cta">
				<?php $linkField = vc_build_link($atts['link']); ?>

                <a <?php if($typelink == $simplelink): ?>
                    class="btn-sodexo btn-sodexo-red" href="<?php echo $linkField['url']; ?>" title="<?php echo $linkField['title']; ?>" <?php if($linkField['target']) : ?>target="_blank"<?php endif; ?>
				<?php elseif($typelink == $typeform) : ?>
                    class="btn-sodexo btn-sodexo-red" href="javascript:;" data-toggle="modal" data-target="#typeformModal" data-typeform="<?php echo $linkField['url']; ?>"
				<?php elseif($typelink == $youtube) : ?>
                    class="btn-sodexo btn-sodexo-red" data-fancybox href="<?php echo $linkField['url']; ?>"
				<?php endif; ?>
                ><?php echo $linkField['title']; ?></a>
            </div>
		<?php endif ?>

    </div><!--.container-->

</section>
