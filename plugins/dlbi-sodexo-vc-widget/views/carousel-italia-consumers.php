<?php
$tabs = wp_get_nav_menu_items('top-menu-bar-customers');
$slides = $atts;
$counter = 1;

$direct_link = get_field('direct_link_for_carousel', "option");
$carrousel_height = get_field('force_same_height_carousel', 'option') ? 'force-eq-height' : '';
// die(var_dump($slides));
// foreach ($slides as $key => $slide) {
//     die(var_dump($slide));
// }
?>

<div class="top_menu">

    <div class="container">


        <div class="fake-select">
            <div class="fake-select_content">
                <a class="fake-select-link" href="#" data-toggle="modal" data-target="#CarouselMenuModal">
                    <span><?php echo __('Select a categorie', 'dlbi-sodexo-vc-widget'); ?></span><i class="icon icon-arrow-down"></i>
                </a>
            </div>
        </div>

        <div class="menu-modal modal" id="CarouselMenuModal" tabindex="-1" role="dialog" aria-labelledby="CarouselMenuModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"><?php echo __('Select a categorie', 'dlbi-sodexo-vc-widget'); ?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="icon icon-arrow-down"></i></span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="top-menu--tabs top-menu--tabs-popup">
                            <ul class="top-menu--tabs_list">
                                <li data-slide-to="0"><a href="#"></a></li>
                                <?php
                                $g = 0;
                                foreach ($tabs as $key => $tab) :
                                    if ($tab->menu_item_parent == 0) :
                                        $currentClass = !empty(get_field('custom-icon', $tab->ID)) ? get_field('custom-icon', $tab->ID) : $tab->classes[0];
	                                    ++$g;
                                        ?>
                                        <li class="nav-item" role="presentation">
                                            <a onclick="dataLayer.push({'event': 'click-header', 'name': '<?php echo addslashes($tab->title); ?>'});" class="top_menu-nav-link nav-link <?php echo $currentClass ?>" href="#" data-slide-to="<?php echo $g; ?>" >
                                                <span><?php echo $tab->title ?></span>
                                            </a>
                                        </li>
                                        <?php
                                    endif;
                                endforeach;
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="top-menu--tabs top-menu--tabs-menu">
                    <ul class="top-menu--tabs_list top-menu--homepage">
                        <li data-slide-to="0"><a href="#"></a></li>
                        <?php
                        $g = 0;
                        // print_r(get_field('custom-icon', 75));
                        // print_r($tabs);
                        foreach ($tabs as $key => $tab) :
                            if ($tab->menu_item_parent == 0) :
                                $currentClass = !empty(get_field('custom-icon', $tab->ID)) ? get_field('custom-icon', $tab->ID) : $tab->classes[0];
                                ++$g;
                                ?>
                                <li class="menu-item-<?php echo $tab->ID ?>">
                                    <a onclick="dataLayer.push({'event': 'click-header', 'name': '<?php echo addslashes($tab->title); ?>'});" href="<?php echo ($direct_link) ? $tab->url : '#'; ?>" data-slide-to="<?php echo $g; ?>" class="<?= $currentClass; ?>">
                                        <span class="top-menu--wave lazy"></span>
                                        <span class="menu-item-span"><?php echo $tab->title ?></span>
                                    </a>
                                </li>
                                <?php
                            endif;
                        endforeach;
                        ?>
                    </ul>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="top-menu--header">

                    <div class="top-menu--header_title">

                        <div class="top-menu-carousel-titles-slick">
                            <div>
                                <p><?php echo array_key_exists("title_top-solutions", $atts) ? $atts["title_top-solutions"] : ''; ?></p>
                            </div>
                            <?php
                            $hide_top_solutions_link = array_key_exists("title_top-solutions-link", $atts) ? $atts["title_top-solutions-link"] : false;

                            foreach ($tabs as $key => $tab) {
                                if ($tab->menu_item_parent == 0) :
                                    ?>
                                    <div>
                                        <?php if ($hide_top_solutions_link != true): ?>
                                            <a class="top-menu--header_top-solution" data-slide-to="0">
                                                <i class="icon-arrow-left" aria-hidden="true"></i>
                                                <span><?php echo __('Our top solution', 'dlbi-sodexo-vc-widget'); ?></span>
                                            </a>
                                        <?php endif ?>
                                        <p><?php echo array_key_exists("title_" . $tab->classes[0], $atts) ? $atts["title_" . $tab->classes[0]] : ''; ?></p>
                                    </div>
                                    <?php
                                endif;
                            }
                            ?>
                        </div>

                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="top-menu-carousel top-menu-carousel-slick">

                    <div>

                        <?php
                        $slides = "slides_top-solution";
                        $slides = vc_param_group_parse_atts($atts["slides_top-solutions"]);
                        $slides_count = count($slides);

                        $product = "product_top-solutions";

                        //$best_seller = "best-seller_top-solutions";
                        $best_seller = "best-seller_top-solutions";
                        ?>

                        <div class="row justify-content-center align-items-stretch <?=$carrousel_height;?>">

                            <?php
                            foreach ($slides as $key => $slide) {
                                // $post_type = get_post_type($slide[$product]);
                                $col = "col-md";
                                $bs = "";
                                $bsClass = "";
                                if (array_key_exists($best_seller, $slide) && $slide[$best_seller] == "Yes") {
                                    $bs = "<span class='label-prize lazy'>Best Seller</span>";
                                    $bsClass = "block-label-prize";
                                    if ($slides_count == 2) {
                                        $col = "col-md-8";
                                    }
                                    if ($slides_count == 3) {
                                        $col = "col-md-6";
                                    }
                                }
                                ?>

                                <div class="col col-12 <?php echo $col; ?>" data-mh="mh-group-<?php echo $counter; ?>">

                                    <a href="<?php echo vc_build_link($slide['product_top-solutions_link'])['url']; ?>" onclick="dataLayer.push({'event': 'click-suggestions', 'name': '<?php echo addslashes($slide['product_top-solutions_title']); ?>'});" class="top-menu-carousel_block <?php echo $bsClass; ?>">
                                        <?php echo $bs; ?>
                                        <div class="media media-ita">
                                            <div class="top-menu-carousel_block-image lazy-container">
                                                <?php
                                                $slide_image = $slide["product_top-solutions_image"];
                                                // $size = 'medium';
                                                /* if ($slide_image) {
                                                  echo wp_get_attachment_image($slide_image['ID'], $size, "", array("class" => "img-responsive"));
                                                  } */
                                                if ($slide_image) {
                                                    $imgsrc = wp_get_attachment_image_url($slide_image, 'medium');
	                                                echo apply_filters( "dlbi_image", $imgsrc );
                                                } ?>
                                            </div>
                                            <div class="media-body">
                                                <div class="top-menu-carousel_block-title">
                                                    <p><?php echo $slide['product_top-solutions_title'] ?></p>
                                                </div>
                                                <div class="top-menu-carousel_block-text">
                                                    <?php echo $slide['product_top-solutions_desc']; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </a>

                                </div>

                            <?php } ?>

                        </div>

                    </div><!-- .empty div -->


                    <?php
                    foreach ($tabs as $key => $tab) :

                        if ($tab->menu_item_parent == 0) :

	                        ++$counter;

                            $slides = "slides_" . $tab->classes[0];
                            $slides = vc_param_group_parse_atts($atts[$slides]);
                            $slides_count = count($slides);

                            $product = "product_" . $tab->classes[0];
                            $best_seller = "best-seller_" . $tab->classes[0];
                            ?>

                            <div>

                                <div class="row justify-content-center align-items-stretch <?=$carrousel_height;?>">

                                    <?php
                                    foreach ($slides as $key => $slide) :

                                        // $post_type = get_post_type($slide[$product]);
                                        $col = "col-md";
                                        $bs = "";
                                        $bsClass = "";
                                        if (array_key_exists($best_seller, $slide) && $slide[$best_seller] == "Yes") {
                                            $bs = "<span class='label-prize lazy'>Best Seller</span>";
                                            $bsClass = "block-label-prize";
                                            if ($slides_count == 2) {
                                                $col = "col-md-8";
                                            }
                                            if ($slides_count == 3) {
                                                $col = "col-md-6";
                                            }
                                        }
                                        ?>
                                        <div class="col col-12 col-md <?php echo $col; ?>" data-mh="mh-group-<?php echo $counter; ?>">
                                            <?php $target = vc_build_link($slide[$product . '_link'])['target'] == ' _blank' ? 'target="_blank"' : ""; ?>
                                            <a href="<?php echo vc_build_link($slide[$product . '_link'])['url']; ?>" onclick="dataLayer.push({'event': 'click-suggestions', 'name': '<?php echo addslashes(get_the_title($slide[$product])); ?>'});" class="top-menu-carousel_block <?php echo $bsClass; ?>" <?php echo $target ?>>
                                                <?php echo $bs; ?>
                                                <div class="media media-ita">
                                                    <div class="top-menu-carousel_block-image lazy-container">
                                                        <?php
                                                        // we alaways show the image on mobile because the autoplay function is disabled on mobile
                                                        $slide_image = $slide[$product . '_image'];
                                                        // $size = 'large'; // (thumbnail, medium, large, full or custom size)
                                                        // if ($slide_image) {
                                                        //     echo wp_get_attachment_image($slide_image['ID'], $size);
                                                        // }
                                                        if ($slide_image) {
                                                            $imgsrc = wp_get_attachment_image_url($slide_image, 'medium');
	                                                        echo apply_filters( "dlbi_image", $imgsrc );
                                                        } ?>
                                                    </div>

                                                    <div class="media-body">
                                                        <div class="top-menu-carousel_block-title">
                                                            <p><?php echo $slide[$product . '_title'] ?></p>
                                                        </div>

                                                        <div class="top-menu-carousel_block-text">
                                                            <?php echo $slide[$product . '_desc']; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>

                                        </div>

                                    <?php endforeach; ?>

                                </div>

                            </div><!-- .empty div -->

                            <?php
                        endif;
                    endforeach;
                    ?>

                </div><!-- .top-menu-carousel top-menu-carousel-slick -->
            </div><!-- .col-md-12 -->
        </div><!-- .row -->

        <?php if (array_key_exists('all-products-link', $atts)): ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="top-menu--footer">
                        <a class="btn-sodexo btn-sodexo-red" href="<?php echo vc_build_link($atts['all-products-link'])['url'] ?>"><?php echo vc_build_link($atts['all-products-link'])['title'] ?></a>
                    </div>
                </div>
            </div>
        <?php endif ?>

    </div><!-- container -->
</div><!-- top_menu -->
