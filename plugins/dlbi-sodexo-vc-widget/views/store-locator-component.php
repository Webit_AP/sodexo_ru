<?php
//List of attributs
$titles = vc_param_group_parse_atts($atts['titles']);

?>
<section class="choose-sodexo widget-store-locator lazy">
    <div class="container">

        <?php
        // Text 1 : Pass restaurant can be used in over
        if (array_key_exists('text_1', $atts)) : ?>
            <p class="sodexo-pretitle">
                <?php echo $atts['text_1']; ?>
                <?php if (array_key_exists('text_2', $atts)) : ?>
                    <strong><?php echo $atts['text_2']; ?></strong>
                <?php endif; ?>
                </p>
        <?php endif; ?>

        <?php
        if (array_key_exists('locator_link',  $atts)): ?>
            <div class="choose-sodexo--cta">
                <?php $linkField = vc_build_link( $atts['locator_link']); ?>
                <a class="btn-sodexo" href="<?php echo $linkField['url']; ?>" title="<?php echo $linkField['title']; ?>" <?php if ($linkField['target']) : ?>target="_blank"<?php endif; ?>><?php echo $linkField['title']; ?></a>
            </div>
        <?php endif;?>

        <?php if (array_key_exists('titles', $atts)) : ?>

            <?php
            // Title : A selection of parteners
            if (array_key_exists('bloc_title', $atts)): ?>
                <p><em><?php echo $atts['bloc_title']; ?></em></p>
            <?php endif ?>

            <div class="row no-gutters">
                <?php

                //Repeated block
                foreach ($titles as $title):
                ?>

                <?php if (array_key_exists('bloc_image', $title)):
                // Get image, title, label and link : 'READ MORE'
                $image = $title['bloc_image'];
                if (!empty($image)):
                    // Link to image
                    if (array_key_exists('bloc_link', $title)): ?>
                        <?php $linkField = vc_build_link($title['bloc_link']); ?>
                        <a href="<?php echo $linkField['url']; ?>" title="<?php echo $linkField['title']; ?>" <?php if ($linkField['target']) : ?>target="_blank"<?php endif; ?>>

                            <?php echo wp_get_attachment_image($image);?>

                        </a>
                    <?php endif;
                endif;
                ?>
                <?php endif ?>


                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>

</section>
