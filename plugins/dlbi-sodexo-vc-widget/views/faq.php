<section class="faq-marchants">
    <div class="container">
    <div class="col-md-12">

        <?php
        $posts_id = vc_param_group_parse_atts($atts['posts_id']);
        $count = count($posts_id);
        ?>
        <!-- Get widget fields -->
        <?php if (array_key_exists('faq_widget_title', $atts)) : ?>
            <h2 class="sodexo-title"><?php echo $atts['faq_widget_title']; ?></h2>
        <?php endif; ?>

        <!-- Get ACF fields -->

            <?php if (array_key_exists('posts_id', $atts)) : ?>
            <div class="row no-gutters">
                <div class="col-sm-12" id="accordion-faq" role="tablist" aria-multiselectable="true">
                    <?php $z = 0; foreach ($posts_id as $post_id) : $faq = get_field('faq_questions', $post_id['post_id']); ?>
                    <div class="panel-group" >
                        <?php $y = 0; foreach( $faq  as $question): ?>
                        <div class="panel panel-default card">
                            <div class="panel-heading" role="tab" id="heading<?php echo $z.$y;?>">
                                <h4 class="panel-title ">
                                    <a class="faq-title collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq" href="#collapse<?php echo $z.$y; ?>" aria-expanded="false" aria-controls="collapse<?php echo $z.$y; ?>">
                                        <?php echo $question['faq_question'] ?>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse<?php echo $z.$y; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $z.$y; ?>">
                                <div class="panel-body">
                                    <?php echo $question['faq_answer'] ?>
                                </div>
                            </div>
                        </div>
                        <?php $y++; endforeach;?>
                    </div>
                    <?php $z++; endforeach;?>
                </div><!--.col-sm-12-->
            </div><!--.row-->
            <?php endif; ?>

           <?php if (array_key_exists('faq_link',  $atts)): ?>
                    <div class="faq-marchants--cta">
                        <?php $linkField = vc_build_link( $atts['faq_link']); ?>
                        <a class="btn-sodexo btn-sodexo-white" href="<?php echo $linkField['url']; ?>" title="<?php echo $linkField['title']; ?>" <?php if ($linkField['target']) : ?>target="_blank"<?php endif; ?>><?php echo $linkField['title']; ?></a>
                    </div>
           <?php endif;?>

        </div><!--.container-->
    </div>
</section><!--.col-md-12-->

