<?php
$posts_id = vc_param_group_parse_atts( $atts['posts_id'] );
$is_feed  = get_field( 'get_feed', 'option' );


if ( $is_feed ) {
	$feed  = fetch_feed( get_field( 'feed_url', 'option' ) );
	$items = $feed ? $feed->get_items() : '';
}

$featured_articles = array_key_exists( 'featured_article_id', $atts ) ? vc_param_group_parse_atts( $atts['featured_article_id'] ) : '';
?>
<section class="blog-component">

    <div class="container">

        <div class="row">

            <div class="col-md-12">
				<?php if ( array_key_exists( 'widget_subtitle', $atts ) ) : ?>
                    <p class="sodexo-pretitle"><?php echo $atts['widget_subtitle']; ?></p>
				<?php endif; ?>

				<?php if ( array_key_exists( 'widget_title', $atts ) ) : ?>
                    <h2 class="sodexo-title"><?php echo $atts['widget_title']; ?></h2>
				<?php endif; ?>
            </div>

        </div>

		<?php if ( ! $is_feed ) : ?>
        <div class="blog-component--block main">
			<?php if ( array_key_exists( 'featured_article_id', $atts ) ) : ?>

                <div class="row">
					<?php
					foreach ( $featured_articles as $featured_article ) :
						// Load featured article
						$data = get_post( $featured_article['featured_article_id'] );
						if ( is_object( $data ) ):
							$title = $data->post_title;

							$content = $data->post_content;
							$content = apply_filters( 'the_content', $content );
							$content = strip_tags( $content );
							$content = str_replace( ']]>', ']]&gt;', $content );
							$content = wp_trim_words( $content, 30, '...' );
							?>
                            <div class="col-12 col-md-7">
								<?php if ( get_post_meta( $featured_article['featured_article_id'], 'post_feed' ) ): ?>
                                <a href="<?php echo get_post_meta( $featured_article['featured_article_id'], 'post_feed', true ); ?>" class="blog-component--container" target="_blank">
									<?php else : ?>
                                    <a href="<?php echo get_permalink( $featured_article['featured_article_id'] ); ?>" class="blog-component--container">
										<?php endif ?>
										<?php echo apply_filters( "dlbi_image", get_the_post_thumbnail_url( $data->ID, 'soxo-blog-medium' ), "", $title ); ?>
                                    </a>
                            </div>
                            <div class="col-12 col-md-5">
								<?php
								$categories = get_the_category( $data->ID );
								foreach ( $categories as $category ) :
									?>
                                    <p class=""><?php echo esc_html( $category->name ); ?></p>
								<?php endforeach; ?>
								<?php if ( get_post_meta( $featured_article['featured_article_id'], 'post_feed' ) ): ?>
                                <a href="<?php echo get_post_meta( $featured_article['featured_article_id'], 'post_feed', true ); ?>" class="blog-component--container" target="_blank">
									<?php else : ?>
                                    <a href="<?php echo get_permalink( $featured_article['featured_article_id'] ); ?>" class="blog-component--container">
										<?php endif ?>
                                        <p class="blog-component--container_date"><?php echo get_the_date( 'd.m.Y', $data->ID ); ?></p>
                                        <p class="blog-component--container_title"><?php echo $title ?></p>
                                        <div class="blog-component--container_content"><?php echo $content ?></div>

                                        <p class="blog-component--container_link">
                                            <i class="fa fa-arrow-right fa-lg" aria-hidden="true"></i><span><?php _e( 'Read more', 'dlbi-sodexo-vc-widget' ) ?></span>
                                        </p>
                                    </a>
                            </div>
						<?php endif ?>
					<?php endforeach; ?>
                </div>

			<?php endif; ?>
        </div>


        <div class="blog-component--block">
			<?php if ( array_key_exists( 'posts_id', $atts ) ) : ?>

                <div class="row">
					<?php
					foreach ( $posts_id as $post_id ) :
						// Load posts
						$data = get_post( $post_id['post_id'] );
						if ( is_object( $data ) ):

							$title = $data->post_title;

							$content = $data->post_content;
							$content = apply_filters( 'the_content', $content );
							$content = strip_tags( $content );
							$content = str_replace( ']]>', ']]&gt;', $content );
							$content = wp_trim_words( $content, 30, '...' );
							?>
                            <div class="col-12 col-md-4">
								<?php if ( get_post_meta( $post_id['post_id'], 'post_feed' ) ): ?>
                                <a href="<?php echo get_post_meta( $post_id['post_id'], 'post_feed', true ); ?>" class="blog-component--container blog-visuel" target="_blank">
									<?php else: ?>
                                    <a href="<?php echo get_permalink( $post_id['post_id'] ); ?>" class="blog-component--container blog-visuel">
										<?php endif ?>
										<?php echo apply_filters( "dlbi_image", get_the_post_thumbnail_url( $data->ID, 'soxo-blog-medium' ), "", $title ); ?>
										<?php // echo get_the_post_thumbnail($data->ID, 'soxo-medium');
										?></a>
									<?php
									$categories = get_the_category( $data->ID );
									foreach ( $categories as $category ) :
										?>
                                        <p class="blog-component--container_category"><?php echo esc_html( $category->name ); ?></p>
									<?php endforeach; ?>
									<?php if ( get_post_meta( $post_id['post_id'], 'post_feed' ) ): ?>
                                    <a href="<?php echo get_post_meta( $post_id['post_id'], 'post_feed', true ); ?>" class="blog-component--container" target="_blank">
										<?php else : ?>
                                        <a href="<?php echo get_permalink( $post_id['post_id'] ); ?>" class="blog-component--container">
											<?php endif ?>
                                            <p class="blog-component--container_date"><?php echo get_the_date( 'd.m.Y', $data->ID ); ?></p>
                                            <p class="blog-component--container_title"><?php echo $title ?></p>
                                            <div class="blog-component--container_content"><?php echo $content ?></div>

                                            <p class="blog-component--container_link">
                                                <i class="fa fa-arrow-right fa-lg" aria-hidden="true"></i><span><?php echo __( 'Read more', 'dlbi-sodexo-vc-widget' ); ?></span>
                                            </p>
                                        </a>
                            </div>
						<?php endif; ?>
					<?php endforeach; ?>

                </div>
			<?php endif; ?>
        </div>


		<?php
		$simplelink = 'simplelink';
		$typeform   = 'typeform';
		$youtube    = 'youtube';
		if ( array_key_exists( 'link', $atts ) ):
			$typelink = '';
			if ( array_key_exists( 'type_link', $atts ) ):
				$typelink = $atts["type_link"];
			endif;
			?>
            <div class="blog-component--cta">
				<?php $linkField = vc_build_link( $atts['link'] ); ?>
				<?php if ( $typelink == $simplelink ): ?>
                    <a class="btn-sodexo btn-sodexo-red" href="<?php echo $linkField['url']; ?>" title="<?php echo $linkField['title']; ?>" <?php if ( $linkField['target'] ) : ?>target="_blank"<?php endif; ?>><?php echo $linkField['title']; ?></a>
				<?php elseif ( $typelink == $typeform ) : ?>
                    <a class="btn-sodexo btn-sodexo-red" href="javascript:;" data-toggle="modal" data-target="#typeformModal" data-typeform="<?php echo $linkField['url']; ?>"><?php echo $linkField['title']; ?></a>
				<?php elseif ( $typelink == $youtube ) : ?>
                    <a class="btn-sodexo btn-sodexo-red" data-fancybox href="<?php echo $linkField['url']; ?>"><?php echo $linkField['title']; ?></a>
				<?php endif; ?>
            </div>
		<?php endif ?>
    </div>
	<?php else: ?>
        <div class="blog-component--block main">
			<?php if ( array_key_exists( 'featured_article_id', $atts ) ) : ?>

                <div class="row">
					<?php
					foreach ( $featured_articles as $featured_article ) :
						// Load featured article
						$data = get_post( $featured_article['featured_article_id'] );

						$title = $data->post_title;

						$content = $data->post_content;
						$content = apply_filters( 'the_content', $content );
						$content = str_replace( ']]>', ']]&gt;', $content );
						$content = wp_trim_words( $content, 30, '...' );
						?>
                        <div class="col-12 col-md-7">
							<?php if ( get_post_meta( $featured_article['featured_article_id'], 'post_feed' ) ): ?>
                            <a href="<?php echo get_post_meta( $featured_article['featured_article_id'], 'post_feed', true ); ?>" class="blog-component--container" target="_blank">
								<?php else : ?>
                                <a href="<?php echo get_permalink( $featured_article['featured_article_id'] ); ?>" class="blog-component--container">
									<?php endif ?>
									<?php echo apply_filters( "dlbi_image", get_the_post_thumbnail_url( $data->ID, 'soxo-blog-medium' ), "", $title ); ?>
                                </a>
                        </div>
                        <div class="col-12 col-md-5">
							<?php
							$categories = get_the_category( $data->ID );
							foreach ( $categories as $category ) :
								?>
                                <p class=""><?php echo esc_html( $category->name ); ?></p>
							<?php endforeach; ?>
							<?php if ( get_post_meta( $featured_article['featured_article_id'], 'post_feed' ) ): ?>
                            <a href="<?php echo get_post_meta( $featured_article['featured_article_id'], 'post_feed', true ); ?>" class="blog-component--container" target="_blank">
								<?php else : ?>
                                <a href="<?php echo get_permalink( $featured_article['featured_article_id'] ); ?>" class="blog-component--container">
									<?php endif ?>
                                    <p class="blog-component--container_date"><?php echo get_the_date( 'd.m.Y', $data->ID ); ?></p>
                                    <p class="blog-component--container_title"><?php echo $title ?></p>
                                    <div class="blog-component--container_content"><?php echo $content ?></div>

                                    <p class="blog-component--container_link">
                                        <i class="fa fa-arrow-right fa-lg" aria-hidden="true"></i><span><?php _e( 'Read more', 'dlbi-sodexo-vc-widget' ) ?></span>
                                    </p>
                                </a>
                        </div>
					<?php endforeach; ?>
                </div>

			<?php endif; ?>
        </div>


        <div class="blog-component--block">
			<?php if ( $items ) : ?>

                <div class="row">
					<?php
					$i = 0;

					foreach ( $items as $item ) :
						if ( $i <= 2 ):
							$title = $item->get_title();

							$content = $item->get_content();
							$content = apply_filters( 'the_content', $content );
							preg_match( '/<img.+src=[\'"](?P<src>.+?)[\'"].*>/i', $content, $result );
							// var_dump($result);
							$img     = $item->get_enclosure()->link ? $item->get_enclosure()->link : $result['src'];
							$content = strip_tags( $content );
							$content = str_replace( ']]>', ']]&gt;', $content );
							$content = wp_trim_words( $content, 30, '...' );
							?>
                            <div class="col-12 col-md-4">
                                <a href="<?php echo $item->get_link(); ?>" class="blog-component--container blog-visuel" target="_blank">
									<?php echo apply_filters( "dlbi_image", $img, "", $title ); ?>
                                </a>

                                <a href="<?php echo $item->get_link(); ?>" class="blog-component--container" target="_blank">
                                    <p class="blog-component--container_date"><?php echo $item->get_date( 'd.m.Y' ); ?></p>
                                    <p class="blog-component--container_title"><?php echo $title ?></p>
                                    <div class="blog-component--container_content"><?php echo $content ?></div>

                                    <p class="blog-component--container_link">
                                        <i class="fa fa-arrow-right fa-lg" aria-hidden="true"></i><span><?php echo __( 'Read more', 'dlbi-sodexo-vc-widget' ); ?></span>
                                    </p>
                                </a>
                            </div>
							<?php
							$i ++;
						endif;
					endforeach;
					?>

                </div>
			<?php endif; ?>
        </div>


		<?php
		$simplelink = 'simplelink';
		$typeform   = 'typeform';
		$youtube    = 'youtube';
		if ( array_key_exists( 'link', $atts ) ):
			$typelink = '';
			if ( array_key_exists( 'type_link', $atts ) ):
				$typelink = $atts["type_link"];
			endif;
			?>
            <div class="blog-component--cta">
				<?php $linkField = vc_build_link( $atts['link'] ); ?>
				<?php if ( $typelink == $simplelink ): ?>
                    <a class="btn-sodexo btn-sodexo-red" href="<?php echo $linkField['url']; ?>" title="<?php echo $linkField['title']; ?>" <?php if ( $linkField['target'] ) : ?>target="_blank"<?php endif; ?>><?php echo $linkField['title']; ?></a>
				<?php elseif ( $typelink == $typeform ) : ?>
                    <a class="btn-sodexo btn-sodexo-red" href="javascript:;" data-toggle="modal" data-target="#typeformModal" data-typeform="<?php echo $linkField['url']; ?>"><?php echo $linkField['title']; ?></a>
				<?php elseif ( $typelink == $youtube ) : ?>
                    <a class="btn-sodexo btn-sodexo-red" data-fancybox href="<?php echo $linkField['url']; ?>"><?php echo $linkField['title']; ?></a>
				<?php endif; ?>
            </div>
		<?php endif ?>
        </div>
	<?php endif ?>
</section>
