<section class="employee-component">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php if (array_key_exists('title', $atts)) : ?>
                    <h2 class="sodexo-title"><?php echo $atts['title']; ?></h2>
                <?php endif; ?>
            </div>
        </div>
        <?php if (array_key_exists('text1', $atts) && array_key_exists('link1', $atts)) : ?>
            <div class="row">
                <div class="col-12 col-sm-12 col-md-6 col-lg-8">
                    <p><?php echo $atts['text1']; ?></p>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                    <div class="employee-component--cta">
                        <?php $linkField = vc_build_link($atts['link1']); ?>
                        <a class="btn-sodexo btn-sodexo-red" href="<?php echo $linkField['url']; ?>" title="<?php echo $linkField['title']; ?>" <?php if ($linkField['target']) : ?>target="_blank"<?php endif; ?>><?php echo $linkField['title']; ?></a>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if (array_key_exists('text2', $atts) && array_key_exists('link2', $atts)) : ?>
            <div class="row">
                <div class="col-12 col-sm-12 col-md-6 col-lg-8">
                    <p><?php echo $atts['text2']; ?></p>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                    <div class="employee-component--cta">
                        <?php $linkField = vc_build_link($atts['link2']); ?>
                        <a class="btn-sodexo btn-sodexo-white" href="<?php echo $linkField['url']; ?>" title="<?php echo $linkField['title']; ?>" <?php if ($linkField['target']) : ?>target="_blank"<?php endif; ?>><?php echo $linkField['title']; ?></a>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</section>
