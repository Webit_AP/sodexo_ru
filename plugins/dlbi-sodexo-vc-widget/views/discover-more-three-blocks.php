<?php
$titles = vc_param_group_parse_atts($atts['titles']);
?>

<section class="discover-three-more">

  <?php /* no container-fluid because the widget is full width */ ?>

    <?php if (array_key_exists('widget_title', $atts)) : ?>
      <div class="container">
        <div class="row">
        <div class="col-md-12">
          <h2 class="sodexo-title"><?php echo $atts['widget_title']; ?></h2>
        </div>
      </div>
    </div>
    <?php endif; ?>

    <?php if (array_key_exists('titles', $atts)) : ?>

      <div class="row no-gutters">
        <?php
        foreach ($titles as $title) :
          $image = $title['image'];
          ?>
          <div class="col-md-4 discover-three-more--block">
            <div class="discover-three-more--container">

              <div class="discover-three-more--container_image" style="background-image: url('<?php echo wp_get_attachment_image_url($image, 'large'); ?>')"></div>

              <div class="discover-three-more--container_content">
                <p class="content_title"><?php echo $title['title']; ?></p>
                <p class="content_description"><?php echo $title['description']; ?></p>
                <?php if (array_key_exists('link', $title)): ?>
                  <?php $linkField = vc_build_link($title['link']); ?>
                  <a class="btn-sodexo btn-sodexo-transparent content_link" href="<?php echo $linkField['url']; ?>" <?php if ($linkField['target']) : ?>target="_blank"<?php endif; ?>><?php echo $linkField['title']; ?></a>
                <?php endif ?>
              </div>

            </div>
          </div>
        <?php endforeach; ?>
      </div>

    <?php endif; ?>

</section>
