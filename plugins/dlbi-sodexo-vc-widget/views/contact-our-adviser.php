<?php
$adviser_contacts = vc_param_group_parse_atts($atts['adviser_contacts']);
?>
<?php if ($adviser_contacts): ?>
    <select>
	<?php foreach ($adviser_contacts as $contact): ?>
	    <option value="<?php echo sanitize_title($contact['contact_name']) ?>"><?php echo $contact['contact_name'] ?></option>
	<?php endforeach; ?>
    </select>
    <?php foreach ($adviser_contacts as $contact): ?>
	<div id="<?php echo sanitize_title($contact['contact_name']) ?>">
	    <?php echo $contact['contact_description'] ?>
	</div>
    <?php endforeach; ?>
    <?php
 endif ?>
