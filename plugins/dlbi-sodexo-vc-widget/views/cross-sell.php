<?php
$posts_id = vc_param_group_parse_atts( $atts['posts_id'] );

// Get color widget
$class_css = 'style-blue';

if ( array_key_exists( 'background_color', $atts ) ) {
	$color = $atts['background_color'];
	if ( $color == '2' ) {
		$class_css = 'style-white';
	}
}
$simplelink = 'simplelink';
$typeform   = 'typeform';
$youtube    = 'youtube';
?>
<section class="cross-sell <?php echo $class_css; ?>">
	<div class="container">
		<?php if ( array_key_exists( 'widget_title', $atts ) ) : ?>
			<div class="row">
				<div class="col-md-12">
					<h2 class="sodexo-title"><?php echo $atts['widget_title']; ?></h2>
				</div>
			</div>
			<?php
		endif;
		// dlbi_display_debug($atts, 0, "orangered");
		if ( array_key_exists( 'posts_id', $atts ) ) :
			?>
			<div class="cross-sell--desktop">
				<div class="row align-items-stretch">
					<?php
					foreach ( $posts_id as $post_id ) :
						// Get post
						$data = get_post( $post_id['post_id'] );
						$cta  = $post_id['post_cta'];

						?>
						<div class="col-12 col-md">
							<div class="cross-sell--container">
								<?php
								$image = get_field( 'product_widget_image', $post_id['post_id'] );
								echo wp_get_attachment_image( $image['id'], 'medium' );
								/*
								if (count($image)) :
									echo '<img src="'.$image['url'].'" class="blog-component--container_image"/>';
								endif;
								*/
								?>

								<h3 class="cross-sell--container_title">
									<a href="<?php echo get_permalink( $post_id['post_id'] ); ?>">
										<?php echo $data->post_title; ?>
									</a>
								</h3>

								<!-- ACF : tabs visual composer widget-->
								<div class="cross-sell--container_description">
									<?php echo get_field( 'product_widget_description', $post_id['post_id'] ); ?>
								</div>
								<?php if ( $cta ) { ?>
									<div class="cross-sell--container_cta">
										<?php echo set_cta_object( $cta ); ?>
									</div>
								<?php } ?>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>

			<div class="cross-sell--mobile">

				<div class="cross-sell-product-slick">
					<?php
					foreach ( $posts_id as $post_id ) :
						// Get post
						$data = get_post( $post_id['post_id'] );
						$cta  = $post_id['post_cta'];
						?>
						<div>
							<div class="cross-sell--container">
								<?php
								$image = get_field( 'product_widget_image', $post_id['post_id'] );
								echo wp_get_attachment_image( $image['id'], 'medium' );
								/*
								if (count($image)) :
									echo '<img src="'.$image['url'].'" class="blog-component--container_image"/>';
								endif;
								*/
								?>
								<h3 class="cross-sell--container_title">
									<a href="<?php echo get_permalink( $post_id['post_id'] ); ?>">
										<?php echo $data->post_title; ?>
									</a>
								</h3>
								<!-- ACF : tabs visual composer widget-->
								<div class="cross-sell--container_description">
									<?php echo get_field( 'product_widget_description', $post_id['post_id'] ); ?>
								</div>
								<?php if ( $cta ) { ?>
									<div class="cross-sell--container_cta">
										<?php echo set_cta_object( $cta ); ?>
									</div>
								<?php } ?>
							</div>
						</div>
					<?php endforeach; ?>

				</div>

			</div>
			<?php
		endif;

		if ( array_key_exists( 'link', $atts ) ) :
			$typelink = '';
			if ( array_key_exists( 'type_link', $atts ) ) {
				$typelink = $atts['type_link'];
			}
			?>
			<div class="cross-sell--cta">
				<?php
				$linkField = vc_build_link( $atts['link'] );
				$class_btn = 'btn-sodexo-red';
				if ( $class_css == 'style-blue' ) {
					$class_btn = 'btn-sodexo-white';
				}
				if ( $linkField['url'] && $linkField['title'] ) {
					switch ( $typelink ) {
						case $simplelink:
							{
							?>
								<a class="btn-sodexo <?php echo $class_btn; ?>" href="<?php echo $linkField['url']; ?>" title="<?php echo $linkField['title']; ?>" 
																<?php
																if ( $linkField['target'] ) :
																	?>
									target="_blank"<?php endif; ?>><?php echo $linkField['title']; ?></a>
								<?php
								break;
						}
						case $typeform:
							{
							?>
								<a class="btn-sodexo <?php echo $class_btn; ?>" href="javascript:;" data-toggle="modal" data-target="#typeformModal" data-typeform="<?php echo $linkField['url']; ?>"><?php echo $linkField['title']; ?></a>
								<?php
								break;
						}
						case $youtube:
							{
							?>
								<a class="btn-sodexo <?php echo $class_btn; ?>" data-fancybox href="<?php echo $linkField['url']; ?>"><?php echo $linkField['title']; ?></a>
								<?php
								break;
						}
					}
				}
				?>
			</div>
		<?php endif ?>
	</div>

</section>
