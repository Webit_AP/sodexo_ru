<section class="poll-sodexo-vc-widget">

  <div class="container">

    <div class="row">

      <div class="col-12">

        <?php if (array_key_exists('sodexo_poll_title', $atts)) : ?>
          <h2 class="sodexo-title"><?php echo $atts['sodexo_poll_title']; ?></h2>
        <?php endif ?>
        <?php if (array_key_exists('sodexo_poll_id', $atts)) : ?>
          <?php echo do_shortcode('[poll id="' . $atts['sodexo_poll_id']. '"]'); ?>
        <?php endif ?>

      </div>

    </div>

  </div>

</section>
