<?php
$titles = vc_param_group_parse_atts($atts['lines']);
?>

<h2><?php echo $atts['widget-title']; ?></h2>

<div class="sodexo-widget-boxes">
    <?php if ($titles): ?>
        <div class="sodexo-widget-boxes-container">

	    <?php foreach ($titles as $title) { ?>
		<div class="box">
		    <h3 class="box-title"><?php echo $title['line']; ?></h3>
		</div>

	    <?php } ?>

        </div>
    <?php endif ?>
</div>
