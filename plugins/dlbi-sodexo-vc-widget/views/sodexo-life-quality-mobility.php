<?php
// Titre
if(isset($atts['title'])) {
    $title = $atts['title'];
}
// Descrition
if(isset($atts['description'])) {
    $desc = $atts['description'];
}
// URL ['url'] and ['title']
if(isset($atts['link'])) {
    $link = vc_build_link($atts['link']);
}
// Image
if(isset($atts['image'])) {
    $image_id = $atts['image'];
}
// Position
if(isset($atts['position'])) {
    $position = $atts['position'];
}

$blocposition = 'lqm-left';
if (isset($position) && $position=="right") {
    $blocposition = "lqm-right";
}

?>

<section class="life-quality-mobility <?php echo $blocposition; ?>">

    <div class="row no-gutters align-items-stretch">

        <div class="col-12 col-md-6">

            <div class="life-quality-mobility_bgimage" style="background-image:url('<?php echo wp_get_attachment_url($image_id, 'large'); ?>');">
            </div>

        </div>

        <div class="col-12 col-md-6 align-self-center">

            <div class="life-quality-mobility_content">

                <?php if (isset($atts['title'])) : ?>
                    <h3 class="sodexo-title sodexo-title--white"><?php echo $title; ?></h3>
                <?php endif; ?>
                <p><?php if(isset($desc)){
                        echo $desc;
                    } ?></p>
                <?php if(isset($link['url']) && $link['url'] && $link['title']):?>
                    <a target="<?php echo $link['target']?>" class="btn-sodexo btn-sodexo-white" href="<?php echo $link['url']; ?>"><?php echo $link['title']; ?></a>
                <?php endif; ?>

            </div>

        </div>

    </div>

</section>
