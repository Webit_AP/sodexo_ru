<?php

class SodexoWidget_ChooseSodexo{

	// Params for display inputs in widget back-office
	var $params = array();
	// Params of widget
	var $widget_params = array();
	// Display an array of custom posts
	var $posts_lists = array();
	// Widget name
	var $name = 'Choose Sodexo Widget (4 colonnes)';
	// Widget slug
	var $base = 'choose_sodexo_widget_4_colonnes';
	// Widget category
	var $category = 'Sodexo';
	// Widget class
	var $class = 'choose-sodexo-widget-4-colonnes';
	// Widget description
	var $description = 'Choose Sodexo Widget (4 colonnes).';
	// Widget view
	var $view = SOD_VCWID_DIR . '/views/choose-sodexo.php';

	function __construct(){
		$this->sodexo_set_widget_params();
		$this->lbisodexo_set_widget_params();
		// Hooks for grid builder
		add_shortcode($this->base, array($this, 'sodexo_grid_render'));
		add_filter('vc_grid_item_shortcodes', array($this, 'sodexo_widget_add_grid_shortcodes'));

		//Hooks for element
		add_action('vc_before_init', array($this, 'sodexo_widget_add_element'));
		add_shortcode($this->base, array($this, 'sodexo_element_render'));

	}

	/**
	 * Set widget params
	 */
	function lbisodexo_set_widget_params(){
		$this->widget_params = array(
			'name'        => __($this->name, 'js_composer'),
			'base'        => $this->base,
			'class'       => $this->class,
			'category'    => __($this->category, "js_composer"),
			'description' => __($this->description, 'js_composer'),
			'params'      => $this->params,
			'icon'        => 'http://fr.sodexo.com/modules/sodexocom-templates/img/sodexo-favicon.ico'
		);
	}

	/**
	 * Load our textdomain for localization.
	 *
	 * @return void
	 */
	public function sodexo_textdomain(){
		load_plugin_textdomain('dlbi-sodexo-vc-widget', false, dirname(plugin_basename(__FILE__)) . '/languages/');
	}

	/**
	 * Sets params for element and grid builder widget settings
	 */
	function sodexo_set_widget_params(){
		$this->params = array(
			array(
				'type'       => 'textfield',
				'holder'     => '',
				'class'      => '',
				'heading'    => __('Widget Title', 'js_composer'),
				'param_name' => 'widget_title',
				'value'      => ''
			),
			// params group
			array(
				'heading'    => __('Titles', 'js_composer'),
				'type'       => 'param_group',
				'value'      => '',
				'param_name' => 'titles',
				// Note params is mapped inside param-group:
				'params'     => array(
					array(
						'type'        => 'attach_image',
						'heading'     => __('Image', 'js_composer'),
						'param_name'  => 'image',
						'description' => __('Select image from media library.', 'js_composer')
					),
					array(
						"type"       => "textfield",
						"holder"     => "",
						"class"      => "",
						"heading"    => __('Subtitle', 'js_composer'),
						"param_name" => 'subtitle',
						"value"      => ''
					),
					array(
						"type"       => "vc_link",
						"holder"     => "",
						"class"      => "",
						"heading"    => __('Link', 'js_composer'),
						"param_name" => 'link',
						"value"      => ''
					),
//					array(
//						"type"       => "vc_link",
//						"holder"     => "",
//						"class"      => "",
//						"heading"    => __('Call to Action', 'js_composer'),
//						"param_name" => 'cta_link',
//						"value"      => ''
//					)
				)
			),
			array(
				"type"       => "textfield",
				"heading"    => __('Pre-text link', 'js_composer'),
				"param_name" => 'pre-text-link',
				"value"      => ''
			),
			array(
				"type"       => "dropdown",
				"heading"    => __("Select a link type", "js_composer"),
				"param_name" => "type_link",
				"value"      => array(
					'Simplelink' => 'simplelink',
					'Typeform'   => 'typeform',
					'Youtube'    => 'youtube'
				),
				'std'        => 'Simplelink' // Your default value
			),
			array(
				"type"       => "vc_link",
				"heading"    => __('Link', 'js_composer'),
				"param_name" => 'link',
				"value"      => ''
			),
		);
	}

	/**
	 * Add widget to grid builder
	 *
	 * @param array $shortcodes
	 *
	 * @return type
	 */
	public function sodexo_widget_add_grid_shortcodes($shortcodes){
		$params              = $this->widget_params;
		$params['post_type'] = Vc_Grid_Item_Editor::postType();

		$shortcodes[$this->base] = $params;

		return $shortcodes;
	}

	/**
	 * Add to posts, pages ...
	 */
	public function sodexo_widget_add_element(){
		// Map the block with vc_map()
		vc_map($this->widget_params);
	}

	/**
	 * Grid output function
	 *
	 * @param type $atts
	 */
	public function sodexo_grid_render($atts){
		SodexoVcwid_Client::sodexo_widget_render($atts, $this->view);
	}

	/**
	 * Element output function
	 *
	 * @param type $atts
	 */
	public function sodexo_element_render($atts){
		SodexoVcwid_Client::sodexo_widget_render($atts, $this->view);
	}

}

new SodexoWidget_ChooseSodexo();
