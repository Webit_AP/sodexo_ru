<?php

class SodexoWidget_EmployeeComponent {

    // Params for display inputs in widget back-office
    var $params = array();
    // Params of widget
    var $widget_params = array();
    // Widget name
    var $name = 'Sodexo Employee';
    // Widget slug
    var $base = 'sodexo_employee';
    // Widget category
    var $category = 'Sodexo';
    // Widget class
    var $class = 'sodexo-employee';
    // Widget description
    var $description = 'Sodexo Employee.';
    // Widget view
    var $view = SOD_VCWID_DIR . '/views/employee-component.php';

    function __construct() {
        // Here put on argument your custom post slug
        $this->sodexo_set_widget_params();
        $this->lbisodexo_set_widget_params();
        // Hooks for grid builder
        add_shortcode($this->base, array($this, 'sodexo_grid_render'));
        add_filter('vc_grid_item_shortcodes', array($this, 'sodexo_widget_add_grid_shortcodes'));

        //Hooks for element
        add_action('vc_before_init', array($this, 'sodexo_widget_add_element'));
        add_shortcode($this->base, array($this, 'sodexo_element_render'));
    }

    /**
     * Set widget params
     */
    function lbisodexo_set_widget_params() {
        $this->widget_params = array(
            'name' => __($this->name, 'js_composer'),
            'base' => $this->base,
            'class' => $this->class,
            'category' => __($this->category, "js_composer"),
            'description' => __($this->description, 'js_composer'),
            'params' => $this->params,
            'icon'  => 'http://fr.sodexo.com/modules/sodexocom-templates/img/sodexo-favicon.ico'
        );
    }

    /**
     * Sets params for element and grid builder widget settings
     */
    function sodexo_set_widget_params() {
        $this->params = array(
            array(
                'type' => 'textfield',
                'holder' => '',
                'class' => '',
                'heading' => __('Title', 'js_composer'),
                'param_name' => 'title',
                'value' => ''
            ),
            array(
                'type' => 'textfield',
                'holder' => '',
                'class' => '',
                'heading' => __('Text 1', 'js_composer'),
                'param_name' => 'text1',
                'value' => ''
            ),
            array(
                "type" => "vc_link",
                "heading" => __('Link 1', 'js_composer'),
                "param_name" => 'link1',
                "value" => ''
            ),
            array(
                'type' => 'textfield',
                'holder' => '',
                'class' => '',
                'heading' => __('Text 2', 'js_composer'),
                'param_name' => 'text2',
                'value' => ''
            ),
            array(
                "type" => "vc_link",
                "heading" => __('Link 2', 'js_composer'),
                "param_name" => 'link2',
                "value" => ''
            ),
        );
    }

    /**
     * Add widget to grid builder
     *
     * @param array $shortcodes
     * @return type
     */
    public function sodexo_widget_add_grid_shortcodes($shortcodes) {
        $params = $this->widget_params;
        $params['post_type'] = Vc_Grid_Item_Editor::postType();

        $shortcodes[$this->base] = $params;

        return $shortcodes;
    }

    /**
     * Add to posts, pages ...
     */
    public function sodexo_widget_add_element() {
        // Map the block with vc_map()
        vc_map($this->widget_params);
    }

    /**
     * Grid output function
     *
     * @param type $atts
     */
    public function sodexo_grid_render($atts) {
        SodexoVcwid_Client::sodexo_widget_render($atts, $this->view);
    }

    /**
     * Element output function
     *
     * @param type $atts
     */
    public function sodexo_element_render($atts) {
        SodexoVcwid_Client::sodexo_widget_render($atts, $this->view);
    }

}
new SodexoWidget_EmployeeComponent();
