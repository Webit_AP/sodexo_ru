<?php

class SodexoWidget_DiscoverMore{

	// Params for display inputs in widget back-office
	var $params = [];
	// Params of widget
	var $widget_params = [];
	// Display an array of custom posts
	var $posts_lists = [];
	// Widget name
	var $name = 'Discover more';
	// Widget slug
	var $base = 'discover_more_mosaic';
	// Widget category
	var $category = 'Sodexo';
	// Widget class
	var $class = 'discover-more-block';
	// Widget description
	var $description = 'Image mosaic for needs';
	// Widget view
	var $view = SOD_VCWID_DIR . '/views/discover-more.php';

	function __construct(){
		$this->sodexo_get_posts_list(['need', 'post', 'page', 'product']);
		$this->sodexo_set_widget_params();
		$this->lbisodexo_set_widget_params();
		// Hooks for grid builder
		add_shortcode($this->base, [$this, 'sodexo_grid_render']);
		add_filter('vc_grid_item_shortcodes', [$this, 'sodexo_widget_add_grid_shortcodes']);

		//Hooks for element
		add_action('vc_before_init', [$this, 'sodexo_widget_add_element']);
		add_shortcode($this->base, [$this, 'sodexo_element_render']);
	}

	/**
	 * Get posts list
	 *
	 * @param array|string $post_type
	 */
	function sodexo_get_posts_list($post_type){
		$this->posts_lists = SodexoVcwid_Client::sodexo_vcwid_get_posts_values($post_type);
	}

	/**
	 * Set widget params
	 */
	function lbisodexo_set_widget_params(){
		$this->widget_params = [
			'name'        => __($this->name, 'js_composer'),
			'base'        => $this->base,
			'class'       => $this->class,
			'category'    => __($this->category, "js_composer"),
			'description' => __($this->description, 'js_composer'),
			'params'      => $this->params,
			'icon'        => 'http://fr.sodexo.com/modules/sodexocom-templates/img/sodexo-favicon.ico',
		];
	}

	/**
	 * Sets params for element and grid builder widget settings
	 */
	function sodexo_set_widget_params(){
		$this->params = [
			[
				'type'       => 'textfield',
				'holder'     => '',
				'class'      => '',
				'heading'    => __('Widget Title', 'js_composer'),
				'param_name' => 'widget_title',
				'value'      => '',
			],
			// params group
			[
				'heading'    => __('Titles', 'js_composer'),
				'type'       => 'param_group',
				'value'      => '',
				'param_name' => 'titles',
				// Note params is mapped inside param-group:
				'params'     => [
					[
						'type'        => 'attach_image',
						'heading'     => __('Background image', 'js_composer'),
						'param_name'  => 'discovermore_image',
						'description' => __('Select image from media library.', 'js_composer'),
					],
					[
						"type"       => "textfield",
						"holder"     => "",
						"class"      => "",
						"heading"    => __('Title', 'js_composer'),
						"param_name" => 'discovermore_title',
						"value"      => '',
					],
					[
						"type"       => "textfield",
						"holder"     => "",
						"class"      => "",
						"heading"    => __('Description', 'js_composer'),
						"param_name" => 'discovermore_description',
						"value"      => '',
					],
					[
						'type'       => 'dropdown',
						'value'      => $this->posts_lists,
						'heading'    => 'Link to post',
						'param_name' => 'post_id_link',
					],
					[
						'type'       => 'dropdown',
						'heading'    => __('Link type', 'js_composer'),
						'param_name' => 'discovermore_linktype',
						'value'      => ['Plus icon' => '1', 'Discover more' => '2'],
					],
				],
			],

		];
	}

	/**
	 * Add widget to grid builder
	 *
	 * @param array $shortcodes
	 *
	 * @return array
	 */
	public function sodexo_widget_add_grid_shortcodes($shortcodes){
		$params              = $this->widget_params;
		$params['post_type'] = Vc_Grid_Item_Editor::postType();

		$shortcodes[$this->base] = $params;

		return $shortcodes;
	}

	/**
	 * Add to posts, pages ...
	 */
	public function sodexo_widget_add_element(){
		// Map the block with vc_map()
		vc_map($this->widget_params);
	}

	/**
	 * Grid output function
	 *
	 * @param array $atts
	 */
	public function sodexo_grid_render($atts){
		SodexoVcwid_Client::sodexo_widget_render($atts, $this->view);
	}

	/**
	 * Element output function
	 *
	 * @param array $atts
	 */
	public function sodexo_element_render($atts){
		SodexoVcwid_Client::sodexo_widget_render($atts, $this->view);
	}

}

new SodexoWidget_DiscoverMore();
