<?php

class SodexoWidget_Boxes {

    // Params for display inputs in widget back-office
    var $params = array();
    // Params of widget
    var $widget_params = array();
    // Display an array of custom posts
    var $posts_lists = array();
    // Widget name
    var $name = 'Sodexo boxes';
    // Widget slug
    var $base = 'boxes';
    // Widget category
    var $category = 'Sodexo';
    // Widget class
    var $class = 'boxes-widget';
    // Widget description
    var $description = 'Boxes component with images and text.';
    // Widget view
    var $view = SOD_VCWID_DIR . '/views/boxes.php';

    function __construct() {
    	// Here put on argument your custom post slug
    	// $this->sodexo_get_posts_list('faq');
    	$this->sodexo_set_widget_params();
    	$this->lbisodexo_set_widget_params();
    	// Hooks for grid builder
    	add_shortcode($this->base, array($this, 'sodexo_grid_render'));
    	add_filter('vc_grid_item_shortcodes', array($this, 'sodexo_widget_add_grid_shortcodes'));

        //Hooks for element
    	add_action('vc_before_init', array($this, 'sodexo_widget_add_element'));
    	add_shortcode($this->base, array($this, 'sodexo_element_render'));

    }

    /**
     * Get posts list
     *
     * @param type $post_type
     */
    function sodexo_get_posts_list($post_type) {
	   $this->posts_lists = SodexoVcwid_Client::sodexo_vcwid_get_posts_values($post_type);
    }

    /**
     * Set widget params
     */
    function lbisodexo_set_widget_params() {
    	$this->widget_params = array(
    	    'name' => __($this->name, 'js_composer'),
    	    'base' => $this->base,
    	    'class' => $this->class,
    	    'category' => __($this->category, "js_composer"),
    	    'description' => __($this->description, 'js_composer'),
    	    'params' => $this->params,
	    'icon'  => 'http://fr.sodexo.com/modules/sodexocom-templates/img/sodexo-favicon.ico'
    	);
    }

    /**
     * Sets params for element and grid builder widget settings
     */
    function sodexo_set_widget_params() {
    	$this->params = array(
          array(
            'type' => 'textfield',
            'holder' => 'div',
            'class' => '',
            'heading' => __('Top title', 'js_composer'),
            'param_name' => 'top-title',
            'value' => __('Default param value', 'js_composer'),
            'description' => __('Title to display in the top.', 'js_composer')
           ),
    	    // params group
    	    array(
        		'heading' => __('Titles', 'js_composer'),
        		'type' => 'param_group',
        		'value' => '',
        		'param_name' => 'titles',
        		// Note params is mapped inside param-group:
        		'params' => array(
              array(
                'type' => 'attach_images',
                'heading' => __('Image size', 'js_composer'),
                'param_name' => 'img_size',
                'description' => __('Enter image size (Example: "thumbnail", "medium", "large", "full" or other sizes defined by theme). Alternatively enter size in pixels (Example: 200x100 (Width x Height)). Leave parameter empty to use "thumbnail" by default.', 'js_composer')
              ),
        		  array(
          			'type' => 'textfield',
                "holder" => "div",
                "class" => "",
          			'value' => __( "Title of the box", "sodexo-text-domain" ),
          			'heading' => 'Box title',
          			'param_name' => 'title',
        		  ),
              array(
          			'type' => 'textfield',
                "holder" => "div",
                "class" => "",
          			'value' => __( "Text of the box", "sodexo-text-domain" ),
          			'heading' => 'Box text',
          			'param_name' => 'text',
        		  ),
        		)
    	    )
    		)
    	;
    }

    /**
     * Add widget to grid builder
     *
     * @param array $shortcodes
     * @return type
     */
    public function sodexo_widget_add_grid_shortcodes($shortcodes) {
    	$params = $this->widget_params;
    	$params['post_type'] = Vc_Grid_Item_Editor::postType();

    	$shortcodes['vc_grid_test'] = $params;

    	return $shortcodes;
    }

    /**
     * Add to posts, pages ...
     */
    public function sodexo_widget_add_element() {
        // Map the block with vc_map()
	    vc_map($this->widget_params);
    }

    /**
     * Grid output function
     *
     * @param type $atts
     */
    public function sodexo_grid_render($atts) {
	   SodexoVcwid_Client::sodexo_widget_render($atts, $this->view);
    }

    /**
     * Element output function
     *
     * @param type $atts
     */
    public function sodexo_element_render($atts) {
	   SodexoVcwid_Client::sodexo_widget_render($atts, $this->view);
    }

}
new SodexoWidget_Boxes();
