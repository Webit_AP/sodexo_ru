<?php

class SodexoWidget_CarouselTheyTrustUs{

	// Params for display inputs in widget back-office
	var $params = [];
	// Params of widget
	var $widget_params = [];
	// Display an array of custom posts
	var $posts_lists = [];
	// Widget name
	var $name = 'Carousel They Trust Us Widget';
	// Widget slug
	var $base = 'carousel_they_trust_widget';
	// Widget category
	var $category = 'Sodexo';
	// Widget class
	var $class = 'carousel-they-trust-widget';
	// Widget description
	var $description = 'Carousel They Trust Us Widget.';
	// Widget view
	var $view = SOD_VCWID_DIR . '/views/carousel-they-trust-us.php';

	function __construct(){
		$this->sodexo_set_widget_params();
		$this->lbisodexo_set_widget_params();
		// Hooks for grid builder
		add_shortcode($this->base, [$this, 'sodexo_grid_render']);
		add_filter('vc_grid_item_shortcodes', [$this, 'sodexo_widget_add_grid_shortcodes']);

		//Hooks for element
		add_action('vc_before_init', [$this, 'sodexo_widget_add_element']);
		add_shortcode($this->base, [$this, 'sodexo_element_render']);
	}

	/**
	 * Get posts list
	 *
	 * @param type $post_type
	 */
	function sodexo_get_posts_list($post_type){
		$this->posts_lists = SodexoVcwid_Client::sodexo_vcwid_get_posts_values($post_type);
	}

	/**
	 * Set widget params
	 */
	function lbisodexo_set_widget_params(){
		$this->widget_params = [
			'name'        => __($this->name, 'js_composer'),
			'base'        => $this->base,
			'class'       => $this->class,
			'category'    => __($this->category, "js_composer"),
			'description' => __($this->description, 'js_composer'),
			'params'      => $this->params,
			'icon'        => 'http://fr.sodexo.com/modules/sodexocom-templates/img/sodexo-favicon.ico',
		];
	}

	/**
	 * Sets params for element and grid builder widget settings
	 */
	function sodexo_set_widget_params(){
		$this->params = [
			[
				'type'       => 'textfield',
				'heading'    => __('Widget Title', 'js_composer'),
				'param_name' => 'widget_title',
				'value'      => '',
			],
			// params group
			[
				'type'        => 'attach_images',
				'heading'     => __('Pictures list', 'js_composer'),
				'param_name'  => 'logos',
				'description' => __('Select pictures from media library.', 'js_composer'),
			],
			[
				'type'       => 'hidden',
				'param_name' => 'link_translation',
				'value'      => __('Link', 'js_composer'),
			],
			[
				'type'       => 'hidden',
				'param_name' => 'target_translation',
				'value'      => __('New tab', 'js_composer'),
			],
			[
				//				'type'       => 'textfield',
				'type'       => 'hidden',
				//				'heading'    => __('Links', 'js_composer'),
				'param_name' => 'links',
				'value'      => '',
			],
			[
				//				'type'       => 'textfield',
				'type'       => 'hidden',
				//				'heading'    => __('Targets', 'js_composer'),
				'param_name' => 'targets',
				'value'      => '',
			],
		];
	}

	/**
	 * Add widget to grid builder
	 *
	 * @param array $shortcodes
	 *
	 * @return type
	 */
	public function sodexo_widget_add_grid_shortcodes($shortcodes){
		$params                  = $this->widget_params;
		$params['post_type']     = Vc_Grid_Item_Editor::postType();
		$shortcodes[$this->base] = $params;
		return $shortcodes;
	}

	/**
	 * Add to posts, pages ...
	 */
	public function sodexo_widget_add_element(){
		// Map the block with vc_map()
		vc_map($this->widget_params);
	}

	/**
	 * Grid output function
	 *
	 * @param type $atts
	 */
	public function sodexo_grid_render($atts){
		SodexoVcwid_Client::sodexo_widget_render($atts, $this->view);
	}

	/**
	 * Element output function
	 *
	 * @param type $atts
	 */
	public function sodexo_element_render($atts){
		SodexoVcwid_Client::sodexo_widget_render($atts, $this->view);
	}
}

new SodexoWidget_CarouselTheyTrustUs();
