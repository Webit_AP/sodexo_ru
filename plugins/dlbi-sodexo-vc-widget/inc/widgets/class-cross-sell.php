<?php

class SodexoWidget_CrossSell {

	// Params for display inputs in widget back-office
	var $params = array();
	// Params of widget
	var $widget_params = array();
	// Display an array of custom posts
	var $posts_lists = array();
	// Widget name
	var $name = 'You may also be interested in (CrossSell)';
	// Widget slug
	var $base = 'cross_sell';
	// Widget category
	var $category = 'Sodexo';
	// Widget class
	var $class = 'cross-sell';
	// Widget description
	var $description = 'You may also be interested in (CrossSell).';
	// Widget view
	var $view = SOD_VCWID_DIR . '/views/cross-sell.php';

	function __construct() {
		// Here put on argument your custom post slug
		$this->sodexo_get_posts_list( 'product' );

		$this->sodexo_set_widget_params();
		$this->lbisodexo_set_widget_params();
		// Hooks for grid builder
		add_shortcode( $this->base, array( $this, 'sodexo_grid_render' ) );
		add_filter( 'vc_grid_item_shortcodes', array( $this, 'sodexo_widget_add_grid_shortcodes' ) );

		// Hooks for element
		add_action( 'vc_before_init', array( $this, 'sodexo_widget_add_element' ) );
		add_shortcode( $this->base, array( $this, 'sodexo_element_render' ) );

	}

	/**
	 * Get posts list
	 *
	 * @param type $post_type
	 */
	function sodexo_get_posts_list( $post_type ) {
		$this->posts_lists = SodexoVcwid_Client::sodexo_vcwid_get_posts_values( $post_type );
	}

	/**
	 * Set widget params
	 */
	function lbisodexo_set_widget_params() {
		$this->widget_params = array(
			'name'        => __( $this->name, 'js_composer' ),
			'base'        => $this->base,
			'class'       => $this->class,
			'category'    => __( $this->category, 'js_composer' ),
			'description' => __( $this->description, 'js_composer' ),
			'params'      => $this->params,
			'icon'        => 'http://fr.sodexo.com/modules/sodexocom-templates/img/sodexo-favicon.ico',
		);
	}

	/**
	 * Sets params for element and grid builder widget settings
	 */
	function sodexo_set_widget_params() {
		$this->params = array(
			array(
				'type'       => 'textfield',
				'holder'     => '',
				'class'      => '',
				'heading'    => __( 'Widget Title', 'js_composer' ),
				'param_name' => 'widget_title',
				'value'      => '',
			),

			array(
				'type'       => 'dropdown',
				'heading'    => __( 'Background color', 'js_composer' ),
				'param_name' => 'background_color',
				'value'      => array(
					'Blue'  => '1',
					'White' => '2',
				),
			),

			// params group
			array(
				'heading'    => __( 'Titles', 'js_composer' ),
				'type'       => 'param_group',
				'value'      => '',
				'param_name' => 'posts_id',
				// Note params is mapped inside param-group:
				'params'     => array(
					array(
						'type'       => 'dropdown',
						'value'      => $this->posts_lists,
						'heading'    => 'Choose your post',
						'param_name' => 'post_id',
					),
					array(
						'type'       => 'vc_link',
						'heading'    => __( 'CTA', 'js_composer' ),
						'param_name' => 'post_cta',
						'value'      => '',
					),
				),
			),
			array(
				'type'       => 'dropdown',
				'heading'    => __( 'Select a link type', 'js_composer' ),
				'param_name' => 'type_link',
				'value'      => array(
					'Simplelink' => 'simplelink',
					'Typeform'   => 'typeform',
					'Youtube'    => 'youtube',
				),
				'std'        => 'Simplelink', // Your default value
			),
			array(
				'type'       => 'vc_link',
				'heading'    => __( 'Link', 'js_composer' ),
				'param_name' => 'link',
				'value'      => '',
			),

		);
	}

	/**
	 * Add widget to grid builder
	 *
	 * @param array $shortcodes
	 * @return type
	 */
	public function sodexo_widget_add_grid_shortcodes( $shortcodes ) {
		$params              = $this->widget_params;
		$params['post_type'] = Vc_Grid_Item_Editor::postType();

		$shortcodes[ $this->base ] = $params;

		return $shortcodes;
	}

	/**
	 * Add to posts, pages ...
	 */
	public function sodexo_widget_add_element() {
		// Map the block with vc_map()
		vc_map( $this->widget_params );
	}

	/**
	 * Grid output function
	 *
	 * @param type $atts
	 */
	public function sodexo_grid_render( $atts ) {
		SodexoVcwid_Client::sodexo_widget_render( $atts, $this->view );
	}

	/**
	 * Element output function
	 *
	 * @param type $atts
	 */
	public function sodexo_element_render( $atts ) {
		SodexoVcwid_Client::sodexo_widget_render( $atts, $this->view );
	}

}
new SodexoWidget_CrossSell();
