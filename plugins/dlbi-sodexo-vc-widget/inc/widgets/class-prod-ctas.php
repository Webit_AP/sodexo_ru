<?php

class SodexoProdCtas {

    // Params for display inputs in widget back-office
    var $params = array();
    // Params of widget
    var $widget_params = array();
    // Widget name
    var $name = 'Product Page CTA Block';
    // Widget slug
    var $base = 'product_page_cta_block';
    // Widget category
    var $category = 'Sodexo';
    // Widget class
    var $class = 'product-page-cta-block';
    // Widget description
    var $description = 'Product Page CTA Block.';
    // Widget view
    var $view = SOD_VCWID_DIR . '/views/prod-ctas.php';

    function __construct() {
	// Here put on argument your custom post slug
	$this->sodexo_set_widget_params();
	$this->lbisodexo_set_widget_params();
	// Hooks for grid builder
	add_shortcode($this->base, array($this, 'sodexo_grid_render'));
	add_filter('vc_grid_item_shortcodes', array($this, 'sodexo_widget_add_grid_shortcodes'));

	//Hooks for element
	add_action('vc_before_init', array($this, 'sodexo_widget_add_element'));
	add_shortcode($this->base, array($this, 'sodexo_element_render'));

    }

    /**
     * Set widget params
     */
    function lbisodexo_set_widget_params() {
	$this->widget_params = array(
	    'name' => __($this->name, 'js_composer'),
	    'base' => $this->base,
	    'class' => $this->class,
	    'category' => __($this->category, "js_composer"),
	    'description' => __($this->description, 'js_composer'),
	    'params' => $this->params,
	    'icon'  => 'http://fr.sodexo.com/modules/sodexocom-templates/img/sodexo-favicon.ico'
	);
    }

    /**
     * Sets params for element and grid builder widget settings
     */
    function sodexo_set_widget_params() {
	$this->params = array(
	    array(
		'type' => 'textfield',
		'heading' => __('Widget title', 'js_composer'),
		'param_name' => 'widget-title',
	    ),
	    array(
		'type' => 'textfield',
		'heading' => __('Text', 'js_composer'),
		'param_name' => 'text',
	    ),
	    // params group
	    array(
		'heading' => __('Key figures', 'js_composer'),
		'type' => 'param_group',
		'param_name' => 'key-figures',
		// Note params is mapped inside param-group:
		'params' => array(
		    array(
			'type' => 'attach_image',
			'heading' => __('Icon', 'js_composer'),
			'param_name' => 'icon',
		    ),
		    array(
			'type' => 'textfield',
			'heading' => __('Number', 'js_composer'),
			'param_name' => 'number',
		    ),
		    array(
			'type' => 'textfield',
			'heading' => __('Title', 'js_composer'),
			'param_name' => 'title',
		    ),
		    array(
			'type' => 'textfield',
			'heading' => __('Read more link', 'js_composer'),
			'param_name' => 'read-more-link',
		    ),
		    array(
			'type' => 'textfield',
			'heading' => __('CTA', 'js_composer'),
			'param_name' => 'cta',
		    ),
		)
	    )
		)
	;
    }

    /**
     * Add widget to grid builder
     *
     * @param array $shortcodes
     * @return type
     */
    public function sodexo_widget_add_grid_shortcodes($shortcodes) {
	$params = $this->widget_params;
	$params['post_type'] = Vc_Grid_Item_Editor::postType();

	$shortcodes[$this->base] = $params;

	return $shortcodes;
    }

    /**
     * Add to posts, pages ...
     */
    public function sodexo_widget_add_element() {
	// Map the block with vc_map()
	vc_map($this->widget_params);
    }

    /**
     * Grid output function
     *
     * @param type $atts
     */
    public function sodexo_grid_render($atts) {
	SodexoVcwid_Client::sodexo_widget_render($atts, $this->view);
    }

    /**
     * Element output function
     *
     * @param type $atts
     */
    public function sodexo_element_render($atts) {
	SodexoVcwid_Client::sodexo_widget_render($atts, $this->view);
    }

}

new SodexoProdCtas();
