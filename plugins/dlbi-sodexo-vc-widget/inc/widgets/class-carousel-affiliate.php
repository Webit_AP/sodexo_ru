<?php

class SodexoWidget_CarouselAffiliate {

    // Params for display inputs in widget back-office
    var $params = array();
    // Params of widget
    var $widget_params = array();
    // Display an array of items menu
    var $items_list = array();
    // Display an array of custom posts
    var $posts_lists = array();
    // Widget name
    var $name = 'Sodexo carousel affiliate';
    // Widget slug
    var $base = 'soxo-carousel-affiliate';
    // Widget category
    var $category = 'Sodexo';
    // Widget class
    var $class = 'carousel-affiliate-widget';
    // Widget description
    var $description = 'Carousel affiliate component with images and text.';
    // Widget view
    var $view = SOD_VCWID_DIR . '/views/carousel-affiliate.php';

    function __construct() {
	$this->sodexo_get_posts_list(SOD_PRO_PTYPE);
	$this->sodexo_get_items_list();
	$this->sodexo_set_widget_params();
	$this->lbisodexo_set_widget_params();
	// Hooks for grid builder
	add_shortcode($this->base, array($this, 'sodexo_grid_render'));
	add_filter('vc_grid_item_shortcodes', array($this, 'sodexo_widget_add_grid_shortcodes'));

	//Hooks for element
	add_action('vc_before_init', array($this, 'sodexo_widget_add_element'));
	add_shortcode($this->base, array($this, 'sodexo_element_render'));
    }

    /**
     * Get posts list
     *
     * @param type $post_type
     */
    function sodexo_get_posts_list($post_type) {
	$this->posts_lists = SodexoVcwid_Client::sodexo_vcwid_get_posts_values($post_type);
    }

    /**
     * Get items menu list
     *
     */
    function sodexo_get_items_list() {
	$this->items_lists = get_option('top-menu-affiliate-bar');
    }

    /**
     * Set widget params
     */
    function lbisodexo_set_widget_params() {
	$this->widget_params = array(
	    'name' => __($this->name, 'js_composer'),
	    'base' => $this->base,
	    'class' => $this->class,
	    'category' => __($this->category, "js_composer"),
	    'description' => __($this->description, 'js_composer'),
	    'front_enqueue_js' => array(plugins_url('/dlbi-sodexo-vc-widget/dist/scripts/widgets.min.js', __FILE__)),
	    'front_enqueue_css' => array(plugins_url('/dlbi-sodexo-vc-widget/dist/styles/widgets.min.css', __FILE__)),
	    'params' => $this->params,
	    'icon' => 'http://fr.sodexo.com/modules/sodexocom-templates/img/sodexo-favicon.ico'
	);
    }

    /**
     * Sets params for element and grid builder widget settings
     */
    function sodexo_set_widget_params() {
	$tabs = $this->items_lists;
	$list = array();

	$list[] = array(
	    'type' => 'vc_link',
	    'heading' => __('All products link', 'js_composer'),
	    'param_name' => 'all-products-link',
	);

	$list[] = array(
	    'type' => 'dropdown',
	    'heading' => __('Type', 'js_composer'),
	    'param_name' => 'type_top-solutions',
	    'group' => 'Top solutions',
	    'value' => array('Simple', 'Simple + List')
	);

	$list[] = array(
	    'type' => 'textfield',
	    'heading' => __('Title', 'js_composer'),
	    'param_name' => 'title_top-solutions',
	    'group' => 'Top solutions',
	);
	
	$list[] = array(
	    'type' => 'checkbox',
	    'heading' => __('Hide top solutions link', 'js_composer'),
	    'param_name' => 'title_top-solutions-link',
	    'group' => 'Top solutions',
	);
	
	$list[] = // params group
		array(
		    'heading' => __('Slides', 'js_composer'),
		    'type' => 'param_group',
		    'value' => '',
		    'param_name' => 'slides_top-solutions',
		    'group' => 'Top solutions',
		    'params' => array(
			array(
			    'type' => 'dropdown',
			    'heading' => __('Best seller', 'js_composer'),
			    'param_name' => 'best-seller_top-solutions',
			    'value' => array('No', 'Yes')
			),
			array(
			    'type' => 'dropdown',
			    'value' => $this->posts_lists,
			    'heading' => 'Choose your post',
			    'param_name' => 'product_top-solutions',
			),
		    )
	);
	if ($tabs) {
	    foreach ($tabs as $tab) {
		if ($tab->menu_item_parent == '0') {
		    $list[] = array(
			'type' => 'dropdown',
			'heading' => __('Type', 'js_composer'),
			'param_name' => 'type_' . $tab->classes[0],
			'group' => $tab->title,
			'value' => array('Simple', 'Simple + List')
		    );
		    $list[] = array(
			'type' => 'textfield',
			'heading' => __('Title', 'js_composer'),
			'param_name' => 'title_' . $tab->classes[0],
			'group' => $tab->title,
		    );

		    $list[] = // params group
			    array(
				'heading' => __('Slides', 'js_composer'),
				'type' => 'param_group',
				'value' => '',
				'param_name' => 'slides_' . $tab->classes[0],
				'group' => $tab->title,
				'params' => array(
				    array(
					'type' => 'dropdown',
					'heading' => __('Best seller', 'js_composer'),
					'param_name' => 'best-seller_' . $tab->classes[0],
					'value' => array('No', 'Yes')
				    ),
				    array(
					'type' => 'dropdown',
					'value' => $this->posts_lists,
					'heading' => 'Choose your post',
					'param_name' => 'product_' . $tab->classes[0],
				    ),
				)
		    );
		}
		$this->params = $list;
	    }
	}
    }

    /**
     * Add widget to grid builder
     *
     * @param array $shortcodes
     * @return type
     */
    public function sodexo_widget_add_grid_shortcodes($shortcodes) {
	$params = $this->widget_params;
	$params['post_type'] = Vc_Grid_Item_Editor::postType();

	$shortcodes['vc_grid_test'] = $params;

	return $shortcodes;
    }

    /**
     * Add to posts, pages ...
     */
    public function sodexo_widget_add_element() {
	// Map the block with vc_map()
	vc_map($this->widget_params);
    }

    /**
     * Grid output function
     *
     * @param type $atts
     */
    public function sodexo_grid_render($atts) {
	SodexoVcwid_Client::sodexo_widget_render($atts, $this->view);
    }

    /**
     * Element output function
     *
     * @param type $atts
     */
    public function sodexo_element_render($atts) {
	SodexoVcwid_Client::sodexo_widget_render($atts, $this->view);
    }

}

new SodexoWidget_CarouselAffiliate();
