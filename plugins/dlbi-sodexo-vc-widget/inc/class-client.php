<?php

class SodexoVcwid_Client{

	function __construct(){
		add_action('plugins_loaded', [$this, 'sodexo_vcwid_get_posts_values']);

		// add_action('wp_enqueue_scripts', array($this, 'sodexo_enqueue_scripts'));
	}

	/**
	 * Return an array of custom post type contains title and ID
	 *
	 * @param $post_type
	 *
	 * @return array|string The list of posts ids or a string error message
	 */
	public static function sodexo_vcwid_get_posts_values($post_type){

		$args       = [
			'posts_per_page' => - 1,
			'orderby'        => 'title',
			'order'          => 'ASC',
			'post_type'      => $post_type,
		];
		$posts      = get_posts($args);
		$posts_list = wp_list_pluck($posts, 'ID', 'post_title');

		//Add the category for products
		if((is_array($post_type) && in_array('product', $post_type)) || $post_type == 'product'){
			$newPostList = [];

			foreach($posts_list as $key => $value){
				global $wpdb;
				$sql = "SELECT $wpdb->terms.name FROM $wpdb->term_relationships
                INNER JOIN $wpdb->term_taxonomy ON($wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id)
                INNER JOIN $wpdb->terms ON($wpdb->term_taxonomy.term_id = $wpdb->terms.term_id)
                WHERE $wpdb->term_relationships.object_id = $value
                AND $wpdb->term_taxonomy.taxonomy = 'category-product'";

				$results = $wpdb->get_results($sql);

				$key2 = $key;
				if(!empty($results) && !empty($results[0]) && !empty($results[0]->name)){
					$key2 .= ' (' . $results[0]->name . ')';
				}

				$newPostList[$key2] = $value;
			}
			$posts_list = $newPostList;
		}

		if($posts_list){
			return $posts_list;
		}else{
			return __('There is a problem !', 'vc_widget');
		}

	}

	/**
	 * Load our textdomain for localization.
	 *
	 * @return void
	 */
	public function textdomain(){
		load_plugin_textdomain('dlbi-sodexo-vc-widget', false, dirname(plugin_basename(__FILE__)) . '/languages/');
	}

	/**
	 *  output function
	 *
	 * @param array  $atts
	 * @param string $path
	 */
	public static function sodexo_widget_render($atts, $path){
		include($path);
	}

	public static function sodexo_enqueue_scripts(){
		// if(!is_admin()) {
		//     wp_enqueue_script('widgets-script', SOD_VCWID_DIR . 'dist/scripts/widgets.min.js');
		//     wp_enqueue_style('widgets-style', SOD_VCWID_DIR . 'dist/styles/widgets.min.css');
		// }
	}

}
