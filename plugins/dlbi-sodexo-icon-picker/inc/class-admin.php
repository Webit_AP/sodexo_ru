<?php

class SodexoIconPicker_Admin {
    public $folders_font = array();
    public $max_file_size = 1048576; //Equals to 1Mo

    public function __construct() {
      $this->checkNewFont();

      add_action('admin_menu', array($this, 'sodexo_iconpicker_scripts_submenu_page'));
      add_action('admin_enqueue_scripts', array($this, 'sodexo_iconpicker_scripts'));
      add_action('wp_enqueue_scripts', array($this, 'sodexo_iconpicker_scripts'));
    }

    function sodexo_iconpicker_scripts_submenu_page() {
      add_submenu_page(
          'tools.php',
          'Font icons',
          'Font icons',
          'customize',
          'font icons',
          array($this, 'font_icons_option_page')
      );
    }

    /**
     * Enqueue scripts and styles for the Web Callback
     */
    public static function sodexo_iconpicker_scripts() {
      //fontIconPicker JS
    	wp_register_script('fonticonpicker-script', plugins_url( '../assets/js/jquery.fonticonpicker.min.js', __FILE__), array(), '', true);
    	wp_register_script('fonticonpicker-script-custom', plugins_url('../assets/js/custom.js', __FILE__), array('fonticonpicker-script'), '', true);
      wp_enqueue_script( 'fonticonpicker-script-custom' );

      $uploads = wp_upload_dir();
      $upload_path = $uploads['baseurl'];
      wp_localize_script( 'fonticonpicker-script-custom', 'starter_global', array(
         'folder_font_default' => plugins_url('../assets/fonts/'.SOD_WEBIP_FONTS_DEFAULT_NAME, __FILE__),
         'folder_font_default_name' => SOD_WEBIP_FONTS_DEFAULT_NAME,
		     'folder_font' => str_replace('/', DIRECTORY_SEPARATOR, $upload_path.'/dlbi-sodexo-icon-picker/fonts/'),
         'list_folders' => json_encode($this->folders_font),
       ));

      //fontIconPicker core CSS
    	wp_register_style('fonticonpicker-style', plugins_url('../assets/css/jquery.fonticonpicker.min.css', __FILE__), array(), '');
    	wp_register_style('fonticonpicker-custom', plugins_url('../assets/css/custom.css', __FILE__), array(), '');
      //required default theme
    	wp_register_style('fonticonpicker-theme', plugins_url('../assets/themes/grey-theme/jquery.fonticonpicker.grey.min.css', __FILE__), array('fonticonpicker-style'), '');
      wp_enqueue_style( array('fonticonpicker-style', 'fonticonpicker-custom', 'fonticonpicker-theme'));

      //Fonts
      // wp_enqueue_style('fonticonpicker-icomoon', plugins_url('../assets/fonts/icomoon/style.css', __FILE__), array(), '');
      $this->loadCustomFonts();

    }

    public static function font_icons_option_page() {
      $result = '';
      if(!empty($_POST))
        $result = $this->validateFormFile();
      elseif(!empty($_GET['type']) && $_GET['type'] == 'remove')
        $this->removeFont(sanitize_text_field($_GET['folder']));
      ?>
      <div class="wrap option-font-icons">
        <h2>Font Icons</h2>

        <div class="current_fonts">
          <p>Fonts availables :</p>
          <ul>
            <?php
            if(file_exists(SOD_WEBIP_FONTS_DEFAULT.DIRECTORY_SEPARATOR.'final-sodexo.min.css')) {
              echo '<li>'.SOD_WEBIP_FONTS_DEFAULT_NAME.'</li>';
            }

            foreach($this->folders_font as $fold) {
              //Only load fonts available in front
              if(file_exists(SOD_WEBIP_FONTS.$fold.DIRECTORY_SEPARATOR.'final-sodexo.min.css')) {
                echo '<li>'.$fold.' <a href="tools.php?page=font+icons&type=remove&folder='.$fold.'" class="dashicons dashicons-trash"></a></li>';
              }
            }
            ?>
          </ul>
        </div>

        <div class="add-font">
          <form method="POST" action="tools.php?page=font+icons" enctype="multipart/form-data">
            <input type="hidden" name="MAX_FILE_SIZE" value="<?=$this->max_file_size;?>" />
            <label for="file-font">Upload a zip file (1Mo max) :</label>
            <input type="file" name="font" id="file-font" />
            <input type="submit" />
          </form>
        </div>

        <br />
        <div class="regenerate-css">
          <form method="POST" action="tools.php?page=font+icons" enctype="multipart/form-data">
            <input type="hidden" name="regenerate_css" value="1" />
            <input type="submit" value="Regenerate CSS Font"/>
          </form>
        </div>

        <?php if(!empty($result)) { echo '<p>'.$result.'</p>'; } ?>
      </div>
      <?php
    }

    public function removeFont($folderName) {
      $folder = realpath(SOD_WEBIP_FONTS.$folderName);
      //If the folder exists and it's under the upload font folder
      if(is_dir($folder) && strstr($folder, SOD_WEBIP_FONTS))
        $this->rrmdir($folder);
    }

    public function validateFormFile() {
      $result = '';
      if(!empty($_FILES['font'])) {
        $valid_extensions = array( 'zip' );

        $upload_extension = strtolower(  substr(  strrchr($_FILES['font']['name'], '.')  ,1)  );

        if ($_FILES['font']['size'] > $this->max_file_size) $erreur = "The file is to big";
        elseif ($_FILES['font']['error'] > 0) $result = "Error during upload";
        elseif ( !in_array($upload_extension,$valid_extensions) ) $result = "Wrong extension";
        else {
          $nameFile = pathinfo ($_FILES['font']['name'], PATHINFO_FILENAME );
          $fileDest = SOD_WEBIP_FONTS.'/'.$nameFile;
          if(is_dir($fileDest)) {
            $result = '<b>The font folder already exists</b>';
          } else{
            $move = unzip_file($_FILES['font']['tmp_name'], $fileDest);
            if ($move) {
              if($this->fontAlreadyExists($fileDest)) {
                  $result = 'A font with the same name/prefix already exists.';
                  $this->rrmdir($fileDest);
              } else {
                $generated = $this->generateCSS($fileDest);
                if($generated['result']) {
                  $result = '<b>Font uploaded. Refresh the page</b>';
                } else {
                  $result = 'The font folder doesn\'t match with the required font.';
                  $this->rrmdir($fileDest);
                }
              }
            } else
              $result = 'Error during the upload';
          }
        }
      } elseif(!empty($_POST['regenerate_css'])) {
        if(file_exists(SOD_WEBIP_FONTS_DEFAULT.DIRECTORY_SEPARATOR.'final-sodexo.min.css')) {
          unlink(SOD_WEBIP_FONTS_DEFAULT.DIRECTORY_SEPARATOR.'final-sodexo.min.css');
        }
        foreach($this->folders_font as $fold) {
          //Only load fonts available in front
          if(file_exists(SOD_WEBIP_FONTS.$fold.DIRECTORY_SEPARATOR.'final-sodexo.min.css')) {
            unlink(SOD_WEBIP_FONTS.$fold.DIRECTORY_SEPARATOR.'final-sodexo.min.css'); //The suppression of this file will force the regeneration
          }
        }

        $this->folders_font = array();
        $this->checkNewFont();
        $result = '<b>CSS Fonts regenerated</b>';
      }

      return $result;
    }

    public function rrmdir($dir) {
      if (is_dir($dir)) {
        $objects = scandir($dir);
        foreach ($objects as $object) {
          if ($object != "." && $object != "..") {
            if (filetype($dir."/".$object) == "dir") $this->rrmdir($dir."/".$object); else unlink($dir."/".$object);
          }
        }
        reset($objects);
        rmdir($dir);
      }
    }

    public function loadCustomFonts() {
      if(file_exists(SOD_WEBIP_FONTS_DEFAULT.DIRECTORY_SEPARATOR.'final-sodexo.min.css')) {
        wp_enqueue_style('fonticonpicker-font-'.SOD_WEBIP_FONTS_DEFAULT_NAME, plugins_url('../assets/fonts/'.SOD_WEBIP_FONTS_DEFAULT_NAME.'/style.css', __FILE__), array(), '');
      }

      $uploads = wp_upload_dir();
      $upload_path = $uploads['baseurl'];
      $folders = array_slice(scandir(SOD_WEBIP_FONTS), 2);
      foreach($folders as $fold) {
        if(file_exists(SOD_WEBIP_FONTS.$fold.DIRECTORY_SEPARATOR.'final-sodexo.min.css')) {
          wp_enqueue_style('fonticonpicker-font-'.$fold, str_replace('/', DIRECTORY_SEPARATOR, $upload_path.'/dlbi-sodexo-icon-picker/fonts/'.$fold.'/style.css'), array(), '');
        }
      }
    }

    public function checkNewFont() {
      if(!file_exists(SOD_WEBIP_FONTS_DEFAULT.DIRECTORY_SEPARATOR.'final-sodexo.min.css')) {
        $this->generateCSS(SOD_WEBIP_FONTS_DEFAULT);
      }

      foreach(array_slice(scandir(SOD_WEBIP_FONTS), 2) as $fold) {
        if(!file_exists(SOD_WEBIP_FONTS.$fold.DIRECTORY_SEPARATOR.'final-sodexo.min.css')) {
          $generated = $this->generateCSS(SOD_WEBIP_FONTS.$fold);
          if($generated['result']) {
            $this->folders_font[] = $fold;
          }
        } else {
          $this->folders_font[] = $fold;
        }
      }

    }

    public function fontAlreadyExists($newFolder) {
      if(!file_exists($newFolder.DIRECTORY_SEPARATOR.'selection.json'))
        return FALSE;

      $stringJson = file_get_contents($newFolder.DIRECTORY_SEPARATOR.'selection.json');

      $prefixTarget = '';
      $fontFamilyTarget = '';
      try {
        $obj = json_decode($stringJson);
        $prefixTarget = $obj->preferences->fontPref->prefix;
        $fontFamilyTarget = $obj->preferences->fontPref->metadata->fontFamily;
      } catch(Exception $e) {
        return FALSE;
      }

      //Check default font
      if(file_exists(SOD_WEBIP_FONTS_DEFAULT.DIRECTORY_SEPARATOR.'selection.json')) {
        $stringJson = file_get_contents(SOD_WEBIP_FONTS_DEFAULT.DIRECTORY_SEPARATOR.'selection.json');

        $prefix = '';
        $fontFamily = '';
        try {
          $obj = json_decode($stringJson);

          if($prefixTarget == $obj->preferences->fontPref->prefix || $fontFamilyTarget == $obj->preferences->fontPref->metadata->fontFamily)
            return TRUE;
        } catch(Exception $e) {
          return FALSE;
        }
      }

      foreach($this->folders_font as $fold) {
        //Only load fonts available in front
        if(file_exists(SOD_WEBIP_FONTS.$fold.DIRECTORY_SEPARATOR.'selection.json')) {
          $stringJson = file_get_contents(SOD_WEBIP_FONTS.$fold.DIRECTORY_SEPARATOR.'selection.json');

          $prefix = '';
          $fontFamily = '';
          try {
            $obj = json_decode($stringJson);

            if($prefixTarget == $obj->preferences->fontPref->prefix || $fontFamilyTarget == $obj->preferences->fontPref->metadata->fontFamily)
              return TRUE;
          } catch(Exception $e) {
            return FALSE;
          }
        }
      }
    }

    public function generateCSS($folder) {
      if(!file_exists($folder.DIRECTORY_SEPARATOR.'selection.json'))
        return array('result' => FALSE, 'prefix' => '');

      $stringJson = file_get_contents($folder.DIRECTORY_SEPARATOR.'selection.json');

      $prefix = '';
      try {
        $obj = json_decode($stringJson);
        $fontFamily = $obj->preferences->fontPref->metadata->fontFamily;
        $prefix = $obj->preferences->fontPref->prefix;
        $classCSS = array();

        foreach($obj->icons as $icon) {
          $data = array('name' => $icon->properties->name,
                        'hexa' => dechex($icon->properties->code));
          $classCSS[] = $data;
        }

        // top-menu--tabs_list top-menu--homepage
        // top-menu-main


        $dataCssMin = "@font-face{font-family:'$fontFamily';src:url(fonts/$fontFamily.eot?-3p8lp4);src:url(fonts/$fontFamily.eot?#iefix-3p8lp4) format('embedded-opentype'),url(fonts/$fontFamily.woff?-3p8lp4) format('woff'),url(fonts/$fontFamily.ttf?-3p8lp4) format('truetype'),url(fonts/$fontFamily.svg?-3p8lp4#$fontFamily) format('svg');font-weight:400;font-style:normal}
        #navbar-combined .navbar-top-menu-bar .menu-top-menu-affiliate-bar-container ul.top-menu-main>li[class*=$prefix]>a::before,#navbar-combined .navbar-top-menu-bar .menu-top-menu-bar-container ul.top-menu-main>li[class*=$prefix]>a::before,.top_menu .top-menu--tabs_list li a[class*=$prefix]::after,#burger-menu li[class*=$prefix]>a::before, .footer-menu-main li[class*=$prefix]>a::before, #menu-top-nav li[class*=$prefix]>a::before {font-family:$fontFamily!important;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}
        #burger-menu li[class*=$prefix]>a::before, #menu-top-nav li[class*=$prefix]>a::before {margin-top:auto;margin-right: 5px;width: auto;height: auto;}";

        $dataCss = $dataCssMin;
        foreach($classCSS as $class) {
          $dataCss.="\n.".$prefix.$class['name'].' > a:before, ul[class*=top-menu-] .'.$prefix.$class['name'].":after {\n\tcontent:'\\".$class['hexa']."';\n}";
          $dataCssMin.= '.'.$prefix.$class['name'].' > a:before, ul[class*=top-menu-] .'.$prefix.$class['name'].':after {content:"\\'.$class['hexa'].'";}';
        }

        file_put_contents($folder.DIRECTORY_SEPARATOR.'final-sodexo.css', $dataCss);
        file_put_contents($folder.DIRECTORY_SEPARATOR.'final-sodexo.min.css', $dataCssMin);

        return array('result' => TRUE, 'prefix' => $prefix);
      } catch(Exception $e) {
        return array('result' => FALSE, 'prefix' => $prefix);
      }
    }

}
