<?php

class SodexoIconPicker_Client {

      public function __construct() {
    	    add_action('wp_enqueue_scripts', array($this, 'sodexo_iconpicker_scripts'));
      }

    /**
     * Enqueue scripts and styles for the Web Callback
     */
     public static function sodexo_iconpicker_scripts() {
        //Fonts
        // wp_enqueue_style('fonticonpicker-icomoon', plugins_url('../assets/fonts/icomoon/test.min.css', __FILE__), array(), '');
        $this->loadCustomFonts();
     }

     public function loadCustomFonts() {
       if(file_exists(SOD_WEBIP_FONTS_DEFAULT.DIRECTORY_SEPARATOR.'final-sodexo.min.css')) {
         wp_enqueue_style('fonticonpicker-font-'.SOD_WEBIP_FONTS_DEFAULT_NAME, plugins_url('../assets/fonts/'.SOD_WEBIP_FONTS_DEFAULT_NAME.'/final-sodexo.min.css', __FILE__), array(), '');
       }

       $uploads = wp_upload_dir();
       $upload_path = $uploads['baseurl'];
       $folders = array_slice(scandir(SOD_WEBIP_FONTS), 2);
       foreach($folders as $fold) {
         // echo SOD_WEBIP_FONTS;
         if(file_exists(SOD_WEBIP_FONTS.$fold.DIRECTORY_SEPARATOR.'final-sodexo.min.css')) {
           // echo $upload_path('../assets/fonts/'.$fold.'/final-sodexo.min.css', __FILE__);
           wp_enqueue_style('fonticonpicker-font-'.$fold, str_replace('/', DIRECTORY_SEPARATOR, $upload_path.'/dlbi-sodexo-icon-picker/fonts/'.$fold.'/final-sodexo.min.css'), array(), '');
         }
       }
     }

}
