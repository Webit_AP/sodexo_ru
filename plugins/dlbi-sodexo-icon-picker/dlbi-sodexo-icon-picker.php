<?php

/*
  Plugin Name:DLBI Sodexo icon Picker
  Description: Plugin for selecting icons for the menu
  Version: 1.0
  Author: Digitas LBI
  Author URI: https://www.digitaslbi.com/
  License: GPLv2 or later
  Text Domain: dlbi-sodexo-icon-picker
 */


// Plugin Consts
define('SOD_WEBIP_VERSION', '1.0');
define('SOD_WEBIP_URL', plugins_url('', __FILE__));
define('SOD_WEBIP_DIR', dirname(__FILE__));

define('SOD_WEBIP_FONTS_DEFAULT_NAME', 'ip-soxo-icons');
define('SOD_WEBIP_FONTS_DEFAULT', dirname(__FILE__).str_replace('/', DIRECTORY_SEPARATOR,'/assets/fonts/'.SOD_WEBIP_FONTS_DEFAULT_NAME.'/'));
define('SOD_WEBIP_FONTS', dirname(__FILE__,3).str_replace('/', DIRECTORY_SEPARATOR, '/uploads/dlbi-sodexo-icon-picker/fonts/'));

//Load class
require( SOD_WEBIP_DIR . '/inc/class-client.php' );
require( SOD_WEBIP_DIR . '/inc/class-admin.php' );

//Init widget our apps
function dlbi_sodexo_icon_picker_init() {
    if(!is_dir(SOD_WEBIP_FONTS))
      mkdir(SOD_WEBIP_FONTS, 0755, TRUE);

    new SodexoIconPicker_Client();

    if(is_admin() && WP_ENV != 'production'){
			new SodexoIconPicker_Admin();
    }
}

add_action('plugins_loaded', 'dlbi_sodexo_icon_picker_init');
