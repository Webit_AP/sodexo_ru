<?php

class SodexoProduct_Admin {

    public function __construct() {
      add_action('restrict_manage_posts',  array($this, 'sodexo_filter_post_type_by_category'));
    }

    public function sodexo_filter_post_type_by_category() {
    	global $typenow;
      $post_type = 'product'; // change to your post type
    	$taxonomy  = 'category-product'; // change to your taxonomy
    	if ($typenow == $post_type) {
    		$selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
    		$info_taxonomy = get_taxonomy($taxonomy);
    		wp_dropdown_categories(array(
    			'show_option_all' => __("Show All {$info_taxonomy->label}"),
    			'taxonomy'        => $taxonomy,
          'hierarchical' => 1,
          'value_field' => 'slug',
    			'name'            => $taxonomy,
    			'orderby'         => 'name',
    			'selected'        => $selected,
    			'show_count'      => true,
    			'hide_empty'      => true,
    		));
    	};
    }
}
