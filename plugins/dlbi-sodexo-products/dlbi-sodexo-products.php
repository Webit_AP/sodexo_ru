<?php

/*
  Plugin Name: Sodexo Products
  Description: Plugin for manage products custom post type
  Version: 0.1.1
  Author: Digitas LBI
  Author URI: https://www.digitaslbi.com/
  License: GPLv2 or later
  Text Domain: dlbi-sodexo-products
 */


// Plugin Consts
define('SOD_PRO_VERSION', '1.0');
define('SOD_PRO_URL', plugins_url('', __FILE__));
define('SOD_PRO_DIR', dirname(__FILE__));
define('SOD_PRO_PTYPE', 'product');

//Load class
require( SOD_PRO_DIR . '/inc/class-client.php' );
require( SOD_PRO_DIR . '/inc/class-admin.php' );

//Init
function sodexo_product_init() {
    new SodexoProduct_Client();
    if(is_admin())
      new SodexoProduct_Admin();

    // Load plugin textdomain
    load_plugin_textdomain( 'dlbi-sodexo-products', false, dirname(plugin_basename(__FILE__)) . '/languages/');
}

add_action('plugins_loaded', 'sodexo_product_init', 11);
