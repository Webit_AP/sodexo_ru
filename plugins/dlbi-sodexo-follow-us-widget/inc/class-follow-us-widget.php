<?php

class SodexoWidget_Follow_Us extends WP_Widget {

    /**
     * construct
     */
    public function __construct() {

        parent::__construct('Follow_Us', 'Follow Us',
            array(
                'classname' => '',
                'description' => "Widget that comes into the footer."
            )
        );
    }

    /**
     * widget
     *
     * @param array $args
     * @param array $instance
     * @return
     */
    public function widget($args, $instance) {

        //echo $args['before_widget'];
        //echo $args['before_title'];
        $widget_id = $args['widget_id'];

        if(get_field('social_buttons_show_block', 'widget_' . $widget_id)):
            if( have_rows('social_buttons', 'widget_' . $widget_id) ): ?>
                <?php if(get_field('social_buttons_title', 'widget_' . $widget_id)):?>
                    <h2 class="widgettitle"><?php echo get_field('social_buttons_title', 'widget_' . $widget_id);?></h2>
                <?php endif;?>
                <?php
                // Display div with id
                while ( have_rows('social_buttons', 'widget_' . $widget_id) ) : the_row();?>
                    <?php

                    if(get_sub_field("social_buttons_select_a_value")):
                        if (get_sub_field("social_buttons_link")): ?>
                            <a class="sociallink" href="<?php echo get_sub_field("social_buttons_link");?>" <?php if(get_sub_field("social_buttons_link_target")):?> target="_blank" <?php endif; ?>>
                              <span class="fa-stack fa-lg">
                                <i class="fa fa-circle fa-stack-2x fa-inverse"></i>
                                <i class="fa fa-<?php echo strtolower(get_sub_field("social_buttons_select_a_value"));?> fa-stack-1x footericon"></i>
                              </span>
                            </a>
                        <?php endif;
                    endif;
                endwhile;?>
            <?php endif;
        endif;

        //echo $args['after_title'];
        //echo $args['after_widget'];
    }

    /**
     * update
     *
     * @param string $new
     * @param string $old
     * @return
     */
    public function update($new, $old) {
        // Save widget
        return $new;
    }

    /**
     * form
     *
     * @param array $instance
     * @return
     */
    public function form($instance) {
        // Form widget
    }
}
