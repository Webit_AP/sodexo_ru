<?php

/*
  Plugin Name: Sodexo Widget Follow US
  Description: Sodexo Widget Follow US
  Version: 1.0
  Author: Digitas LBI
  Author URI: https://www.digitaslbi.com/
  License: GPLv2 or later
  Text Domain: dlbi-sodexo-follow-us-widget
 */


// Plugin Consts
define('SOD_FOL_VERSION', '1.0');
define('SOD_FOL_URL', plugins_url('', __FILE__));
define('SOD_FOL_DIR', dirname(__FILE__));

//Load class
require( SOD_FOL_DIR . '/inc/class-follow-us-widget.php' );

//Init widget our apps
function dlbi_sodexo_follow_us_init() {
    register_widget('SodexoWidget_Follow_Us');

    // Load plugin textdomain
    load_plugin_textdomain( 'dlbi-sodexo-follow-us-widget', false, dirname(plugin_basename(__FILE__)) . '/languages/');
}

add_action('widgets_init', 'dlbi_sodexo_follow_us_init');
