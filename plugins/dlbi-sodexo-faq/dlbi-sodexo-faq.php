<?php

/*
  Plugin Name: Sodexo FAQ
  Description: Plugin for manage FAQ custom post type
  Version: 0.1.1
  Author: Digitas LBI
  Author URI: https://www.digitaslbi.com/
  License: GPLv2 or later
  Text Domain: dlbi-sodexo-faq
 */


// Plugin Consts
define('SOD_FAQ_VERSION', '1.0');
define('SOD_FAQ_URL', plugins_url('', __FILE__));
define('SOD_FAQ_DIR', dirname(__FILE__));
define('SOD_FAQ_PTYPE', 'faq');

//Load class
require( SOD_FAQ_DIR . '/inc/class-client.php' );

//Init
function dlbi_sodexo_faq_init() {
    new SodexoFaq_Client();
    // Load plugin textdomain
    load_plugin_textdomain( 'dlbi-sodexo-faq', false, dirname(plugin_basename(__FILE__)) . '/languages/');
}

add_action('plugins_loaded', 'dlbi_sodexo_faq_init', 11);
