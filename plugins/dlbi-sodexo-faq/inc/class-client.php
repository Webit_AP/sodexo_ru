<?php

class SodexoFaq_Client {

    public function __construct() {
        add_action('init', array($this, 'sodexo_faq_register_cpt'));
        add_action('init', array($this, 'sodexo_faq_register_taxonomies'));
        add_action('pre_get_posts', array($this, 'sodexo_alter_main_query'));
    }

    /**
     * Register the faq post type
     *
     * @return boolean
     * @author Digitas LBI
     */
    public static function sodexo_faq_register_cpt() {

        $slug = get_field('url_faq', 'option') ? get_field('url_faq', 'option') : SOD_FAQ_PTYPE;
        register_post_type(SOD_FAQ_PTYPE, array(
            'labels' => array(
                'name' => __('FAQ', 'dlbi-sodexo-faq'),
                'singular_name' => __('FAQ', 'dlbi-sodexo-faq'),
                'add_new' => __('Add new', 'dlbi-sodexo-faq'),
                'add_new_item' => __('Add new FAQ', 'dlbi-sodexo-faq'),
                'edit_item' => __('Edit FAQ', 'dlbi-sodexo-faq'),
                'new_item' => __('New FAQ', 'dlbi-sodexo-faq'),
                'view_item' => __('Show FAQ', 'dlbi-sodexo-faq'),
                'search_items' => __('Search FAQ', 'dlbi-sodexo-faq'),
                'not_found' => __('No FAQ found', 'dlbi-sodexo-faq'),
                'not_found_in_trash' => __('No FAQ found in trash', 'dlbi-sodexo-faq'),
                'parent_item_colon' => __('Parent FAQ', 'dlbi-sodexo-faq'),
            ),
            'description' => '',
            'publicly_queryable' => true,
            'exclude_from_search' => false,
            'map_meta_cap' => true,
            'capability_type' => 'post',
            'public' => true,
            'hierarchical' => false,
            'rewrite' => array(
                'slug' => $slug,
                'with_front' => true,
                'pages' => true,
                'feeds' => true,
            ),
            'has_archive' => $slug,
            'query_var' => SOD_FAQ_PTYPE,
            'supports' => array(
                0 => 'title',
                1 => 'editor',
                2 => 'thumbnail',
                3 => 'excerpt',
                4 => 'page-attributes'
            ),
            'taxonomies' => array('category', 'post_tag'),
            'show_ui' => true,
            'menu_position' => 25,
            'menu_icon' => 'dashicons-list-view',
            'can_export' => true,
            'show_in_nav_menus' => true,
            'show_in_menu' => true,
        ));

        return true;
    }

    /**
     * Register the faq taxonomies
     *
     * @return boolean
     * @author Digitas LBI
     */
    public static function sodexo_faq_register_taxonomies() {
        // create a new taxonomy
        register_taxonomy(
                __('profile'), SOD_FAQ_PTYPE, array(
            'label' => __('Profile'),
            'rewrite' => array('slug' => __('profile', 'dlbi-sodexo-faq')),
            'capabilities' => array(
                'assign_terms' => 'manage_categories',
                'edit_terms' => 'manage_categories'
            )
                )
        );
        register_taxonomy(
                __('topic'), SOD_FAQ_PTYPE, array(
            'label' => __('Topic'),
            'rewrite' => array('slug' => __('topic', 'dlbi-sodexo-faq')),
            'capabilities' => array(
                'assign_terms' => 'manage_categories',
                'edit_terms' => 'manage_categories'
            )
                )
        );
        register_taxonomy_for_object_type('profile', 'faq');
        register_taxonomy_for_object_type('topic', 'faq');

        return true;
    }

    /**
     * Register the faq taxonomies
     *
     * @param WP_Query $query
     * @return boolean
     * @author Digitas LBI
     */
    public function sodexo_alter_main_query($query) {
        if ($query->is_post_type_archive(SOD_FAQ_PTYPE) && $query->is_main_query() && !is_admin()) {
            $query->set('posts_per_page', 10);
            $profile = get_query_var(__('profile'));
            $topic = get_query_var(__('topic'));
            $taxquery = array(
                array(
                    'taxonomy' => 'profile',
                    'field' => 'slug',
                    'terms' => array($profile),
                    'operator' => 'IN'
                ),
                array(
                    'taxonomy' => 'topic',
                    'field' => 'slug',
                    'terms' => array($topic),
                    'operator' => 'IN'
                )
            );

            $query->set('tax_query', $taxquery);

            return true;
        }
    }

}
