<?php

class SodexoWidget_Visit_Our_Faq extends WP_Widget {

   /**
    * construct
    */
    public function __construct() {

        parent::__construct('Visit_Our_Faq', 'Visit Our FAQ',
            array(
              'classname'   => '',
              'description' => "Widget that comes into the footer."
            )
        );

    }

    /**
     * widget
     *
     * @param array $args
     * @param array $instance
     * @return
     */
    public function widget($args, $instance) {

      $widget_id = $args['widget_id'];

      if(get_field('visit_our_faq_show_block', 'widget_' . $widget_id)):

          // echo $args['before_widget'];
          echo $args['before_title'];

          if(get_field('visit_our_faq_title', 'widget_' . $widget_id)):
              $post = get_field('visit_our_faq_link', 'widget_' . $widget_id);
              ?>
          <div class="lmt-panel">
             <a href="<?php echo get_permalink($post->ID) ?>" class="btn-sodexo btn-sodexo-footer btn-sodexo-callusfooter"><?php echo get_field('visit_our_faq_title', 'widget_' . $widget_id);?></a>
          </div>
          <?php endif;

          echo $args['after_title'];
          // echo $args['after_widget'];

      endif;
    }

    /**
     * update
     *
     * @param string $new
     * @param string $old
     * @return
     */
    public function update($new, $old) {
        // Save widget
        return $new;
    }

    /**
     * form
     *
     * @param array $instance
     * @return
     */
    public function form($instance) {
        // Form widget
    }
}
