<?php

class SodexoWidget_Our_Apps extends WP_Widget {

    /**
     * construct
     */
    public function __construct() {

        parent::__construct('Our_Apps', 'Our Apps',
            array(
                'classname' => '',
                'description' => "Widget that comes into the footer."
            )
        );
    }

    /**
     * widget
     *
     * @param array $args
     * @param array $instance
     * @return
     */
    public function widget($args, $instance) {

        // echo $args['before_widget'];
        // echo $args['before_title'];
        $widget_id = $args['widget_id'];

        if (get_field('our_apps_show_block', 'widget_' . $widget_id)): ?>

            <div class="row">

                <div class="col">

                    <?php if( have_rows('our_ios_apps', 'widget_' . $widget_id) ): ?>
                        <?php if(get_field('our_ios_apps_title', 'widget_' . $widget_id)):?>
                            <p class="mt-3"><?php echo get_field('our_ios_apps_title', 'widget_' . $widget_id);?></p>
                        <?php endif;?>
                        <div class="img-apps">
                            <?php
                            while ( have_rows('our_ios_apps', 'widget_' . $widget_id) ) : the_row();
                                ?>
                                <?php if(get_sub_field("our_ios_apps_link")): ?>
                                    <?php $pictureios =  get_sub_field("our_ios_apps_image"); ?>
                                    <a href="<?php echo get_sub_field("our_ios_apps_link");?>" onclick="dataLayer.push({'event': 'click-apps-download','name':'<?php echo addslashes($pictureios['title']); ?>','os':'iOS' });" <?php if(get_sub_field("our_ios_apps_link_target")):?> target="_blank" <?php endif; ?> >
                                        <?php if (!empty($pictureios)) :?>
	                                        <?php echo apply_filters( "dlbi_image", $pictureios['sizes']['thumbnail'] ); ?>
                                        <?php endif;?>
                                    </a>
                                <?php endif;
                            endwhile;?>
                        </div>
                    <?php endif; ?>

                </div>

                <div class="col">

                    <?php if( have_rows('our_android_apps', 'widget_' . $widget_id) ): ?>
                        <?php if(get_field('our_android_apps_title', 'widget_' . $widget_id)):?>
                            <p class="mt-3"><?php echo get_field('our_android_apps_title', 'widget_' . $widget_id);?></p>
                        <?php endif;?>
                        <div class="img-apps">
                            <?php
                            while ( have_rows('our_android_apps', 'widget_' . $widget_id) ) : the_row();
                                if (get_sub_field("our_android_apps_link")):
                                   $pictureandroid =  get_sub_field("our_android_apps_image"); ?>
                                   <a href="<?php echo get_sub_field("our_android_apps_link");?>" onclick="dataLayer.push({'event': 'click-apps-download','name':'<?php echo addslashes($pictureandroid['title']); ?>','os':'Android' });" <?php if(get_sub_field("our_android_apps_link_target")):?> target="_blank" <?php endif; ?>>
                                      <?php if (!empty($pictureandroid)) :?>
	                                      <?php echo apply_filters( "dlbi_image", $pictureandroid['sizes']['thumbnail'] ); ?>
                                      <?php endif;?>
                                    </a>
                                <?php
                                endif;
                            endwhile;?>
                        </div>
                    <?php endif; ?>

                </div>

            </div>

        <?php endif;
        // echo $args['after_title'];
        // echo $args['after_widget'];
    }

    /**
     * update
     *
     * @param string $new
     * @param string $old
     * @return
     */
    public function update($new, $old) {
        // Save widget
        return $new;
    }

    /**
     * form
     *
     * @param array $instance
     * @return
     */
    public function form($instance) {
        // Form widget
    }
}
