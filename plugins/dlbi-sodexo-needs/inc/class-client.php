<?php

class SodexoNeeds_Client {

    public function __construct() {
        add_action('init', array($this, 'sodexo_need_register_cpt'));
        add_filter('acf/settings/save_json', array($this, 'sodexo_acf_json_save_point'));
        add_filter('acf/settings/load_json', array($this, 'sodexo_acf_json_load_point'));
    }

    public function sodexo_acf_json_save_point($path) {
        // update path
        $path = SOD_NEED_DIR . '/acf-json';
        // return
        return $path;
    }

    public function sodexo_acf_json_load_point($paths) {
        // remove original path (optional)
        unset($paths[0]);

        // append path
        $paths[] = SOD_NEED_DIR . '/acf-json';

        // return
        return $paths;
    }

    /**
     * Register the need post type
     *
     * @return boolean
     * @author Digitas LBI
     */
    public static function sodexo_need_register_cpt() {

        $slug_needs = get_field('url_needs', 'option') ? get_field('url_needs', 'option') : SOD_NEED_PTYPE;

        register_post_type(
                SOD_NEED_PTYPE, array(
            'labels' => array(
                'name' => __('Needs', 'dlbi-sodexo-needs'),
                'singular_name' => __('Need', 'dlbi-sodexo-needs'),
                'add_new' => __('Add new', 'dlbi-sodexo-needs'),
                'add_new_item' => __('Add new Need', 'dlbi-sodexo-needs'),
                'edit_item' => __('Edit Need', 'dlbi-sodexo-needs'),
                'new_item' => __('New Need', 'dlbi-sodexo-needs'),
                'view_item' => __('Show Need', 'dlbi-sodexo-needs'),
                'search_items' => __('Search Needs', 'dlbi-sodexo-needs'),
                'not_found' => __('No Needs found', 'dlbi-sodexo-needs'),
                'not_found_in_trash' => __('No Needs found in trash', 'dlbi-sodexo-needs'),
                'parent_item_colon' => __('Parent Need', 'dlbi-sodexo-needs'),
            ),
            'description' => '',
            'publicly_queryable' => true,
            'exclude_from_search' => false,
            'map_meta_cap' => true,
            'capability_type' => 'post',
            'public' => true,
            'hierarchical' => false,
            'rewrite' => array(
                'slug' => $slug_needs,
                'with_front' => true,
                'pages' => true,
                'feeds' => true,
            ),
            'has_archive' => false,
            'query_var' => SOD_NEED_PTYPE,
            'supports' => array(
                0 => 'title',
                1 => 'editor',
                2 => 'thumbnail',
                3 => 'excerpt',
                4 => 'page-attributes'
            ),
            'show_ui' => true,
            'menu_position' => 25,
            'menu_icon' => 'dashicons-admin-post',
            'can_export' => true,
            'show_in_nav_menus' => true,
            'show_in_menu' => true,
                )
        );

        return true;
    }
}
