<?php

/*
  Plugin Name: Sodexo Needs
  Description: Plugin for manage Needs custom post type
  Version: 0.1.1
  Author: Digitas LBI
  Author URI: https://www.digitaslbi.com/
  License: GPLv2 or later
  Text Domain: dlbi-sodexo-needs
 */

// Plugin Consts
define('SOD_NEED_VERSION', '1.0');
define('SOD_NEED_URL', plugins_url('', __FILE__));
define('SOD_NEED_DIR', dirname(__FILE__));
define('SOD_NEED_PTYPE', 'need');

//Load class
require( SOD_NEED_DIR . '/inc/class-client.php' );

//Init
function sodexo_need_init() {
    new SodexoNeeds_Client();

    // Load plugin textdomain
    load_plugin_textdomain( 'dlbi-sodexo-needs', false, dirname(plugin_basename(__FILE__)) . '/languages/');
}

add_action('plugins_loaded', 'sodexo_need_init', 11);
