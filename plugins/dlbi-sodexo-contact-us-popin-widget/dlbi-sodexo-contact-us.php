<?php

/*
  Plugin Name: Widget Contact US Popin
  Description: Widget Contact US Popin
  Version: 1.0
  Author: Digitas LBI
  Author URI: https://www.digitaslbi.com/
  License: GPLv2 or later
  Text Domain: dlbi-sodexo-contact-us-popin-widget
 */


// Plugin Consts
define('SOD_CON_VERSION', '1.0');
define('SOD_CON_URL', plugins_url('', __FILE__));
define('SOD_CON_DIR', dirname(__FILE__));

//Load class
require( SOD_CON_DIR . '/inc/class-contact-us-widget.php' );

//Init widget newletter
function dlbi_sodexo_contact_us_init() {
    register_widget('SodexoWidget_Contact_Us');

    // Load plugin textdomain
    load_plugin_textdomain( 'dlbi-sodexo-contact-us-popin-widget', false, dirname(plugin_basename(__FILE__)) . '/languages/');
}

add_action('widgets_init', 'dlbi_sodexo_contact_us_init');
