<?php

class SodexoWidget_Contact_Us extends WP_Widget {

	/**
	* construct
	*/
	public function __construct() {

		parent::__construct(
			'contact_us_popin',
			'Contact Us Popin-in',
			array(
				'classname'   => '',
				'description' => 'Contact Us Pop-in.',
			)
		);
	}

	/**
	* widget
	*
	* @param array $args
	* @param array $instance
	*
	* @return
	*/
	public function widget($args, $instance){

		// echo $args['before_widget'];
		// echo $args['before_title'];

		$widget_id = $args['widget_id'];


		if(get_field('contact_popin_show_block', 'widget_' . $widget_id)):
			?>

			<div id="contactPopinFooter">

				<h2 class="widgettitle"><?php echo get_field('contact_popin_title', 'widget_' . $widget_id); ?></h2>

				<div class="lmt-panel">

					<button class="btn-sodexo btn-sodexo-footer" data-toggle="collapse" data-target="#collapseCallusfooter" aria-controls="collapseCallusfooter" aria-expanded="false">
						<i class="icon-phone"></i> <?php echo get_field('contact_popin_call_us_btn_title', 'widget_' . $widget_id); ?>
					</button>

					<div class="collapse" id="collapseCallusfooter">

						<div id="lmt-layer-footer" class="lmt-layer lmt-layer-contact layer-footer">

							<button type="button" class="lmt-close" data-toggle="collapse" data-target="#collapseCallusfooter" aria-controls="collapseCallusfooter" aria-expanded="false">
								<span class="sr-only"><?php echo __('Close overlay', 'dlbi-sodexo-contact-us-popin-widget') ?></span>
							</button>
							<form action="" method="POST">
								<?php 
								$call_back_repeater_array = get_field('call_back_repeater','option');
								$contact_style = "";
								if(count($call_back_repeater_array)<=1):
									$contact_style = "-white";
								endif;	
								?>
								<div class="header<?php echo $contact_style ?>">
									<p class="tt<?php echo $contact_style ?>">
										<label for="contact-input"><?php echo __('Contact us', 'dlbi-sodexo-contact-us-popin-widget') ?></label>
										<?php 
										if(count($call_back_repeater_array)>1):
										?>
											<select class="custom-select">
												<?php
													// One builds the options of the select which display the div
													foreach($call_back_repeater_array as $row):
														if($row['call_back_select_a_value']):
												?>
															<option value="<?php echo rawurldecode( sanitize_title( $row['call_back_select_a_value'] ) ) ?>-footer"><?php echo $row['call_back_select_a_value']; ?></option>
												<?php
														endif;
													endforeach;
												?>
											</select>
										<?php 
										else:
											$row = get_field('call_back_repeater', 'option')[0];
										?>
											<div id="<?php echo rawurldecode( sanitize_title( $row['call_back_select_a_value'] ) ) ?>-footer" class="lmt-contact-info">
												<article>
													<header>
														<?php
														// is picture editable?
														// if ($row['call_back_icon_1']):
														// $picture = $row['call_back_icon_1'];
														// if (!empty($picture))
														?>
														<div class="lmt-contact-info-icon">
															<?php if($row['call_back_icon_1']){
																$picture = $row['call_back_icon_1'];
																echo apply_filters("dlbi_image", $picture);
															}else{ ?>
																<span class="assistance-icon">
																	<svg viewBox="85.5 13.5 45.6 39.9">
																		<path d="M128.3,27.2l-3.7,0l-0.7,0.1l-0.1-0.7c-1.3-7.6-7.9-13.1-15.6-13.1c-7.7,0-14.2,5.5-15.6,13.1l-0.1,0.7
																		l-0.8-0.1l-3.6,0c-1.5,0-2.7,1.2-2.7,2.7v7c0,1.5,1.2,2.7,2.7,2.7l4.3,0l0,1.2c0,4.8,3.8,8.8,8.6,9.1l0.7,0v0.7
																		c0,1.5,1.2,2.7,2.7,2.7h7c1.5,0,2.7-1.2,2.7-2.7v-3.5c0-1.5-1.2-2.7-2.7-2.7h-7c-1.5,0-2.7,1.2-2.7,2.7v0.8l-0.8-0.1
																		c-3.7-0.4-6.5-3.4-6.5-7.2V29.3c0-7.6,6.2-13.8,13.8-13.8c7.6,0,13.8,6.2,13.8,13.8l0,7.6c0,1.5,1.2,2.7,2.7,2.7h3.5
																		c1.5,0,2.7-1.2,2.7-2.7v-7C131,28.4,129.8,27.2,128.3,27.2z M92.4,37.7h-5v-8.5h5V37.7z M103.7,46.5l0.8,0h7.8v5h-8.5V46.5z
																		M129.1,37.7h-5v-8.5h5V37.7z M110.1,37c-0.4,0.6-1,1-1.7,1.1c-0.8,0.1-1.6-0.3-2.1-1c-0.3-0.5-1-0.7-1.5-0.3
																		c-0.5,0.3-0.7,1-0.3,1.5c0.8,1.3,2.2,2.1,3.7,2.1c0.1,0,0.3,0,0.4,0c1.4-0.1,2.7-0.9,3.5-2.2c0.3-0.5,0.1-1.2-0.4-1.5
																		C111.1,36.3,110.4,36.4,110.1,37z"/>
																	</svg>
																</span>
															<?php } ?>
														</div>

														<?php if($row['call_back_title']): ?>
															<h3 class="tt"><?php echo $row['call_back_title']; ?></h3>
														<?php endif; ?>
													</header>

													<?php if($row['call_back_text']): ?>
														<?php echo $row['call_back_text']; ?>
													<?php endif; ?>

													<?php if($row['call_back_telephone']): ?>
														<a href="tel:<?php echo str_replace([" ", "-", "_"], '', $row['call_back_telephone']); ?>"><?php echo $row['call_back_telephone']; ?></a>
													<?php endif; ?>

													<?php if($row['call_back_link']): ?>
														<p class="lmt-contact-email">

															<?php if($row['call_back_icon_2']){

																$picture = $row['call_back_icon_2']; ?>

																<a href="mailto:<?php echo $row['call_back_link']; ?>">
																	<?php echo apply_filters("dlbi_image", $picture); ?><br>
																	<?php if($row['call_back_link_label']){
																		echo $row['call_back_link_label'];
																	}else{
																		echo __('Send us an email', 'dlbi-sodexo-contact-us-popin-widget');
																	} ?>
																</a>

															<?php }else{ ?>

																<a href="mailto:<?php echo $row['call_back_link']; ?>">
																	<i class="icon-envelop"></i>
																	<br>
																	<?php if($row['call_back_link_label']){
																		echo $row['call_back_link_label'];
																	}else{
																		echo __('Send us an email', 'dlbi-sodexo-contact-us-popin-widget');
																	} ?>
																</a>

															<?php } ?>

														</p>
													<?php endif; ?>

												</article>
											</div>									
										<?php
										endif; 
										?>
									</p>
								</div>
							</form>

							<?php
							if(get_field('call_back_repeater', 'option')):
								foreach(get_field('call_back_repeater', 'option') as $key => $row):
									if($key === 0){
										continue;
									}
									?>

									<div id="<?php echo rawurldecode( sanitize_title( $row['call_back_select_a_value'] ) ) ?>-footer" class="lmt-contact-info">
										<article>
											<header>
												<?php
												// is picture editable?
												// if ($row['call_back_icon_1']):
												// $picture = $row['call_back_icon_1'];
												// if (!empty($picture))
												?>
												<div class="lmt-contact-info-icon">
													<?php if($row['call_back_icon_1']){
														$picture = $row['call_back_icon_1'];
														echo apply_filters("dlbi_image", $picture);
													}else{ ?>
														<span class="assistance-icon">
															<svg viewBox="85.5 13.5 45.6 39.9">
																<path d="M128.3,27.2l-3.7,0l-0.7,0.1l-0.1-0.7c-1.3-7.6-7.9-13.1-15.6-13.1c-7.7,0-14.2,5.5-15.6,13.1l-0.1,0.7
																l-0.8-0.1l-3.6,0c-1.5,0-2.7,1.2-2.7,2.7v7c0,1.5,1.2,2.7,2.7,2.7l4.3,0l0,1.2c0,4.8,3.8,8.8,8.6,9.1l0.7,0v0.7
																c0,1.5,1.2,2.7,2.7,2.7h7c1.5,0,2.7-1.2,2.7-2.7v-3.5c0-1.5-1.2-2.7-2.7-2.7h-7c-1.5,0-2.7,1.2-2.7,2.7v0.8l-0.8-0.1
																c-3.7-0.4-6.5-3.4-6.5-7.2V29.3c0-7.6,6.2-13.8,13.8-13.8c7.6,0,13.8,6.2,13.8,13.8l0,7.6c0,1.5,1.2,2.7,2.7,2.7h3.5
																c1.5,0,2.7-1.2,2.7-2.7v-7C131,28.4,129.8,27.2,128.3,27.2z M92.4,37.7h-5v-8.5h5V37.7z M103.7,46.5l0.8,0h7.8v5h-8.5V46.5z
																M129.1,37.7h-5v-8.5h5V37.7z M110.1,37c-0.4,0.6-1,1-1.7,1.1c-0.8,0.1-1.6-0.3-2.1-1c-0.3-0.5-1-0.7-1.5-0.3
																c-0.5,0.3-0.7,1-0.3,1.5c0.8,1.3,2.2,2.1,3.7,2.1c0.1,0,0.3,0,0.4,0c1.4-0.1,2.7-0.9,3.5-2.2c0.3-0.5,0.1-1.2-0.4-1.5
																C111.1,36.3,110.4,36.4,110.1,37z"/>
															</svg>
														</span>
													<?php } ?>
												</div>

												<?php if($row['call_back_title']): ?>
													<h3 class="tt"><?php echo $row['call_back_title']; ?></h3>
												<?php endif; ?>
											</header>

											<?php if($row['call_back_text']): ?>
												<?php echo $row['call_back_text']; ?>
											<?php endif; ?>

											<?php if($row['call_back_telephone']): ?>
												<a href="tel:<?php echo str_replace([" ", "-", "_"], '', $row['call_back_telephone']); ?>"><?php echo $row['call_back_telephone']; ?></a>
											<?php endif; ?>

											<?php if($row['call_back_link']): ?>
												<p class="lmt-contact-email">

													<?php if($row['call_back_icon_2']){

														$picture = $row['call_back_icon_2']; ?>

														<a href="mailto:<?php echo $row['call_back_link']; ?>">
															<?php echo apply_filters("dlbi_image", $picture); ?><br>
															<?php if($row['call_back_link_label']){
																echo $row['call_back_link_label'];
															}else{
																echo __('Send us an email', 'dlbi-sodexo-contact-us-popin-widget');
															} ?>
														</a>

													<?php }else{ ?>

														<a href="mailto:<?php echo $row['call_back_link']; ?>">
															<i class="icon-envelop"></i>
															<br>
															<?php if($row['call_back_link_label']){
																echo $row['call_back_link_label'];
															}else{
																echo __('Send us an email', 'dlbi-sodexo-contact-us-popin-widget');
															} ?>
														</a>

													<?php } ?>

												</p>
											<?php endif; ?>

										</article>
									</div>
								<?php endforeach; ?>
							<?php endif; ?>
						</div>

					</div>

				</div>

				<div class="clearfix"></div>

				<div class="lmt-panel">
					<?php
					$title = get_field('send_us_btn_send_title', 'widget_' . $widget_id);
					$href  = get_field('contact_popin_typeform_url', 'widget_' . $widget_id);
					switch(get_field('send_us_btn_type', 'widget_' . $widget_id)){
						case "modal":
						default:
						{
							?>
							<!-- contact by mail -->
							<button class="btn-sodexo btn-sodexo-footer" data-toggle="modal" data-target="#typeformModal" data-typeform="<?php echo $href ?>" aria-expanded="false">
								<i class="icon-envelop"></i> <?php echo $title ?>
							</button>
							<?php
							break;
						}
						//												default:
						case "link":
						{
							$attr = '';
							$tab  = get_field('send_us_btn_new_tab', 'widget_' . $widget_id);
							if($tab){
								$attr .= " target='_blank'";
							}
							?>
							<!-- contact by mail -->
							<a class="btn-sodexo btn-sodexo-footer" href="<?php echo $href ?>" <?php echo $attr; ?>>
								<i class="icon-envelop"></i> <?php echo $title ?>
							</a>
							<?php
							break;
						}
					}

					?>


					<?php ?>
					<div class="collapse" id="collapseMessagefooter" data-iframe="<?php echo get_field('contact_popin_typeform_url', 'widget_' . $widget_id) ?>">

						<div class="lmt-layer lmt-layer-quote layer-footer form-layer">

							<button type="button" class="lmt-close" data-toggle="modal" data-target="#typeformModal" aria-expanded="false">
								<span class="sr-only"><?php echo __('Close overlay', 'dlbi-sodexo-contact-us-popin-widget') ?></span>
							</button>
							<div class="header">
								<p class="tt"><?php echo __('Send a message', 'dlbi-sodexo-contact-us-popin-widget') ?></p>
							</div>
							<div class="iframe-container">
							</div>
						</div>

					</div>

				</div>

			</div>

		<?php endif;

	}

	/**
	* update
	*
	* @param string $new
	* @param string $old
	*
	* @return
	*/
	public function update($new, $old){
		// Save widget
		return $new;
	}

	/**
	* form
	*
	* @param array $instance
	*
	* @return
	*/
	public function form($instance){
		// Form widget
	}

}
