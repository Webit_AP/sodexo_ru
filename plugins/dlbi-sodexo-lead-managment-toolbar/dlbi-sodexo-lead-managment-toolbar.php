<?php

/*
  Plugin Name: Sodexo Lead managment toolbar
  Description: Plugin for manage Lead managment toolbar custom post type
  Version: 0.1.1
  Author: Digitas LBI
  Author URI: https://www.digitaslbi.com/
  License: GPLv2 or later
  Text Domain: dlbi-sodexo-lead-managment-toolbar
 */

// Plugin Consts
define('SOD_LMT_VERSION', '1.0');
define('SOD_LMT_URL', plugins_url('', __FILE__));
define('SOD_LMT_DIR', dirname(__FILE__));
define('SOD_LMT_PTYPE', 'lmt');

//Load class
require( SOD_LMT_DIR . '/inc/class-client.php' );

//Init
function sodexo_lmt_init() {
    new SodexoLmt_Client();

    // Load plugin textdomain
    load_plugin_textdomain( 'dlbi-sodexo-lmts', false, dirname(plugin_basename(__FILE__)) . '/languages/');
}

add_action('plugins_loaded', 'sodexo_lmt_init', 11);
