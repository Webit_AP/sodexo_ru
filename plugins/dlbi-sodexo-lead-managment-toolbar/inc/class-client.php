<?php

class SodexoLmt_Client {

    public function __construct() {
	add_action('init', array($this, 'sodexo_lmt_register_cpt'));
	add_action('pre_get_posts', array($this, 'sodexo_alter_main_query'));
	add_filter('acf/settings/save_json', array($this, 'sodexo_lmts_acf_json_save_point'));
    add_filter('acf/settings/load_json', array($this, 'sodexo_lmts_acf_json_load_point'));
    }

    /**
     * Register the lmt post type
     *
     * @return boolean
     * @author Digitas LBI
     */
    public static function sodexo_lmt_register_cpt() {
	register_post_type(SOD_LMT_PTYPE, array(
	    'labels' => array(
		'name' => __('Lead managment tool', 'dlbi-sodexo-lmts'),
		'singular_name' => __('Lead managment tool', 'dlbi-sodexo-lmts'),
		'add_new' => __('Add new', 'dlbi-sodexo-lmts'),
		'add_new_item' => __('Add new Lead managment tool', 'dlbi-sodexo-lmts'),
		'edit_item' => __('Edit Lead managment tool', 'dlbi-sodexo-lmts'),
		'new_item' => __('New Lead managment tool', 'dlbi-sodexo-lmts'),
		'view_item' => __('Show Lead managment tool', 'dlbi-sodexo-lmts'),
		'search_items' => __('Search Lmt', 'dlbi-sodexo-lmts'),
		'not_found' => __('No Lmt found', 'dlbi-sodexo-lmts'),
		'not_found_in_trash' => __('No Lmt found in trash', 'dlbi-sodexo-lmts'),
		'parent_item_colon' => __('Parent Lead managment tool', 'dlbi-sodexo-lmts'),
	    ),
	    'description' => '',
	    'publicly_queryable' => false,
	    'exclude_from_search' => true,
	    'map_meta_cap' => true,
	    'capability_type' => 'post',
	    'public' => false,
	    'hierarchical' => false,
	    'has_archive' => false,
	    'query_var' => SOD_LMT_PTYPE,
	    'supports' => array(
		0 => 'title',
		1 => 'editor',
		2 => 'thumbnail',
		3 => 'excerpt',
	    ),
	    'show_ui' => true,
	    'menu_position' => 35,
	    'menu_icon' => 'dashicons-exerpt-view',
	    'can_export' => true,
	    'show_in_nav_menus' => true,
	    'show_in_menu' => true,
	));

	return true;
    }

    /**
     * Register the faq taxonomies
     *
     * @param WP_Query $query
     * @return boolean
     * @author Digitas LBI
     */
    public function sodexo_alter_main_query($query) {
	if ($query->is_post_type_archive(SOD_TES_PTYPE) && $query->is_main_query() && !is_admin()) {
	    $query->set('posts_per_page', 16);
	    
	}
    }
    public function sodexo_lmts_acf_json_save_point($path) {
    	 
        // update path
        $path = SOD_TES_DIR . '/acf-json';

        // return
        return $path;
    }

    public function sodexo_lmts_acf_json_load_point($paths) {
        // remove original path (optional)
        unset($paths[0]);

        // append path
        $paths[] = SOD_TES_DIR . '/acf-json';

        // return
        return $paths;
    }

}
