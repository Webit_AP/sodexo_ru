const NODE_ENV = process.env.NODE_ENV || 'development'
const COUNTRY = process.argv[2] || 'france'
const PRODUCT = process.argv[3] || COUNTRY

module.exports = {
  /** The environment to use when building the project */
  env: NODE_ENV,
  /** The country to select when launching or building the project */
  country: COUNTRY,
  /** The product, if exists, to select when launching or building the project */
  product: PRODUCT,
  /** The full path to the project's root directory */
  basePath: __dirname,
  /** The name of the directory containing the application source code */
  srcDir: 'src',
  /** The file name of the application's entry point */
  main: 'main',
  /** The name of the directory in which to emit compiled assets */
  outDir: `./../plugins/dlbi-sodexo-${COUNTRY}-simulator/app-${PRODUCT}/dist`,
  /** The base path for all projects assets (relative to the website root) */
  publicPath: '/',
  /** Whether to generate sourcemaps */
  sourcemaps: true,
  /** A hash map of keys that the compiler should treat as external to the project */
  externals: {},
  /** A hash map of variables and their values to expose globally */
  globals: {},
  /** Whether to enable verbose logging */
  verbose: false,
  /** The list of modules to bundle separately from the core application code */
  vendors: [
    'react',
    'react-dom',
  ],
}
