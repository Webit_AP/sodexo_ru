import React from 'react'
// import PropTypes from 'prop-types'
import update from 'immutability-helper'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'

// import Toggle from './../../components/Toggle/Toggle'
import CounterWithIcons from './../../components/CounterWithIcons/CounterWithIcons'
import SliderTooltip from './../../components/SliderTooltip/SliderTooltip'
import Doughnut from './../../components/Doughnut/Doughnut'
// import DoughnutSummary from './../../components/Doughnut/DoughnutSummary'

import money1 from './../../images/money-1.svg'
import money2 from './../../images/money-2.svg'
import money3 from './../../images/money-3.svg'

import people1 from './../../images/people-1.svg'
import people2 from './../../images/people-2.svg'
import people3 from './../../images/people-3.svg'

import FoodChildcareTransportSimulator from './../../utils/spainProducts.js'
import axios from 'axios'

// import classNames from 'classnames'

import './transport.scss'

class SpainTransportApp extends React.Component {
  constructor (props) {
    // Pass props to parent class
    super(props)
    // Set initial state
    this.state = {
      // Data inputs
      salario: 25000,
      regular_contribution: 50,
      numero_emplea_medio: 10,

      // Other data inputs
      max_regular_contribution: 136.36,
      factor_for_year: 11,
      toggle_results: false,
      loading: true,
      showTypeform: false,

      // Results
      dataOutput : {
        retencion_IRPF_medio: 0,
        retencion_IRPF_ticket: 0,
        taxes_saved: 0,
        company_spent: 0,
        employees_save: 0,
      }
    }

    this.toggleResultsChange = this.toggleResultsChange.bind(this)
    this.nbEmployeeChange = this.nbEmployeeChange.bind(this)
    this.voucherChange = this.voucherChange.bind(this)
    this.salaireBrutChange = this.salaireBrutChange.bind(this)
    this.handleClickShowMore = this.handleClickShowMore.bind(this)

    this.onClickTypeform = this.onClickTypeform.bind(this)
    this.onClickSendEmail = this.onClickSendEmail.bind(this)
    this.onClickDownloadPdf = this.onClickDownloadPdf.bind(this)
    this.closeTypeform = this.closeTypeform.bind(this)
    this.createIframe = this.createIframe.bind(this)

    window.dataLayer = window.dataLayer || []

    this._getDataParams()
  }

  /* UTILS */

  _getDataParams () {
    let url = document.location.origin + '/wp-json/acf/v2/options/'
    // let url = document.location.origin + '/front-wp-dev/wp-json/acf/v2/options/'
    if (__DEV__) {
      url = 'http://10.224.86.238:3000/wp-json/acf/v2/options/'
      // url = 'http://10.0.75.1:3000/front-wp-dev/wp-json/acf/v2/options/'
    }

    return axios({
      method:'get',
      url: url,
    })
    .then((response) => {
      if (response.status === 200) {
        return this.setState({ dataParams: response.data.acf }, () => {
          this.simulator = new FoodChildcareTransportSimulator(
            this.state.salario,
            this.state.regular_contribution,
            this.state.numero_emplea_medio,
            this.state.max_regular_contribution,
            this.state.factor_for_year,
            this.state.dataParams
          )

          const newDataOutput = update(this.state.dataOutput, { $merge: {
            retencion_IRPF_medio: this.simulator.dataOutput.retencion_IRPF_medio,
            retencion_IRPF_ticket: this.simulator.dataOutput.retencion_IRPF_ticket,
            taxes_saved: this.simulator.dataOutput.taxes_saved,
            company_spent: this.simulator.dataOutput.company_spent,
            employees_save: this.simulator.dataOutput.employees_save,
          } })

          this.setState({
            typeform: this.state.dataParams['get_a_quote_transport'],
            ctaSendPerEmailLabel:  this.state.dataParams['pdf_modal_title'],
            ctaBeContactedLabel:  this.state.dataParams['simulator_be_contacted_label'],
            ctaDownloadLabel:  this.state.dataParams['simulator_download_label'],
            ctaDisclaimer:  this.state.dataParams['simulator_legend'],
            ctaPLaceanOrderLabel:  this.state.dataParams['simulator_place_an_order_label'],
            ctaPLaceanOrderLink:  this.state.dataParams['simulator_place_an_order_link'],
	    maxTransportPerYear:  this.state.dataParams['max_transport_per_year'] / 12,
            dataOutput: newDataOutput,
            loading: false
          })
        })
      }
    })
  }

  _updateSimultorState (value) {
    window.dataLayer.push({ 'event':'click-simulator' })
    this.setState(value, () => {
      this.simulator = new FoodChildcareTransportSimulator(
        this.state.salario,
        this.state.regular_contribution,
        this.state.numero_emplea_medio,
        this.state.max_regular_contribution,
        this.state.factor_for_year,
        this.state.dataParams
      )

      const newDataOutput = update(this.state.dataOutput, { $merge: {
        retencion_IRPF_medio: this.simulator.dataOutput.retencion_IRPF_medio,
        retencion_IRPF_ticket: this.simulator.dataOutput.retencion_IRPF_ticket,
        taxes_saved: this.simulator.dataOutput.taxes_saved,
        company_spent: this.simulator.dataOutput.company_spent,
        employees_save: this.simulator.dataOutput.employees_save,
      } })

      this.setState({ dataOutput: newDataOutput }, () => {
        if (this.state.toggle_results) {
          const newDataOutput = update(this.state.dataOutput, { $merge: {
            retencion_IRPF_medio: this.state.dataOutput.retencion_IRPF_medio / 12,
            retencion_IRPF_ticket: this.state.dataOutput.retencion_IRPF_ticket / 12,
            company_spent: this.state.dataOutput.company_spent / 12,
            employees_save: this.state.dataOutput.employees_save / 12,
          } })
          this.setState({ dataOutput: newDataOutput })
        }
      })
    })
  }

  _toQueryString (obj) {
    let parts = []
    for (var i in obj) {
      if (obj.hasOwnProperty(i)) {
        parts.push(encodeURIComponent(i) + '=' + encodeURIComponent(obj[i]))
      }
    }
    return parts.join('&')
  }

  /* ON CHANGE */

  toggleResultsChange (value) {
    this.setState({ toggle_results: !this.state.toggle_results }, () => {
      this.simulator = new FoodChildcareTransportSimulator(
        this.state.salario,
        this.state.regular_contribution,
        this.state.numero_emplea_medio,
        this.state.max_regular_contribution,
        this.state.factor_for_year,
        this.state.dataParams
      )

      const newDataOutputTrue = update(this.state.dataOutput, { $merge: {
        retencion_IRPF_medio: this.simulator.dataOutput.retencion_IRPF_medio,
        retencion_IRPF_ticket: this.simulator.dataOutput.retencion_IRPF_ticket,
        taxes_saved: this.simulator.dataOutput.taxes_saved,
        company_spent: this.simulator.dataOutput.company_spent,
        employees_save: this.simulator.dataOutput.employees_save,
      } })

      this.setState({ dataOutput: newDataOutputTrue }, () => {
        if (this.state.toggle_results) {
          const newDataOutput = update(this.state.dataOutput, { $merge: {
            retencion_IRPF_medio: this.state.dataOutput.retencion_IRPF_medio / 12,
            retencion_IRPF_ticket: this.state.dataOutput.retencion_IRPF_ticket / 12,
            company_spent: this.state.dataOutput.company_spent / 12,
            employees_save: this.state.dataOutput.employees_save / 12,
          } })
          this.setState({ dataOutput: newDataOutput })
        }
      })
    })
  }

  nbEmployeeChange (value) {
    return this._updateSimultorState({ numero_emplea_medio: value })
  }

  voucherChange (value) {
    return this._updateSimultorState({ regular_contribution: value })
  }

  salaireBrutChange (value) {
    return this._updateSimultorState({ salario: value })
  }

  handleClickShowMore () {
    this.setState({ showMore: !this.state.showMore }, () => {
      // if (this.state.showMore) {
      //   document.getElementById('root').classList.add('overlay')
      //   document.body.classList.add('simulator_mobile-open')
      // } else {
      //   document.getElementById('root').classList.remove('overlay')
      //   document.body.classList.remove('simulator_mobile-open')
      // }
    })
  }

  /* CTA PART */

  onClickSendEmail (event) {
    event.preventDefault()
    let data = {
      simulator: 'transport',
      productname: 'Transporte Pass',
      numberofemployee: this.state.numero_emplea_medio,
      vouchervalue: this.state.regular_contribution,
      salary: this.state.salario,
      retencionirpfmedia: this.state.dataOutput.retencion_IRPF_medio,
      retencionirpfticket: this.state.dataOutput.retencion_IRPF_ticket,
      taxessaved: this.state.dataOutput.taxes_saved,
      companyspent: this.state.dataOutput.company_spent,
      employeessave: this.state.dataOutput.employees_save
    }

    data = this._toQueryString(data)
    document.getElementById('mail-pdf-form').action = document.location.origin + this.state.dataParams['pdf_action_url_adress'] + '?' + data
  }

  onClickDownloadPdf (event) {
    event.preventDefault()
    window.dataLayer.push({ 'event': 'download', 'name':'PDF simulador España Transporte pass' })
    let data = {
      simulator: 'transport',
      productname: 'Transporte Pass',
      numberofemployee: this.state.numero_emplea_medio,
      vouchervalue: this.state.regular_contribution,
      salary: this.state.salario,
      retencionirpfmedia: this.state.dataOutput.retencion_IRPF_medio,
      retencionirpfticket: this.state.dataOutput.retencion_IRPF_ticket,
      taxessaved: this.state.dataOutput.taxes_saved,
      companyspent: this.state.dataOutput.company_spent,
      employeessave: this.state.dataOutput.employees_save
    }

    data = this._toQueryString(data)
    window.location.href = document.location.origin + this.state.dataParams['pdf_action_url_adress'] + '?' + data
  }


    onClickTypeform (event) {
        event.preventDefault()
        document.body.classList.add("no-scroll");
        document.body.classList.add("overlay");
        this.setState({ showTypeform: true })
        window.dataLayer.push({ 'event':'click-contactus', 'location':'simulator' })
    }

    closeTypeform (event) {
        event.preventDefault()
        document.body.classList.remove("no-scroll");
        document.body.classList.remove("overlay");
        this.setState({ showTypeform: false })
    }

  createIframe () {
    let data = {
      // simulator: 'childcare',
      numberofemployee: this.state.numero_emplea_medio,
      vouchervalue: this.state.regular_contribution,
      salary: this.state.salario,
      retencionirpfmedia: this.state.dataOutput.retencion_IRPF_medio,
      retencionirpfticket: this.state.dataOutput.retencion_IRPF_ticket,
      taxessaved: this.state.dataOutput.taxes_saved,
      companyspent: this.state.dataOutput.company_spent,
      employeessave: this.state.dataOutput.employees_save
    }

    data = this._toQueryString(data)
    return {
      __html: '<div class="iframe-wrapper"><iframe src="' + this.state.typeform + '?' + data + '" width="540" height="450"></iframe></div>'
    }
  }

  /* RESULTS RENDER */

  getFullResults (className, width, height) {
    return (
      <div id='box' className={className} key='key'>
        <div className={'results__header'}>
          <h3 className='results-title simulator-title'>Ahorro:</h3>
          {/* <div className='results-toggle'>
            <Toggle
              trueLabel={'Per year'}
              falseLabel={'Per month'}
              defaultChecked={this.state.toggle_results}
              onChange={(value) => { this.toggleResultsChange(value) }}
            />
          </div> */}
        </div>
        <ul className='results-list'>
          <li>
            <p className='results-label'>IRPF con Transporte Pass</p>
            <p className='results-value'>{ this.state.dataOutput.retencion_IRPF_medio.toLocaleString('es-ES', { style: 'currency', currency: 'EUR' })}</p>
          </li>
          <li>
            <p className='results-label'>IRPF con el equivalente en salario</p>
            <p className='results-value'>{ this.state.dataOutput.retencion_IRPF_ticket.toLocaleString('es-ES', { style: 'currency', currency: 'EUR' })}</p>
          </li>
        </ul>

        <Doughnut
          className={className + '_doughnut'}
          width={width}
          height={height}
          percent={this.state.dataOutput.taxes_saved}
          duration={1500}
          customTextBottom={'IRPF ahorrado'}
        />

        <ul className='results-more'>
          <li>
            <p className='results-label'>La empresa gasta</p>
            <p className='results-value'>{ this.state.dataOutput.company_spent.toLocaleString('es-ES', { style: 'currency', currency: 'EUR' })}</p>
          </li>
          <li>
            <p className='results-label'>El neto de los empleados aumenta en</p>
            <p className='results-value results-important'>{ this.state.dataOutput.employees_save.toLocaleString('es-ES', { style: 'currency', currency: 'EUR' })}</p>
          </li>
        </ul>
      </div>
    )
  }

  /* RENDER */

  render () {
    let showMoreClass = this.state.showMore ? 'open' : ''
    // let showMoreText = this.state.showMore ? '-' : '+'
    let showMoreCheuvronClass = this.state.showMore ? '' : 'bottom'
    let component = this.state.showMore ? this.getFullResults('results__mobile_full', 180, 180) : ''
    let showTypeform = this.state.showTypeform ? 'show-typeform' : ''
    // let showTypeformOverlay = this.state.showTypeform ? 'overlay' : ''

    if (this.state.loading) {
      return (
        <div className='loading'>
          <p>Loading...</p>
        </div>
      )
    } else {
      return (
        <div>
          <section className={'simulator product '}>
            <div className='settings'>
              <div className='container_simulator' id='scrollbar'>

                <div className='settings__block'>
                  <CounterWithIcons
                    className='settings__block-employees'
                    text='Número de empleados'
                    icons={[people1, people2, people3]}
                    value={this.state.numero_emplea_medio}
                    min={2}
                    max={999}
                    step={10}
                    onChange={(value) => { this.nbEmployeeChange(value) }}
                  />

                  <CounterWithIcons
                    className='settings__block-voucher'
                    text='Importe mensual (€)'
                    icons={[money1, money2, money3]}
                    value={this.state.regular_contribution}
                    min={10}
                    max={this.state.maxTransportPerYear}
                    step={1}
                    stepInput={0.01}
                    onChange={(value) => { this.voucherChange(value) }}
                  />
                </div>

                <div className='settings__block settings-salary'>
                  <h3 className='simulator-label'>Salario medio</h3>
                  <SliderTooltip
                    value={this.state.salario}
                    min={15000}
                    max={100000}
                    step={5000}
                    placement='top'
                    onChange={(value) => { this.salaireBrutChange(value) }}
                  />
                </div>
              </div>
            </div>

            <div className='results'>
              <div className={'results__mobile ' + showMoreClass}>
                <div className='results__mobile_summary'>
                  {/* <DoughnutSummary
                    className={'results__mobile_summary_doughnut'}
                    width={50}
                    height={50}
                    percent={this.state.dataOutput.taxes_saved}
                    duration={1200}
                  />  */}

                  <div className='results__mobile_summary_content'>
                    <span className='results__mobile_summary_content-text'>IRPF ahorrado</span>
                    <span className='results__mobile_summary_content-percentage'>{ this.state.dataOutput.taxes_saved.toLocaleString('es-ES', { maximumFractionDigits: 2 }) }%</span>
                  </div>
                </div>

                <ReactCSSTransitionGroup
                  transitionName='slide'
                  transitionAppear
                  transitionAppearTimeout={500}
                  transitionEnterTimeout={300}
                  transitionLeaveTimeout={300}
                >
                  {component}
                </ReactCSSTransitionGroup>
                <div className={'results__show-more ' + showMoreClass} onClick={this.handleClickShowMore}>
                  <span className={'chevron ' + showMoreCheuvronClass} />
                </div>
              </div>

              { this.getFullResults('results__full', 250, 250) }

            </div>

              <div className={'iframe-typeform-wrapper ' + showTypeform}>
                  <div className='iframe-typeform'>
                      <div dangerouslySetInnerHTML={this.createIframe()} />
              <a className='product-close' onClick={this.closeTypeform} href='#'><i className='icon-close'></i></a>
            </div>
            </div>
          </section>

          <section className='callToAction'>
            <div className='callToAction-links'>
              <div className='callToAction-groupLinks'>
                <a href='#' onClick={this.onClickDownloadPdf} className='callToAction-link'>{this.state.ctaDownloadLabel}</a>
                <a href='#' className='callToAction-link' onClick={this.onClickSendEmail} data-toggle='modal' data-target='#sendmailmodal'>{this.state.ctaSendPerEmailLabel}</a>
              </div>
              <div className='callToAction-groupLinks'>
                <a href='#' className='callToAction-link btn-sodexo btn-sodexo-white' onClick={this.onClickTypeform}>{this.state.ctaBeContactedLabel}</a>
                <a href={this.state.ctaPLaceanOrderLink} className='callToAction-link callToAction-link-order btn-sodexo btn-sodexo-red' target='_blank'>{this.state.ctaPLaceanOrderLabel}</a>
              </div>
            </div>
            <p className='callToAction-disclaimer'>{this.state.ctaDisclaimer}</p>
          </section>
        </div>
      )
    }
  }
}

export default SpainTransportApp
