import React from 'react'
// import PropTypes from 'prop-types'
import update from 'immutability-helper'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'

import './card.scss'

import Toggle from './../../components/Toggle/Toggle'
import CounterWithIcons from './../../components/CounterWithIcons/CounterWithIcons'
// import SliderTooltip from './../../components/SliderTooltip/SliderTooltip'
import Doughnut from './../../components/Doughnut/Doughnut'
import DoughnutSummary from './../../components/Doughnut/DoughnutSummary'

import money1 from './../../images/money-1.svg'
import money2 from './../../images/money-2.svg'
import money3 from './../../images/money-3.svg'

import people1 from './../../images/people-1.svg'
import people2 from './../../images/people-2.svg'
import people3 from './../../images/people-3.svg'

import calendar1 from './../../images/agenda-1.svg'
import calendar2 from './../../images/agenda-2.svg'
import calendar3 from './../../images/agenda-3.svg'

import scales1 from './../../images/scales-1.svg'
import scales2 from './../../images/scales-2.svg'
// import scales3 from './../../images/scales-3.svg'

import ItalyCardSimulator from './../../utils/italyCardSimulator.js'

import axios from 'axios'
// import classNames from 'classnames'

class ItalyCardApp extends React.Component {
  constructor (props) {
    // Pass props to parent class
    super(props)
    // Set initial state
    this.state = {
      // Data inputs
      dataInputs: {
        valeurTitre: 7,
        nombreSalaries: 20,
        nombreTicketsResto: 31,
      },

      // Other data inputs
      toggle_results: false,
      loading: true,
      showTypeform: false,

      // Results
      dataOutputs : {
        budgetMensuelTotal: 0,
        participationEmployeur: 0,
        economiesRealiseesAnnuellesEmployeur: 0,
        pouvoirAchatAdditionnel: 0,
        gainEmployee: 0
      },
      valeurTitreMax: 8.97,
      exonerationArray: {
        50: 10.76,
        51: 10.55,
        52: 10.35,
        53: 10.15,
        54: 9.96,
        55: 9.78,
        56: 9.61,
        57: 9.44,
        58: 9.28,
        59: 9.12,
        60: 8.97
      },
    }

    this.toggleResultsChange = this.toggleResultsChange.bind(this)
    this.nbEmployeeChange = this.nbEmployeeChange.bind(this)
    this.voucherChange = this.voucherChange.bind(this)
    this.workingDayChange = this.workingDayChange.bind(this)
    this.employerParticipationChange = this.employerParticipationChange.bind(this)
    this.handleClickShowMore = this.handleClickShowMore.bind(this)

    this.onClickTypeform = this.onClickTypeform.bind(this)
    this.onClickSendEmail = this.onClickSendEmail.bind(this)
    this.onClickDownloadPdf = this.onClickDownloadPdf.bind(this)
    this.closeTypeform = this.closeTypeform.bind(this)
    this.createIframe = this.createIframe.bind(this)

    window.dataLayer = window.dataLayer || []

    this._getDataParams()
  }

  /* UTILS */

  _getDataParams () {
    let url = document.location.origin + '/wp-json/acf/v2/options/'
    // let url = document.location.origin + '/front-wp-dev/wp-json/acf/v2/options/'
    if (__DEV__) {
      url = 'http://10.224.86.238:3000/wp-json/acf/v2/options/'
      // url = 'http://10.0.75.1:3000/front-wp-dev/wp-json/acf/v2/options/'
    }

    return axios({
      method:'get',
      url: url,
    })
    .then((response) => {
      // console.log(response)
      if (response.status === 200) {
        return this.setState({ dataParams: response.data.acf }, () => {
          this.state.dataParams.valoreDefiscalizzato = parseFloat(response.data.acf['ita_card_valore_defiscalizzato'])
          this.state.dataParams.inps = parseFloat(response.data.acf['ita_card_inps_a_carico_dellazienda'])
          this.state.dataParams.tfr = parseFloat(response.data.acf['ita_card_tfr_rateo_13ma_rateo_14ma'])
          this.state.dataParams.irap = parseFloat(response.data.acf['ita_card_irap'])
          this.state.dataParams.ires = parseFloat(response.data.acf['ita_card_ires_su_irap'])
          this.state.dataParams.inpsCarico = parseFloat(response.data.acf['ita_card_inps_a_carico_del_dipendente'])
          this.state.dataParams.irpef = parseFloat(response.data.acf['ita_card_irpef'])
          
          this.simulator = new ItalyCardSimulator(
            this.state, this.state.dataParams
          )

          const newDataOutputs = update(this.state.dataOutputs, { $merge: {
            budgetMensuelTotal: this.simulator.dataOutputs.budgetMensuelTotal,
            participationEmployeur: this.simulator.dataOutputs.participationEmployeur,
            pouvoirAchatAdditionnel: this.simulator.dataOutputs.pouvoirAchatAdditionnel,
            economiesRealiseesAnnuellesEmployeur: this.simulator.dataOutputs.economiesRealiseesAnnuellesEmployeur,
            gainEmployee: this.simulator.dataOutputs.gainEmployee
          } })

          this.setState({
            typeform: this.state.dataParams['form_simulator_typeform_url'],
            ctaSendPerEmailLabel:  this.state.dataParams['pdf_modal_title'],
            ctaBeContactedLabel:  this.state.dataParams['simulator_be_contacted_label'],
            ctaDownloadLabel:  this.state.dataParams['simulator_download_label'],
            ctaDisclaimer:  this.state.dataParams['simulator_legend'],
            ctaPLaceanOrderLabel:  this.state.dataParams['simulator_place_an_order_label'],
            ctaPLaceanOrderLink:  this.state.dataParams['simulator_place_an_order_link'],

            tooltip1: this.state.dataParams['tooltip_nombre_salaries'],
            tooltip2: this.state.dataParams['tooltip_nombre_titre'],
            tooltip3: this.state.dataParams['tooltip_valeur_titre'],
            tooltip4: this.state.dataParams['tooltip_participation_employeur'],

            dataOutputs: newDataOutputs,
            loading: false
          })
        })
      }
    })
  }

  _updateSimultorState (value) {
    window.dataLayer.push({ 'event':'click-simulator' })
    const newDataInput = update(this.state.dataInputs, { $merge: value })
    this.setState({ dataInputs: newDataInput }, () => {
      
      this.simulator = new ItalyCardSimulator(
        this.state, this.state.dataParams
      )

      const newDataOutputs = update(this.state.dataOutputs, { $merge: {
        budgetMensuelTotal: this.simulator.dataOutputs.budgetMensuelTotal,
        participationEmployeur: this.simulator.dataOutputs.participationEmployeur,
        pouvoirAchatAdditionnel: this.simulator.dataOutputs.pouvoirAchatAdditionnel,
        economiesRealiseesAnnuellesEmployeur: this.simulator.dataOutputs.economiesRealiseesAnnuellesEmployeur,
        gainEmployee: this.simulator.dataOutputs.gainEmployee
      } })

      this.setState({ dataOutputs: newDataOutputs }, () => {
        if (this.state.toggle_results) {
          const newDataOutputs = update(this.state.dataOutputs, { $merge: {
            budgetMensuelTotal: this.state.dataOutputs.budgetMensuelTotal * 12,
            participationEmployeur: this.state.dataOutputs.participationEmployeur * 12,
            pouvoirAchatAdditionnel: this.simulator.dataOutputs.pouvoirAchatAdditionnel * 12,
            economiesRealiseesAnnuellesEmployeur: this.state.dataOutputs.economiesRealiseesAnnuellesEmployeur * 12,
            gainEmployee: this.simulator.dataOutputs.gainEmployee * 12
          } })
          this.setState({ dataOutputs: newDataOutputs })
        }
      })
    })
  }

  _toQueryString (obj) {
    let parts = []
    for (var i in obj) {
      if (obj.hasOwnProperty(i)) {
        parts.push(encodeURIComponent(i) + '=' + encodeURIComponent(obj[i]))
      }
    }
    return parts.join('&')
  }

  /* ON CHANGE */

  toggleResultsChange (value) {
    this.setState({ toggle_results: !this.state.toggle_results }, () => {
      
      this.simulator = new ItalyCardSimulator(
        this.state, this.state.dataParams
      )

      const newDataOutputsTrue = update(this.state.dataOutputs, { $merge: {
        budgetMensuelTotal: this.simulator.dataOutputs.budgetMensuelTotal,
        participationEmployeur: this.simulator.dataOutputs.participationEmployeur,
        pouvoirAchatAdditionnel: this.simulator.dataOutputs.pouvoirAchatAdditionnel,
        economiesRealiseesAnnuellesEmployeur: this.simulator.dataOutputs.economiesRealiseesAnnuellesEmployeur,
        gainEmployee: this.simulator.dataOutputs.gainEmployee
      } })

      this.setState({ dataOutputs: newDataOutputsTrue }, () => {
        if (this.state.toggle_results) {
          const newDataOutputs = update(this.state.dataOutputs, { $merge: {
            budgetMensuelTotal: this.state.dataOutputs.budgetMensuelTotal * 12,
            participationEmployeur: this.state.dataOutputs.participationEmployeur * 12,
            pouvoirAchatAdditionnel: this.simulator.dataOutputs.pouvoirAchatAdditionnel * 12,
            economiesRealiseesAnnuellesEmployeur: this.state.dataOutputs.economiesRealiseesAnnuellesEmployeur * 12,
            gainEmployee: this.simulator.dataOutputs.gainEmployee * 12
          } })
          this.setState({ dataOutputs: newDataOutputs })
        }
      })
    })
  }

  nbEmployeeChange (value) {
    return this._updateSimultorState({ nombreSalaries: value })
  }

  voucherChange (value) {
    console.log(value)
    return this._updateSimultorState({ valeurTitre: value })
  }

  workingDayChange (value) {
    return this._updateSimultorState({ nombreTicketsResto: value })
  }

  employerParticipationChange (value) {
    this.setState({ valeurTitreMax : this.state.exonerationArray[value] })
    return this._updateSimultorState({ participationEmployeur: value / 100 })
  }

  handleClickShowMore () {
    this.setState({ showMore: !this.state.showMore })
  }

  /* CTA PART */

  onClickSendEmail (event) {
    event.preventDefault()
    let data = {
      simulator: 'italyCard',
      nombresalaries: this.state.dataInputs.nombreSalaries,
      nombreticketsresto: this.state.dataInputs.nombreTicketsResto,
      valeurtitre: this.state.dataInputs.valeurTitre,
      valeurparticipationemployeur: this.state.dataInputs.participationEmployeur,
      budgettotal: this.state.dataOutputs.budgetMensuelTotal,
      participationemployeur: this.state.dataOutputs.participationEmployeur,
      pouvoirachatadditionnel: this.state.dataOutputs.pouvoirAchatAdditionnel,
      economiesrealiseesemployeur: this.state.dataOutputs.economiesRealiseesAnnuellesEmployeur,
      gainemployee: this.state.dataOutputs.gainEmployee
    }

    data = this._toQueryString(data)
    document.getElementById('mail-pdf-form').action = document.location.origin + this.state.dataParams['pdf_action_url_adress'] + '?' + data
  }

  onClickDownloadPdf (event) {
    event.preventDefault()
    window.dataLayer.push({ 'event': 'download', 'name':'PDF simulateur ItalyCard' })
    let data = {
      simulator: 'italyCard',
      nombresalaries: this.state.dataInputs.nombreSalaries,
      nombreticketsresto: this.state.dataInputs.nombreTicketsResto,
      valeurtitre: this.state.dataInputs.valeurTitre,
      valeurparticipationemployeur: this.state.dataInputs.participationEmployeur,
      budgettotal: this.state.dataOutputs.budgetMensuelTotal,
      participationemployeur: this.state.dataOutputs.participationEmployeur,
      pouvoirachatadditionnel: this.state.dataOutputs.pouvoirAchatAdditionnel,
      economiesrealiseesemployeur: this.state.dataOutputs.economiesRealiseesAnnuellesEmployeur,
      gainemployee: this.state.dataOutputs.gainEmployee
    }

    data = this._toQueryString(data)
    window.location.href = document.location.origin + this.state.dataParams['pdf_action_url_adress'] + '?' + data
  }


    onClickTypeform (event) {
        event.preventDefault()
        document.body.classList.add("no-scroll");
        document.body.classList.add("overlay");
        this.setState({ showTypeform: true })
        window.dataLayer.push({ 'event':'click-contactus', 'location':'simulator' })
    }

    closeTypeform (event) {
        event.preventDefault()
        document.body.classList.remove("no-scroll");
        document.body.classList.remove("overlay");
        this.setState({ showTypeform: false })
    }

  createIframe () {
    let data = {
      nombresalaries: this.state.dataInputs.nombreSalaries,
      nombreticketsresto: this.state.dataInputs.nombreTicketsResto,
      valeurtitre: this.state.dataInputs.valeurTitre,
      valeurparticipationemployeur: this.state.dataInputs.participationEmployeur,
      budgettotal: this.state.dataOutputs.budgetMensuelTotal,
      participationemployeur: this.state.dataOutputs.participationEmployeur,
      pouvoirachatadditionnel: this.state.dataOutputs.pouvoirAchatAdditionnel,
      economiesrealiseesemployeur: this.state.dataOutputs.economiesRealiseesAnnuellesEmployeur,
      gainemployee: this.state.dataOutputs.gainEmployee
    }

    data = this._toQueryString(data)
    return {
      __html: '<div class="iframe-wrapper"><iframe src="' + this.state.typeform + '?' + data + '" width="540" height="450"></iframe></div>'
    }
  }

  /* RESULTS RENDER */

  getFullResults (className, width, height) {
    return (
      <div id='box' className={className} key='key'>
        <div className={'results__header'}>
          <h3 className='results-title simulator-title'>Risultati:</h3>
          <div className='results-toggle'>
            <Toggle
              trueLabel={'Mensile'}
              falseLabel={'Annuale'}
              defaultChecked={this.state.toggle_results}
              onChange={(value) => { this.toggleResultsChange(value) }}
            />
          </div>
        </div>

        <ul className='results-list'>
          <li>
            <p className='results-label'>Costo aziendale con buono pasto</p>
            <p className='results-value'>{ Math.round(this.state.dataOutputs.budgetMensuelTotal).toLocaleString('it-IT', {currency: 'EUR' }) + " €"}</p>
          </li>
          <li>
            <p className='results-label'>Costo aziendale con denaro in busta paga</p>
            <p className='results-value'>{ Math.round(this.state.dataOutputs.participationEmployeur).toLocaleString('it-IT', {currency: 'EUR' }) + " €"}</p>
          </li>
        </ul>

        <Doughnut
          className={className + '_doughnut'}
          width={width}
          height={height}
          percent={70}
          duration={1500}
          customTextTop={ Math.round(this.state.dataOutputs.economiesRealiseesAnnuellesEmployeur).toLocaleString('it-IT', {currency: 'EUR' }) + " €"}
          customTextBottom={'Risparmio per l\'azienda'}
        />

        <ul className='results-more'>
          <li>
            <p className='results-label'>Risparmio per il dipendente</p>
            <p className='results-value results-important'>{ Math.round(this.state.dataOutputs.gainEmployee).toLocaleString('it-IT', {currency: 'EUR' }) + " €"} </p>
          </li>
        </ul>
      </div>
    )
  }

  /* RENDER */

  render () {
    let showMoreClass = this.state.showMore ? 'open' : ''
    // let showMoreText = this.state.showMore ? '-' : '+'
    let showMoreCheuvronClass = this.state.showMore ? '' : 'bottom'
    let component = this.state.showMore ? this.getFullResults('results__mobile_full', 180, 180) : ''
    let showTypeform = this.state.showTypeform ? 'show-typeform' : ''
    // let showTypeformOverlay = this.state.showTypeform ? 'overlay' : ''

    if (this.state.loading) {
      return (
        <div className='loading'>
          <p>Loading...</p>
        </div>
      )
    } else {
      return (
        <div>
          <section className={'simulator italyCard '}>
            <div className='settings'>
              <div className='container_simulator' id='scrollbar'>
                <div className='settings__block'>
                  <CounterWithIcons
                    className='settings__block-employees'
                    text='Numero dipendenti'
                    icons={[people1, people2, people3]}
                    value={this.state.dataInputs.nombreSalaries}
                    min={1}
                    max={999999}
                    step={1}
                    tooltip={this.state.tooltip1}
                    onChange={(value) => { this.nbEmployeeChange(value) }}
                  />
                  <CounterWithIcons
                    className='settings__block-voucher'
                    text='Valore buono'
                    icons={[money1, money2, money3]}
                    value={this.state.dataInputs.valeurTitre}
                    min={0.10}
                    max={999999}
                    step={0.01}
                    tooltip={this.state.tooltip3}
                    onChange={(value) => { this.voucherChange(value) }}
                    />
                </div>

                <div className='settings__block'>
                  <CounterWithIcons
                    className='settings__block-working_day'
                    text='Giorni lavorativi in un mese'
                    icons={[calendar1, calendar2, calendar3]}
                    value={this.state.dataInputs.nombreTicketsResto}
                    min={1}
                    max={31}
                    step={1}
                    tooltip={this.state.tooltip2}
                    onChange={(value) => { this.workingDayChange(value) }}
                  />
                </div>
              </div>
            </div>

            <div className='results'>
              <div className={'results__mobile ' + showMoreClass}>
                <div className='results__mobile_summary'>
                  <DoughnutSummary
                    className={'results__mobile_summary_doughnut'}
                    width={50}
                    height={50}
                    percent={70}
                    duration={1200}
                    customText={'Risparmio per l\'azienda'}
                  />

                  <div className='results__mobile_summary_content'>
                    <span className='results__mobile_summary_content-text'>Risparmio per l'azienda</span>
                    <span className='results__mobile_summary_content-percentage'>
                      { Math.round(this.state.dataOutputs.economiesRealiseesAnnuellesEmployeur).toLocaleString('it-IT', {currency: 'EUR' }) + " €" }
                    </span>
                  </div>
                </div>

                <ReactCSSTransitionGroup
                  transitionName='slide'
                  transitionAppear
                  transitionAppearTimeout={500}
                  transitionEnterTimeout={300}
                  transitionLeaveTimeout={300}
                >
                  {component}
                </ReactCSSTransitionGroup>
                <div className={'results__show-more ' + showMoreClass} onClick={this.handleClickShowMore}>
                  <span className={'chevron ' + showMoreCheuvronClass} />
                </div>
              </div>

              { this.getFullResults('results__full', 250, 250) }

            </div>

              <div className={'iframe-typeform-wrapper ' + showTypeform}>
                  <div className='iframe-typeform'>
                      <div dangerouslySetInnerHTML={this.createIframe()} />
              <a className='product-close' onClick={this.closeTypeform} href='#'><i className='icon-close'></i></a>
            </div>
            </div>
          </section>

          <section className='callToAction'>
            <div className='callToAction-links'>
              <div className='callToAction-groupLinks'>
                <a href='#' onClick={this.onClickDownloadPdf} className='callToAction-link'>{this.state.ctaDownloadLabel}</a>
                <a href='#' className='callToAction-link' onClick={this.onClickSendEmail} data-toggle='modal' data-target='#sendmailmodal'>{this.state.ctaSendPerEmailLabel}</a>
              </div>
              <div className='callToAction-groupLinks'>
                <a href='#' className='callToAction-link btn-sodexo btn-sodexo-white' onClick={this.onClickTypeform}>{this.state.ctaBeContactedLabel}</a>
                <a href={this.state.ctaPLaceanOrderLink} className='callToAction-link callToAction-link-order btn-sodexo btn-sodexo-red' target='_blank'>{this.state.ctaPLaceanOrderLabel}</a>
              </div>
            </div>
            <p className='callToAction-disclaimer'>{this.state.ctaDisclaimer}</p>
          </section>
        </div>
      )
    }
  }
}

export default ItalyCardApp
