import React from 'react'
// import Platform from 'react-native';
// import PropTypes from 'prop-types'
import update from 'immutability-helper'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'

import './france.scss'

import Toggle from './../../components/Toggle/Toggle'
import CounterWithIcons from './../../components/CounterWithIcons/CounterWithIcons'
// import SliderTooltip from './../../components/SliderTooltip/SliderTooltip'
import Doughnut from './../../components/Doughnut/Doughnut'
import DoughnutSummary from './../../components/Doughnut/DoughnutSummary'

import money1 from './../../images/money-1.svg'
import money2 from './../../images/money-2.svg'
import money3 from './../../images/money-3.svg'

import people1 from './../../images/people-1.svg'
import people2 from './../../images/people-2.svg'
import people3 from './../../images/people-3.svg'

import calendar1 from './../../images/agenda-1.svg'
import calendar2 from './../../images/agenda-2.svg'
import calendar3 from './../../images/agenda-3.svg'

import scales1 from './../../images/scales-1.svg'
import scales2 from './../../images/scales-2.svg'
// import scales3 from './../../images/scales-3.svg'

import FranceSimulator from './../../utils/franceSimulator.js'

import axios from 'axios'
// import classNames from 'classnames'

class FranceApp extends React.Component {
  constructor (props) {
    // Pass props to parent class
    super(props)
    // Set initial state
    this.state = {
      // Data inputs
      dataInputs: {
        valeurTitre: 8,
        tauxAugmentationSouhaite: 0,
        nombreSalaries: 10,
        nombreTicketsResto: 20,
        participationEmployeur: 0.60,
        salaireMensuelBrut: 2000,
      },

      // Other data inputs
      toggle_results: false,
      loading: true,
      showTypeform: false,

      // Results
      dataOutputs : {
        budgetMensuelTotal: 0,
        participationEmployeur: 0,
        coFinancement: 0,
        gainPouvoirAchatPourcentage: 0,
        economiesRealiseesAnnuellesEmployeur: 0,
        economiesRealiseesAnnuellesEmployee: 0,
        pouvoirAchatAdditionnel: 0,
        gainEmployee: 0
      },
      // valeurTitreMax: 8.97,
    }

    this.toggleResultsChange = this.toggleResultsChange.bind(this)
    this.nbEmployeeChange = this.nbEmployeeChange.bind(this)
    this.voucherChange = this.voucherChange.bind(this)
    this.workingDayChange = this.workingDayChange.bind(this)
    this.employerParticipationChange = this.employerParticipationChange.bind(this)
    this.handleClickShowMore = this.handleClickShowMore.bind(this)

    this.onClickTypeform = this.onClickTypeform.bind(this)
    this.onClickSendEmail = this.onClickSendEmail.bind(this)
    this.onClickDownloadPdf = this.onClickDownloadPdf.bind(this)
    this.closeTypeform = this.closeTypeform.bind(this)
    this.createIframe = this.createIframe.bind(this)

    window.dataLayer = window.dataLayer || []

    this._getDataParams()
  }

  /* UTILS */

  _getDataParams () {
    let url = document.location.origin + '/wp-json/acf/v2/options/'
    // let url = document.location.origin + '/front-wp-dev/wp-json/acf/v2/options/'
    if (__DEV__) {
      url = 'http://10.224.86.238:3000/wp-json/acf/v2/options/'
      // url = 'http://10.0.75.1:3000/front-wp-dev/wp-json/acf/v2/options/'
    }

    return axios({
      method:'get',
      url: url,
    })
    .then((response) => {
      // console.log(response)
      if (response.status === 200) {
        return this.setState({ dataParams: response.data.acf }, () => {
          this.state.dataParams.limite_exoneration_charge = response.data.acf['limite_exoneration_charge']
          this.state.valeurTitreMax = response.data.acf['limite_exoneration_charge'] / (60/100)
          this.simulator = new FranceSimulator(
            this.state, this.state.dataParams
          )
          const newDataOutputs = update(this.state.dataOutputs, { $merge: {
            budgetMensuelTotal: this.simulator.dataOutputs.budgetMensuelTotal,
            participationEmployeur: this.simulator.dataOutputs.participationEmployeur,
            coFinancement: this.simulator.dataOutputs.coFinancement,
            gainPouvoirAchatPourcentage: this.simulator.dataOutputs.gainPouvoirAchatPourcentage,
            pouvoirAchatAdditionnel: this.simulator.dataOutputs.pouvoirAchatAdditionnel,
            economiesRealiseesAnnuellesEmployeur: this.simulator.dataOutputs.economiesRealiseesAnnuellesEmployeur,
            economiesRealiseesAnnuellesEmployee: this.simulator.dataOutputs.economiesRealiseesAnnuellesEmployee,
            gainEmployee: this.simulator.dataOutputs.gainEmployee
          } })

          this.setState({
            typeform: this.state.dataParams['form_simulator_typeform_url'],
            ctaSendPerEmailLabel:  this.state.dataParams['pdf_modal_title'],
            ctaBeContactedLabel:  this.state.dataParams['simulator_be_contacted_label'],
            ctaDownloadLabel:  this.state.dataParams['simulator_download_label'],
            ctaDisclaimer:  this.state.dataParams['simulator_legend'],
            ctaPLaceanOrderLabel:  this.state.dataParams['simulator_place_an_order_label'],
            ctaPLaceanOrderLink:  this.state.dataParams['simulator_place_an_order_link'],

            tooltip1: this.state.dataParams['tooltip_nombre_salaries'],
            tooltip2: this.state.dataParams['tooltip_nombre_titre'],
            tooltip3: this.state.dataParams['tooltip_valeur_titre'],
            tooltip4: this.state.dataParams['tooltip_participation_employeur'],

            dataOutputs: newDataOutputs,
            loading: false
          })
        })
      }
    })
  }

  _updateSimultorState (value) {
    window.dataLayer.push({ 'event':'click-simulator' })
    const newDataInput = update(this.state.dataInputs, { $merge: value })
    this.setState({ dataInputs: newDataInput }, () => {
      this.simulator = new FranceSimulator(
        this.state, this.state.dataParams
      )

      const newDataOutputs = update(this.state.dataOutputs, { $merge: {
        budgetMensuelTotal: this.simulator.dataOutputs.budgetMensuelTotal,
        participationEmployeur: this.simulator.dataOutputs.participationEmployeur,
        coFinancement: this.simulator.dataOutputs.coFinancement,
        gainPouvoirAchatPourcentage: this.simulator.dataOutputs.gainPouvoirAchatPourcentage,
        pouvoirAchatAdditionnel: this.simulator.dataOutputs.pouvoirAchatAdditionnel,
        economiesRealiseesAnnuellesEmployeur: this.simulator.dataOutputs.economiesRealiseesAnnuellesEmployeur,
        economiesRealiseesAnnuellesEmployee: this.simulator.dataOutputs.economiesRealiseesAnnuellesEmployee,
        gainEmployee: this.simulator.dataOutputs.gainEmployee
      } })

      this.setState({ dataOutputs: newDataOutputs }, () => {
        if (this.state.toggle_results) {
          const newDataOutputs = update(this.state.dataOutputs, { $merge: {
            budgetMensuelTotal: this.state.dataOutputs.budgetMensuelTotal / 11,
            participationEmployeur: this.state.dataOutputs.participationEmployeur / 11,
            coFinancement: this.simulator.dataOutputs.coFinancement / 11,
            gainPouvoirAchatPourcentage: this.simulator.dataOutputs.gainPouvoirAchatPourcentage,
            pouvoirAchatAdditionnel: this.simulator.dataOutputs.pouvoirAchatAdditionnel / 11,
            economiesRealiseesAnnuellesEmployeur: this.state.dataOutputs.economiesRealiseesAnnuellesEmployeur / 11,
            economiesRealiseesAnnuellesEmployee: this.state.dataOutputs.economiesRealiseesAnnuellesEmployee / 11,
            gainEmployee: this.simulator.dataOutputs.gainEmployee / 11
          } })
          this.setState({ dataOutputs: newDataOutputs })
        }
      })
    })
  }

  _toQueryString (obj) {
    let parts = []
    for (var i in obj) {
      if (obj.hasOwnProperty(i)) {
        parts.push(encodeURIComponent(i) + '=' + encodeURIComponent(obj[i]))
      }
    }
    return parts.join('&')
  }

  /* ON CHANGE */

  toggleResultsChange (value) {
    this.setState({ toggle_results: !this.state.toggle_results }, () => {
      this.simulator = new FranceSimulator(
        this.state, this.state.dataParams
      )

      const newDataOutputsTrue = update(this.state.dataOutputs, { $merge: {
        budgetMensuelTotal: this.simulator.dataOutputs.budgetMensuelTotal,
        participationEmployeur: this.simulator.dataOutputs.participationEmployeur,
        coFinancement: this.simulator.dataOutputs.coFinancement,
        gainPouvoirAchatPourcentage: this.simulator.dataOutputs.gainPouvoirAchatPourcentage,
        pouvoirAchatAdditionnel: this.simulator.dataOutputs.pouvoirAchatAdditionnel,
        economiesRealiseesAnnuellesEmployeur: this.simulator.dataOutputs.economiesRealiseesAnnuellesEmployeur,
        economiesRealiseesAnnuellesEmployee: this.simulator.dataOutputs.economiesRealiseesAnnuellesEmployee,
        gainEmployee: this.simulator.dataOutputs.gainEmployee
      } })

      this.setState({ dataOutputs: newDataOutputsTrue }, () => {
        if (this.state.toggle_results) {
          const newDataOutputs = update(this.state.dataOutputs, { $merge: {
            budgetMensuelTotal: this.state.dataOutputs.budgetMensuelTotal / 11,
            participationEmployeur: this.state.dataOutputs.participationEmployeur / 11,
            coFinancement: this.simulator.dataOutputs.coFinancement / 11,
            gainPouvoirAchatPourcentage: this.simulator.dataOutputs.gainPouvoirAchatPourcentage,
            pouvoirAchatAdditionnel: this.simulator.dataOutputs.pouvoirAchatAdditionnel / 11,
            economiesRealiseesAnnuellesEmployeur: this.state.dataOutputs.economiesRealiseesAnnuellesEmployeur / 11,
            economiesRealiseesAnnuellesEmployee: this.state.dataOutputs.economiesRealiseesAnnuellesEmployee / 11,
            gainEmployee: this.simulator.dataOutputs.gainEmployee / 11
          } })
          this.setState({ dataOutputs: newDataOutputs })
        }
      })
    })
  }

  nbEmployeeChange (value) {
    return this._updateSimultorState({ nombreSalaries: value })
  }

  voucherChange (value) {
    console.log(value)
    return this._updateSimultorState({ valeurTitre: value })
  }

  workingDayChange (value) {
    return this._updateSimultorState({ nombreTicketsResto: value })
  }

  employerParticipationChange (value) {
    let exonerationArray = {
      50: this.state.dataParams.limite_exoneration_charge / (50/100),
      51: this.state.dataParams.limite_exoneration_charge / (51/100),
      52: this.state.dataParams.limite_exoneration_charge / (52/100),
      53: this.state.dataParams.limite_exoneration_charge / (53/100),
      54: this.state.dataParams.limite_exoneration_charge / (54/100),
      55: this.state.dataParams.limite_exoneration_charge / (55/100),
      56: this.state.dataParams.limite_exoneration_charge / (56/100),
      57: this.state.dataParams.limite_exoneration_charge / (57/100),
      58: this.state.dataParams.limite_exoneration_charge / (58/100),
      59: this.state.dataParams.limite_exoneration_charge / (59/100),
      60: this.state.dataParams.limite_exoneration_charge / (60/100)
    }
    this.setState({ valeurTitreMax : exonerationArray[value] })
    return this._updateSimultorState({ participationEmployeur: value / 100 })
  }

  handleClickShowMore () {
    this.setState({ showMore: !this.state.showMore })
  }

  /* CTA PART */

  onClickSendEmail (event) {
    event.preventDefault()
    let data = {
      simulator: 'france',
      // annual: this.state.toggle_results,
      nombresalaries: this.state.dataInputs.nombreSalaries,
      nombreticketsresto: this.state.dataInputs.nombreTicketsResto,
      valeurtitre: this.state.dataInputs.valeurTitre,
      valeurparticipationemployeur: this.state.dataInputs.participationEmployeur,
      budgettotal: this.state.dataOutputs.budgetMensuelTotal,
      participationemployeur: this.state.dataOutputs.participationEmployeur,
      cofinancement: this.state.dataOutputs.coFinancement,
      pouvoirachatadditionnel: this.state.dataOutputs.pouvoirAchatAdditionnel,
      economiesrealiseesemployeur: this.state.dataOutputs.economiesRealiseesAnnuellesEmployeur,
      gainemployee: this.state.dataOutputs.gainEmployee
    }

    data = this._toQueryString(data)
    document.getElementById('mail-pdf-form').action = document.location.origin + this.state.dataParams['pdf_action_url_adress'] + '?' + data
  }

  onClickDownloadPdf (event) {
    event.preventDefault()
    window.dataLayer.push({ 'event': 'download', 'name':'PDF simulateur France' })
    let data = {
      simulator: 'france',
      // annual: this.state.toggle_results,
      nombresalaries: this.state.dataInputs.nombreSalaries,
      nombreticketsresto: this.state.dataInputs.nombreTicketsResto,
      valeurtitre: this.state.dataInputs.valeurTitre,
      valeurparticipationemployeur: this.state.dataInputs.participationEmployeur,
      budgettotal: this.state.dataOutputs.budgetMensuelTotal,
      participationemployeur: this.state.dataOutputs.participationEmployeur,
      cofinancement: this.state.dataOutputs.coFinancement,
      pouvoirachatadditionnel: this.state.dataOutputs.pouvoirAchatAdditionnel,
      economiesrealiseesemployeur: this.state.dataOutputs.economiesRealiseesAnnuellesEmployeur,
      gainemployee: this.state.dataOutputs.gainEmployee
    }

    data = this._toQueryString(data)
    window.location.href = document.location.origin + this.state.dataParams['pdf_action_url_adress'] + '?' + data
  }

  onClickTypeform (event) {
    event.preventDefault()
      document.body.classList.add("no-scroll");
      document.body.classList.add("overlay");
    this.setState({ showTypeform: true })
    window.dataLayer.push({ 'event':'click-contactus', 'location':'simulator' })
  }

  closeTypeform (event) {
    event.preventDefault()
      document.body.classList.remove("no-scroll");
      document.body.classList.remove("overlay");
    this.setState({ showTypeform: false })
  }

  createIframe () {
    let data = {
      // simulator: 'france',
      // annual: this.state.toggle_results,
      nombresalaries: this.state.dataInputs.nombreSalaries,
      nombreticketsresto: this.state.dataInputs.nombreTicketsResto,
      valeurtitre: this.state.dataInputs.valeurTitre,
      valeurparticipationemployeur: this.state.dataInputs.participationEmployeur,
      budgettotal: this.state.dataOutputs.budgetMensuelTotal,
      participationemployeur: this.state.dataOutputs.participationEmployeur,
      cofinancement: this.state.dataOutputs.coFinancement,
      pouvoirachatadditionnel: this.state.dataOutputs.pouvoirAchatAdditionnel,
      economiesrealiseesemployeur: this.state.dataOutputs.economiesRealiseesAnnuellesEmployeur,
      gainemployee: this.state.dataOutputs.gainEmployee
    }

    data = this._toQueryString(data)
    let typeForm = this.state.typeform
    if(window.location.protocol.indexOf("s") !== -1) {
        typeForm = this.state.typeform.replace("http:", "https:")
    }
    let classe = 'iframe-wrapper';

    let isipad = navigator.userAgent.match(/iPad/i) != null;
    let isiphone = (navigator.userAgent.match(/iPhone/i) != null) || (navigator.userAgent.match(/iPod/i) != null);
    if(isipad||isiphone){
        classe += " apple";
    }

    return {
      __html: '<div class="'+classe+'"><iframe src="' + typeForm + '?' + data + '" width="540" height="450"></iframe></div>'
    }
  }

  /* RESULTS RENDER */

  getFullResults (className, width, height) {
    return (
      <div id='box' className={className} key='key'>
        <div className={'results__header'}>
          <h3 className='results-title simulator-title'>Résultat:</h3>
          <div className='results-toggle'>
            <Toggle
              trueLabel={'Annuel'}
              falseLabel={'Mensuel'}
              defaultChecked={this.state.toggle_results}
              onChange={(value) => { this.toggleResultsChange(value) }}
            />
          </div>
        </div>

        <ul className='results-list'>
          <li>
            <p className='results-label'>Budget total</p>
            <p className='results-value'>{ this.state.dataOutputs.budgetMensuelTotal.toLocaleString('fr-FR', { style: 'currency', currency: 'EUR' }) }</p>
          </li>
          <li>
            <p className='results-label'>Coût pour l'employeur</p>
            <p className='results-value'>{ this.state.dataOutputs.participationEmployeur.toLocaleString('fr-FR', { style: 'currency', currency: 'EUR' }) }</p>
          </li>
          <li>
            <p className='results-label'>Coût pour le salarié</p>
            <p className='results-value'>{ this.state.dataOutputs.coFinancement.toLocaleString('fr-FR', { style: 'currency', currency: 'EUR' }) }</p>
          </li>
        </ul>

        <Doughnut
          className={className + '_doughnut'}
          width={width}
          height={height}
          percent={70}
          duration={1500}
          customTextTop={this.state.dataOutputs.pouvoirAchatAdditionnel.toLocaleString('fr-FR', { style: 'currency', currency: 'EUR' })}
          customTextBottom={'Gain de pouvoir d\'achat annuel/mensuel du salarié'}
        />

        <h4 className='results-subtitle'>Economie vs augmentation de salaire équivalente</h4>
        <ul className='results-more'>
          <li>
            <p className='results-label'>Pour l'employeur</p>
            <p className='results-value results-important'>{ this.state.dataOutputs.economiesRealiseesAnnuellesEmployeur.toLocaleString('fr-FR', { style: 'currency', currency: 'EUR' }) }</p>
          </li>
          <li>
            <p className='results-label'>Pour le salarié</p>
            <p className='results-value'>{ (this.state.dataOutputs.gainEmployee).toLocaleString('fr-FR', { style: 'currency', currency: 'EUR' }) } </p>
          </li>
        </ul>
      </div>
    )
  }

  /* RENDER */

  render () {
    let showMoreClass = this.state.showMore ? 'open' : ''
    // let showMoreText = this.state.showMore ? '-' : '+'
    let showMoreCheuvronClass = this.state.showMore ? '' : 'bottom'
    let component = this.state.showMore ? this.getFullResults('results__mobile_full', 180, 180) : ''
    let showTypeform = this.state.showTypeform ? 'show-typeform' : ''
    // let showTypeformOverlay = this.state.showTypeform ? 'overlay' : ''

    if (this.state.loading) {
      return (
        <div className='loading'>
          <p>Loading...</p>
        </div>
      )
    } else {
      return (
        <div>
          <section className='simulator france'>
            <div className='settings'>
              <div className='container_simulator' id='scrollbar'>
                <div className='settings__block'>
                  <CounterWithIcons
                    className='settings__block-employees'
                    text='Nombre de salariés'
                    icons={[people1, people2, people3]}
                    value={this.state.dataInputs.nombreSalaries}
                    min={1}
                    max={999999}
                    step={1}
                    // tooltip={'La participation patronnale ( 60%) est plafonnée à 5,38€ par titre restaurant pour 2017'}
                    tooltip={this.state.tooltip1}
                    onChange={(value) => { this.nbEmployeeChange(value) }}
                  />

                  <CounterWithIcons
                    className='settings__block-working_day'
                    text='Nombre de titres-restaurant par salarié par mois'
                    icons={[calendar1, calendar2, calendar3]}
                    value={this.state.dataInputs.nombreTicketsResto}
                    min={1}
                    max={20}
                    step={1}
                    tooltip={this.state.tooltip2}
                    onChange={(value) => { this.workingDayChange(value) }}
                  />
                </div>

                <div className='settings__block'>
                  <CounterWithIcons
                    className='settings__block-voucher'
                    text='Valeur du titre-restaurant (€)'
                    icons={[money1, money2, money3]}
                    label={'Plafond d\'exonération ' + this.state.valeurTitreMax.toFixed(2) + '€'}
                    value={this.state.dataInputs.valeurTitre}
                    min={1}
                    max={15}
                    step={1}
                    stepInput='0.01'
                    currency
                    suffix='€'
                    tooltip={this.state.tooltip3}
                    onChange={(value) => { this.voucherChange(value) }}
                    />

                  <CounterWithIcons
                    className='settings__block-employer_participation'
                    text='Participation employeur (%)'
                    icons={[scales1, scales2]}
                    value={this.state.dataInputs.participationEmployeur * 100}
                    min={50}
                    max={60}
                    step={1}
                    suffix='%'
                    tooltip={this.state.tooltip4}
                    onChange={(value) => { this.employerParticipationChange(value) }}
                  />
                </div>
              </div>
            </div>

            <div className='results'>
              <div className={'results__mobile ' + showMoreClass}>
                <div className='results__mobile_summary'>
                  <DoughnutSummary
                    className={'results__mobile_summary_doughnut'}
                    width={50}
                    height={50}
                    percent={70}
                    duration={1200}
                    customText={'Gain de pouvoir d\'achat du salarié'}
                  />

                  <div className='results__mobile_summary_content'>
                    <span className='results__mobile_summary_content-text'>Gain de pouvoir d'achat du salarié</span>
                    <span className='results__mobile_summary_content-percentage'>{ this.state.dataOutputs.pouvoirAchatAdditionnel.toLocaleString('fr-FR', { style: 'currency', currency: 'EUR' }) }</span>
                  </div>
                </div>

                <ReactCSSTransitionGroup
                  transitionName='slide'
                  transitionAppear
                  transitionAppearTimeout={500}
                  transitionEnterTimeout={300}
                  transitionLeaveTimeout={300}
                >
                  {component}
                </ReactCSSTransitionGroup>
                <div className={'results__show-more ' + showMoreClass} onClick={this.handleClickShowMore}>
                  <span className={'chevron ' + showMoreCheuvronClass} />
                </div>
              </div>

              { this.getFullResults('results__full', 250, 250) }

            </div>
              <div className={'iframe-typeform-wrapper ' + showTypeform}>
                <div className='iframe-typeform'>
                  <div dangerouslySetInnerHTML={this.createIframe()} />
                  {/*<a className='product-close' onClick={this.closeTypeform} href='#'><i className='icon-close'></i></a>*/}
                    <button type="button" className="lmt-close" onClick={this.closeTypeform} aria-controls="collapseQuote" aria-expanded="true">
                        <span className="sr-only">Close overlay</span>
                    </button>
                </div>
              </div>

          </section>

          <section className='callToAction'>
            <div className='callToAction-links'>
              <div className='callToAction-groupLinks'>
                <a href='#' onClick={this.onClickDownloadPdf} className='callToAction-link'>{this.state.ctaDownloadLabel}</a>
                <a href='#' className='callToAction-link' onClick={this.onClickSendEmail} data-toggle='modal' data-target='#sendmailmodal'>{this.state.ctaSendPerEmailLabel}</a>
              </div>
                <div className='callToAction-groupLinks'>
                    <a href='#' className='callToAction-link btn-sodexo btn-sodexo-white' data-toggle="modal" data-target="#typeformModal" >{this.state.ctaBeContactedLabel}</a>
                    <a href={this.state.ctaPLaceanOrderLink} className='callToAction-link callToAction-link-order btn-sodexo btn-sodexo-red' target='_blank'>{this.state.ctaPLaceanOrderLabel}</a>
                </div>
            </div>
            <p className='callToAction-disclaimer'>{this.state.ctaDisclaimer}</p>
          </section>
        </div>
      )
    }
  }
}

export default FranceApp
