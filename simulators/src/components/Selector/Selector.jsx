import React from 'react'
import PropTypes from 'prop-types'
import update from 'immutability-helper'

import Product from './components/Product'

import styles from './selector.scss'

export default class Selector extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      products: this.props.products,
    }

    this.handleCloseFoodPass = this.handleCloseFoodPass.bind(this)
    this.handleCloseChildcarePass = this.handleCloseChildcarePass.bind(this)
    this.handleCloseTransportPass = this.handleCloseTransportPass.bind(this)
    this.handleCloseHealthPass = this.handleCloseHealthPass.bind(this)
    this.handleCloseTrainingPass = this.handleCloseTrainingPass.bind(this)

    this.handleChangeFoodPass = this.handleChangeFoodPass.bind(this)
    this.handleChangeChildcarePass = this.handleChangeChildcarePass.bind(this)
    this.handleChangeTransportPass = this.handleChangeTransportPass.bind(this)
    this.handleChangeHealthPass = this.handleChangeHealthPass.bind(this)
    this.handleChangeTrainingPass = this.handleChangeTrainingPass.bind(this)
  }

  componentWillMount () {
    this.forceUpdate()
  }

  componentWillReceiveProps (nextProps) {
    // if ('checked' in nextProps) {
    //   this.setState({
    //     checked: !!nextProps.checked,
    //   })
    // }
  }

  _updateProductSelectedState (name, object, value) {
    let obj = {}
    const newProduct = update(object, { $merge: value })
    obj[name] = newProduct
    const newProducts = update(this.state.products, { $merge: obj })
    this.setState({ products: newProducts }, () => {
      this.props.onChange(this.state.products)
    })
  }

  /* ON CLOSE */

  handleCloseFoodPass () {
    return this._updateProductSelectedState('foodPass', this.state.products.foodPass, { selected : !this.state.products.foodPass.selected })
  }

  handleCloseTransportPass () {
    return this._updateProductSelectedState('transportPass', this.state.products.transportPass, { selected : !this.state.products.transportPass.selected })
  }

  handleCloseChildcarePass () {
    return this._updateProductSelectedState('childcarePass', this.state.products.childcarePass, { selected : !this.state.products.childcarePass.selected })
  }

  handleCloseHealthPass () {
    return this._updateProductSelectedState('healthPass', this.state.products.healthPass, { selected : !this.state.products.healthPass.selected })
  }

  handleCloseTrainingPass () {
    return this._updateProductSelectedState('trainingPass', this.state.products.trainingPass, { selected : !this.state.products.trainingPass.selected })
  }

  /* ON CHANGE */

  handleChangeFoodPass (value) {
    return this._updateProductSelectedState('foodPass', this.state.products.foodPass, value)
  }

  handleChangeTransportPass (value) {
    return this._updateProductSelectedState('transportPass', this.state.products.transportPass, value)
  }

  handleChangeChildcarePass (value) {
    return this._updateProductSelectedState('childcarePass', this.state.products.childcarePass, value)
  }

  handleChangeHealthPass (value) {
    return this._updateProductSelectedState('healthPass', this.state.products.healthPass, value)
  }

  handleChangeTrainingPass (value) {
    return this._updateProductSelectedState('trainingPass', this.state.products.trainingPass, value)
  }

  render () {
    const foodPassContent = this.state.products.foodPass.selected
      ? <div><Product
        data={this.state.products.foodPass}
        onCloseClick={() => { this.handleCloseFoodPass() }}
        onChange={(value) => { this.handleChangeFoodPass(value) }}
        /></div>
      : null

    const transportPassContent = this.state.products.transportPass.selected
      ? <div><Product
        data={this.state.products.transportPass}
        onCloseClick={() => { this.handleCloseTransportPass() }}
        onChange={(value) => { this.handleChangeTransportPass(value) }}
      /></div>
      : null

    const childcarePassContent = this.state.products.childcarePass.selected
    ? <div><Product
      data={this.state.products.childcarePass}
      onCloseClick={() => { this.handleCloseChildcarePass() }}
      onChange={(value) => { this.handleChangeChildcarePass(value) }}
    /></div>
    : null

    const healthPassContent = this.state.products.healthPass.selected
    ? <div><Product
      data={this.state.products.healthPass}
      onCloseClick={() => { this.handleCloseHealthPass() }}
      onChange={(value) => { this.handleChangeHealthPass(value) }}
    /></div>
    : null

    const trainingPassContent = this.state.products.trainingPass.selected
      ? <div><Product
        data={this.state.products.trainingPass}
        onCloseClick={() => { this.handleCloseTrainingPass() }}
        onChange={(value) => { this.handleChangeTrainingPass(value) }}
      /></div>
      : null

    return (
      <div className='products'>
        <div className='products__selector'>

          <h3 className='products-title'>Selecciona los productos que necesitas:</h3>

          <div className='products__selector--button'>
            <input type='checkbox' name='checkbox' id='foodPass' checked={this.state.products.foodPass.selected} onChange={this.handleCloseFoodPass} />
            <label htmlFor='foodPass'><span>{this.state.products.foodPass.title}</span></label>
          </div>

          <div className='products__selector--button'>
            <input type='checkbox' name='checkbox' id='transportPass' checked={this.state.products.transportPass.selected} onChange={this.handleCloseTransportPass} />
            <label htmlFor='transportPass'><span>{this.state.products.transportPass.title}</span></label>
          </div>

          <div className='products__selector--button'>
            <input type='checkbox' name='checkbox' id='healthPass' checked={this.state.products.healthPass.selected} onChange={this.handleCloseHealthPass} />
            <label htmlFor='healthPass'><span>{this.state.products.healthPass.title}</span></label>
          </div>

          <div className='products__selector--button'>
            <input type='checkbox' name='checkbox' id='childcarePass' checked={this.state.products.childcarePass.selected} onChange={this.handleCloseChildcarePass} />
            <label htmlFor='childcarePass'><span>{this.state.products.childcarePass.title}</span></label>
          </div>

          <div className='products__selector--button'>
            <input type='checkbox' name='checkbox' id='trainingPass'checked={this.state.products.trainingPass.selected} onChange={this.handleCloseTrainingPass} />
            <label htmlFor='trainingPass'><span>{this.state.products.trainingPass.title}</span></label>
          </div>
        </div>

        { foodPassContent }

        { transportPassContent }

        { childcarePassContent }

        { healthPassContent}

        { trainingPassContent }
      </div>
    )
  }
}

Selector.propTypes = {
  products: PropTypes.any,
  onChange: PropTypes.func

}

Selector.defaultProps = {
  products: {},
}
