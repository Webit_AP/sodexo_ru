import React from 'react'
import PropTypes from 'prop-types'

import Slider, { Handle, createSliderWithTooltip } from 'rc-slider'
import Tooltip from 'rc-tooltip'

import styles from './sliderTooltip.scss'



export default class SliderTooltip extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      placement: this.props.placement || 'bottom',
    }
  }
  
  marks = {
    10000: {
      label: '10K€',
    },
    25000: {
      label: '25K€',
    },
    50000: {
      label: '50K€',
    },
    75000: {
      label: '75K€',
    },
    100000: {
      label: '100K€',
    },
  };

  handle = (props) => {
    const { value, dragging, index, ...restProps } = props
    let valueLocale = value.toLocaleString('es-ES', { style: 'currency', currency: 'EUR' })
    return (
      <Tooltip
        prefixCls='rc-slider-tooltip'
        overlay={valueLocale}
        defaultVisible
        visible={dragging}
        placement={this.state.placement}
        key={index}
        overlayStyle={{
          marginLeft: -20,
          marginTop: -10,
          top: 380,
          left: 52.5
        }}
      >
        <Handle value={value} {...restProps} />
      </Tooltip>
    )
  };

  render () {
    return (
      <Slider
        id='rc-slider-container'
        defaultValue={this.props.value}
        min={this.props.min}
        max={this.props.max}
        step={this.props.step}
        marks={this.marks}
        handle={this.handle}
        maximumTrackStyle={{ backgroundColor: '#eae6e3', height: 6 }}
        minimumTrackStyle={{ backgroundColor: '#283897', height: 6 }}
        onAfterChange={(value) => { this.props.onChange(value) }}
        handleStyle={{
          height: 17,
          width: 17,
          marginLeft: -10,
          marginTop: -5,
          backgroundColor: '#ed424a',
          border: '2px solid white',
          boxShadow: '0px 1px 1px 0px rgba(3, 3, 3, 0.35)'
        }}
      />
    )
  }
}


SliderTooltip.propTypes = {
  value: PropTypes.any,
  min: PropTypes.any,
  max: PropTypes.any,
  step: PropTypes.any,
  placement: PropTypes.any,
  onChange: PropTypes.func,
}

SliderTooltip.defaultProps = {
  value: 10000,
  min: 10000,
  max: 1100000,
  step: 50000,
  placement: 'bottom'
}
