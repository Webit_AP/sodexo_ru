import React, { Component } from 'react'
import PropTypes from 'prop-types'
const classNames = require('classnames')

import styles from './toggle.scss';

function noop () {
}

export default class Toggle extends Component {
  constructor (props) {
    super(props)

    let checked = false
    if ('checked' in props) {
      checked = !!props.checked
    } else {
      checked = !!props.defaultChecked
    }
    this.state = { checked }

    this.handleKeyDownBind = this.handleKeyDown.bind(this)
    this.toggleBind = this.toggle.bind(this)
    this.handleMouseUpBind = this.handleMouseUp.bind(this)
  }

  componentWillReceiveProps (nextProps) {
    if ('checked' in nextProps) {
      this.setState({
        checked: !!nextProps.checked,
      })
    }
  }

  setChecked (checked) {
    if (!('checked' in this.props)) {
      this.setState({
        checked,
      })
    }
    this.props.onChange(!checked)
  }

  toggle () {
    const checked = !this.state.checked
    this.setChecked(checked)
    window.dataLayer.push({'event':'click-simulator'})
  }

  handleKeyDown (e) {
    if (e.keyCode === 37) {
      this.setChecked(false)
    }
    if (e.keyCode === 39) {
      this.setChecked(true)
    }
  }

  // Handle auto focus when click switch in Chrome
  handleMouseUp (e) {
    if (this.refs.node) {
      this.refs.node.blur()
    }
    if (this.props.onMouseUp) {
      this.props.onMouseUp(e)
    }
  }

  render () {
    const { className, prefixCls, disabled, trueLabel, falseLabel, ...restProps } = this.props
    const checked = this.state.checked
    const switchClassName = classNames({
      [className]: !!className,
      [prefixCls]: true,
      [`${prefixCls}-checked`]: checked,
      [`${prefixCls}-disabled`]: disabled,
    })
    return (
      <div
        {...restProps}
        className={switchClassName}
        tabIndex={disabled ? -1 : 0}
        ref='node'
        onKeyDown={this.handleKeyDownBind}
        onClick={disabled ? noop : this.toggleBind}
        onMouseUp={this.handleMouseUpBind}
      >

        <span className={`${prefixCls}-false`}>{ trueLabel }</span>
        <span className={`${prefixCls}-true`}>{ falseLabel }</span>
      </div>
    )
  }
}

Toggle.propTypes = {
  className: PropTypes.string,
  prefixCls: PropTypes.string,
  disabled: PropTypes.bool,
  trueLabel: PropTypes.any,
  falseLabel: PropTypes.any,
  onChange: PropTypes.func,
  onMouseUp: PropTypes.func,
  checked: PropTypes.bool,
  defaultChecked: PropTypes.bool,
}

Toggle.defaultProps = {
  prefixCls: 'toggle',
  trueLabel: null,
  falseLabel: null,
  className: '',
  defaultChecked: false,
  onChange: noop
}
