export default class ItalyCardSimulator {
  constructor (state, dataParams) {
    /* Inputs Param Mocks */
    this.dataInputs = state.dataInputs

    let myTaxes = 0;
    if(this.dataInputs.valeurTitre > dataParams.valoreDefiscalizzato)
      myTaxes = this.dataInputs.valeurTitre - dataParams.valoreDefiscalizzato

    let budgetMensuelTotal = (this.dataInputs.valeurTitre * this.dataInputs.nombreTicketsResto * this.dataInputs.nombreSalaries) + ((myTaxes * (dataParams.inps / 100 + dataParams.tfr / 100 + dataParams.irap / 100 + dataParams.ires / 100)) * this.dataInputs.nombreSalaries * this.dataInputs.nombreTicketsResto)

    let participationEmployeur = this.dataInputs.valeurTitre * this.dataInputs.nombreSalaries * this.dataInputs.nombreTicketsResto + (this.dataInputs.valeurTitre * (dataParams.inps / 100 + dataParams.tfr / 100 + dataParams.irap / 100 + dataParams.ires / 100) *this.dataInputs.nombreSalaries * this.dataInputs.nombreTicketsResto)

    let economiesRealiseesAnnuellesEmployeur = participationEmployeur - budgetMensuelTotal

    let gainEmployee = (this.dataInputs.nombreTicketsResto * this.dataInputs.valeurTitre * (dataParams.inpsCarico / 100 + dataParams.irpef / 100)) - (myTaxes * (dataParams.inpsCarico / 100 + dataParams.irpef / 100) * this.dataInputs.nombreTicketsResto)

    this.dataOutputs = {
      budgetMensuelTotal: budgetMensuelTotal,
      participationEmployeur: participationEmployeur,
      pouvoirAchatAdditionnel: this.dataInputs.valeurTitre * this.dataInputs.nombreTicketsResto * participationEmployeur,
      economiesRealiseesAnnuellesEmployeur: economiesRealiseesAnnuellesEmployeur,
      gainEmployee: gainEmployee
    }
    
  }
}

