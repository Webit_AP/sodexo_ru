export default class SacrificeSimulator {

  /* End mock params */

  constructor (state, dataParams) {
    this.dataProducts = state.dataProducts

    this.dataParamsDefault = {
      secuMinBase: 13288.80,
      secuMaxBase: 43704,
      secuBasePercent: 6.35,
      oneChild: 2400,
      twoChild: 2700,
      threeChild: 4000,
      overThreeChild: 4500,
      fixedTaxReduce: 2000,
      childTaxReduce: 600,
      brackets: [
        {
          taxBracket: 0,
          taxPayed: 0,
          marginalTaxRate: 0.19
        },
        {
          taxBracket: 12450,
          taxPayed: 2365.50,
          marginalTaxRate: 0.24
        },
        {
          taxBracket: 20200,
          taxPayed: 4225.5,
          marginalTaxRate: 0.30
        },
        {
          taxBracket: 35200,
          taxPayed: 8725.5,
          marginalTaxRate: 0.37
        },
        {
          taxBracket: 60000,
          taxPayed: 17901.5,
          marginalTaxRate: 0.45
        }
      ],
    }

     /* Mock Params */
    this.dataParams = (typeof dataParams !== 'undefined') ? dataParams : this.dataParamsDefault
    /* Mock Params */

    this.personalFamilyReduction = 5550
    this.taxReduction = 2000
    this.firstChildTaxReduction = 2400
    this.secondChildTaxReduction = 2700
    this.thirdChildTaxReduction = 4000
    this.overThreeChildTaxReduction = 4500
    this.taxReducUnderThreeYearsOld = 2800

    /* Mock Selected Product From Front */
    this.dataProductsSelected = []
    this.dataProductsSelected['foodPassActivate'] = this.dataProducts.foodPass.selected
    this.dataProductsSelected['transportPassActivate'] = this.dataProducts.transportPass.selected
    this.dataProductsSelected['childcarePassActivate'] = this.dataProducts.childcarePass.selected
    this.dataProductsSelected['healthPassActivate'] = this.dataProducts.healthPass.selected
    this.dataProductsSelected['trainingPassActivate'] = this.dataProducts.trainingPass.selected
    
    /* End Mock Selected Product From Front */

    /* Mock inputs */
    this.dataInput = state.dataInput

    if (this.dataProductsSelected['foodPassActivate']) {
      this.dataInput.valeurTicketResto = this.dataProducts.foodPass.firstValue.value
      this.dataInput.nombreMoisResto = this.dataProducts.foodPass.secondValue.value
    } else {
      this.dataInput.valeurTicketResto = 0
      this.dataInput.nombreMoisResto = 0
    }

    if (this.dataProductsSelected['transportPassActivate']) {
      this.dataInput.dedomagementTransport = this.dataProducts.transportPass.firstValue.value
      this.dataInput.nombreMoisTransport = this.dataProducts.transportPass.secondValue.value
    } else {
      this.dataInput.dedomagementTransport = 0
      this.dataInput.nombreMoisTransport = 0
    }

    if (this.dataProductsSelected['childcarePassActivate']) {
      this.dataInput.montantGarderieMensuel = this.dataProducts.childcarePass.firstValue.value
      this.dataInput.nombreMoisGarderie = this.dataProducts.childcarePass.secondValue.value
    } else {
      this.dataInput.montantGarderieMensuel = 0
      this.dataInput.nombreMoisGarderie = 0
    }

    if (this.dataProductsSelected['healthPassActivate']) {
      this.dataInput.budgetAnnuelSante = this.dataProducts.healthPass.firstValue.value
      this.dataInput.nombreAssures = this.dataProducts.healthPass.secondValue.value
    } else {
      this.dataInput.budgetAnnuelSante = 0
      this.dataInput.nombreAssures = 0
    }

    if (this.dataProductsSelected['trainingPassActivate']) {
      this.dataInput.montantAnnuelFormation = this.dataProducts.trainingPass.firstValue.value
    } else {
      this.dataInput.montantAnnuelFormation = 0
    }

    /* End mock inputs */

    this.dataOutput = {
      socialSecurityTaxes: this.socialSecurityTaxesCalculation(),
      totalFacialBenefits: this.totalFacialBenefitsCalculation()
    }

    /* Without Salary Sacrifice simulation */
    this.subCalculationWithoutSacrifice = []
    this.subCalculationWithoutSacrifice['maxWithoutTax'] = this.maxWithoutTaxCalculation()
    this.subCalculationWithoutSacrifice['additionalReductionLowSalaries'] = this.additionalReductionForLawSalaryCalculationWithoutSalarySacrifice()
    this.subCalculationWithoutSacrifice['overTwoChildTaxReduction'] = this.twoChildTaxReduction()
    this.subCalculationWithoutSacrifice['sumDescendentTaxReductions'] = this.descendentTaxesReductionCalculation()
    this.subCalculationWithoutSacrifice['hastaReduction'] = this.hastaReductionCalculation()
    this.subCalculationWithoutSacrifice['restoReduction'] = this.restoReductionCalculation()
    this.subCalculationWithoutSacrifice['grossSalaryMinusReduction'] = this.grossSalaryMinusReductionsCalculationWithoutSalarySacrifice()
    this.subCalculationWithoutSacrifice['hastaTaxToPay'] = this.hastaTaxToPayCalculationWithoutSalarySacrifice()
    this.subCalculationWithoutSacrifice['restoTaxToPay'] = this.restoTaxToPayCalculationWithoutSalarySacrifice()
    this.subCalculationWithoutSacrifice['famillyReductionApplied'] = this.famillyReductionAppliedCalculationWithoutSalarySacrifice()
    this.subCalculationWithoutSacrifice['taxMinusReduction'] = this.taxMinusReductionCalculationWithoutSalarySacrifice()
    this.subCalculationWithoutSacrifice['selectiveMinimum'] = this.selectiveMinimumCalculationWithoutSalarySacrifice()


    /* With Salary Sacrifice Simulation */
    this.subCalculationWithSacrifice = []
    this.subCalculationWithSacrifice['maxWithoutTax'] = this.maxWithoutTaxCalculation()
    this.subCalculationWithSacrifice['grossSalaryForTaxesPurpose'] = this.grossSalaryForTaxesPurposes()
    this.subCalculationWithSacrifice['overTwoChildTaxReduction'] = this.twoChildTaxReduction()
    this.subCalculationWithSacrifice['additionalReductionLowSalaries'] = this.additionalReductionForLawSalaryCalculationWithSalarySacrifice()
    this.subCalculationWithSacrifice['grossSalaryMinusReduction'] = this.grossSalaryMinusReductionsCalculationWithSalarySacrifice()

    this.subCalculationWithSacrifice['sumDescendentTaxReductions'] = this.descendentTaxesReductionCalculation()
    this.subCalculationWithSacrifice['hastaReduction'] = this.hastaReductionCalculation()
    this.subCalculationWithSacrifice['restoReduction'] = this.restoReductionCalculation()

    this.subCalculationWithSacrifice['hastaTaxToPay'] = this.hastaTaxToPayCalculationWithSalarySacrifice()
    this.subCalculationWithSacrifice['restoTaxToPay'] = this.restoTaxToPayCalculationWithSalarySacrifice()

    this.subCalculationWithSacrifice['famillyReductionApplied'] = this.famillyReductionAppliedCalculationWithSalarySacrifice()
    this.subCalculationWithSacrifice['taxMinusReduction'] = this.taxMinusReductionCalculationWithSalarySacrifice()
    this.subCalculationWithSacrifice['selectiveMinimum'] = this.selectiveMinimumCalculationWithSalarySacrifice()

    this.dataOutput.totalAnnualTaxWithoutSalarySacrifice = this.totalAnnualTaxCalculationWithoutSalarySacrifice()
    this.dataOutput.totalAnnualTaxWithSalarySacrifice = this.totalAnnualTaxCalculationWithSalarySacrifice()
    this.dataOutput.netSalaryAvailableWithoutSalarySacrifice = this.netSalaryAvailableWithoutSalarySacrifice()
    this.dataOutput.netSalaryAvailableWithSalarySacrifice = this.netSalaryAvailableWithSalarySacrifice()
    this.dataOutput.annualEuroSave = this.annualEuroSaveCalculation()

    // console.log('dataInput', this.dataInput)
    // console.log('dataParams', this.dataParams)
    // console.log('dataProducts', this.dataProducts)
    // console.log('subCalculationWithoutSacrifice', this.subCalculationWithoutSacrifice)
    // console.log('subCalculationWithSacrifice', this.subCalculationWithSacrifice)
    // console.log('dataOutput', this.dataOutput)

  }

  /* Common Simulation */

  /**
   * =SI(C5<13288,8;13288,8*6,35%;SI(C5<43704;C5*6,35%;2775,204))
   */
  socialSecurityTaxesCalculation () {
    if (this.dataInput.salaireBrut < this.dataParams.secuMinBase) {
      return this.dataParams.secuMinBase * this.dataInput.salaireBrut / 100
    } else if (this.dataInput.salaireBrut < this.dataParams.secuMaxBase) {
      return this.dataParams.secuBasePercent * this.dataInput.salaireBrut / 100
    } else {
      return 2775.204
    }
  }

  twoChildTaxReduction () {
    if (this.totalChildren() > 2) {
      return 600
    } else {
      return 0
    }
  }

  totalChildren () {
    return this.dataInput.nbrChild
  }

  /**
   * 
   */
  totalFacialBenefitsCalculation () {
    return this.dataInput.valeurTicketResto * 20 * this.dataInput.nombreMoisResto + this.dataInput.dedomagementTransport * this.dataInput.nombreMoisTransport + this.dataInput.montantGarderieMensuel * this.dataInput.nombreMoisGarderie + this.dataInput.budgetAnnuelSante * this.dataInput.nombreAssures + this.dataInput.montantAnnuelFormation
  }

  maxWithoutTaxCalculation () {
    return this.dataInput.salaireBrut * 0.3
  }

  /**
   * Additional reduction for low salaries: 3.700 if gross salary  < 11.250; 0 otherwise
   */
  additionalReductionForLawSalaryCalculationWithoutSalarySacrifice () {
    if (this.dataInput.salaireBrut - this.dataOutput.socialSecurityTaxes < 11250) {
      return 3700
    } else if (this.dataInput.salaireBrut - this.dataOutput.socialSecurityTaxes < 14450) {
      return 3700 - 1.15625 * ((this.dataInput.salaireBrut - this.dataOutput.socialSecurityTaxes) - 11250)
    } else {
      return 0
    }
  }

  additionalReductionForLawSalaryCalculationWithSalarySacrifice () {
    if (this.subCalculationWithSacrifice.grossSalaryForTaxesPurpose - this.dataOutput.socialSecurityTaxes < 11250) {
      return 3700
    } else if (this.subCalculationWithSacrifice.grossSalaryForTaxesPurpose - this.dataOutput.socialSecurityTaxes < 14450) {
      return 3700 - 1.15625 * ((this.subCalculationWithSacrifice.grossSalaryForTaxesPurpose - this.dataOutput.socialSecurityTaxes) - 11250)
    } else {
      return 0
    }
  }

  grossSalaryForTaxesPurposes () {
    if (this.dataOutput.totalFacialBenefits < this.subCalculationWithSacrifice.maxWithoutTax) {
      return this.dataInput.salaireBrut - this.dataOutput.totalFacialBenefits
    } else {
      return this.dataInput.salaireBrut - this.subCalculationWithSacrifice.maxWithoutTax
    }
  }
  grossSalaryMinusReductionsCalculationWithoutSalarySacrifice () {
    return this.dataInput.salaireBrut - this.taxReduction - this.subCalculationWithoutSacrifice.additionalReductionLowSalaries - this.subCalculationWithoutSacrifice.overTwoChildTaxReduction - this.dataOutput.socialSecurityTaxes
  }

  grossSalaryMinusReductionsCalculationWithSalarySacrifice () {
    return this.subCalculationWithSacrifice.grossSalaryForTaxesPurpose - this.dataOutput.socialSecurityTaxes - this.taxReduction - this.subCalculationWithSacrifice.overTwoChildTaxReduction - this.subCalculationWithSacrifice.additionalReductionLowSalaries
  }

  descendentTaxesReductionCalculation () {
    let childTaxReduction = 0
    let joinTaxDeclaration = 0

    if (this.totalChildren() > 0) {
      childTaxReduction = this.firstChildTaxReduction
    }

    if (this.totalChildren() > 1) {
      childTaxReduction = childTaxReduction + this.secondChildTaxReduction
    }

    if (this.totalChildren() > 2) {
      childTaxReduction = childTaxReduction + this.thirdChildTaxReduction
    }

    if (this.totalChildren() > 3) {
      childTaxReduction = childTaxReduction + (this.overThreeChildTaxReduction * (this.totalChildren() - 3))
    }

    if (this.dataInput.nbrChildUnderThree > 0) {
      childTaxReduction = childTaxReduction + this.dataInput.nbrChildUnderThree * this.taxReducUnderThreeYearsOld
    }

    if (!this.dataInput.declarationCommune) {
      joinTaxDeclaration = childTaxReduction * 0.5
    }
    return childTaxReduction + this.personalFamilyReduction - joinTaxDeclaration
  }

  hastaReductionCalculation () {
    let nbrbrackets = this.dataParams.brackets.length

    for (var i = 0; i < nbrbrackets; i++) {
      if (this.descendentTaxesReductionCalculation() < this.dataParams.brackets[i]['taxBracket']) {
        return this.dataParams.brackets[i - 1]['taxPayed']
      }
    }
  }

  restoReductionCalculation () {
    let nbrbrackets = this.dataParams.brackets.length
    for (var i = 0; i < nbrbrackets; i++) {
      if (this.dataParams.brackets[i]['taxPayed'] == this.hastaReductionCalculation()) {
        return (this.descendentTaxesReductionCalculation() - this.dataParams.brackets[i]['taxBracket']) * this.dataParams.brackets[i]['marginalTaxRate']
      }
    }
  }

  hastaTaxToPayCalculationWithoutSalarySacrifice () {
    let nbrBrackets = this.dataParams.brackets.length
    for (var i = 0; i < nbrBrackets; i++) {
      if (this.subCalculationWithoutSacrifice.grossSalaryMinusReduction < this.dataParams.brackets[i]['taxBracket']) {
        return this.dataParams.brackets[i - 1]['taxPayed']
      }
    }
    return this.dataParams.brackets[nbrBrackets - 1]['taxPayed']
  }

  hastaTaxToPayCalculationWithSalarySacrifice () {
    let nbrBrackets = this.dataParams.brackets.length
    for (var i = 0; i < nbrBrackets; i++) {
      if (this.subCalculationWithSacrifice.grossSalaryMinusReduction < this.dataParams.brackets[i]['taxBracket']) {
        return this.dataParams.brackets[i - 1]['taxPayed']
      }
    }
    return this.dataParams.brackets[nbrBrackets - 1]['taxPayed']
  }

  restoTaxToPayCalculationWithoutSalarySacrifice () {
    let nbrBrackets = this.dataParams.brackets.length
    for (var i = 0; i < nbrBrackets; i++) {
      if (this.hastaTaxToPayCalculationWithoutSalarySacrifice() === this.dataParams.brackets[i]['taxPayed']) {
        return (this.subCalculationWithoutSacrifice.grossSalaryMinusReduction - this.dataParams.brackets[i]['taxBracket']) * this.dataParams.brackets[i]['marginalTaxRate']
      }
    }
  }

  restoTaxToPayCalculationWithSalarySacrifice () {
    let nbrBrackets = this.dataParams.brackets.length
    for (var i = 0; i < nbrBrackets; i++) {
      if (this.hastaTaxToPayCalculationWithSalarySacrifice() === this.dataParams.brackets[i]['taxPayed']) {
        return (this.subCalculationWithSacrifice.grossSalaryMinusReduction - this.dataParams.brackets[i]['taxBracket']) * this.dataParams.brackets[i]['marginalTaxRate']
      }
    }
  }

  famillyReductionAppliedCalculationWithoutSalarySacrifice () {
    return this.subCalculationWithoutSacrifice.hastaReduction + this.subCalculationWithoutSacrifice.restoReduction
  }

  famillyReductionAppliedCalculationWithSalarySacrifice () {
    return this.subCalculationWithSacrifice.hastaReduction + this.subCalculationWithSacrifice.restoReduction
  }

  taxMinusReductionCalculationWithoutSalarySacrifice () {
    if (this.subCalculationWithoutSacrifice.hastaTaxToPay + this.subCalculationWithoutSacrifice.restoTaxToPay - this.famillyReductionAppliedCalculationWithoutSalarySacrifice() < 0) {
      return 0
    } else {
      return this.subCalculationWithoutSacrifice.hastaTaxToPay + this.subCalculationWithoutSacrifice.restoTaxToPay - this.famillyReductionAppliedCalculationWithoutSalarySacrifice()
    }
  }

  taxMinusReductionCalculationWithSalarySacrifice () {
    if (this.subCalculationWithSacrifice.hastaTaxToPay + this.subCalculationWithSacrifice.restoTaxToPay - this.famillyReductionAppliedCalculationWithSalarySacrifice() < 0) {
      return 0
    } else {
      return this.subCalculationWithSacrifice.hastaTaxToPay + this.subCalculationWithSacrifice.restoTaxToPay - this.famillyReductionAppliedCalculationWithSalarySacrifice()
    }
  }

  selectiveMinimumCalculationWithoutSalarySacrifice () {
    if (this.totalChildren() == 0) {
      return (this.dataInput.salaireBrut - 12000) * 0.43
    } else if (this.totalChildren() == 1) {
      return (this.dataInput.salaireBrut - 12607) * 0.43
    } else {
      return (this.dataInput.salaireBrut - 13275) * 0.43
    }
  }

  selectiveMinimumCalculationWithSalarySacrifice () {
    if (this.totalChildren() == 0) {
      return (this.subCalculationWithSacrifice['grossSalaryForTaxesPurpose'] - 12000) * 0.43
    } else if (this.totalChildren() == 1) {
      return (this.subCalculationWithSacrifice['grossSalaryForTaxesPurpose'] - 12607) * 0.43
    } else {
      return (this.subCalculationWithSacrifice['grossSalaryForTaxesPurpose'] - 13275) * 0.43
    }
  }

  totalAnnualTaxCalculationWithoutSalarySacrifice () {
    if (this.subCalculationWithoutSacrifice['taxMinusReduction'] > this.subCalculationWithoutSacrifice['selectiveMinimum']) {
      if (this.subCalculationWithoutSacrifice['selectiveMinimum'] > 0) {
        return this.subCalculationWithoutSacrifice['selectiveMinimum']
      } else {
        return 0
      }
    } else {
      if (this.subCalculationWithoutSacrifice['taxMinusReduction'] > 0) {
        return this.subCalculationWithoutSacrifice['taxMinusReduction']
      } else {
        return 0
      }
    }
  }

  totalAnnualTaxCalculationWithSalarySacrifice () {
    if (this.subCalculationWithSacrifice['taxMinusReduction'] > this.subCalculationWithSacrifice['selectiveMinimum'] ) {
      if (this.subCalculationWithSacrifice['selectiveMinimum'] > 0) {
        return this.subCalculationWithSacrifice['selectiveMinimum']
      } else {
        return 0
      }
    } else {
      if (this.subCalculationWithSacrifice['taxMinusReduction'] > 0) {
        return this.subCalculationWithSacrifice['taxMinusReduction']
      } else {
        return 0
      }
    }
  }

  netSalaryAvailableWithoutSalarySacrifice () {
    return this.dataInput.salaireBrut - this.dataOutput['socialSecurityTaxes'] - this.dataOutput['totalFacialBenefits'] - this.dataOutput['totalAnnualTaxWithoutSalarySacrifice']
  }

  netSalaryAvailableWithSalarySacrifice () {
    return this.dataInput.salaireBrut - this.dataOutput['socialSecurityTaxes'] - this.dataOutput['totalFacialBenefits'] - this.dataOutput['totalAnnualTaxWithSalarySacrifice']
  }

  annualEuroSaveCalculation () {
    return this.dataOutput['totalAnnualTaxWithoutSalarySacrifice'] - this.dataOutput['totalAnnualTaxWithSalarySacrifice']
  }
}
