
export default class FoodChildcareTransportSimulator {
  constructor (salario, regular_contribution, numero_emplea_medio, max_regular_contribution, factor_for_year, dataParams) {
    // Inputs
    this.salario_medio = salario
    this.regular_contribution = regular_contribution
    this.numero_emplea_medio = numero_emplea_medio

    this.total_per_year = ''
    if (this.regular_contribution <= max_regular_contribution) {
      this.total_per_year = this.regular_contribution * factor_for_year
    }
    this.salario_with_cheques = this.salario_medio + this.total_per_year

    this.with_vouchers = {
      social_security : 0,
      other_expenses : 2000,
      reduction_low_salaries : 0,
      base_taxe_calculate : 0
    }
    this.with_salary_raise = {
      social_security : 0,
      other_expenses : 2000,
      reduction_low_salaries : 0,
      base_taxe_calculate : 0
    }

    this.sodexo_initialize_salary_vouchers_vars('with_vouchers', this.salario_medio)
    this.sodexo_initialize_salary_vouchers_vars('with_salary_raise', this.salario_with_cheques)

    this.minimum_reduction = 1054.5

    this.tax_bracket_first_level = 0
    this.tax_payed_up_to_each_bracket_level_1 = 0
    this.remainder_level_1 = 12450
    this.marginal_tax_rate_level_1 = 0.19
    this.average_tax_rate_level_1 = 0.19
    this.levels = [
      {
        tax_bracket: 0,
        tax_payed_up_to_each_bracket: 0,
        marginal_tax_rate: 0.19
      },
      {
        tax_bracket: 12450,
        tax_payed_up_to_each_bracket: 2365.50,
        marginal_tax_rate: 0.24
      },
      {
        tax_bracket: 20200,
        tax_payed_up_to_each_bracket: 4225.5,
        marginal_tax_rate: 0.30
      },
      {
        tax_bracket: 35200,
        tax_payed_up_to_each_bracket: 8725.5,
        marginal_tax_rate: 0.37
      },
      {
        tax_bracket: 60000,
        tax_payed_up_to_each_bracket: 17901.5,
        marginal_tax_rate: 0.45
      }
    ]

    // this.tax_bracket_first_level = dataParams.tax_bracket_first_level
    // this.tax_payed_up_to_each_bracket_level_1 = dataParams.tax_payed_up_to_each_bracket_level_1
    // this.remainder_level_1 = dataParams.remainder_level_1
    // this.marginal_tax_rate_level_1 = dataParams.marginal_tax_rate_level_1
    // this.average_tax_rate_level_1 = dataParams.average_tax_rate_level_1
    // this.levels = dataParams.levels;

    this.tipos_bases_hasta_resto = []

    this.dataOutput = this.sodexo_return_response()
  }

  sodexo_initialize_salary_vouchers_vars (mode, salario) {
    this.sodexo_initialize_salary_vouchers__social_security(mode, salario)
    this.sodexo_initialize_salary_vouchers__reduction_low_salaries(mode, salario)
    this.sodexo_initialize_salary_vouchers__base_taxe_calculate(mode, salario)
  }

  sodexo_initialize_salary_vouchers__social_security (mode, salario) {
    if (salario < 13288.8) {
      this[mode].social_security = 13288.8 * 0.0635
    } else if (salario < 43704) {
      this[mode].social_security = salario * 0.0635
    } else {
      this[mode].social_security = 2775.204
    }
  }

  sodexo_initialize_salary_vouchers__reduction_low_salaries (mode, salario) {
    if ((salario - this[mode].social_security) < 11250) {
      this[mode].reduction_low_salaries = 3700
    } else if ((salario - this[mode].social_security) < 14450) {
      this[mode].reduction_low_salaries = 3700 - 1.15625 * ((salario - this[mode].social_security) - 11250)
    } else {
      this[mode].reduction_low_salaries = 0
    }
  }

  sodexo_initialize_salary_vouchers__base_taxe_calculate (mode, salario) {
    this[mode].base_taxe_calculate = salario - this[mode].social_security - this[mode].other_expenses - this[mode].reduction_low_salaries
  }

  sodexo_simulator_sumprod (array, index1, index2) {
    let result = 0
    for (let i = 0; i < array.length; i++) {
      result += array[i][index1] * array[i][index2]
    }

    return result
  }

  sodexo_simulator_tipo_hasta_IRPF_medio (average_wage, tax_bracket_average_tax_rate) {
    let tipo_hasta_IRPF_medio = 0
    let index = 1

    for (let index = 1; index < tax_bracket_average_tax_rate.length; index++) {
      if (average_wage < tax_bracket_average_tax_rate[index]['tax_bracket']) {
        tipo_hasta_IRPF_medio = 0
        if (tax_bracket_average_tax_rate[index - 2] !== undefined) {
          tipo_hasta_IRPF_medio = tax_bracket_average_tax_rate[index - 2]['average_tax_rate']
        }
        break
      } else if (index == tax_bracket_average_tax_rate.length - 1 && average_wage > tax_bracket_average_tax_rate[index]['tax_bracket']) {
        tipo_hasta_IRPF_medio = tax_bracket_average_tax_rate[index - 1]['average_tax_rate']
      }
    }

    return tipo_hasta_IRPF_medio
  }

  sodexo_simulator_tipo_resto_IRPF_medio (irpf_medio, average_tax_rate) {
    if (irpf_medio == 0) {
      return this.marginal_tax_rate_level_1
    } else if (irpf_medio && average_tax_rate) {
      for (let i = 0; i < average_tax_rate.length; i++) {
        if (irpf_medio == average_tax_rate[i]['average_tax_rate']) {
          if (average_tax_rate[i + 1] !== undefined) {
            return average_tax_rate[i + 1]['marginal_tax_rate']
          } else {
            return average_tax_rate[average_tax_rate.length - 1]['marginal_tax_rate']
          }
        }
      }
    }
  }

  sodexo_simulator_base_hasta_IRPF_medio (tipo_resto_IRPF_medio, average_tax_rate) {
    if (tipo_resto_IRPF_medio == 0) {
      return 0
    } else if (tipo_resto_IRPF_medio && average_tax_rate) {
      for (let i = 0; average_tax_rate.length; i++) {
        if (tipo_resto_IRPF_medio == average_tax_rate[i]['marginal_tax_rate']) {
          return average_tax_rate[i]['tax_bracket']
        }
      }
    }
  }

  sodexo_simulator_base_resto_IRPF_medio (tipo_hasta_IRPF_medio, average_tax_rate, salario_medio) {
    if (tipo_hasta_IRPF_medio == 0) {
      return salario_medio
    } else if (tipo_hasta_IRPF_medio && average_tax_rate && salario_medio) {
      for (let i = 0; average_tax_rate.length; i++) {
        if (tipo_hasta_IRPF_medio == average_tax_rate[i]['average_tax_rate']) {
          return salario_medio - average_tax_rate[i + 1]['tax_bracket']
        }
      }
    }
  }

  sodexo_return_response () {
    let taxes_and_rates_one = []
    taxes_and_rates_one['tax_bracket'] = this.tax_bracket_first_level
    taxes_and_rates_one['tax_payed_up_to_each_bracket'] = this.tax_payed_up_to_each_bracket_level_1 / 100
    taxes_and_rates_one['remainder'] = this.remainder_level_1
    taxes_and_rates_one['marginal_tax_rate'] = this.marginal_tax_rate_level_1
    taxes_and_rates_one['average_tax_rate'] = this.average_tax_rate_level_1

    let taxes_and_rates = []
    taxes_and_rates = [taxes_and_rates_one]

    let levels_size = this.levels.length
    for (let index = 1; index < this.levels.length; index++) {
      let line_tax_and_rates = []

      line_tax_and_rates['tax_bracket'] = this.levels[index]['tax_bracket']

      line_tax_and_rates['tax_payed_up_to_each_bracket'] = taxes_and_rates[index - 1]['tax_payed_up_to_each_bracket'] + (taxes_and_rates[index - 1]['remainder'] * taxes_and_rates[index - 1]['marginal_tax_rate'])

      line_tax_and_rates['remainder'] = ''
      if (index < (levels_size - 1)) {
        line_tax_and_rates['remainder'] = this.levels[index + 1]['tax_bracket'] - line_tax_and_rates['tax_bracket']
      }

      line_tax_and_rates['marginal_tax_rate'] = this.levels[index]['marginal_tax_rate']

      taxes_and_rates.push(line_tax_and_rates)

      line_tax_and_rates['average_tax_rate'] = ''
      if (index < (levels_size - 1)) {
        let sum_prod = this.sodexo_simulator_sumprod(taxes_and_rates, 'remainder', 'marginal_tax_rate')
        let rate = sum_prod / this.levels[index + 1]['tax_bracket']
        line_tax_and_rates['average_tax_rate'] = rate
      }
      taxes_and_rates[index]['average_tax_rate'] = line_tax_and_rates['average_tax_rate']
    }

    this.tipos_bases_hasta_resto['tipo_hasta_IRPF_medio'] = this.sodexo_simulator_tipo_hasta_IRPF_medio(this.with_vouchers.base_taxe_calculate, taxes_and_rates)
    this.tipos_bases_hasta_resto['tipo_hasta_IRPF_ticket'] = this.sodexo_simulator_tipo_hasta_IRPF_medio(this.with_salary_raise.base_taxe_calculate, taxes_and_rates)

    this.tipos_bases_hasta_resto['tipo_resto_IRPF_medio'] = this.sodexo_simulator_tipo_resto_IRPF_medio(this.tipos_bases_hasta_resto['tipo_hasta_IRPF_medio'], taxes_and_rates)
    this.tipos_bases_hasta_resto['tipo_resto_IRPF_ticket'] = this.sodexo_simulator_tipo_resto_IRPF_medio(this.tipos_bases_hasta_resto['tipo_hasta_IRPF_ticket'], taxes_and_rates)

    this.tipos_bases_hasta_resto['base_hasta_IRPF_medio'] = this.sodexo_simulator_base_hasta_IRPF_medio(this.tipos_bases_hasta_resto['tipo_resto_IRPF_medio'], taxes_and_rates)
    this.tipos_bases_hasta_resto['base_hasta_IRPF_ticket'] = this.sodexo_simulator_base_hasta_IRPF_medio(this.tipos_bases_hasta_resto['tipo_resto_IRPF_ticket'], taxes_and_rates)

    this.tipos_bases_hasta_resto['base_resto_IRPF_medio'] = this.sodexo_simulator_base_resto_IRPF_medio(this.tipos_bases_hasta_resto['tipo_hasta_IRPF_medio'], taxes_and_rates, this.salario_medio)
    this.tipos_bases_hasta_resto['base_resto_IRPF_ticket'] = this.sodexo_simulator_base_resto_IRPF_medio(this.tipos_bases_hasta_resto['tipo_hasta_IRPF_ticket'], taxes_and_rates, this.salario_with_cheques)

    this.tipos_bases_hasta_resto['retencion_IRPF_medio'] = this.tipos_bases_hasta_resto['base_hasta_IRPF_medio'] * this.tipos_bases_hasta_resto['tipo_hasta_IRPF_medio'] + this.tipos_bases_hasta_resto['base_resto_IRPF_medio'] * this.tipos_bases_hasta_resto['tipo_resto_IRPF_medio'] - this.minimum_reduction
    this.tipos_bases_hasta_resto['retencion_IRPF_ticket'] = this.tipos_bases_hasta_resto['base_hasta_IRPF_ticket'] * this.tipos_bases_hasta_resto['tipo_hasta_IRPF_ticket'] + this.tipos_bases_hasta_resto['base_resto_IRPF_ticket'] * this.tipos_bases_hasta_resto['tipo_resto_IRPF_ticket'] - this.minimum_reduction

    let difference = this.tipos_bases_hasta_resto['retencion_IRPF_ticket'] - this.tipos_bases_hasta_resto['retencion_IRPF_medio']
    let ticket_difference = this.total_per_year - difference
    let taxes_saved = (difference / this.total_per_year) * 100
    let company_spent = this.total_per_year * this.numero_emplea_medio
    let employees_save = this.numero_emplea_medio * difference

    let response = {
      retencion_IRPF_medio: this.tipos_bases_hasta_resto['retencion_IRPF_medio'],
      retencion_IRPF_ticket: this.tipos_bases_hasta_resto['retencion_IRPF_ticket'],
      taxes_saved: taxes_saved,
      company_spent: company_spent,
      employees_save: employees_save
    }

    return response
  }
}
