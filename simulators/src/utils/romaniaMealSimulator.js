export default class RomaniaMealSimulator {
  constructor (state, dataParams) {
    /* Inputs Param Mocks */
    this.dataInputs = state.dataInputs

    let budgetMensuelTotal = this.dataInputs.valeurTitre * this.dataInputs.nombreSalaries * this.dataInputs.nombreTicketsResto

    let participationEmployeur = this.dataInputs.nombreTicketsResto * this.dataInputs.valeurTitre * (dataParams.incomeTax / 100)

    let pouvoirAchatAdditionnel = ((1 - dataParams.incomeTax / 100) / (1 - dataParams.employerCost) - 1) / ((1 - dataParams.incomeTax / 100) / (1 - dataParams.employerCost)) * 100

    let gainEmployer = this.dataInputs.nombreSalaries * this.dataInputs.nombreTicketsResto * this.dataInputs.valeurTitre * ((1 - dataParams.incomeTax / 100) / (1 - dataParams.employerCost) - 1)

    this.dataOutputs = {
      budgetMensuelTotal: budgetMensuelTotal * 12,
      participationEmployeur: participationEmployeur * 12,
      pouvoirAchatAdditionnel: pouvoirAchatAdditionnel,
      gainEmployer: gainEmployer * 12
    }
    
  }
}

