export default class FranceSimulator {
  constructor (state, dataParams) {
    /* Inputs Param Mocks */

    this.dataInputs = state.dataInputs
    // this.dataInputs = {
    //   valeurTitre: 10,
    //   tauxAugmentationSouhaite: 0,
    //   nombreSalaries: 10,
    //   nombreTicketsResto: 20,
    //   participationEmployeur: 0.60,
    //   salaireMensuelBrut: 2000,
    // }
    /* END Inputs Param Mocks */

    /* Params Mocks */
    this.dataParamsDefault = {
      limiteExonerationCharge: dataParams.limite_exoneration_charge,
      chargesPatronales: 0.55,
      chargesSalariales: 0.20,
    }

    /* End params Mock */


    this.dataSubCalculationEmployee = []
    this.dataSubCalculationCompany = []

    this.dataSubCalculationCompany['budgetMensuelTotal'] = this.dataInputs['nombreSalaries'] * this.dataInputs['valeurTitre'] * this.dataInputs['nombreTicketsResto'] * this.dataInputs['participationEmployeur']
    this.dataSubCalculationCompany['coFinancement'] = this.dataInputs['valeurTitre'] * this.dataInputs['participationEmployeur']

    this.dataSubCalculationEmployee['budgetMensuelTotal'] = this.dataInputs['valeurTitre'] * this.dataInputs['nombreTicketsResto'] * (1 - this.dataInputs['participationEmployeur'])
    this.dataSubCalculationEmployee['coFinancement'] = this.dataInputs['valeurTitre'] * (1 - this.dataInputs['participationEmployeur'])
    this.dataSubCalculationEmployee['pouvoirAchatSup'] = this.dataInputs['valeurTitre'] * this.dataInputs['nombreTicketsResto'] * this.dataInputs['participationEmployeur']

    this.dataSubCalculationCompany['augmentationDeSalaire'] = []
    this.dataSubCalculationCompany['misePlaceTicketResto'] = []
    this.dataSubCalculationEmployee['augmentationDeSalaire'] = []
    this.dataSubCalculationEmployee['misePlaceTicketResto'] = []

    // Cas en dessous de la limite d'exonération de charges
    if (this.dataInputs['valeurTitre'] * this.dataInputs['participationEmployeur'] < this.dataParamsDefault['limiteExonerationCharge']) {
      this.dataSubCalculationCompany['augmentationDeSalaire']['coutMensuel'] = this.dataSubCalculationCompany['budgetMensuelTotal'] * (1 + this.dataParamsDefault['chargesPatronales'])
      this.dataSubCalculationCompany['augmentationDeSalaire']['coutAnnuel'] = this.dataSubCalculationCompany['augmentationDeSalaire']['coutMensuel'] * 11
      //this.dataSubCalculationCompany['misePlaceTicketResto']['coutMensuel'] = this.dataInputs['nombreSalaries'] * this.dataInputs['valeurTitre'] * this.dataInputs['nombreTicketsResto'] * this.dataInputs['participationEmployeur']
      this.dataSubCalculationCompany['misePlaceTicketResto']['coutMensuel'] = this.dataInputs.nombreSalaries * this.dataInputs.nombreTicketsResto * this.dataInputs.valeurTitre * this.dataInputs.participationEmployeur
      this.dataSubCalculationCompany['misePlaceTicketResto']['coutAnnuel'] = this.dataSubCalculationCompany['misePlaceTicketResto']['coutMensuel'] * 11
      this.dataSubCalculationCompany['economiesRealiseesMensuelles'] = this.dataSubCalculationCompany['augmentationDeSalaire']['coutMensuel'] - this.dataSubCalculationCompany['misePlaceTicketResto']['coutMensuel']
      this.dataSubCalculationCompany['economiesRealiseesAnnuelles'] = this.dataSubCalculationCompany['economiesRealiseesMensuelles'] * 11

      this.dataSubCalculationEmployee['augmentationDeSalaire']['coutMensuel'] = this.dataInputs['valeurTitre'] * this.dataInputs['nombreTicketsResto'] * this.dataInputs['participationEmployeur'] * (1 - this.dataParamsDefault['chargesSalariales'])
      this.dataSubCalculationEmployee['augmentationDeSalaire']['coutAnnuel'] = this.dataSubCalculationEmployee['augmentationDeSalaire']['coutMensuel'] * 11
      this.dataSubCalculationEmployee['misePlaceTicketResto']['coutMensuel'] = this.dataInputs['valeurTitre'] * this.dataInputs['nombreTicketsResto'] * (1 - this.dataInputs['participationEmployeur'])
      this.dataSubCalculationEmployee['misePlaceTicketResto']['coutAnnuel'] = this.dataSubCalculationEmployee['misePlaceTicketResto']['coutMensuel'] * 11
    } else { // Cas au dessus de la limite d'exonération de charges
      this.dataSubCalculationCompany['augmentationDeSalaire']['coutMensuel'] = this.dataSubCalculationCompany['budgetMensuelTotal'] * (1 + this.dataParamsDefault['chargesPatronales'])
      this.dataSubCalculationCompany['augmentationDeSalaire']['coutAnnuel'] = this.dataSubCalculationCompany['augmentationDeSalaire']['coutMensuel']*11
      // this.dataSubCalculationCompany['misePlaceTicketResto']['coutMensuel'] = (this.dataParamsDefault['limiteExonerationCharge']*this.dataInputs['nombreSalaries'] * this.dataInputs['nombreTicketsResto']) + (0.62 * this.dataInputs['nombreSalaries'] * this.dataInputs['nombreTicketsResto'] * (1 - this.dataParamsDefault['chargesPatronales']))
      
      this.dataSubCalculationCompany['misePlaceTicketResto']['coutMensuel'] = (this.dataParamsDefault['limiteExonerationCharge'] * this.dataInputs.nombreSalaries * this.dataInputs.nombreTicketsResto) + (((this.dataInputs.valeurTitre * this.dataInputs.participationEmployeur) - this.dataParamsDefault['limiteExonerationCharge']) * this.dataInputs['nombreSalaries'] * this.dataInputs.nombreTicketsResto * (1 + this.dataParamsDefault['chargesPatronales']))
      
      
      this.dataSubCalculationCompany['misePlaceTicketResto']['coutAnnuel'] = this.dataSubCalculationCompany['misePlaceTicketResto']['coutMensuel'] * 11
      this.dataSubCalculationCompany['economiesRealiseesMensuelles'] = this.dataSubCalculationCompany['augmentationDeSalaire']['coutMensuel'] - this.dataSubCalculationCompany['misePlaceTicketResto']['coutMensuel']
      this.dataSubCalculationCompany['economiesRealiseesAnnuelles'] = this.dataSubCalculationCompany['economiesRealiseesMensuelles'] * 11

      this.dataSubCalculationEmployee['augmentationDeSalaire']['coutMensuel'] = this.dataInputs['valeurTitre'] * this.dataInputs['nombreTicketsResto'] * this.dataInputs['participationEmployeur'] * (1 - this.dataParamsDefault['chargesSalariales'])
      this.dataSubCalculationEmployee['augmentationDeSalaire']['coutAnnuel'] = this.dataSubCalculationEmployee['augmentationDeSalaire']['coutMensuel'] * 11
      this.dataSubCalculationEmployee['misePlaceTicketResto']['coutMensuel'] = this.dataInputs['valeurTitre'] * this.dataInputs['nombreTicketsResto'] * (1 - this.dataInputs['participationEmployeur'])
      this.dataSubCalculationEmployee['misePlaceTicketResto']['coutAnnuel'] = this.dataSubCalculationEmployee['misePlaceTicketResto']['coutMensuel'] * 11
    }

    let participationEmployeur = this.dataSubCalculationCompany['misePlaceTicketResto']['coutMensuel'] * 11

    let coFinancement = this.dataSubCalculationEmployee['budgetMensuelTotal'] * 11

    this.dataOutputs = {
      participationEmployeur: participationEmployeur,
      coFinancement: coFinancement,    
      budgetMensuelTotal: participationEmployeur + (coFinancement * this.dataInputs['nombreSalaries']),
      gainPouvoirAchatPourcentage :  ((this.dataSubCalculationEmployee['augmentationDeSalaire']['coutAnnuel'] * 100) / this.dataSubCalculationEmployee['misePlaceTicketResto']['coutAnnuel']),
      pouvoirAchatAdditionnel: this.dataInputs.valeurTitre * this.dataInputs.nombreTicketsResto * this.dataInputs.participationEmployeur * 11,
      economiesRealiseesAnnuellesEmployeur: this.dataSubCalculationCompany['economiesRealiseesAnnuelles'],
      economiesRealiseesAnnuellesEmployee: this.dataSubCalculationEmployee['gainPouvoirAchatAnnuel'],
      dataSubCalculationCompany: this.dataSubCalculationCompany,
      dataSubCalculationEmployee: this.dataSubCalculationEmployee,
      gainEmployee: (this.dataSubCalculationEmployee.pouvoirAchatSup - this.dataSubCalculationEmployee['augmentationDeSalaire']['coutMensuel']) * 11


    }
    
    console.log(this.dataOutputs)


  }
}

