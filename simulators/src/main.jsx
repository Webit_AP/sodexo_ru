import React from 'react'
import ReactDOM from 'react-dom'

if (__DEV__) {
  require('./styles/dev.scss')
}

require('./styles/main.scss')

let capitalizeOne = (str) => {
  return str.charAt(0).toUpperCase().concat(str.slice(1).toLowerCase())
}

const __COUNTRY__ = capitalizeOne(process.argv.COUNTRY)
const __PRODUCT__ = capitalizeOne(process.argv.PRODUCT)

// Render Setup
// ------------------------------------
const MOUNT_NODE = document.getElementById('root')

let render = () => {
  const App = require(`./views/${__COUNTRY__}/${__PRODUCT__}`).default
  ReactDOM.render(
    <App />,
    MOUNT_NODE
  )
}

// Development Tools
// ------------------------------------
if (__DEV__) {
  if (module.hot) {
    const renderApp = render
    const renderError = (error) => {
      const RedBox = require('redbox-react').default

      ReactDOM.render(<RedBox error={error} />, MOUNT_NODE)
    }

    render = () => {
      try {
        console.log(__COUNTRY__, __PRODUCT__)
        renderApp()
      } catch (e) {
        console.error(e)
        renderError(e)
      }
    }

    // Setup hot module replacement
    module.hot.accept([
      `./views/${__COUNTRY__}/${__PRODUCT__}`
    ], () =>
      setImmediate(() => {
        // console.log(MOUNT_NODE, __COUNTRY__, __PRODUCT__)
        // debugger;
        ReactDOM.unmountComponentAtNode(MOUNT_NODE)
        render()
      })
    )
  }
}

// Let's Go!
// ------------------------------------
if (!__TEST__) render()
