# SODEXO SIMULATORS

## Table of Contents
1. [Requirements](#requirements)
1. [Installation](#getting-started)
1. [Running the Project](#running-the-project)
1. [Project Structure](#project-structure)
1. [Components](#components)
1. [Live Development](#local-development)
    * [Hot Reloading](#hot-reloading)
1. [Building for Production](#building-for-production)
1. [Deployment](#deployment)

## Requirements
* node `^5.0.0`
* yarn `^0.23.0` or npm `^3.0.0`

## Installation

After confirming that your environment meets the above [requirements](#requirements), you can create a new project based on `react-redux-starter-kit` by doing the following:

```bash
$ git clone https://<your-lion-username>40publicisgroupe.net@tools.publicis.sapient.com/bitbucket/scm/soxo/back-wp.git <my-project-name>

$ cd <my-project-name>/simulators
```

When that's done, install the project dependencies. It is recommended that you use [Yarn](https://yarnpkg.com/) for deterministic dependency management, but `npm install` will suffice.

```bash
$ yarn  # Install project dependencies
$ cd src/views/<Country>/<Product>*.jsx
```

In the method `_getDataParam()`, replace the URL in the conditon `__DEV__` with your gulp URL (ex: http://192.168.1.40:3000/front-wp-dev/)

## Running the Project

After completing the [installation](#installation) step, you're ready to start the project!

```bash
$ yarn start <country> <product>  # Start the development server (For ex.: `yarn start france` or `yarn start spain salary`)
```

While developing, you will probably rely mostly on `yarn start`; however, there are additional scripts at your disposal:

|`yarn <script>`    |Description|
|-------------------|-----------|
|`start <country> <product>`            |Serves your app at `localhost:3003`|
|`build <country> <product>`            |Builds the application to ./../../../plugins/dlbi-sodexo-```<country>```-simulator/app-```<product>```/dist|

If the product is the same of the country, you can ignore the product in the script.
For each country, we have : 

|Country (- Product)|Description|
|-------------------|-----------|
|France|`yarn start france`|
|Spain - Salary sacrifice  |`yarn start spain salary`|
|Spain - Childcare Pass |`yarn start spain childcare`|
|Spain - Restaurant Pass |`yarn start spain restaurant`|
|Spain - Transport Pass |`yarn start spain transport`|
|Chile |`yarn start chile`|


(Please, add the new simulators in the list)

## Project Structure

```
├──app
│  ├── simulators                     # All build-related code
│  │  ├── build                       # All build-related code
│  │  ├── public                      # Static public assets (not imported anywhere in source code)
│  │  ├── server                      # Express application that provides webpack middleware
│  │  │   └── main.js                 # Server application entry point
│  │  ├── src                         # Application source code
│  │  │   ├── index.html              # Main HTML page container for app
│  │  │   ├── main.js                 # Application bootstrap and rendering
│  │  │   ├── normalize.js            # Browser normalization and polyfills
│  │  │   ├── components              # Global reusable components
│  │  │   ├── fonts                   # Fonts folder
│  │  │   ├── images                  # Images folder
│  │  │   ├── utils                   # Simulators logic
│  │  │   ├── views                   # Simulators app
│  │  │   │   └── <Country>           # Country folder
│  │  │   │      ├── <Product>.jsx    # Bootstrap simulator application
│  │  │   │      └── <product>.scss   # Specific stylesheets for the simulator 
│  │  │   └── styles                  # Application-wide styles (global reusable styles) 
│  │  │ 
```

## Components 

There are reusable components on every simulators you can reuse or combine together to create ones. 

```bash
$ cd src/components
```

|`yarn <script>`    |Description|Status|
|-------------------|-----------|------|
|Counter            |This component needs to be fix deeply. Especially the error alert. An other problem is how to pass dynamic value in real time (for example in the salary sacrifice : when you change the number of children, the number of children under three is not update because its state can't change)  |TO FIX |
|CounterWithIcons   |This component use the Counter component. It's a block with a set of icons (array), a title, a subtitle (optional) and a tooltip (optional). |TO UPDATE if there are new props in the Counter components|
|SliderTooltip      |This component comes from [react-component](https://github.com/react-component/slider) | OK |
|Toggle             | This component is a simple switch | OK |
|Selector           | This component is only use in *Spain Salary* for managing the products (restaurant, childcare, etc.). Its distinctive feature is to use nested components. *Selector* use *Product* (in the same folder) which use two counters. Pay attention with the props by passing in every parents or children components if you adding ones | OK |
|Messages           | This component is only use in *Spain Salary* for displaying overlay message on the results. It could be more stand-alone (some styles are managing in the ```<Product>.jsx```) | OK (Could be better) |
|Bar                | This component is a bar chart | OK |
|Doughnut           | This component is a doughnut chart | OK |

To get the props of a each component, take a look in the constructor of their ```*.jsx``` file in 


## Live Development

### Hot Reloading

Hot reloading is enabled by default when the application is running in development mode (`yarn start`). This feature is implemented with webpack's [Hot Module Replacement](https://webpack.github.io/docs/hot-module-replacement.html) capabilities, where code updates can be injected to the application while it's running, no full reload required. Here's how it works:

* For **JavaScript** modules, a code change will trigger the application to re-render from the top of the tree. **Global state is preserved (i.e. redux), but any local component state is reset**. This differs from React Hot Loader, but we've found that performing a full re-render helps avoid subtle bugs caused by RHL patching.

* For **Sass**, any change will update the styles in realtime, no additional configuration or reload needed.

## Building for Production

## Deployment

Out of the box, this starter kit is deployable by serving the `./dist` folder generated by `yarn build <country> <product>`. This project does not concern itself with the details of server-side rendering or API structure, since that demands a more opinionated structure that makes it difficult to extend the starter kit. The simplest deployment strategy is a [static deployment](#static-deployments).

### Static Deployments

Serve the application with a web server such as nginx by pointing it at your `./dist` folder. Make sure to direct incoming route requests to the root `./dist/index.html` file so that the client application will be loaded; react-router will take care of the rest. If you are unsure of how to do this, you might find [this documentation](https://github.com/reactjs/react-router/blob/master/docs/guides/Histories.md#configuring-your-server) helpful. The Express server that comes with the starter kit is able to be extended to serve as an API and more, but is not required for a static deployment.

## Useful links

* [Webpack](https://webpack.js.org/concepts/)
* [Get started with react](https://facebook.github.io/react/docs/hello-world.html) 
* [Build with react](http://buildwithreact.com/tutorial)



