<?php
// Post grid for All our solutions
$simplelink = 'Simple link';
$typeform   = 'Typeform';
$youtube    = 'Youtube';
?>
<section class="block-posts-grid block-posts-grid-loginpage">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="sodexo-title"><?php echo the_sub_field('posts_grid_block_title'); ?></h2>
            </div>
        </div>
        <div class="row align-items-stretch">
			<?php
			if(have_rows('posts_grid')){
				while(have_rows('posts_grid')){
					the_row();
					$type           = get_sub_field('posts_grid_type');
					$image          = get_sub_field('posts_grid_image');
					$image_link     = get_sub_field('posts_grid_image_link');
					$linked_post_id = get_sub_field('posts_grid_link');
					$link_type      = get_sub_field('posts_grid_image_select_a_link_type');
					$cta            = get_sub_field('posts_grid_cta');
					//					$cta_target     = get_sub_field('posts_grid_cta_target');
					//					$cta_title      = get_sub_field('posts_grid_cta_title');
					//					dlbi_display_debug($cta, 0, "orangered");
					$cta_link = "";
					$href     = "";
					if(is_array($cta)){
						$href       = $cta["url"];
						$cta_target = $cta["target"] ? "target='" . $cta["target"] . "'" : "";
						$cta_title  = $cta["title"] ? $cta["title"] : $cta["href"];
						$cta_link   = "<a href='$href' class='btn-sodexo btn-sodexo-red' $cta_target>$cta_title</a>";
					}
					//					if($cta && $cta_title && !$cta_link){
					//						if($cta_target && $cta_target !== "No"){
					//							$cta_target = "target='$cta_target'";
					//						}
					//						$cta_link = "<a href='$cta' class='btn-sodexo btn-sodexo-red' $cta_target>$cta_title</a>";
					//					}
					switch($type){
						case 'Image':
							{
								?>
                                <div class="col-12 col-sm-6 col-md-4 sameheight">
                                    <div class="block-posts-grid_content block-posts-grid_image">
										<?php
										//										dlbi_display_debug($image, 0, "orangered");
										//										dlbi_display_debug($cta, 0, "orangered");
										//										dlbi_display_debug(get_sub_field('posts_grid_image_link'), 0, "orangered");

										switch($link_type){
											case $simplelink:
											default:
												{
													//													$target_html = "";
													//													if($image_link["target"]){
													//														$target_html = "target='" . $image_link["target"] . "'";
													//													}

													if($href){ ?>
                                                        <a href="<?php echo $href; ?>" <?php echo $cta_target ?> >
													<?php } ?>
                                                    <div class="block-posts-grid_content--item" <?php echo apply_filters("lazyload-style", "background-image:url('".$image['sizes']['soxo-medium']."');"); ?>></div>
													<?php
													if($href){ ?>
                                                        </a>
													<?php }
													$cta_link = "";
													break;
												}
											case $typeform:
												{
													?>
                                                    <a href="javascript:;" data-toggle="modal" data-target="#typeformModal" data-typeform="<?php echo $image_link["url"]; ?>">
                                                        <div class=" block-posts-grid_content--item" <?php echo apply_filters("lazyload-style", "background-image:url('".$image['sizes']['soxo-medium']."');"); ?>>
                                                        </div>
                                                    </a>
													<?php

													break;
												}
											case $youtube:
												{
													?>
                                                    <a data-fancybox href="<?php echo $image_link["url"]; ?>">
                                                        <div class="block-posts-grid_content--item" <?php echo apply_filters("lazyload-style", "background-image:url('".$image['sizes']['soxo-medium']."');"); ?>></div>
                                                    </a>
													<?php
													break;
												}
										}

										if($cta_link){
											?>
                                            <div class="block-posts-grid_content--item_cta-wrapper"><?php echo $cta_link; ?></div>
										<?php } ?>
                                    </div>
                                </div>
								<?php
								break;
							}
						case 'Post':
							{
								?>
                                <div class="col-12 col-sm-6 col-md-4 sameheight">
                                    <div class="block-posts-grid_content block-posts-grid_post real_post">
                                        <a href="<?php echo get_permalink($linked_post_id->ID); ?>" class="block-posts-grid_content--item">
                                            <div class="block-posts-grid_content--item_image">
												<?php echo apply_filters("dlbi_image", $image['sizes']['soxo-medium']); ?>
                                            </div>
                                            <div class="block-posts-grid_content--item_text">
                                                <h3><?php echo get_the_title($linked_post_id->ID) ?></h3>
												<?php if(get_sub_field('posts_grid_description')){ ?>
                                                    <p><?php echo get_sub_field('posts_grid_description'); ?></p>
												<?php } ?>
                                            </div>
                                        </a>
										<?php
										if($cta_link){
											?>
                                            <div class="block-posts-grid_content--item_cta-wrapper"><?php echo $cta_link; ?></div>
										<?php } ?>
                                    </div>

                                </div>
								<?php
								break;
							}
						default:
							{
								$link_type = get_sub_field('posts_grid_image_select_a_link_type');
								?>
                                <div class="col-12 col-sm-6 col-md-4 sameheight">
                                    <div class="block-posts-grid_content block-posts-grid_post default_block">
										<?php
										if($link_type == $simplelink){
										if($image_link){
										?>
                                        <a href="<?php echo $image_link["url"]; ?>" <?php if($image_link["target"]): ?> target="_blank" <?php endif ?> class="block-posts-grid_content--item">
											<?php } ?>
											<?php }elseif($link_type == $typeform){ ?>
                                                <a class="block-posts-grid_content--item" href="javascript:;" data-toggle="modal" data-target="#typeformModal" data-typeform="<?php echo $image_link["url"]; ?>">
													<?php echo get_field('posts_grid_image_label'); ?>
                                                </a>
											<?php }
                                            elseif($link_type == $youtube){ ?>
                                            <a class="block-posts-grid_content--item" data-fancybox href="<?php echo $image_link["url"]; ?>">
												<?php } ?>
                                                <div class="block-posts-grid_content--item_image">
													<?php echo apply_filters("dlbi_image", $image['sizes']['soxo-medium']); ?>
                                                </div>
                                                <div class="block-posts-grid_content--item_text">
													<?php if(get_sub_field('posts_grid_title')){ ?>
                                                        <h3><?php echo get_sub_field('posts_grid_title'); ?></h3>
													<?php } ?>
													<?php if(get_sub_field('posts_grid_description')){ ?>
                                                        <p><?php echo get_sub_field('posts_grid_description'); ?></p>
													<?php } ?>
                                                </div>
												<?php if($link_type){ ?>
                                            </a>
										<?php }
										if($cta_link){
											?>
                                            <div class="block-posts-grid_content--item_cta-wrapper"><?php echo $cta_link; ?></div>
										<?php } ?>
                                    </div>
                                </div>
								<?php
								break;
							}
					}
				}
				wp_reset_postdata();
			}
			?>
        </div>
    </div>
</section>
