<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */
get_header();
$current_country     = do_shortcode('[geoip_detect2 property="country" lang="en"]');
$rp_translations     = get_field('rp_translations', 'option');
$default_translation = get_field('rp_default_translation', 'option');
$default_label       = get_field('rp_default_label', 'option');
$country_name        = get_field('rp_current_country', 'option');
$is_rp_content       = get_field('is_rp_content', 'option');
?>
<div class="hero">
	<?php if(has_post_thumbnail(get_the_ID())) :
		echo apply_filters("dlbi_image", get_the_post_thumbnail_url(get_the_ID(), 'soxo-hero-header'));
	endif; ?>
</div>
<?php
if($rp_translations && $is_rp_content){
	foreach($rp_translations as $translation){
		if($translation['rp_country'] == $current_country){
			$rp_translation = $translation['rp_text'];
			$rp_label       = $translation['rp_label'];
			$rp_link        = $translation['rp_link'];
			$rp_flag        = true;
			break;
		}else{
			$rp_translation = $default_translation;
			$rp_label       = $default_label;
		}
	}
}

if($is_rp_content): ?>
    <div class="col-md-12 blog-header">
        <div class="container">
            <div class="row">
                <div class="blog-header-content">
					<?php if($rp_translation && $rp_label && !empty($rp_link) && $rp_flag): ?>
						<?php echo $rp_translation ?>
                        <a href="<?php echo $rp_link ?>" target="_blank" class="blog-header-link"><?php echo $rp_label ?></a>
					<?php else: ?>
						<?php echo $default_translation ?>
                        <div class="blog-header-dropdown-content">
                            <button id="show-dropdown"><?php echo $country_name ?></button>
                            <ul class="blog-header-dropdown">
								<?php foreach($rp_translations as $translation) : ?>
                                    <li><a href="<?php echo $translation['rp_link'] ?>" target="_blank"><?php echo $translation['rp_country'] ?></a></li>
								<?php endforeach ?>
                            </ul>
                        </div>
					<?php endif ?>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>
<div class="col-md-12 blog-article">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-8">
                <!-- display content -->
				<?php while(have_posts()) : the_post(); ?>
                    <!-- Get content -->
					<?php get_template_part('loop-templates/content-single'); ?>

                    <!-- Pagination -->
                    <div class="row pager">
                        <div class="col-sm-6">
							<?php if(get_next_post_link()): ?>
                            <p class="blog-component--container_link prev">
                                <i class="fa fa-arrow-left fa-lg" aria-hidden="true"></i><span><?php next_post_link('%link', __('Previous article', 'lbi-sodexo-theme')) ?></span>
								<?php endif; ?>
                            </p>
                        </div>
                        <div class="col-sm-6">
							<?php if(get_previous_post_link()): ?>
                                <p class="blog-component--container_link next">
                                    <span><?php previous_post_link('%link', __('Next article', 'lbi-sodexo-theme')) ?></span><i class="fa fa-arrow-right fa-lg" aria-hidden="true"></i></p>
							<?php endif; ?>
                        </div>
                    </div>
				<?php endwhile; // end of the loop.    ?>

            </div>
            <!-- Do the right SIDEBAR -->
			<?php get_sidebar('right-single'); ?>
        </div>
    </div>
</div>


<!-- Article from the same category -->
<?php
$related      = false; //SOXINT-806 : remove related articles
$show_related = get_field('show_related');
//dlbi_display_debug($show_related, 0, "orangered");
if($show_related){
	$related = get_posts([
		'category__in' => wp_get_post_categories(get_the_ID()),
		'numberposts'  => 1,
		'post__not_in' => [get_the_ID()]
	]);
}

if($related){
	foreach($related as $post){
		?>
        <div class="related-article">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="blog-item">
							<?php
							// Get Image
							if(has_post_thumbnail($post->ID)) :
								?>
                                <div class="media">
									<?php echo apply_filters("dlbi_image", get_the_post_thumbnail_url($post->ID, 'soxo-blog-medium'), "blog-component--container_image", $post->post_title); ?>
                                </div>
							<?php endif; ?>
                            <div class="blog-component--container">
								<?php
								// Get category article
								$categories = get_the_category();
								if(!empty($categories)) :
									?>
                                    <p class="blog-component--container_cat"><?php echo esc_html($categories[0]->name); ?></p>
								<?php endif; ?>
                                <p class="blog-component--container_date"><?php echo get_the_date('d.m.Y'); ?></p>

                                <p class="blog-component--container_title"><a href="<?php echo get_permalink($post->ID); ?>" rel="bookmark" title="<?php echo $post->post_title; ?>">
										<?php echo $post->post_title; ?>
                                    </a></p>
                                <div class="blog-component--container_content">
									<?php echo wp_trim_words(strip_shortcodes($post->post_content), 70); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<?php
	}
	wp_reset_postdata();

} ?>
<!-- PANEL PRODUCTS: retrieve product sheets set in posts -->
<?php
$orig_post        = $post;
$posts_list       = get_field('featured_products');
$posts_list_title = get_field('featured_products_title');
//$like_cross_sell  = get_field("featured_products_display_like_cross_sell");
$like_cross_sell = true; //SOXINT-881 always set to true

$call_to_action = get_field("featured_products_CTA");
//dlbi_display_debug($call_to_action, 0, "orangered");

$link_URL = "";
$blank = false;
if(isset($call_to_action["url"])){
	$link_URL = $call_to_action["url"];
}
$link_text = $link_URL;
if(isset($call_to_action["title"])){
	$link_text = $call_to_action["title"];
}
if(isset($call_to_action["target"])){
	$blank = $call_to_action["target"];
}

if(!$link_text){
	$link_text = $link_URL;
}
//dlbi_display_debug($blank, 0, "orangered");

//[vc_row][vc_column][cross_sell
if($posts_list){
	if($like_cross_sell && $posts_list_title){
		$ids   = ' posts_id="';
		$value = "[";
		$sep   = '';
		foreach($posts_list as $post){
			$value .= $sep . '{"post_id":"' . $post->ID . '"}';
			$sep   = ',';
		}
		$value .= "]";
		$value = url_encode($value);
		$ids   .= $value . '"';

		$content = "[vc_row][vc_column][cross_sell";
		$content .= $ids . ' widget_title="' . $posts_list_title . '" type_link="simplelink"';
		if($link_URL){
			$link   = url_encode($link_URL);
			$text   = url_encode($link_text);
			$target = "";
			if($blank){
				$target = "target:%20_blank";
			}
			$content .= ' link="url:' . $link . '|title:' . $text . '|' . $target . '|"';
		}

		$content .= "][/vc_column][/vc_row]";
//		dlbi_display_debug($content, 0, "orangered");
		echo do_shortcode($content);
	}else{
		//kept it for a fallback, can be removed if this (buggy) fallback is no longer needed

		foreach($posts_list as $post){
			// feature image
			if(has_post_thumbnail($post->ID)){
				echo get_the_post_thumbnail($post->ID, 'large', array('class' => 'blog-component--container_image'));
			}
			$data = get_post($post->ID);
			echo apply_filters('the_content', $data->post_content);
			?>
            <a class="btn-sodexo btn-sodexo-white" href="<?php echo get_permalink($post->ID) ?>"><?php echo get_the_title($post->ID); ?></a>
		<?php } ?>
        <ul class="nav nav-pills">
        <li role="presentation"><a class="cta_products" href="<?php echo get_post_type_archive_link(SOD_PRO_PTYPE); ?>"><?php echo __('ALL PRODUCTS', 'lbi-sodexo-theme'); ?></a></li>
        </ul><?php
	}
} ?>

<div class="breadcrumb">
	<?php
	if(function_exists('bcn_display')):
		bcn_display();
	endif;
	?>
</div>
<?php get_footer(); ?>
