
<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
	<div class="input-group">

		<input class="field form-control" id="s" name="s" type="text" value="<?php echo get_search_query() ?>"
			placeholder="<?php esc_attr_e( 'Search', 'lbi-sodexo-theme' ); ?>">
		<span class="input-group-btn">
			<input class="submit btn btn-primary" id="searchsubmit" name="submit" type="submit"
			value="<?php esc_attr_e( 'Search', 'lbi-sodexo-theme' ); ?>">
	</span>
	</div>
  <input type="hidden" name="origin" value="Burger">
</form>
