<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */
get_header();
// get permalink does not work in this context, it can return the wrong url
$permalink_page = 'http' . ( isset( $_SERVER['HTTPS'] ) ? 's' : '' ) . '://' . "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";

$is_rp_content    = get_field( 'is_rp_content', 'option' );
$hide_all         = get_field( 'hide_category_all', 'option' );
$hide_testimonial = get_field( 'hide_category_testimonials', 'option' );
$per_page         = 8;
$query_pages      = 0;
// $per_page = 2;
?>
<div class="blog-homepage">
	<?php
	// Get template slider
	get_template_part( 'templates/content', 'slider-hp-blog' );

	// Get link ALL
	$args   = [
		'post_type'  => 'page',
		'fields'     => 'ids',
		'nopaging'   => true,
		'meta_key'   => '_wp_page_template',
		'meta_value' => 'homepage-blog.php',
	];
	$pages  = get_posts( $args );
	$plugin = 'dlbi-sodexo-testimonials/dlbi-sodexo-testimonials.php';
	?>

	<div class="bg-grey">
		<div class="container">
			<div class="content">

				<?php if ( is_post_type_archive( 'testimonial' ) ) : ?>
					<h1 class="sodexo-title">
						<?php echo __( 'TESTIMONIALS', 'lbi-sodexo-theme' ); ?>
					</h1>
					<?php
				elseif ( is_category() ) :

					?>
					<h1 class="sodexo-title">
						<?php echo esc_html( single_cat_title() ); ?>
					</h1>

				<?php elseif ( ! $hide_all ) : ?>
					<h1 class="sodexo-title">
						<?php echo __( 'ALL', 'lbi-sodexo-theme' ); ?>
					</h1>
				<?php endif; ?>
				<!-- Barre de nav -->
				<div class="nav-dropdown mb-5">
					<ul class="nav nav-pills nav-categories" id="blog-categories">
						<li role="presentation" class="trigger">
							<?php if ( is_post_type_archive( 'testimonial' ) && ! $hide_testimonial ) : ?>
								<button data-toggle-class="blog-categories"><?php echo __( 'TESTIMONIALS', 'lbi-sodexo-theme' ); ?></button>
							<?php elseif ( is_category() ) : ?>
								<button data-toggle-class="blog-categories"> <?php echo esc_html( single_cat_title() ); ?></button>
							<?php elseif ( ! $hide_all ) : ?>
								<button data-toggle-class="blog-categories"><?php echo __( 'ALL', 'lbi-sodexo-theme' ); ?></button>
							<?php endif; ?>
						</li>

						<?php if ( ! $hide_all ) : ?>
<<<<<<< HEAD
							<li role="presentation"
=======
							<li role="presentation" class="nav-categories-item"
>>>>>>> wdg_master/wdg_master
							<?php
							if ( ! is_post_type_archive( 'testimonial' ) and count( get_the_category() ) < 1 ) :
								?>
								 class="current" <?php endif; ?>>
								<a href="<?php echo get_permalink( $pages[0] ); ?>">
									<?php echo __( 'ALL', 'lbi-sodexo-theme' ); ?>
								</a>
							</li>
							<?php
						endif;

						// Check if plugin is enabled
						if ( is_plugin_active( $plugin ) && ! $is_rp_content && ! $hide_testimonial ) :
							?>
							<li role="presentation" class="nav-categories-item"
							<?php
							if ( is_post_type_archive( 'testimonial' ) ) :
								?>
								 class="current" <?php endif; ?>>
								<a href="<?php echo get_post_type_archive_link( SOD_TES_PTYPE ); ?>">
									<?php echo __( 'TESTIMONIALS', 'lbi-sodexo-theme' ); ?>
								</a>
							</li>
							<?php
						endif;
						// Get list category
												$categories    = get_categories(array(
														'orderby'   => 'name',
														'post_type' => 'post'
													)
												);
												//						dlbi_display_debug( get_category( get_query_var( 'cat' ) ), 0, "green" );
												foreach($categories as $category) :
													if($category->term_id):
														$current_cat = get_category(get_query_var('cat'));
														@$idcat = $current_cat->term_id;

														$class_presentation = "";

														if(is_category() && $idcat == $category->term_id){
															$class_presentation = "current";
														}
														?>
						                                <li role="presentation" class="<?php echo $class_presentation ?> nav-categories-item">
						                                    <a href="<?php echo esc_url(get_category_link($category->term_id)) ?>">
																<?php echo esc_html($category->name) ?>
						                                    </a>
						                                </li>
													<?php endif; ?>
												<?php endforeach; ?>
					</ul>
				</div>
				<!-- Blog content -->
				<section class="blog-component">
					<div id="ajax-posts" class="blog-component--block">
						<ul class="row" id="blog-articles">
							<?php
							// Protect against arbitrary paged values
							$paged       = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
							$array_posts = array( 'post' );
							// Check if plugin is enabled
							if ( is_plugin_active( $plugin ) ) {
								$array_posts[] = 'testimonial';
							}
							$is_posts = get_field( 'show_posts', 'option' );

							if ( $is_posts ) {
								if ( ( $key = array_search( 'post', $array_posts ) ) !== false ) {
									unset( $array_posts[ $key ] );
								}
							}

							$args               = array(
								'post_type'      => $array_posts,
								'posts_per_page' => $per_page + 1,
								'paged'          => $paged,
							);
							$the_query          = new WP_Query( $args );
							$nb_element         = $the_query->found_posts;
							$count_posts        = 0;
							$display_pagination = false;
							if ( get_field( 'is_rp_content', 'option' ) ) {
								dlbi_display_debug( 'is_rp_content', 0, 'orangered' );
								get_template_part( 'loop-templates/content-rp' );
							} elseif ( $the_query->have_posts() && is_page_template( 'homepage-blog.php' ) ) {
								while ( $the_query->have_posts() ) {
									$the_query->the_post();
									$count_posts = $the_query->current_post + 1; // counts posts in loop
									$query_pages = $the_query->max_num_pages;
									if ( $count_posts < $per_page ) {
										if ( $count_posts == 9 ) :
											// Display product
											get_template_part( 'templates/content', 'product-blog-hp' );
										endif;
										// Display posts
										echo '<li class="blog-item col-sm-6 col-lg-3">';
										get_template_part( 'loop-templates/content-post' );
										echo '</li>';
									}
									if ( $count_posts == $per_page + 1 ) {
										$display_pagination = true;
									}
								}
								echo $the_query->paginate_links();

								wp_reset_postdata();
								// */
							} elseif ( have_posts() ) {
								// dlbi_display_debug("have posts", 0, "orangered");
								global $wp_query;
								$nb_query = $wp_query->post_count;
								while ( have_posts() ) {
									the_post();
									++ $count_posts;
									echo '<li class="blog-item col-sm-6 col-lg-3">';
									get_template_part( 'loop-templates/content-post' );
									echo '</li>';

								}
								if ( dlbi_more_posts() ) {
									$display_pagination = true;
								}
								if ( $count_posts ) {
									$query_pages = $wp_query->max_num_pages;
								}
								wp_reset_postdata();
							}
							?>
						</ul>
					</div>
					<?php

					if ( $query_pages > 1 ) {
						?>
						<div class="pagination-blog">
							<div class="pagination-blog-inner-wrapper" data-children="<?php echo $query_pages; ?>">
								<?php
								$prec = $next = $pagination_HTML = '';
								for ( $i = 1;$i <= $query_pages;++ $i ) {
									$link                  = $permalink_page;
									$class_pagination_item = 'pagination-item';
									$page_suffix           = '';
									$current_suffix        = "page/$paged/"; // with trailing slash
									if ( $i > 1 ) {
										$page_suffix = "page/$i/";
									}
									if ( $paged > 1 ) {
										$link = str_replace( $current_suffix, $page_suffix, $permalink_page );
									} else {
										$link .= $page_suffix;
									}
									if ( $i === $paged ) {
										$class_pagination_item .= ' current';
									}
									$pagination_HTML .= "<a href=\"$link\" class=\"$class_pagination_item\">$i</a>";
									if ( $i === $paged - 1 ) {
										$prec .= "<a href=\"$link\" rel=\"prev\" class=\"$class_pagination_item\">&lt;</a>";
									}
									if ( $i === $paged + 1 ) {
										$next .= "<a href=\"$link\" rel=\"next\" class=\"$class_pagination_item\">&gt;</a>";
									}
								}
								echo $prec . $pagination_HTML . $next;
								?>
							</div>
						</div>
						<?php
					}
					?>
				</section>
			</div>
		</div>
	</div>
	<div class="breadcrumb">
		<?php
		if ( function_exists( 'bcn_display' ) ) :
			bcn_display();
		endif;
		?>
	</div>
	<?php get_footer(); ?>
