
<?php
// We retrieve the product that has been flagged show in blog homepage
$args = array(
	'post_type' => array('product'),
	'posts_per_page' => -1,
	'meta_query' => array(
		array(
			'key' => 'show_in_page_blog_homepage',
			'value' => 'Yes',
		)
	)
);
$products = get_posts( $args );

// This information is set in the product sheet tab
// Show in blog HP
if ( $products ) :

	foreach ($products as $data) :

        ?>
        <!-- <div class="col-md-4"> -->
            <a class="cross-sell btn-sodexo-grey cross-sell--container_cta" href="<?php echo get_permalink($data->ID) ?>">
                <div class="cross-sell--container">
                    <!-- ACF : "Show in blog HP" tab -->
                    <?php
                        $image = get_field("product_show_in_blog_hp_image", $data->ID);
                    if ( count( $image ) ) :
	                    echo apply_filters( "dlbi_image", $image, "blog-component--container_image" );
                    endif;
                    ?>
                    <!-- permalink product-->
                    <h3 class="cross-sell--container_title"> <?php echo $data->post_title ?> </h3>
                    <!-- ACF : "Show in blog HP" tab -->
                    <div class="cross-sell--container_desc">
                     <?php echo get_field("product_show_in_blog_hp_description", $data->ID) ?>
                    </div>
                    <span class="bt">Discover</span>
                </div>
            </a>
        <!-- </div> -->
   <?php endforeach;
    wp_reset_postdata();
 endif;
?>