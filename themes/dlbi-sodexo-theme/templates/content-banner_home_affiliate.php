<?php
/* banner affiliate */
?>
<div class="banner-home cbha <?php if(get_field('homepage_affiliate_banner_color') == 'blacktxt') : echo 'lighten'; endif; ?>">
	<?php
	$size = '';
	$idSlider = get_field('id_slider');
	if(!empty($idSlider)){ ?>
        <div class="banner-home-type" style="z-index:0;">
			<?php layerslider($idSlider); ?>
        </div>
	<?php }else{ ?>
        <div class="banner-home-type <?php if(get_field('homepage_affiliate_banner_enable_video')) : echo 'video-on';else : echo 'video-off'; endif; ?>">
            <div class="banner-home-type-image">
				<?php
				// we always show the image on mobile because the autoplay function is disabled on mobile
				$hp_image = get_hp_image();
				if($hp_image){
					echo wp_get_attachment_image($hp_image, $size);
					$hp_image_src = wp_get_attachment_image_src($hp_image, $size);
				}
				?>
            </div>
			<?php
			$seen_video_id  = get_field('homepage_banner_image_seen_video', 'option');
			$seen_video_src = false;
			if($seen_video_id){
				$seen_video_src = wp_get_attachment_image_src($seen_video_id, $size);
			}
			if($seen_video_src){
				?>
                <div class="banner-home-type-image-alt">
                    <img id="banner-alt-image" src="<?php echo $hp_image_src[0]; ?>" data-src="<?php echo $seen_video_src[0]; ?>" width="<?php echo $seen_video_src[1]; ?>" height="<?php echo $seen_video_src[2]; ?>">
                </div>
				<?php
			}
			if(get_field('homepage_affiliate_banner_enable_video')){
				//check if the cookie does not exists or if isset, has a false value, IE this block does not shows if the cookie is set
				if(!getCookieValue("homepage_banner_video_shown")){
					$cookie_duration = get_field('homepage_affiliate_video_cookie_duration') * 1;
					if(!$cookie_duration){
						$cookie_duration = 365 * 3600 * 24; //1 year
					}
					setcookie("homepage_affiliate_video_shown", "true", time() + $cookie_duration);
					//					dlbi_display_debug("setCookie", 0, "darkOrchid");
					?>
                    <div class="banner-home-type-video">
                        <div class="banner-home-type-video-container">
							<?php
							// The iframe is placed on the image if enabled but hidden on mobile (no autoplay)
							// get iframe HTML
							$url = get_field('homepage_affiliate_banner_video', false, false);
							preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url, $matches);
							$video_id = $matches[1];
							?>
                            <div id="sodexoVideoPlayerBanner"></div>
                            <script async src="https://www.youtube.com/iframe_api"></script>
                            <script>
                                if (!localStorage.getItem("homepage_banner_video_shown")) {
                                    function onYouTubeIframeAPIReady() {
                                        var player;
                                        player = new YT.Player('sodexoVideoPlayerBanner', {
                                            videoId: '<?php echo $video_id; ?>', // YouTube Video ID
                                            // width: 2000,       // Player width (in px)
                                            // height: 850,       // Player height (in px)
                                            playerVars: {
                                                playlist: '<?php echo $video_id; ?>',
                                                autoplay: 1,        // Auto-play the video on load
                                                controls: 0,        // Show pause/play buttons in player
                                                showinfo: 0,        // Hide the video title
                                                modestbranding: 1,  // Hide the Youtube Logo
                                                loop: 1,            // Run the video in a loop
                                                fs: 0,              // Hide the full screen button
                                                cc_load_policy: 0,  // Hide closed captions
                                                iv_load_policy: 3,  // Hide the Video Annotations
                                                autohide: 0         // Hide video controls when playing
                                            },
                                            events: {
                                                onReady: function (e) {
                                                    e.target.mute();
                                                }
                                            }
                                        });
                                    }

                                    localStorage.setItem("homepage_banner_video_shown", true);
                                } else {

                                }
                            </script>
                        </div>
                    </div>
					<?php

				}
			} ?>
        </div>
        <div class="banner-home-text">
			<?php $hp_text = get_field('homepage_affiliate_banner_text');
			if($hp_text){ ?>
                <h1><?php echo $hp_text; ?></h1>
			<?php } ?>
            <a href="#"><i class="fa fa-angle-down fa-lg" aria-hidden="true"></i></a>
        </div>

		<?php get_cta_home();
	} ?>
</div>
