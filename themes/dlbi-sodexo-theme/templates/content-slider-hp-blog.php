<?php
/**
 * Partial template for content in homepage-blog.php
 *
 * @package understrap
 */

// We load the posts and the flagrant testimonials like publish in the home page bolg
$args = array(
	'post_type' => array('post','testimonial'),
	'posts_per_page' => 6,
	'meta_query' => array(
		array(
			'key' => 'stiky',
			'value' => 'Yes',
		)
	)
);
$postslist = get_posts( $args );

//Required when we are on a category of the blog
$args = [
    'post_type' => 'page',
    'fields' => 'ids',
    'nopaging' => true,
    'meta_key' => '_wp_page_template',
    'meta_value' => 'homepage-blog.php'
];
$pages = get_posts( $args );
foreach ( $pages as $pageID ) {
	$idSlider = get_field('id_slider', $pageID);
	break;
}

if(!empty($idSlider)) {
?>
	<div class="lay-slider">
	  <?php layerslider($idSlider); ?>
	</div>
<?php } else { ?>
	<section id="blog-carousel" class="carousel hero-push slide <?php if (count($postslist)==1) { echo "oneslide"; } ?>" data-ride="false">
	    <div class="carousel-inner" role="listbox">

	    <?php
	    if ( $postslist ) :

	    	$blogSliderI = 0;

		    foreach ( $postslist as $post ) :
			    $link_label = get_field('testimonial_link');
			    $categories = get_the_category();

				if ( !empty( $categories ) ) :
		    	 ?>
					<div class="carousel-item <?php echo( $blogSliderI === 0 ? "active" : "" ); ?>">
						<?php
							if( has_post_thumbnail( $post->ID ) ) :
						?>
							<div class="media-container"><?php echo get_the_post_thumbnail( $post->ID, 'full', array( 'class' => 'd-block img-fluid' ) ); ?> </div>
						<?php
							endif;
						?>
						<div class="container">
			    			<span class="cat"><?php echo esc_html( $categories[0]->name ); ?></span>
				     		<p class="date"><?php echo get_the_date( 'd.m.Y', $post->ID ); ?></p>
								<a href="<?php echo get_the_permalink($post->ID) ?>" class="title-link"><h2 class="title"><?php echo $post->post_title; ?></h2></a>
								<?php	if ($link_label) { ?>
									<a href="<?php echo $link_label['url']; ?>" class="btn-sodexo btn-sodexo-red"><?php echo $link_label['title'];?></a>
								<?php } ?>
		        		</div>
			    	</div>
			    <?php
			    	$blogSliderI++;
				endif; ?>

		    <?php endforeach;

		    wp_reset_postdata();
		endif;
		?>
	  </div>
	  <a class="carousel-control-prev" href="#blog-carousel" role="button" data-slide="prev">
	    <!-- <span class="carousel-control-prev-icon" aria-hidden="true"></span> -->
	    <span class="sr-only"><?php  echo __('Previous', 'lbi-sodexo-theme');?></span>
	  </a>
	  <a class="carousel-control-next" href="#blog-carousel" role="button" data-slide="next">
	    <!-- <span class="carousel-control-next-icon" aria-hidden="true"></span> -->
	    <span class="sr-only"><?php  echo __('Next', 'lbi-sodexo-theme');?></span>
	  </a>
	  <ol class="carousel-indicators alt">
	  	<?php
	  	$sliderI = 0;
	  	 foreach ( $postslist as $post ) :
		    $categories = get_the_category();
			if ( !empty( $categories ) ) :
	  	?>
	    <li data-target="#blog-carousel" data-slide-to="<?php echo $sliderI; ?>" class="<?php if( $sliderI === 0 ){ echo "active"; } ?>"></li>
	    <?php
	    	$sliderI++;
	    	endif;
	    endforeach;
	    wp_reset_postdata();
		?>
	  </ol>
	</section>
<?php } ?>
