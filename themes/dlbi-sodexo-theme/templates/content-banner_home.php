<?php
/* Homepage banner */
?>
<?php
// Customer wants to keep it in case
//if (isset($_COOKIE['product_homepage']) && get_field('homepage_show_last_viewed_product', 'option')) :
if(false){
	$product_id = $_COOKIE['product_homepage'];
	?>
    <section class="prod-head">
        <!-- Product head block -->
		<?php $prod_head_img_bg = get_field('product_header_image_background', $product_id);
		$style = apply_filters("lazyload-style", "background-image: url('" . $prod_head_img_bg['url'] . "');");
		?>
        <div class="prod-head--background" <?php echo $style; ?>>
            <div class="prod-head--content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="prod-head--content_textblock hidden-sm-down">
                                <h2 class="sodexo-page-head-title"><?php echo get_field('product_header_title', $product_id) ?></h2>
                                <div class="sodexo-page-head-description"><?php echo get_field('product_header_description', $product_id) ?></div>
                                <a class="btn-sodexo btn-sodexo-red" href="<?php echo get_permalink($product_id) ?>"><?php echo __('Discover more', 'lbi-sodexo-theme'); ?></a>
                            </div>
                        </div><!--.product-head-block-->
                        <div class="col-md-6 col-12 align-self-center text-center">
							<?php
							$prod_head_img = get_field('product_header_image', $product_id);
							if(!empty($prod_head_img)):
								echo apply_filters("dlbi_image", $prod_head_img, "prod-head--content_image");
							endif;
							?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="prod-head--content_mobile hidden-md-up">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="prod-head--content_textblock">
							<?php $link_simulate = get_field('product_header_link_simulate', $product_id); ?>
                            <h1 class="sodexo-page-head-title_mobile"><?php echo get_field('product_header_title', $product_id) ?></h1>
                            <div class="sodexo-page-head-description"><?php echo get_field('product_header_description', $product_id) ?></div>
							<?php if($link_simulate["url"]){ ?>
                                <a class="btn-sodexo btn-sodexo-red" href="<?php echo $link_simulate["url"] ?>">
									<?php if($link_simulate["title"]): ?>
										<?php echo $link_simulate["title"]; ?>
									<?php else : ?>
										<?php echo __('Discover more', 'lbi-sodexo-theme'); ?>
									<?php endif ?>
                                </a>
							<?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--.product-head-block-->

    </section>
<?php }else{
	$banner_home_class = "";
	if(get_field('homepage_banner_color', 'option') == 'blacktxt'){
		$banner_home_class = 'lighten';
	}
	?>

    <div class="banner-home cbh <?php echo $banner_home_class; ?>">
		<?php
		$id_slider = get_field('id_slider');
		if(!empty($id_slider)){
			?>
            <div class="banner-home-type" style="z-index:0;">
				<?php layerslider($id_slider); ?>
            </div>
		<?php }else{
			$banner_home_type_class = "video-off";
			if(get_field('homepage_banner_enable_video', 'option')){
				$banner_home_type_class = 'video-on';
			}
			?>
            <div class="banner-home-type <?php echo $banner_home_type_class; ?>">
                <div class="banner-home-type-image">
					<?php
					// we always show the image on mobile because the autoplay function is disabled on mobile
					$hp_image = get_hp_image();
					$size = 'full';
					if($hp_image){
						echo wp_get_attachment_image($hp_image, $size);
						$hp_image_src = wp_get_attachment_image_src($hp_image, $size);
					}
					?>
                </div>
				<?php
				$seen_video_id  = get_field('homepage_banner_image_seen_video', 'option');
				$seen_video_src = false;
				if($seen_video_id){
					$seen_video_src = wp_get_attachment_image_src($seen_video_id, $size);
				}
				if($seen_video_src){
					//					dlbi_display_debug($hp_image_src, 0, "darkOrchid");
					?>
                    <div class="banner-home-type-image-alt">
                        <img id="banner-alt-image" src="<?php echo $hp_image_src[0]; ?>" data-src="<?php echo $seen_video_src[0]; ?>" width="<?php echo $seen_video_src[1]; ?>" height="<?php echo $seen_video_src[2]; ?>">
                    </div>
					<?php
				}
				//				dlbi_display_debug(get_field('homepage_banner_enable_video', "option"), 0, "darkRed");
				if(get_field('homepage_banner_enable_video', "option")){
					//check if the cookie does not exists or if isset, has a false value, IE this block does not shows if the cookie is set
					if(!getCookieValue("homepage_banner_video_shown")){
						$cookie_duration = get_field('homepage_affiliate_video_cookie_duration') * 1;
						if(!$cookie_duration){
							$cookie_duration = 365 * 3600 * 24; //1 year
						}
						$setted = setcookie("homepage_affiliate_video_shown", "true", time() + $cookie_duration);
						?>
                        <div class="banner-home-type-video">
                            <div class="banner-home-type-video-container">
								<?php
								if(get_field('homepage_banner_video', 'option')){ // if a Youtube URL is set
									// The iframe is placed on the image if enabled but hidden on mobile (no autoplay)
									$url = get_field('homepage_banner_video', 'option', false, false);
									preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url, $matches);
									$video_id = $matches[1];
									?>
                                    <div id="sodexoVideoPlayerBanner" style="opacity:0;"></div>
                                    <script>
                                        if (!localStorage.getItem("homepage_banner_video_shown")) {
                                            var tag = document.createElement('script');
                                            tag.src = "https://www.youtube.com/iframe_api";
                                            var firstScriptTag = document.getElementsByTagName('script')[0];
                                            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

                                            function onYouTubeIframeAPIReady() {
                                                var player;
                                                player = new YT.Player('sodexoVideoPlayerBanner', {
                                                    videoId: '<?php echo $video_id; ?>',
                                                    playerVars: {
                                                        playlist: '<?php echo $video_id; ?>',
                                                        autoplay: 1,
                                                        controls: 0,
                                                        showinfo: 0,
                                                        modestbranding: 1,
                                                        loop: 1,
                                                        fs: 0,
                                                        cc_load_policy: 0,
                                                        iv_load_policy: 3,
                                                        autohide: 0
                                                    },
                                                    events: {
                                                        'onReady': onPlayerReady,
                                                        'onStateChange': onPlayerStateChange
                                                    }
                                                });
                                            }

                                            function onPlayerReady(event) {
                                                event.target.mute();
                                                // event.target.playVideo();
                                            }

                                            function onPlayerStateChange(event) {
                                                var videoBanner = document.getElementById("sodexoVideoPlayerBanner");
                                                videoBanner.style.transition = "opacity 0.5s";
                                                if (event.data === 1) {
                                                    videoBanner.style.opacity = "1";
                                                }
                                            }

                                            localStorage.setItem("homepage_banner_video_shown", true);
                                        }
                                    </script>
								<?php } ?>
                            </div>
                        </div>
						<?php

					}
				}
				?>
            </div>
            <div class="banner-home-text">
				<?php
				$hp_text = get_field('homepage_banner_text', 'option');
				if($hp_text){
					?><h2><?php echo $hp_text; ?></h2><?php } ?>
                <br>
                <a id="banner_arrow_down" href="Javascript:void(0)"><i class="fa fa-angle-down fa-lg" aria-hidden="true"></i></a>
            </div>

			<?php
			get_cta_home();
		} ?>
    </div>
<?php }
