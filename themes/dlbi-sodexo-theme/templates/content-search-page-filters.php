<?php
/**
 * The right sidebar containing the search-page-filters
 *
 * @package understrap
 */
?>


<?php
global $wp_query;
foreach ($wp_query->query as $key => $value):
    $param = "?" . $key . "=" . $value . "&";
endforeach;

$search_query = new WP_Query();
$search_query_faq = new WP_Query();
$search_query_needs = new WP_Query();
$search_query_pages = new WP_Query();
$search_query_blog = new WP_Query();
$search_query_products = new WP_Query();
$search_query_testimonials = new WP_Query();

if(get_field('phonetic_search', 'option')) {
  global $ids_allowed_search; //Access the array defined in "search_filter()" (in functions.php)
  $search_posts = $search_query->query(array('post__in' => $ids_allowed_search, 'posts_per_page' => -1, 'post_type' => array('post', 'page', 'faq', 'testimonial', 'need', 'product')));
  $search_posts_faq = $search_query_faq->query(array('post__in' => $ids_allowed_search, 'posts_per_page' => -1, 'post_type' => 'faq'));
  $search_posts_needs = $search_query_needs->query(array('post__in' => $ids_allowed_search, 'posts_per_page' => -1, 'post_type' => 'need'));
  $search_posts_pages = $search_query_pages->query(array('post__in' => $ids_allowed_search, 'posts_per_page' => -1, 'post_type' => 'page'));
  $search_posts_blog = $search_query_pages->query(array('post__in' => $ids_allowed_search, 'posts_per_page' => -1, 'post_type' => 'post'));
  $search_posts_products = $search_query_products->query(array('post__in' => $ids_allowed_search, 'posts_per_page' => -1, 'post_type' => 'product'));
  $search_posts_testimonials = $search_query_testimonials->query(array('post__in' => $ids_allowed_search, 'posts_per_page' => -1, 'post_type' => 'testimonial'));
} else {
  $search_posts = $search_query->query(array('s' => get_query_var('s'), 'posts_per_page' => -1, 'post_type' => array('post', 'page', 'faq', 'testimonial', 'need', 'product')));
  $search_posts_faq = $search_query_faq->query(array('s' => get_query_var('s'), 'posts_per_page' => -1, 'post_type' => 'faq'));
  $search_posts_needs = $search_query_needs->query(array('s' => get_query_var('s'), 'posts_per_page' => -1, 'post_type' => 'need'));
  $search_posts_pages = $search_query_pages->query(array('s' => get_query_var('s'), 'posts_per_page' => -1, 'post_type' => 'page'));
  $search_posts_blog = $search_query_pages->query(array('s' => get_query_var('s'), 'posts_per_page' => -1, 'post_type' => 'post'));
  $search_posts_products = $search_query_products->query(array('s' => get_query_var('s'), 'posts_per_page' => -1, 'post_type' => 'product'));
  $search_posts_testimonials = $search_query_testimonials->query(array('s' => get_query_var('s'), 'posts_per_page' => -1, 'post_type' => 'testimonial'));
}

$query_var = '';
if(!empty(get_query_var('s')))
  $query_var = get_query_var('s');
elseif(!empty($_GET['s']))
  $query_var = $_GET['s'];
?>

<div class="search-page-filters">

    <h5 class="search-page-filters--title"><?php echo __('Filters', 'lbi-sodexo-theme'); ?></h5>

    <ul class="search-page-filters--items">

	<?php /*
	  <form name="FormulaireCritere" id="search-page-filter-form" action="" method="POST">
	  <h2 class="tt"> <label for="contact-input"><?php  echo __('Sorting', 'lbi-sodexo-theme');?></label><br />
	  <select id="criterion" name="criterion" class="custom-select">
	  <option value="ddd">Critère 1</option>
	  <option value="dd">Critère 2</option>
	  </select>
	  </h2>
	  </form>
	 */ ?>

	<li>
	    <a href="<?php echo site_url() . '/?s=' . $query_var ?>"><?php echo __('All', 'lbi-sodexo-theme'); ?></a>
	    <span><?php echo count($search_posts); ?></span>
	</li>
	<li>
	    <a href="<?php echo site_url() . '/?s=' . $query_var ?>&search=blog" id="blogs-critere">
		<?php echo __('Blogs', 'lbi-sodexo-theme'); ?>
	    </a>
	    <span><?php echo count($search_posts_blog) ?></span>
	</li>
	<li>
	    <a href="<?php echo site_url() . '/?s=' . $query_var ?>&search=pages" id="pages-critere">
		<?php echo __('Pages', 'lbi-sodexo-theme'); ?>
	    </a>
	    <span><?php echo count($search_posts_pages) ?></span>
	</li>
	<?php
	$plugin = 'dlbi-sodexo-needs/dlbi-sodexo-needs.php';
	// Check if plugin is enabled
	if (is_plugin_active($plugin)) :
	    ?>
    	<li>
    	    <a href="<?php echo site_url() . '/?s=' . $query_var ?>&search=need" id="need-critere">
		    <?php echo __('Needs', 'lbi-sodexo-theme'); ?>
    	    </a>
    	    <span><?php echo count($search_posts_needs) ?></span>
    	</li>
	<?php endif; ?>
	<?php
	$plugin = 'dlbi-sodexo-products/dlbi-sodexo-products.php';
	// Check if plugin is enabled
	if (is_plugin_active($plugin)) :
	    ?>
    	<li>
    	    <a href="<?php echo site_url() . '/?s=' . $query_var ?>&search=products" id="products-critere">
		    <?php echo __('Products', 'lbi-sodexo-theme'); ?>
    	    </a>
    	    <span><?php echo count($search_posts_products) ?></span>
    	</li>
	<?php endif; ?>
	<?php
	$plugin = 'dlbi-sodexo-testimonials/dlbi-sodexo-testimonials.php';
	// Check if plugin is enabled
	if (is_plugin_active($plugin)) :
	    ?>
    	<li>
    	    <a href="<?php echo site_url() . '/?s=' . $query_var ?>&search=testimonials" id="testimonials-critere">
		    <?php echo __('Testimonials', 'lbi-sodexo-theme'); ?>
    	    </a>
    	    <span><?php echo count($search_posts_testimonials) ?></span>
    	</li>
	<?php endif; ?>
	<?php
	$plugin = 'dlbi-sodexo-faq/dlbi-sodexo-faq.php';
	// Check if plugin is enabled
	if (is_plugin_active($plugin)) :
	    ?>
    	<li>
    	    <a href="<?php echo site_url() . '/?s=' . $query_var ?>&search=faq" id="faq-critere">
		    <?php echo __('Faq', 'lbi-sodexo-theme'); ?>
    	    </a>
    	    <span><?php echo count($search_posts_faq) ?></span>
    	</li>
	<?php endif; ?>
    </ul>

</div>
