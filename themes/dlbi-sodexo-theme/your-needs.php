<?php
/*
 * Template Name: Your Needs
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
*/
get_header();

$need = get_post( get_the_ID() );
?>

<?php
$needsheadimg     = get_field( 'your_needs_header_image' );
$image_attributes = wp_get_attachment_image_src( $needsheadimg, 'full' );

$idSlider = get_field( 'id_slider' );
if ( ! empty( $idSlider ) ) : ?>
    <div class="lay-slider">
		<?php layerslider( $idSlider ); ?>
    </div>
<?php else : ?>
    <div class="hero">
		<?php
		$needsheadimg = get_field( 'your_needs_header_image' );
		$size         = 'soxo-hero-header';
		if ( $needsheadimg ) : ?>
			<?php
			echo apply_filters( "dlbi_image", wp_get_attachment_image_url( $needsheadimg, $size ) ); endif; ?>
    </div>
<?php endif; ?>

<?php get_template_part( 'layouts-acf/block_posts-grid' ); // Load post grid layout ?>

<!-- display block link -->
<div class="container-fluid">
	<?php echo do_shortcode( $need->post_content ); ?>
</div>
<div class="breadcrumb">
	<?php
	if ( function_exists( 'bcn_display' ) ):
		bcn_display();
	endif; ?>
</div>
<?php get_footer(); ?>
