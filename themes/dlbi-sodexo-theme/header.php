<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 * Soxo favicons package http://realfavicongenerator.net/favicon_result?file_id=p1blod806d18svgr11lvu15co1bnh6
 */
// $container = get_theme_mod( 'understrap_container_type' );
$imagepath     = get_stylesheet_directory_uri() . '/dist/images';
$loginurl      = get_field( 'url_login', 'option' );
$customlogo    = get_field( 'custom_logo', 'option' );
$is_rp_content = get_field( 'is_rp_content', 'option' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<?php
		if ( the_field( 'analytics_head_code', 'option' ) ) {
			echo the_field( 'analytics_head_code', 'option' );
		}
		if ( get_field( 'enable_ab_tasty', 'option' ) ) {
			echo '<script type="text/javascript" src="//try.abtasty.com/3dcc003fe36fabcf437a31a8a3c4f23c.js"></script>';
		}
		?>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<meta name="mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?>">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $imagepath; ?>/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo $imagepath; ?>/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $imagepath; ?>/favicon-16x16.png">
		<link rel="mask-icon" href="<?php echo $imagepath; ?>/safari-pinned-tab.svg" color="#283897">
		<link rel="shortcut icon" href="<?php echo $imagepath; ?>/favicon.ico">
		<meta name="theme-color" content="#283897">
		<meta name="google-site-verification" content="HptBSJQhrOq8E78UlypRE7bFHoT4HFNf-pQwqMmN2ek" />
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		<?php wp_head(); ?>

		<!--[if lt IE 9]>
			<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
		<![endif]-->
	</head>

	<body <?php body_class(); ?>>

		<?php
		if ( get_field( 'analytics_body_code', 'option' ) ) {
			echo the_field( 'analytics_body_code', 'option' );
		}
		?>

		<div id="page">
			<a class="skip-link screen-reader-text sr-only" href="#content"><?php esc_html_e( 'Skip to content', 'lbi-sodexo-theme' ); ?></a>
			<?php
			if ( is_front_page() || $is_rp_content ) :
				?>
					<nav id="MenuTop" class="navbar">
						<div class="top-menu-left-section">
							<h1>
								<a class="navbar-brand" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
									<?php
									if ( ! $customlogo ) :
										echo apply_filters( 'dlbi_image', $imagepath . '/logo-sodexo-header.svg', '', get_bloginfo( 'name' ), '', 200, 70 );
									else :
										echo apply_filters( 'dlbi_image', $customlogo['url'], '', get_bloginfo( 'name' ), '', 185, 65 );
									endif;
									?>
								</a>
							</h1>
						</div>
						<?php
						wp_nav_menu(
							array(
								'theme_location'  => 'top-nav',
								'container_class' => '',
								'container_id'    => 'top-nav',
								'menu_class'      => 'navbar-nav',
								'fallback_cb'     => '',
								'menu_id'         => '',
								'walker'          => new Walker_Nav_Menu_Top(),
							)
						);
						?>
						<div class="top-menu-right-section">
						<?php if ( $loginurl && ! $is_rp_content ) : ?>
							<div class="top-menu-login-item hidden-md-down">
								<a title="<?php echo __( 'Login', 'lbi-sodexo-theme' ); ?>" href="<?php echo $loginurl; ?>" onclick="dataLayer.push({'event': 'click-login'});" class="btn-sodexo btn-sodexo-white icon-person"><?php echo __( 'Login', 'lbi-sodexo-theme' ); ?></a>
							</div>
						<?php endif; ?>
							<div class="burger">
								<button class="soxo-hamburger collapsed" type="button" data-toggle="collapse" data-target="#sideNav" aria-controls="sideNav" aria-expanded="false" aria-label="Toggle navigation">
									<span><?php echo __( 'toggle menu', 'lbi-sodexo-theme' ); ?></span>
								</button>
								<div><?php echo get_field( 'burger_title', 'option' ); ?></div>
							</div>
						<?php if ( is_multisite() && get_blog_count() > 1 ) : ?>
							<div>
								<div class='multilingue'>
								<?php
								if ( function_exists( 'the_msls' ) ) {
										the_msls();
								}
								?>
								</div>
							</div>
						<?php endif; ?>
					</nav><!-- .site-navigation -->
			<?php endif; ?>
			<div id="sideNavOverlay" role="button" class="sidenav-overlay collapsed" data-toggle="collapse" data-target="#sideNav" aria-controls="sideNav" aria-expanded="false"></div>

			<nav id="sideNav" class="collapse navbar-collapse">
				<button class="soxo-hamburger" type="button" data-toggle="collapse" data-target="#sideNav" aria-controls="sideNav" aria-expanded="true" aria-label="Toggle navigation"><span><?php echo __( 'toggle menu', 'lbi-sodexo-theme' ); ?></span></button>
				<div class="navbar-burger-menu-bar">

					<?php if ( $loginurl && ! $is_rp_content ) { ?>
						<div class="side-menu-login-item">
							<a class="btn-sodexo btn-sodexo-red icon-person" target="_blank" href="<?php echo $loginurl; ?>" onclick="dataLayer.push({'event': 'click-login'});" title="<?php echo __( 'LOGIN TO MY ACCOUNT', 'lbi-sodexo-theme' ); ?>"><?php echo __( 'LOGIN TO MY ACCOUNT', 'lbi-sodexo-theme' ); ?></a>
						</div>
					<?php } ?>

					<?php get_template_part( 'inc/banner-search-burger' ); ?>

					<?php get_template_part( 'layouts-menu/burger-menu' ); // Load burger menu layout ?>

				</div>
			</nav>

			<?php
			if ( is_front_page() && ! $is_rp_content ) {
				get_template_part( 'templates/content', 'banner_home' );
			} elseif ( is_page_template( 'homepage-affiliate.php' ) ) {
				get_template_part( 'templates/content', 'banner_home_affiliate' );
			}
			?>
			<?php
			$affiliate_menu           = array();
			$customers_menu           = array();
			$pages_for_affiliate_menu = get_field( 'pages_for_affiliate_menu', 'option' );
			$pages_for_customers_menu = get_field( 'pages_for_customers_menu', 'option' );
			if ( $pages_for_affiliate_menu ) {
				$affiliate_menu = wp_list_pluck( $pages_for_affiliate_menu, 'ID' );
			}
			if ( $pages_for_customers_menu ) {
				$customers_menu = wp_list_pluck( $pages_for_customers_menu, 'ID' );
			}

			if ( in_array( get_the_ID(), $affiliate_menu ) ) {
				$custom_nav = 'Top Menu affiliate Bar';
			} elseif ( in_array( get_the_ID(), $customers_menu ) ) {
				$custom_nav = 'Top Menu Bar customers';
			} else {
				$custom_nav = 'Top Menu Bar';
			}
			?>
			<?php if ( ! $is_rp_content ) : ?>
				<nav id="navbar-combined" class="navbar">
					<div class="container">
						<div class="navbar-top-menu-bar">
							<a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
								<?php echo apply_filters( 'dlbi_image', $imagepath . '/logo-sodexo-sticky.svg', '', get_bloginfo( 'name' ), '', 185, 65 ); ?>
							</a>

							<?php
							wp_nav_menu(
								array(
									'menu'            => $custom_nav,
									// 'container' => false,
									'container_class' => '',
									'container_id'    => '',
									'menu_class'      => 'top-menu-main',
									'fallback_cb'     => '',
									'menu_id'         => '',
									'before'          => '',
									'after'           => '',
									'walker'          => new Walker_Nav_Top_Menu(),
								)
							);

							if ( $loginurl && ! $is_rp_content ) {
								?>
								<div class="combined-menu-login-item">
									<a title="<?php echo __( 'Login', 'lbi-sodexo-theme' ); ?>" href="<?php echo $loginurl; ?>" onclick="dataLayer.push({'event': 'click-login'});" class="icon-person"><span class="sr-only"><?php echo __( 'Login', 'lbi-sodexo-theme' ); ?></span></a>
								</div>
							<?php } ?>

							<div class="burger">
								<button class="soxo-hamburger collapsed" type="button" data-toggle="collapse" data-target="#sideNav" aria-controls="sideNav" aria-expanded="false" aria-label="Toggle navigation">
									<span><?php echo __( 'toggle menu', 'lbi-sodexo-theme' ); ?></span>
								</button>
								<div><?php echo get_field( 'burger_title', 'option' ); ?></div>
							</div>

							<?php if ( is_multisite() && get_blog_count() > 1 ) : ?>
								<div class='multilingue'>
									<?php
									if ( function_exists( 'the_msls' ) ) {
										the_msls();
									}
									?>
								</div>
							<?php endif; ?>

						</div>
					</div>
				</nav>
				<?php
			endif ?>
