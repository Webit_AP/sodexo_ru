// Plugins
var argv = require('minimist')(process.argv.slice(2));
var autoprefixer = require('gulp-autoprefixer');
var babel = require('gulp-babel');
var browsersync = require('browser-sync');
var changed = require('gulp-changed');
var concat = require('gulp-concat');
var cssnano = require('gulp-cssnano');
var del = require('del');
var eslint = require('gulp-eslint');
var flatten = require('gulp-flatten');
var gulp = require('gulp');
var gulpif = require('gulp-if');
var imagemin = require('gulp-imagemin');
var imageminJpg = require('imagemin-jpeg-recompress');
var imageminPng = require('imagemin-pngquant');
var notify = require('gulp-notify');
var plumber = require('gulp-plumber');
var rename = require('gulp-rename');
var runSequence = require('run-sequence');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');

// Get project config
var config = require('./src/config.json'),
    path = config.path,
    dependencies = config.dependencies;

// Command line options
var enabled = {

    // Enable production build argument
    production: argv.production
};

// Custom error handler to send native system notifications
var onError = function (err) {
    notify.onError({
        title: "Gulp",
        message: "<%= error.message %>"
    })(err);
    this.emit('end');
};

var plumberOptions = {
    errorHandler: onError,
};

//
// Tasks
//

// ## Styles
// 'gulp styles' - Compiles, autoprefixes, minifies and generates source maps
// for styles.
gulp.task('styles', function () {
    return gulp.src(dependencies.styles)
        .pipe(plumber(plumberOptions))
        //.pipe(gulpif(!enabled.production, sourcemaps.init()))
        .pipe(concat('main.css'))
        .pipe(rename({suffix: '.min'}))
        .pipe(sass({
            outputStyle: 'nested',
        }))
        .pipe(autoprefixer({
            browsers: ['last 2 versions']
        }))
        //.pipe(cssnano())
        //.pipe(gulpif(!enabled.production, sourcemaps.write()))
        .pipe(gulp.dest(path.dist + 'styles'))
        .pipe(browsersync.stream());
});
gulp.task('stylesAdmin', function () {
    return gulp.src(dependencies.stylesAdmin)
        .pipe(plumber(plumberOptions))
        //.pipe(gulpif(!enabled.production, sourcemaps.init()))
        .pipe(concat('admin.css'))
        .pipe(rename({suffix: '.min'}))
        .pipe(sass({
            outputStyle: 'nested',
        }))
        .pipe(autoprefixer({
            browsers: ['last 2 versions']
        }))
        //.pipe(cssnano())
        //.pipe(gulpif(!enabled.production, sourcemaps.write()))
        .pipe(gulp.dest(path.dist + 'styles'))
        .pipe(browsersync.stream());
});

gulp.task('stylesForProd', function () {
    return gulp.src(dependencies.styles)
        .pipe(plumber(plumberOptions))
        //.pipe(gulpif(!enabled.production, sourcemaps.init()))
        .pipe(concat('main.css'))
        .pipe(rename({suffix: '.min'}))
        .pipe(sass({
            outputStyle: 'compressed',
        }))
        .pipe(autoprefixer({
            browsers: ['last 2 versions']
        }))
        //.pipe(cssnano())
        //.pipe(gulpif(!enabled.production, sourcemaps.write()))
        .pipe(gulp.dest(path.dist + 'styles'))
        .pipe(browsersync.stream());
});

gulp.task('stylesAdminForProd', function () {
    return gulp.src(dependencies.stylesAdmin)
        .pipe(plumber(plumberOptions))
        //.pipe(gulpif(!enabled.production, sourcemaps.init()))
        .pipe(concat('admin.css'))
        .pipe(rename({suffix: '.min'}))
        .pipe(sass({
            outputStyle: 'compressed',
        }))
        .pipe(autoprefixer({
            browsers: ['last 2 versions']
        }))
        //.pipe(cssnano())
        //.pipe(gulpif(!enabled.production, sourcemaps.write()))
        .pipe(gulp.dest(path.dist + 'styles'))
        .pipe(browsersync.stream());
});

// gulp.task('stylesWidgets', function () {
//     return gulp.src(dependencies.stylesWidgets)
//         .pipe(plumber(plumberOptions))
//         //.pipe(gulpif(!enabled.production, sourcemaps.init()))
//         .pipe(concat('widgets.css'))
//         .pipe(rename({ suffix: '.min' }))
//         .pipe(sass({
//             outputStyle: 'nested',
//         }))
//         .pipe(autoprefixer({
//             browsers: ['last 2 versions']
//         }))
//         //.pipe(cssnano())
//         //.pipe(gulpif(!enabled.production, sourcemaps.write()))
//         .pipe(gulp.dest(path.distWidgets + 'styles'))
//         .pipe(browsersync.stream());
// });


// ## Scripts
// 'gulp scripts' - Lints, transpiles ES6, combines, minifies and generates
// source maps for scripts
gulp.task('scripts', ['lint'], function () {
    return gulp.src(dependencies.scripts)
        .pipe(plumber(plumberOptions))
        .pipe(gulpif(!enabled.production, sourcemaps.init()))
        .pipe(concat('main.js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(uglify())
        .pipe(gulpif(!enabled.production, sourcemaps.write()))
        .pipe(gulp.dest(path.dist + 'scripts'))
        .pipe(browsersync.stream());
});

gulp.task('scriptsBo', ['lint'], function () {
    return gulp.src(dependencies.scriptsBo)
        .pipe(plumber(plumberOptions))
        .pipe(gulpif(!enabled.production, sourcemaps.init()))
        .pipe(concat('main-bo.js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(uglify())
        .pipe(gulpif(!enabled.production, sourcemaps.write()))
        .pipe(gulp.dest(path.dist + 'scripts'))
        .pipe(browsersync.stream());
});

// ## Scripts for Prod
// 'gulp scripts' - Lints, transpiles ES6, combines, minifies and generates
// no source maps for scripts
gulp.task('scriptsForProd', ['lint'], function () {
    return gulp.src(dependencies.scripts)
        .pipe(plumber(plumberOptions))
        // .pipe(gulpif(!enabled.production, sourcemaps.init()))
        .pipe(concat('main.js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        // .pipe(gulpif(!enabled.production, sourcemaps.write()))
        .pipe(gulp.dest(path.dist + 'scripts'))
    // .pipe(browsersync.stream());
});
gulp.task('scriptsBoForProd', ['lint'], function () {
    return gulp.src(dependencies.scriptsBo)
        .pipe(plumber(plumberOptions))
        // .pipe(gulpif(!enabled.production, sourcemaps.init()))
        .pipe(concat('main-bo.js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        // .pipe(gulpif(!enabled.production, sourcemaps.write()))
        .pipe(gulp.dest(path.dist + 'scripts'))
    // .pipe(browsersync.stream());
});

// gulp.task('scriptsWidgets', function () {
//     return gulp.src(dependencies.scriptsWidgets)
//         .pipe(plumber(plumberOptions))
//         .pipe(gulpif(!enabled.production, sourcemaps.init()))
//         .pipe(concat('widgets.js'))
//         .pipe(rename({ suffix: '.min' }))
//         .pipe(babel({
//             presets: ['es2015']
//         }))
//         .pipe(uglify())
//         .pipe(gulpif(!enabled.production, sourcemaps.write()))
//         .pipe(gulp.dest(path.distWidgets + 'scripts'))
//         .pipe(browsersync.stream());
// });

gulp.task('scriptsVendors', function () {
    var scripts = [
        path.vendors + 'js/tether.js', // Must be loaded before BS4

        // Start - All BS4 stuff
        path.vendors + 'js/bootstrap4/bootstrap.js',

        // End - All BS4 stuff

        path.vendors + 'js/skip-link-focus-fix.js',
        path.assets + 'scripts/lib/**/*.js'
    ];
    gulp.src(scripts)
        .pipe(concat('vendors.min.js'))
        //.pipe(uglify())
        .pipe(gulp.dest(path.dist + 'scripts'))

});

gulp.task('scriptsVendorsForProd', function () {
    var scripts = [
        path.vendors + 'js/tether.js', // Must be loaded before BS4

        // Start - All BS4 stuff
        path.vendors + 'js/bootstrap4/bootstrap.js',

        // End - All BS4 stuff

        path.vendors + 'js/skip-link-focus-fix.js',
        path.assets + 'scripts/lib/**/*.js'
    ];
    gulp.src(scripts)
        .pipe(concat('vendors.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(path.dist + 'scripts'))

});

// ## Images
// 'gulp images' - Optimizes images with imagemin
gulp.task('images', function () {
    return gulp.src(path.assets + 'images/**/*')
        .pipe(changed(path.dist + 'images'))
        .pipe(imagemin(
            [imageminPng(), imageminJpg()],
            {verbose: true}
        ))
        .pipe(gulp.dest(path.dist + 'images'))
        .pipe(browsersync.stream());
});

// ## Fonts
// 'gulp fonts' - Gathers font files and outputs to flat directory structure
gulp.task('fonts', function () {
    return gulp.src([
        path.assets + 'fonts/**/*.eot',
        path.assets + 'fonts/**/*.svg',
        path.assets + 'fonts/**/*.ttf',
        path.assets + 'fonts/**/*.woff',
        path.assets + 'fonts/**/*.woff2'
    ])
        .pipe(flatten())
        .pipe(gulp.dest(path.dist + 'fonts'))
        .pipe(browsersync.stream());
});

// ## Lint
// 'gulp lint' - Lints main project scripts with eslint
gulp.task('lint', function () {
    return gulp.src([
        path.assets + 'scripts/**/*.js',
        '!' + path.assets + 'scripts/lib/**/*.js'
    ])
        .pipe(plumber(plumberOptions))
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});

// gulp.task('lint-widgets', function () {
//     return gulp.src(path.assetsWidgets + 'scripts/**/*.js')
//         .pipe(plumber(plumberOptions))
//         .pipe(eslint())
//         .pipe(eslint.format())
//         .pipe(eslint.failAfterError());
// });

// ## Clean
// 'gulp clean' - Deletes the dist directory
gulp.task('clean', del.bind(null, [path.dist, path.distWidgets]));

// ## Reload
// 'gulp reload' - Forces a manual browser reload
gulp.task('reload', function () {
    browsersync.reload();
});

// ## Build
// 'gulp build' - Builds all assets without cleaning dist, you
//  should use the `gulp` task to ensure a proper build
gulp.task('buildForDev', function () {
    require('gulp-stats')(gulp);
    runSequence('styles', 'scripts', 'scriptsVendors', 'scriptsBo', 'stylesAdmin', ['fonts', 'images']);
});

// ## Build (for prod)
// 'gulp build' - Builds all assets without cleaning dist, you
//  should use the `gulp` task to ensure a proper build
gulp.task('build', function () {
    require('gulp-stats')(gulp);
    runSequence('stylesForProd', 'scriptsForProd', 'scriptsVendorsForProd', 'scriptsBoForProd', 'stylesAdminForProd', ['fonts', 'images']);
});

// ## Gulp
// 'gulp' - Builds all assets
// 'gulp --production' - Builds all assets for production (no source maps)
gulp.task('default', function () {
    runSequence('clean', 'build');
});

// ## Watch
// 'gulp watch' - Monitors theme files and assets for changes and live reloads
// with Browsersync. You must update your devUrl in config.json to reflect your
// local development hostname.
gulp.task('watch', ['buildForDev'], function () {
    browsersync.init({
        proxy: config.devUrl,
        cors: true,
        snippetOptions: {
            whitelist: ['/wp-admin/admin-ajax.php'],
            blacklist: ['/wp-admin/**']
        }

    });

    gulp.watch(['**/*.php'], ['reload']);
    gulp.watch([path.assets + 'styles/**/*'], ['styles']);
    gulp.watch([path.assets + 'styles/admin/**/*'], ['stylesAdmin']);
    gulp.watch([path.assets + 'scripts/**/*'], ['scripts']);
    gulp.watch([path.assets + 'scripts/admin/**/*'], ['scriptsBo']);

    // gulp.watch([path.assetsWidgets + 'styles/**/*'], ['stylesWidgets']);
    // gulp.watch([path.assetsWidgets + 'scripts/**/*'], ['scriptsWidgets']);

    gulp.watch([path.assets + 'fonts/**/*'], ['fonts']);
    gulp.watch([path.assets + 'images/**/*'], ['images']);
});


// Run:
// gulp post-install.
// Copy all needed dependency assets files from bower_component assets to themes /js, /scss and /fonts folder. Run this task after bower install or bower update


// Copy all Bootstrap JS files
gulp.task('post-install', function () {

    ////////////////// All Bootstrap 4 Assets /////////////////////////
    // Copy all Bootstrap JS files
    gulp.src(path.node + 'bootstrap/dist/js/**/*.js')
        .pipe(gulp.dest(path.vendors + 'js/bootstrap4'));

    // Copy all Bootstrap SCSS files
    gulp.src(path.node + 'bootstrap/scss/**/*.scss')
        .pipe(gulp.dest(path.vendors + 'sass/bootstrap4'));
    ////////////////// End Bootstrap 4 Assets /////////////////////////

    // Copy all UnderStrap SCSS files
    gulp.src(path.node + 'understrap/sass/**/*.scss')
        .pipe(gulp.dest(path.vendors + 'sass/understrap'));

    // Copy all Font Awesome Fonts
    gulp.src(path.node + 'font-awesome/fonts/**/*.{ttf,woff,woff2,eof,svg}')
        .pipe(gulp.dest('./fonts'));

    // Copy all Font Awesome SCSS files
    gulp.src(path.node + 'font-awesome/scss/*.scss')
        .pipe(gulp.dest(path.vendors + 'sass/fontawesome'));

    // Copy jQuery
    gulp.src(path.node + 'jquery/dist/*.js')
        .pipe(gulp.dest(path.vendors + 'js'));

    // _s SCSS files
    gulp.src(path.node + 'undescores-for-npm/sass/**/*.scss')
        .pipe(gulp.dest(path.vendors + 'sass/underscores'));

    // _s JS files
    gulp.src(path.node + 'undescores-for-npm/js/*.js')
        .pipe(gulp.dest(path.vendors + 'js'));

    // Copy Tether JS files
    gulp.src(path.node + 'tether/dist/js/*.js')
        .pipe(gulp.dest(path.vendors + 'js'));

    // Copy Tether CSS files
    gulp.src(path.node + 'tether/dist/css/*.css')
        .pipe(gulp.dest(path.vendors + 'css'));
});