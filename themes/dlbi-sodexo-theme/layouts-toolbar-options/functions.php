<?php
/**
 * This file manage the layouts-toolbar-options components
 *
 * @package understrap
 * @subpackage dlbi-sodexo-theme
 */

/**
 * Take and return an array,
 * if quotation exist,
 * sort this array with quotation at the first index.
 *
 * @param type $array array.
 * @param type $regex string.
 * @return type $array an array.
 */
function set_array_quotation_first( $array, $regex = 'devis' ) {
	foreach ( $array as $key => $value ) {
		if ( isset( $value['type_of_block'][0]['external_link']['title'] ) ) {
			$quotation = $value['type_of_block'][0]['external_link']['title'];
			if ( preg_match( '/' . $regex . '/', $quotation ) ) {
				array_unshift( $array, $value );
				unset( $array[ $key + 1 ] );
				return array_values( $array );
			}
		}
	}
	return $array;
}
