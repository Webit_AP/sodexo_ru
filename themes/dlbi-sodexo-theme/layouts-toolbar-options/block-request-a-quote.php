<?php
// REQUEST A QUOTE
// $request_picto= get_field('request_a_quote_picto', 'option');





if (get_field('request_a_quote_show_block', 'option')): ?>
    <li>
        <?php
        // LINK
        if (get_field('request_a_quote_link', 'option')):
        ?>


        <?php
        // -----------------------------------------------------------------
        // CUSTOM PICTO
        $customPicto = get_field( "request_a_quote_picto", 'option' );
        if( $customPicto ) {
        //echo json_encode($customPicto);
        //echo $customPicto['sizes']['medium'] ;
        $style = apply_filters("lazyload-style", "background-image:url('".$customPicto['sizes']['medium']."');background-size:contain;background-repeat:no-repeat;background-position:left;");
        ?>
        <button type="button" class="quote" aria-controls="lmt-quote" aria-expanded="false" <?php echo $style; ?> >

            <?php
            } else { ?>

            <button type="button" class="quote icon-messages" aria-controls="lmt-quote" aria-expanded="false">

                <?php } ?>
                <span>Ask for a quote</span>
            </button>

            <div id="lmt-quote" class="lmt-layer-quote form-layer" aria-hidden="true">

                <button type="button" class="lmt-close">
                    <span><?php echo __('Close overlay', 'lbi-sodexo-theme') ?></span>
                </button>

                <div class="header">
                    <p class="tt"><?php echo get_field('request_a_quote_title', 'option') ?></p>
                </div>
                <div><iframe src="<?php echo get_field('request_a_quote_link', 'option') ?>" border="0"></iframe></div>
            </div>

            <?php endif;?>
    </li>
<?php endif ?>
