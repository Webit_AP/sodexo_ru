<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

get_header();
?>
<div class="col-md-12">
  <div class="container">
  <div class="row">
   <h1 class="sodexo-title"><?php the_title(); ?></h1>
      <div class="col-sm-12">
        <?php
        if ( have_posts() ) :
          while ( have_posts() ) : the_post();
           // Get list FAQ
           $faq = get_field('faq_questions', get_the_ID());
           ?>
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

            <?php
            $z = 0;
            foreach( $faq  as $question): ?>

              <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                      <a class="faq-title collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $z; ?>" aria-expanded="true" aria-controls="collapse<?php echo $z; ?>">
                      <?php echo $question['faq_question'] ?>
                      </a>
                    </h4>
                  </div>
                  <div id="collapse<?php echo $z; ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading<?php echo $z; ?>">
                    <div class="panel-body">
                      <?php echo $question['faq_answer'] ?>
                    </div>
                  </div>
              </div>

            <?php
            $z++;
            endforeach;?>
            </div>
        <?php endwhile;?>
      <?php endif ?>
      </div>
      </div>
  </div>
</div>
<div class="breadcrumb">
    <?php
    if(function_exists('bcn_display')):
        bcn_display();
    endif; ?>
</div>
<?php get_footer(); ?>
