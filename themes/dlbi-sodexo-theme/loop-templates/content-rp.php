<?php
/**
 * Partial template for content in homepage-blog.php
 *
 * @package understrap
 */
// Load posts
?>
<?php
$args = array(
    'numberposts' => 3,
    'post_type' => 'post',
    'post__in' => get_option('sticky_posts'),
    'ignore_sticky_posts' => 1
);
$obj = get_queried_object();
if (array_key_exists('name', $obj)) {
    $args['category_name'] = $obj->name;
}
$sticky = get_posts($args);
?>
<?php

foreach ($sticky as $post):

    $date = get_the_date('d.m.Y', $post->ID);

    ?>
    <li class="blog-item col-sm-12 col-lg-6 sticky">
        <a href = "<?php echo get_the_permalink($post->ID) ?>">

	        <?php if ( has_post_thumbnail( $post->ID ) ) :
		        echo apply_filters( "dlbi_image", get_the_post_thumbnail_url( $post->ID, 'soxo-blog-medium' ), "", get_the_title( $post->ID ) );
	        endif; ?>
            <div class="blog-component--container">

                <?php
                $categories = get_the_category($post->ID);

                if (!empty($categories)) :
                    ?>
                    <p class="blog-component--container_cat"><?php echo esc_html($categories[0]->name); ?></p>
                <?php endif; ?>

                <p class="blog-component--container_date"><?php echo $date; ?></p>
                <p class="blog-component--container_title"><?php echo get_the_title($post->ID); ?></p>
                <?php if (get_post_type() != SOD_TES_PTYPE) : ?>
                    <div  class="blog-component--container_content"><?php echo strip_shortcodes(wp_trim_words($post->post_content, 20, '...')); ?></div>
                <?php endif ?>
                <p class="blog-component--container_link icon-arrow-right">
                    <span><?php echo __('Read more', 'lbi-sodexo-theme') ?></span>
                </p>
            </div>
        </a>
    </li>
<?php endforeach; ?>
<?php
$args = array(
    'numberposts' => -1,
    'post_type' => 'post'
);
$obj = get_queried_object();
if (array_key_exists('name', $obj)) {
    $args['category_name'] = $obj->name;
}
if($sticky) {
    $sticky_post = wp_list_pluck($sticky, 'ID');
    $args['exclude'] = $sticky_post;
}
$posts = get_posts($args);
?>
<?php
$count_posts = 0;
foreach ($posts as $post):
    $date = get_the_date('d.m.Y', $post->ID);
    if (!array_key_exists('name', $obj)) {
        if ($count_posts == 0): 
            // Get template widget
            echo('<li class="blog-item col-lg-3">');
            // get_template_part('templates/content', 'widget-newsletter');
            get_sidebar('right-single');
            echo('</li>');
        endif;
    }
    ?>

    <li class="blog-item col-lg-3">
        <a href = "<?php echo get_the_permalink($post->ID) ?>">
	        <?php if ( has_post_thumbnail( $post->ID ) ) :
		        echo apply_filters( "dlbi_image", get_the_post_thumbnail_url( $post->ID, 'medium' ), "", get_the_title( $post->ID ) ); ?>
		        <?php // echo get_the_post_thumbnail( get_the_ID(), 'medium', array( 'class' => 'blog-component--container_image' ) );
		        ?>
	        <?php endif; ?>
            <div class="blog-component--container">

                <?php
                $categories = get_the_category($post->ID);

                if (!empty($categories)) :
                    ?>
                    <p class="blog-component--container_cat"><?php echo esc_html($categories[0]->name); ?></p>
                <?php endif; ?>

                <p class="blog-component--container_date"><?php echo $date; ?></p>
                <p class="blog-component--container_title"><?php echo get_the_title($post->ID); ?></p>
                <?php if (get_post_type() != SOD_TES_PTYPE) : ?>
                    <div  class="blog-component--container_content"><?php echo strip_shortcodes(wp_trim_words($post->post_content, 20, '...')); ?></div>
                <?php endif ?>
                <p class="blog-component--container_link icon-arrow-right">
                    <span><?php echo __('Read more', 'lbi-sodexo-theme') ?></span>
                </p>
            </div>
        </a>
    </li>
<?php $count_posts++; ?>
<?php endforeach; ?>