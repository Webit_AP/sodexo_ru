<?php
/**
 * Partial template for content in homepage-blog.php
 *
 * @package understrap
 */
// Load posts
$feed = fetch_feed(get_field('feed_url', 'option'));
if (!array_key_exists ('errors', $feed)) {
    $items = $feed->get_items();
}

$feed_category = get_field('feed_post_categories', 'option');
?>

<?php
$i = 0;
if (!array_key_exists ('errors', $feed)):
    foreach ($items as $item):
        if ($i <= 15):
            ?>
            <li class="blog-item col-sm-6 col-lg-3">
                <a href="<?php echo $item->get_link(); ?>">
	                <?php echo apply_filters( "dlbi_image", $item->get_enclosure()->link, "", $item->get_title() ); ?>
                    <div class="blog-component--container">

                        <?php
                        $categories = get_the_category_by_ID($feed_category[0]);
                        if ($categories) :
                            ?>
                            <p class="blog-component--container_cat"><?php echo $categories; ?></p>
                        <?php endif; ?>

                        <p class="blog-component--container_date"><?php echo $item->get_date('d.m.Y'); ?></p>
                        <p class="blog-component--container_title"><?php echo $item->get_title(); ?></p>
                        <div  class="blog-component--container_content"><?php echo wp_trim_words($item->get_content(), 20, '...'); ?></div>
                        <p class="blog-component--container_link icon-arrow-right">
                            <span><?php echo __('Read more', 'lbi-sodexo-theme') ?></span>
                        </p>
                    </div>
                </a>
            </li>
            <?php
            $i++;
        endif;
    endforeach;
endif;