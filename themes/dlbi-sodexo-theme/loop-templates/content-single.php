<?php
/**
 * Single post partial template.
 *
 * @package understrap
 */
$content = do_shortcode( get_the_content() );
$title   = get_the_title();
if ( strpos( $content, "<h1" ) !== false ) {
	//title is already defined in the content SOXINT-768
	$title = '<span class="sodexo-article-title">' . $title . '</span>';
} else {
	$title = '<h1 class="sodexo-article-title">' . $title . '</h1>';
}

?>
<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
    <header class="entry-header">
        <!-- display title -->
		<?php echo $title ?>
        <!-- display author -->
        <p class="article-infos"><?php echo get_the_date( 'd M Y' ); ?></p>
    </header><!-- .entry-header -->
    <div class="entry-content static-content">
		<?php echo $content ?>
    </div><!-- .entry-content -->
</article><!-- #post-## -->