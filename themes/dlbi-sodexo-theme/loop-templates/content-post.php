<?php
/**
 * Partial template for content in homepage-blog.php
 *
 * @package understrap
 */
// Load posts
$date       = get_the_date( 'd.m.Y', get_the_ID() );
$useWrapper = false;
if ( isset( $_REQUEST["use-image-wrapper"] ) && $_REQUEST["use-image-wrapper"] != "false" ) {
	$useWrapper = true;
}
// $content = apply_filters('the_content', the_content());
// $content = str_replace(']]>', ']]&gt;', the_content());
?>

<a href="<?php the_permalink() ?>" data-id="<?php the_ID() ?>" data-type="<?php echo get_post_type( get_the_ID() ) ?>">

	<?php if ( has_post_thumbnail( get_the_ID() ) ) {
		if ( $useWrapper ) {
			?><div class="image-wrapper"><?php
		}
		echo apply_filters( "dlbi_image", get_the_post_thumbnail_url( get_the_ID(), 'medium' ), "", get_the_title() );
		if ( $useWrapper ) {
			?></div><?php
		}
	} ?>
    <div class="blog-component--container">

		<?php
		$categories = get_the_category();

		if ( ! empty( $categories ) ) :
			?>
            <p class="blog-component--container_cat"><?php echo esc_html( $categories[0]->name ); ?></p>
		<?php endif; ?>

        <p class="blog-component--container_date"><?php echo $date; ?></p>
        <p class="blog-component--container_title"><?php echo the_title(); ?></p>
		<?php if ( get_post_type() != SOD_TES_PTYPE ) : ?>
            <div class="blog-component--container_content"><?php echo strip_shortcodes( wp_trim_words( get_the_content(), 20, '...' ) ); ?></div>
		<?php endif ?>
        <p class="blog-component--container_link icon-arrow-right">
            <span><?php echo __( 'Read more', 'lbi-sodexo-theme' ) ?></span>
        </p>
    </div>
</a>
