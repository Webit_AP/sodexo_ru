<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

?>
<?php if(!empty($_GET['s'])): ?>
<section class="no-results not-found">

	<header class="page-header">

		<h1 class="page-title"><?php echo __( 'Nothing Found', 'lbi-sodexo-theme' ); ?></h1>

	</header><!-- .page-header -->

	<div class="page-content">

		<?php
		if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

			<p><?php printf( wp_kses( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'lbi-sodexo-theme' ), array(
	'a' => array(
		'href' => array(),
	),
) ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

		<?php elseif ( is_search() ) : ?>

			<p><?php echo __( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'lbi-sodexo-theme' ); ?></p>
			<?php
				//get_search_form();
		else : ?>

			<p><?php echo __( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'lbi-sodexo-theme' ); ?></p>
			<?php
				//get_search_form();
		endif; ?>
	</div><!-- .page-content -->
	
</section><!-- .no-results -->
<?php endif ?>
