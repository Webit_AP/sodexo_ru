<?php
	/*
	 * Template Name: Store Locator Page
	 *
	*/
?>

<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php
$idSlider = get_field('id_slider');
if(!empty($idSlider)) : ?>
	<div class="lay-slider">
		<?php layerslider($idSlider); ?>
	</div>
<?php endif; ?>

<div class="store-locator">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="sodexo-title"><?php the_title(); ?></h1>
				<?php the_content(); ?>
			</div>
		</div>
	</div>
	<div data-iframe-map="{&quot;located&quot;: &quot;<?php if( get_field('store_locator_located') ){ echo get_field('store_locator_located'); } ?>&quot;, &quot;unknown&quot;: &quot;<?php if( get_field('store_locator_not_located') ){ echo get_field('store_locator_not_located'); } ?>&quot;}">

		<?php if( get_field('loading_message') ){
			echo get_field('loading_message');
		?>
			<span class="loading-deco"><span></span></span>
		<?php
		} ?>

	</div>
</div>
<?php endwhile; endif; ?>

<?php get_footer(); ?>
