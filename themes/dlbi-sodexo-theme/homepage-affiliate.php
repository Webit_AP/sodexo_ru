<?php
/*
  Template Name: Home Page Affiliate
 */
get_header();

// $container = get_theme_mod('understrap_container_type');

?>

<div id="content" tabindex="-1">

	  <?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?>

			   <?php the_content(); ?>

			<?php endwhile; ?>
	  <?php else : ?>

		<?php get_template_part('loop-templates/content', 'none'); ?>

	    <?php endif; ?>

</div><!-- #content -->

<div class="breadcrumb">
    <?php
    if(function_exists('bcn_display')):
        bcn_display();
    endif; ?>
</div>

<?php get_footer(); ?>
