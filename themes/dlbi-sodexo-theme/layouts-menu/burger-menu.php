
<?php
// Flat table
function wp_nav_menu_object_tree( $nav_menu_items_array ) {
    foreach ( $nav_menu_items_array as $key => $value ) {
        $value->children = array();
        $nav_menu_items_array[ $key ] = $value;
    }

    $nav_menu_levels = array();
    $index = 0;
    if ( ! empty( $nav_menu_items_array ) ) do {
        if ( $index == 0 ) {
            foreach ( $nav_menu_items_array as $key => $obj ) {
                if ( $obj->menu_item_parent == 0 ) {
                    $nav_menu_levels[ $index ][] = $obj;
                    unset( $nav_menu_items_array[ $key ] );
                }
            }
        } else {
            foreach ( $nav_menu_items_array as $key => $obj ) {
                if ( in_array( $obj->menu_item_parent, $last_level_ids ) ) {
                    $nav_menu_levels[ $index ][] = $obj;
                    unset( $nav_menu_items_array[ $key ] );
                }
            }
        }
        $last_level_ids = wp_list_pluck( $nav_menu_levels[ $index ], 'db_id' );
        $index++;
    } while ( ! empty( $nav_menu_items_array ) );

    $nav_menu_levels_reverse = array_reverse( $nav_menu_levels );

    $nav_menu_tree_build = array();
    $index = 0;
    if ( ! empty( $nav_menu_levels_reverse ) ) do {
        if ( count( $nav_menu_levels_reverse ) == 1 ) {
            $nav_menu_tree_build = $nav_menu_levels_reverse;
        }
        $current_level = array_shift( $nav_menu_levels_reverse );
        if ( isset( $nav_menu_levels_reverse[ $index ] ) ) {
            $next_level = $nav_menu_levels_reverse[ $index ];
            foreach ( $next_level as $nkey => $nval ) {
                foreach ( $current_level as $ckey => $cval ) {
                    if ( $nval->db_id == $cval->menu_item_parent ) {
                        $nval->children[] = $cval;
                    }
                }
            }
        }
    } while ( ! empty( $nav_menu_levels_reverse ) );

    $nav_menu_object_tree = $nav_menu_tree_build[ 0 ];
    return $nav_menu_object_tree;
}

$menu_name = 'burger-menu';
if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[$menu_name] ) ) {
    $menu = wp_get_nav_menu_object( $locations[$menu_name] );
    $menu_items = wp_get_nav_menu_items($menu->term_id);
}

$nav_menu_items = wp_nav_menu_object_tree( $menu_items );
?>

<div id="burger-menu" class="menu-burger-menu-container">
    <ul id="menu-burger-menu" class="navbar-nav">

        <?php $index_lv1 = 0; ?>
        <?php foreach ( (array) $nav_menu_items as $key => $menu_item ):?>
            <li class="<?php echo implode(" ",$menu_item->classes);?> menu-item menu-item-has-children nav-item dropdown">
                <?php
                $two = count($menu_item->children);
                $index_lv1++;
                ?>
                <a <?php if($menu_item->target):?>target="_blank"<?php endif; ?> title="<?php echo $menu_item->title;?>" href="<?php if($two > 0): echo "#dp-".$index_lv1; else: echo $menu_item->url; endif ?>" <?php if($two > 0):?>data-toggle="collapse"<?php endif ?> class="nav-link <?php if($two>0):?>dropdown-toggle<?php endif ?>" aria-expanded="false" aria-controls="<?php echo "dp-".$index_lv1; ?>">
                    <?php echo $menu_item->title;?>
                </a>

                <?php if($two > 0):?>
                    <?php $index_lv2 = 0; ?>
                    <!-- Dropdown niveau 1 -->
                    <ul class="sub-menu collapse" role="menu" id="<?php echo "dp-".$index_lv1; ?>">
                        <?php foreach ( (array) $menu_item->children as $key => $menu_children ):
                            $three = count($menu_children->children);
                            $index_lv2++;
                        ?>
                        <li class="<?php echo implode(" ",$menu_children->classes);?> menu-item nav-item">
                            <a <?php if($menu_children->target):?>target="_blank"<?php endif;?> title="<?php echo $menu_children->title;?>" href="<?php if($three > 0): echo "#dp-".$index_lv1."-".$index_lv2; else: echo $menu_children->url; endif?>" <?php if($three > 0):?>data-toggle="collapse"<?php endif ?> class="nav-link <?php if($three > 0):?>dropdown-toggle<?php endif ?>" aria-expanded="false" aria-controls="<?php echo "dp-".$index_lv1."-".$index_lv2; ?>">
                                <?php echo $menu_children->title;?>
                            </a>
                            <?php if($three > 0):?>
                                <!-- Dropdown niveau 2 -->

                                <ul class="sub-menu collapse" role="menu" id="<?php echo "dp-".$index_lv1."-".$index_lv2; ?>">
                                    <?php foreach ( (array) $menu_children->children as $key => $children ):?>
                                       <li class="<?php echo implode(" ",$children->classes);?> menu-item nav-item">
                                             <a <?php if($children->target):?>target="_blank"<?php endif;?> title="<?php echo $children->title;?>" href="<?php echo $children->url;?>" class="nav-link">
                                                <?php echo $children->title;?>
                                            </a>
                                       </li>
                                   <?php endforeach; ?>
                                </ul>
                            <?php endif ?>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif ?>
            </li>
        <?php endforeach; ?>
    </ul>
</div>