<?php
/*
 * Template Name: Become An Affiliate
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
*/
get_header();

$becomanaffiliate = get_post(get_the_ID());
?>

<?php
$idSlider = get_field('id_slider');
if(!empty($idSlider)) : ?>
	<section class="lay-slider">
		<?php layerslider($idSlider); ?>
	</section>
<?php else : ?>
  <section class="hero-push">
      <div class="inner">
          <div class="item">
              <?php
              // Header image
              if (get_field('become_an_affiliate_header_image')):
                  $picture = get_field('become_an_affiliate_header_image');
                  if (!empty($picture)) :?>
                      <div class="media-container">
	                      <?php echo apply_filters( "dlbi_image", $picture['sizes']['large'], "img-fluid" ); ?>
                      </div>
                  <?php endif;
              endif;
              ?>
              <div class="container">
                  <?php

                  if (get_field("become_an_affiliate_header_title")) :
                      // Header title
                      echo '<h1 class="title">';
                      echo get_field('become_an_affiliate_header_title');
                      echo '</h1>';
                  endif;

                  if (get_field("become_an_affiliate_header_description")) :
                     // Header Description
                      echo '<div class="content">';
                      echo get_field('become_an_affiliate_header_description');
                      echo '</div>';
                  endif;

                  if (is_array(get_field('become_an_affiliate_header_link'))):
                      $header_link = get_field("become_an_affiliate_header_link");
                      if ($header_link["url"]):
  			$header_type = get_field('become_an_affiliate_header_select_a_link_type');
  			?>
                          <a <?php if ($header_type == 'Simple link'):?>
                                class="btn-sodexo btn-sodexo-red" href="<?php echo $header_link["url"]; ?>" <?php if($header_link["target"]):?> target="_blank" <?php endif ?>
                          <?php elseif ($header_type == 'Typeform'): ?>
                                class="btn-sodexo btn-sodexo-red" href="javascript:;" data-toggle="modal" data-target="#typeformModal" data-typeform="<?php echo $header_link["url"]; ?>"
                          <?php elseif ($header_type == 'Youtube'): ?>
                                class="btn-sodexo btn-sodexo-red" data-fancybox href="<?php echo $header_link["url"]; ?>"
                          <?php endif; ?>
                         ><?php echo $header_link["title"]; ?></a>
                      <?php endif; ?>
                 <?php endif; ?>
              </div>
          </div>
      </div>

  </section>
<?php endif; ?>

<section class="bg-grey">
    <div class="container become-affiliate-container">

        <?php
        // Title block becom an affiliate
        if (get_field("become_an_affiliate_block_title")) :
            echo '<h2 class="sodexo-title">'. get_field('become_an_affiliate_block_title') .'</h2>';
        endif;

        //--------START We will look for the ACF image of BECOME AN AFFILIATE
        if( have_rows('become_an_affiliate_repeater') ):
        ?>
        <div class="become-affiliate-boxes">

          <div class="become-affiliate-boxes--mobile">

            <div class="become-affiliate-slick">

            <?php foreach(get_field('become_an_affiliate_repeater') as $row): ?>

              <div>

                <div class="become-affiliate-boxes-item">
                  <div>
                    <?php
                    // Icon
                    if ($row['become_an_affiliate_icon']):
                      $picture = $row['become_an_affiliate_icon'];
                      if (!empty($picture)) :?>
                      <div class="media">
                        <img class="img-fluid" src="<?php echo $picture['sizes']['large'] ?>" />
                      </div>
                    <?php endif;
                  endif; ?>

                  <div class="content">
                    <?php if ($row['become_an_affiliate_title']):
                      echo '<div class="choose-sodexo--container_subtitle"><p>'. $row['become_an_affiliate_title'] .'</p></div>';
                    endif; ?>
                    <div class="download-apps-block-text">
                      <?php
                      // Description
                      if ($row['become_an_affiliate_description']):
                        echo $row['become_an_affiliate_description'];
                      endif; ?>
                    </div>
                  </div>
                </div>
              </div>

            </div>

            <?php endforeach; ?>

          </div>

        </div>

        <div class="become-affiliate-boxes--desktop">

          <div class="row">

            <?php foreach(get_field('become_an_affiliate_repeater') as $row): ?>

              <div class="col-md-6 mt-3">

                <div class="become-affiliate-boxes-item">
                  <div>
                    <?php
                    // Icon
                    if ($row['become_an_affiliate_icon']):
                      $picture = $row['become_an_affiliate_icon'];
                      if (!empty($picture)) :?>
                      <div class="media">
                        <img class="img-fluid" src="<?php echo $picture['sizes']['large'] ?>" />
                      </div>
                    <?php endif;
                  endif; ?>

                  <div class="content">
                    <?php
                    if ($row['become_an_affiliate_title']):
                      echo '<div class="choose-sodexo--container_subtitle"><p>'. $row['become_an_affiliate_title'] .'</p></div>';
                    endif; ?>
                    <div class="download-apps-block-text">
                      <?php
                      // Description
                      if ($row['become_an_affiliate_description']):
                        echo $row['become_an_affiliate_description'];
                      endif; ?>
                    </div>
                  </div>
                </div>
              </div>

            </div>

          <?php endforeach; ?>

        </div>

      </div>

    </div>

      <?php endif; ?>

        <?php
        if (get_field('becom_an_affiliate_select_a_link_type')):?>
            <div class="btm-buttons">
            <?php
                if (is_array(get_field('becom_an_affiliate_link'))):
                    // LINK BECOM AN AFFILIATE
                    $affiliate_link = get_field("becom_an_affiliate_link");
                    if ($affiliate_link["url"]):
			$header_type = get_field('become_an_affiliate_header_select_a_link_type');
			?>
                            <a <?php if ($header_type == 'Simple link'):?>
                                class="btn-sodexo" href="<?php echo $affiliate_link["url"]; ?>" <?php if($affiliate_link["target"]):?> target="_blank" <?php endif ?>
                            <?php elseif ($header_type == 'Typeform'): ?>
                                class="btn-sodexo" href="javascript:;" data-toggle="modal" data-target="#typeformModal" data-typeform="<?php echo get_field('becom_an_affiliate_link2');?>"
                            <?php elseif ($header_type == 'Youtube'): ?>
                                class="btn-sodexo" data-fancybox href="<?php echo get_field('becom_an_affiliate_link2');?>"
                            <?php endif ?>
                            >
                            <?php if($affiliate_link["title"]):?>
                                <?php echo $affiliate_link["title"]; ?>
                            <?php else : ?>
                                <?php echo __('BECOME AN AFFILIATE', 'lbi-sodexo-theme');?>
                            <?php endif ?>
                            </a>
                    <?php endif ?>
                <?php endif ?>
            </div>
        <?php endif; ?>

    </div>
</section>


<!-- display widget -->
<!-- <div class="row"> -->
    <!-- <div class="col-12"> -->
        <?php
        echo do_shortcode($becomanaffiliate->post_content);
        ?>
    <!-- </div> -->
<!-- </div> -->

<div class="breadcrumb">
    <?php
    if(function_exists('bcn_display')):
        bcn_display();
    endif; ?>
</div>
<?php get_footer(); ?>
