<?php
define('LAZYSRC', 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==');

function understrap_remove_scripts(){

	// Remove parent theme styles and scripts
	wp_dequeue_style('understrap-styles');
	wp_deregister_style('understrap-styles');

	wp_dequeue_script('understrap-scripts');
	wp_deregister_script('understrap-scripts');

	// Remove js composer styles and scripts
	wp_dequeue_style('js_composer_front');
	wp_deregister_style('js_composer_front');

	wp_dequeue_script('wpb_composer_front_js');
	wp_deregister_script('wpb_composer_front_js');

	// Removes the parent themes stylesheet and scripts from inc/enqueue.php
}

add_action('wp_enqueue_scripts', 'understrap_remove_scripts', 20);

add_action('wp_enqueue_scripts', 'theme_enqueue_styles');

function theme_enqueue_styles(){

	// Get the theme data
	$the_theme         = wp_get_theme();
	$theme_version     = $the_theme->get('Version');
	$version_for_cache = apply_filters("dlbi_file_version", "1015");

	wp_enqueue_style('sodexo-styles', get_stylesheet_directory_uri() . '/dist/styles/main.min.css', array(), $version_for_cache);
	wp_enqueue_script('sodexo-scripts-vendors', get_stylesheet_directory_uri() . '/dist/scripts/vendors.min.js', array(), $theme_version, true);
	wp_enqueue_script('sodexo-scripts', get_stylesheet_directory_uri() . '/dist/scripts/main.min.js', array(), $version_for_cache, true);
	wp_enqueue_style('sodexo-styles-locale', get_stylesheet_directory_uri() . '/style.css', array(), $theme_version);

	// wp_enqueue_style('sodexo-styles-widgets', plugins_url('/dlbi-sodexo-vc-widget/dist/styles/widgets.min.css'), array(), $the_theme->get('Version'));
	// wp_enqueue_script('sodexo-scripts-widgets', plugins_url('/dlbi-sodexo-vc-widget/dist/scripts/widgets.min.js'), array(), $the_theme->get('Version'), true);
	// pass Ajax Url to script.js
	wp_localize_script('sodexo-scripts', 'ajaxurl', admin_url('admin-ajax.php'));
	wp_localize_script('sodexo-scripts', 'SimulatorsSettings', apply_filters("simulator-settings", ["active" => false, "showBlock" => false]));
	wp_localize_script('sodexo-scripts', 'LazyloadSettings', apply_filters("lazyload-settings", ["active" => false]));


	$dqueActive = is_plugin_active('dlbi-dqe/dlbi-dqe.php');
	@wp_localize_script('sodexo-scripts', 'DQEPluginIsActive', $dqueActive);
	if($dqueActive){
		wp_localize_script('sodexo-scripts', 'DQEPluginURL', trailingslashit(get_site_url()) . "app/plugins/dlbi-dqe/assets/php/dqe.php");
		wp_localize_script('sodexo-scripts', 'DQEJSUrl', trailingslashit(get_site_url()) . "app/plugins/dlbi-dqe/assets/js/jquery.dqe.js");
		wp_localize_script('sodexo-scripts', 'DQEJSB2BUrl', trailingslashit(get_site_url()) . "app/plugins/dlbi-dqe/assets/js/jquery.dqeb2b.js");

	}


	wp_localize_script('sodexo-scripts-vendors', 'closetext', __('Close overlay', 'lbi-sodexo-theme'));

	// Direct Link for Carousel
	$direct_link = get_field('direct_link_for_carousel', "option");
	if($direct_link){
		wp_localize_script("sodexo-scripts-vendors", "direct_link", '1');
	}else{
		wp_localize_script("sodexo-scripts-vendors", "direct_link", '0');
	}
}

/* Sodexo custom functions starts here */

// Custom login logo
add_action('login_enqueue_scripts', 'soxo_login_logo');

function soxo_login_logo(){
	?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/dist/images/logo-sodexo-login.svg);
            height: 70px;
            width: 200px;
            background-size: 200px 70px;
            background-repeat: no-repeat;
        }
    </style>
	<?php
}

add_filter('login_headerurl', 'soxo_login_headerurl');

function soxo_login_headerurl(){
	return get_bloginfo('url');
}

add_filter('login_headertitle', 'soxo_login_headertitle');

function soxo_login_headertitle(){
	return get_bloginfo('name');
}

// Custom image size
add_image_size('soxo-medium', 650, 500, true);

// Hero image header
add_image_size('soxo-hero-header', 1500, 350, true);

// Image blog homepage large (= 510x320 format)
add_image_size('soxo-blog-medium', 590, 370, true);


// Image sub menu (= 200x125 format)
add_image_size('soxo-sub-menu', 200, 125, true);

/**
 * Register Sidebar.
 *
 * @since dlbi-sodexo-theme 1.0
 *
 * @return void
 */
function lbi_sodexo_theme_widgets_init(){
	/**
	 * Sidebar that appears right in a single blog
	 * page and single testimonials
	 *
	 */
	$args = [
		'name'          => __('Single Right Sidebar', 'dlbi-sodexo-theme'),
		'id'            => 'right-single',
		'description'   => __('Right widget sidebar area for page single : blog and testimonials', 'dlbi-sodexo-theme'),
		'class'         => '',
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget'  => '</li>',
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => '</h2>',
	];

	// Register sidebar
	register_sidebar($args);

	/**
	 * Footer 1
	 *
	 */
	$args = [
		'name'          => __('Footer 1', 'dlbi-sodexo-theme'),
		'id'            => 'footer-1',
		'description'   => __('Footer 1', 'dlbi-sodexo-theme'),
		'class'         => '',
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget'  => '</li>',
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => '</h2>',
	];

	// Register sidebar
	register_sidebar($args);

	/**
	 * Footer 2
	 *
	 */
	$args = [
		'name'          => __('Footer 2', 'dlbi-sodexo-theme'),
		'id'            => 'footer-2',
		'description'   => __('Footer 2', 'dlbi-sodexo-theme'),
		'class'         => '',
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget'  => '</li>',
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => '</h2>',
	];

	// Register sidebar
	register_sidebar($args);
	/**
	 * Footer 3
	 *
	 */
	$args = [
		'name'          => __('Footer 3', 'dlbi-sodexo-theme'),
		'id'            => 'footer-3',
		'description'   => __('Footer 3', 'dlbi-sodexo-theme'),
		'class'         => '',
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget'  => '</li>',
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => '</h2>',
	];

	// Register sidebar
	register_sidebar($args);

	// Unregister sidebar
	unregister_sidebar('right-sidebar');
	unregister_sidebar('left-sidebar');
	unregister_sidebar('hero');
	unregister_sidebar('statichero');
	unregister_sidebar('footerfull');
}

/*
 * Init Sidebar
 */
add_action('widgets_init', 'lbi_sodexo_theme_widgets_init', 11);


if(!function_exists('lbi_sodexo_theme_setup')) :

	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function lbi_sodexo_theme_setup(){
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on lbi-sodexo-theme, use a find and replace
		 * to change 'lbi-sodexo-theme' to the name of your theme in all the template files.
		 */

		//load_child_theme_textdomain('lbi-sodexo-theme', get_template_directory_child() . '/languages');
		load_child_theme_textdomain('lbi-sodexo-theme', get_stylesheet_directory() . '/languages');

		// Add default posts and comments RSS feed links to head.
		add_theme_support('automatic-feed-links');

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support('title-tag');

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support('post-thumbnails');

		/*
		 * Unregister Understrap menus
		 */
		unregister_nav_menu('primary');

		/*
		 * Register the soxo menus
		 */
		register_nav_menus([
			'top-nav'      => 'Top Nav Menu', // Top nav
			'top-menu-bar' => 'Main Menu', // Top Menu Bar
			'burger-menu'  => 'Burger Menu', // Buger side menu
			'footer-nav'   => 'Footer Menu',
		]);


		// This theme uses wp_nav_menu() in one location.
		lbi_sodexo_create_menus('Top Menu Bar', 'top-menu-bar', [
			[
				'menu-item-title'   => __('Your needs'),
				'menu-item-classes' => 'your-needs',
				'menu-item-url'     => home_url('/'),
				'menu-item-status'  => 'publish',
			],
			[
				'menu-item-title'   => __('Meal Solutions'),
				'menu-item-classes' => 'meal-solutions',
				'menu-item-url'     => home_url('/'),
				'menu-item-status'  => 'publish',
			],
			[
				'menu-item-title'   => __('Gift Solutions'),
				'menu-item-classes' => 'gift-solutions',
				'menu-item-url'     => home_url('/'),
				'menu-item-status'  => 'publish',
			],
			[
				'menu-item-title'   => __('Leisure Offers'),
				'menu-item-classes' => 'leisure-offers',
				'menu-item-url'     => home_url('/'),
				'menu-item-status'  => 'publish',
			],
			[
				'menu-item-title'   => __('Public Solutions'),
				'menu-item-classes' => 'public-solutions',
				'menu-item-url'     => home_url('/'),
				'menu-item-status'  => 'publish',
			],
			[
				'menu-item-title'   => __('Mobility Solutions'),
				'menu-item-classes' => 'mobility-solutions',
				'menu-item-url'     => home_url('/'),
				'menu-item-status'  => 'publish',
			],
		]);
		lbi_sodexo_create_menus('Top Menu Bar affiliate', 'top-menu-affiliate-bar', [
			[
				'menu-item-title'   => __('Your needs'),
				'menu-item-classes' => 'your-needs',
				'menu-item-url'     => home_url('/'),
				'menu-item-status'  => 'publish',
			],
		]);

		update_blog_option(get_current_blog_id(), 'top-menu-affiliate-bar', wp_get_nav_menu_items('top-menu-affiliate-bar'));

		lbi_sodexo_create_menus('Top Menu Bar customers', 'top-menu-customers-bar', [
			[
				'menu-item-title'   => __('Your needs'),
				'menu-item-classes' => 'your-needs',
				'menu-item-url'     => home_url('/'),
				'menu-item-status'  => 'publish',
			],
		]);

		update_option('top-menu-customers-bar', wp_get_nav_menu_items('Top Menu Bar customers'));

		lbi_sodexo_create_menus('Top nav', 'top-nav', [
			[
				'menu-item-title'   => __('Check my balance'),
				'menu-item-classes' => 'check-my-balance',
				'menu-item-url'     => home_url('/'),
				'menu-item-status'  => 'publish',
			],
			[
				'menu-item-title'   => __('Become on affiliate'),
				'menu-item-classes' => 'become-an-affiliate',
				'menu-item-url'     => home_url('/'),
				'menu-item-status'  => 'publish',
			],
			[
				'menu-item-title'   => __('Place an order'),
				'menu-item-classes' => 'place-an-order',
				'menu-item-url'     => home_url('/'),
				'menu-item-status'  => 'publish',
			],
		]);

		lbi_sodexo_create_menus('Burger Menu', 'burger-menu', [
			[
				'menu-item-title'   => __('Your Needs'),
				'menu-item-classes' => 'your-needs',
				'menu-item-url'     => home_url('/'),
				'menu-item-status'  => 'publish',
			],
			[
				'menu-item-title'   => __('All Our Solutions'),
				'menu-item-classes' => 'all-our-solutions',
				'menu-item-url'     => home_url('/'),
				'menu-item-status'  => 'publish',
			],
			[
				'menu-item-title'   => __('About Sodexo'),
				'menu-item-classes' => 'about-sodexo',
				'menu-item-url'     => home_url('/'),
				'menu-item-status'  => 'publish',
			],
			[
				'menu-item-title'   => __('Sodexo Blog'),
				'menu-item-classes' => 'sodexo-blog',
				'menu-item-url'     => home_url('/'),
				'menu-item-status'  => 'publish',
			],
			[
				'menu-item-title'   => __('Contact & FAQ'),
				'menu-item-classes' => 'contact-faq',
				'menu-item-url'     => home_url('/'),
				'menu-item-status'  => 'publish',
			],
			[
				'menu-item-title'   => __('Ask for a Quote'),
				'menu-item-classes' => 'ask-for-a-quote',
				'menu-item-url'     => home_url('/'),
				'menu-item-status'  => 'publish',
			],
		]);

		update_option('top-menu-bar', wp_get_nav_menu_items('top-menu-bar'));

		lbi_sodexo_create_menus('Footer nav', 'footer-nav', [
			[
				'menu-item-title'  => __('Accesibility'),
				'menu-item-url'    => home_url('/'),
				'menu-item-status' => 'publish',
			],
			[
				'menu-item-title'  => __('Legal'),
				'menu-item-url'    => home_url('/'),
				'menu-item-status' => 'publish',
			],
			[
				'menu-item-title'  => __('Sitemap'),
				'menu-item-url'    => home_url('/'),
				'menu-item-status' => 'publish',
			],
		]);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support('html5', [
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		]);

		// Set up the WordPress core custom background feature.
		add_theme_support('custom-background', apply_filters('lbi_sodexo_theme_custom_background_args', [
			'default-color' => 'ffffff',
			'default-image' => '',
		]));

		// Add theme support for selective refresh for widgets.
		add_theme_support('customize-selective-refresh-widgets');
	}

endif;

if(!function_exists('lbi_sodexo_create_menus')):

	function lbi_sodexo_create_menus($menu_name, $menu_location, $items){
		if(!empty($menu_name)){
			// If it doesn't exist, let's create it.
			if(!wp_get_nav_menu_object($menu_name)){
				$menu_id = wp_create_nav_menu($menu_name);


				// Set up links and add them to the menu.
				if($items){
					foreach($items as $item){
						wp_update_nav_menu_item($menu_id, 0, $item);
					}
				}

				// Grab the theme locations and assign our newly-created menu
				if(!has_nav_menu($menu_location)){
					$locations                 = get_theme_mod('nav_menu_locations');
					$locations[$menu_location] = $menu_id;
					set_theme_mod('nav_menu_locations', $locations);
				}
			}
		}
	}

endif;

add_action('init', 'lbi_sodexo_theme_setup');

// Add ACF option page
if(function_exists('acf_add_options_page')){
	// add parent
	$parent = acf_add_options_page([
		'page_title' => 'Theme General Settings',
		'menu_title' => 'Theme Settings',
		'redirect'   => false,
	]);

	// add sub page
	acf_add_options_sub_page([
		'page_title'  => 'Homepage Settings',
		'menu_title'  => 'Homepage',
		'parent_slug' => $parent['menu_slug'],
	]);

	// add sub page
	acf_add_options_sub_page([
		'page_title'  => 'URL',
		'menu_title'  => 'URL',
		'parent_slug' => $parent['menu_slug'],
	]);

	// add sub page
	acf_add_options_sub_page([
		'page_title'  => 'Social Settings',
		'menu_title'  => 'Social',
		'parent_slug' => $parent['menu_slug'],
	]);

	// add sub page
	acf_add_options_sub_page([
		'page_title'  => 'Lead Management Toolbar Settings',
		'menu_title'  => 'Lead Management Toolbar',
		'parent_slug' => $parent['menu_slug'],
	]);

	// add sub page footer
	acf_add_options_sub_page([
		'page_title'  => 'Footer Settings',
		'menu_title'  => 'Footer',
		'parent_slug' => $parent['menu_slug'],
	]);

	// add sub page
	acf_add_options_sub_page([
		'page_title'  => 'Archives Settings',
		'menu_title'  => 'Archives',
		'parent_slug' => $parent['menu_slug'],
	]);

	// add sub page
	acf_add_options_sub_page([
		'page_title'  => '404 Settings',
		'menu_title'  => '404 Settings',
		'parent_slug' => $parent['menu_slug'],
	]);

	// add sub page
	acf_add_options_sub_page([
		'page_title'  => 'Search Results',
		'menu_title'  => 'Search Results',
		'parent_slug' => $parent['menu_slug'],
	]);

	// add sub page
	acf_add_options_sub_page([
		'page_title'  => 'Newsletter',
		'menu_title'  => 'Newsletter',
		'parent_slug' => $parent['menu_slug'],
	]);

	// add sub page
	acf_add_options_sub_page([
		'page_title'  => 'SMTP',
		'menu_title'  => 'SMTP',
		'parent_slug' => $parent['menu_slug'],
	]);
	// add sub page
	acf_add_options_sub_page([
		'page_title'  => 'Simulators Send by mail',
		'menu_title'  => 'Simulators Send by mail',
		'parent_slug' => $parent['menu_slug'],
	]);
	// add sub page
	acf_add_options_sub_page([
		'page_title'  => 'Head/Body Tags',
		'menu_title'  => 'Head/Body Tags',
		'parent_slug' => $parent['menu_slug'],
	]);
	// add sub page
	acf_add_options_sub_page([
		'page_title'  => 'RSS Feed',
		'menu_title'  => 'RSS Feed',
		'parent_slug' => $parent['menu_slug'],
	]);

	// add sub page
	acf_add_options_sub_page([
		'page_title'  => 'Web Callback settings',
		'menu_title'  => 'Web Callback',
		'parent_slug' => $parent['menu_slug'],
	]);

	// add sub page
	acf_add_options_sub_page([
		'page_title'  => 'Sticky menu',
		'menu_title'  => 'Sticky menu',
		'parent_slug' => $parent['menu_slug'],
	]);

	// add sub page
	acf_add_options_sub_page([
		'page_title'  => 'Home page blog',
		'menu_title'  => 'Home page blog',
		'parent_slug' => $parent['menu_slug'],
	]);

	// add sub page
	acf_add_options_sub_page([
		'page_title'  => 'Form Settings',
		'menu_title'  => 'Form Settings',
		'parent_slug' => $parent['menu_slug'],
	]);

	// add sub page
	acf_add_options_sub_page([
		'page_title'  => 'RP Content',
		'menu_title'  => 'RP Content',
		'parent_slug' => $parent['menu_slug'],
	]);

	// add sub page
	acf_add_options_sub_page([
		'page_title'  => 'Background Gravity',
		'menu_title'  => 'Background Gravity',
		'parent_slug' => $parent['menu_slug'],
	]);

	// add sub page
	acf_add_options_sub_page([
		'page_title'  => 'Belgium',
		'menu_title'  => 'Belgium',
		'parent_slug' => $parent['menu_slug'],
	]);
	// add sub page
	acf_add_options_sub_page([
		'page_title'  => 'SEO',
		'menu_title'  => 'SEO',
		'parent_slug' => $parent['menu_slug'],
	]);
	// add sub page
	acf_add_options_sub_page([
		'page_title'  => 'Hubspot',
		'menu_title'  => 'Hubspot',
		'parent_slug' => $parent['menu_slug'],
	]);
}

// sodexo_alter_query_main
add_action('pre_get_posts', 'sodexo_alter_query_main', 11);

function sodexo_alter_query_main($query){
	if(is_category() && $query->is_main_query() && !is_admin()){
		$query->set('posts_per_page', 16);
	}

	// If this is a search
	if(is_search() && $query->is_main_query() && !is_admin()
	   // and more precisely of a faq search
	   && (get_query_var('search') && get_query_var('search') == 'faq')){
		$query->set('post_type', ['faq']);
	}elseif(is_search() && $query->is_main_query() && !is_admin() && (get_query_var('search') && get_query_var('search') == 'need')){
		$query->set('post_type', ['need']);
	}elseif(is_search() && $query->is_main_query() && !is_admin() && (get_query_var('search') && get_query_var('search') == 'pages')){
		$query->set('post_type', ['page']);
	}elseif(is_search() && $query->is_main_query() && !is_admin() && (get_query_var('search') && get_query_var('search') == 'products')){
		$query->set('post_type', ['product']);
	}elseif(is_search() && $query->is_main_query() && !is_admin() && (get_query_var('search') && get_query_var('search') == 'testimonials')){
		$query->set('post_type', ['testimonial']);
	}elseif(is_search() && $query->is_main_query() && !is_admin() && (get_query_var('search') && get_query_var('search') == 'blog')){
		$query->set('post_type', ['post']);
	}elseif(is_search() && $query->is_main_query() && !is_admin()){
		$query->set('post_type', ['post', 'page', 'faq', 'testimonial', 'need', 'product']);
	}
}

// Download an alternative template
add_action('template_include', 'sodexo_search_template');

function sodexo_search_template($template){
	// If this is a search
	if(is_search()){
		//Load loads the search results template
		$new_template = locate_template(['search-result.php']);
		if('' != $new_template){
			return $new_template;
		}
	}

	return $template;
}

// Load ACF fields
add_filter('acf/settings/save_json', 'sodexo_acf_json_save_point');
add_filter('acf/settings/load_json', 'sodexo_acf_json_load_point');

function sodexo_acf_json_save_point($path){
	// update path
	$path = get_template_directory() . '/acf-json';

	// return
	return $path;
}

function sodexo_acf_json_load_point($paths){
	// append path
	$paths[] = get_template_directory() . '/acf-json';

	// return
	return $paths;
}

function get_template_directory_child(){
	// get directory parent
	$directory_template = get_template_directory();
	$directory_child    = str_replace('understrap', '', $directory_template) . 'dlbi-sodexo-theme';

	// return
	return $directory_child;
}

class Walker_Nav_Top_Menu extends Walker_Nav_Menu{

	public function start_lvl(&$output, $depth = 0, $args = []){

		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent<div class=\"sub-menu\">\n";
		$output .= "<div class=\"container\">\n";
		$output .= "<ul class=\"submenu-container\">\n";
		$output .= "<div class=\"row\">\n";
	}

	function ends_with($haystack, $needle){
		$length = strlen($needle);

		return $length === 0 || (substr($haystack, - $length) === $needle);
	}

	function start_el(&$output, $item, $depth = 0, $args = [], $id = 0){
		global $wp_query;

		$indent = ($depth) ? str_repeat("\t", $depth) : '';

		if($depth == 0){ // Main menu
			$class_names = $value = '';
			$classes     = empty($item->classes) ? [] : (array) $item->classes;

			$is_current = in_array('current-menu-item', $classes);
			$classes    = !empty(get_field('custom-icon', $item->ID)) ? [get_field('custom-icon', $item->ID)] : $classes;

			if(!$is_current){
				switch(get_post_type(get_post())){
					case 'need' :
						$slug_needs = get_field('url_needs', 'option') ? get_field('url_needs', 'option') : SOD_NEED_PTYPE;
						if(!empty($slug_needs) && (strstr($item->url, get_permalink(get_the_ID())) || rtrim($item->url, '/') == home_url() . '/' . $slug_needs)){
							$is_current = true;
						}
						break;
					case 'product' :

						try{
							$slug = get_the_terms(get_the_ID(), 'category-product')[0]->slug;
						}catch(Exception $e){
							$slug = '';
						}
						if(!empty($slug) && (strstr($item->url, get_permalink(get_the_ID())) || $this->ends_with(rtrim($item->url, '/'), $slug))){
							$is_current = true;
						}
						break;
					case 'page' :
						break;
				}
			}
			if($is_current){
				$classes[] = 'current-menu-item';
			}

			$class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item));
			$class_names = ' class="' . esc_attr($class_names) . '"';

			$output .= $indent . '<li id="menu-item-' . $item->ID . '"' . $value . $class_names . '>';

			$attributes = !empty($item->attr_title) ? ' title="' . esc_attr($item->attr_title) . '"' : '';
			$attributes .= !empty($item->target) ? ' target="' . esc_attr($item->target) . '"' : '';
			$attributes .= !empty($item->xfn) ? ' rel="' . esc_attr($item->xfn) . '"' : '';
			$attributes .= !empty($item->url) ? ' href="' . esc_attr($item->url) . '"' : '';

			$description = !empty($item->description) ? '<span>' . esc_attr($item->description) . '</span>' : '';

			$item_output = $args->before;

			$item_output .= '<a' . $attributes . '>';

			$item_output .= '<span data-hover="' . $item->title . '">';
			$item_output .= $args->link_before . apply_filters('the_title', $item->title, $item->ID);
			$item_output .= $description . $args->link_after;
			$item_output .= '</span>';
			$item_output .= '</a>';
			$item_output .= $args->after;
		}elseif($depth == 1){ // Submenu
			$class_names = $value = '';
			$classes     = empty($item->classes) ? [] : (array) $item->classes;
			//$class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item));
			$class_names = ' class="col ' . esc_attr($class_names) . '"';

			$output .= $indent . '<li id="menu-item-' . $item->ID . '"' . $value . $class_names . '>';

			if(get_field('submenu_type', $item) == 'Image menu item'){

				$attributes = !empty($item->attr_title) ? ' title="' . esc_attr($item->attr_title) . '"' : '';
				$attributes .= !empty($item->target) ? ' target="' . esc_attr($item->target) . '"' : '';
				$attributes .= !empty($item->xfn) ? ' rel="' . esc_attr($item->xfn) . '"' : '';
				$attributes .= !empty($item->url) ? ' href="' . esc_attr($item->url) . '"' : '';

				$item_output = $args->before;
				$item_output .= '<a' . $attributes . '>';

				$post_id  = url_to_postid($item->url);
				$thumb_id = get_post_thumbnail_id($post_id);
				if(!empty($thumb_id)){
					$thumb_id_url = wp_get_attachment_image_url($thumb_id, 'soxo-sub-menu');
					$item_output  .= apply_filters("dlbi_image", $thumb_id_url);
				}

				$item_output .= '<span>' . $item->title . '</span>';
				$item_output .= '</a>';
				$item_output .= $args->after;
			}elseif(get_field('submenu_type', $item) == 'List group'){
				if(have_rows('submenu_list_group', $item)):
					$item_output = $args->before;
					$item_output .= '<ul class="submenu-list-items">';
					while(have_rows('submenu_list_group', $item)) : the_row();
						$submenulinksgroupsdata = get_sub_field('submenu_link_item');
						$item_output            .= '<li class="submenu-list-item"><a href="' . $submenulinksgroupsdata['url'] . '" target="' . $submenulinksgroupsdata['target'] . '"><i class="fa fa-arrow-right" aria-hidden="true"></i>' . $submenulinksgroupsdata['title'] . '</a></li>';
					endwhile;
					$item_output .= '</ul>';
					$item_output .= $args->after;
				endif;
			}
		}
		$output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
	}

	public function end_lvl(&$output, $depth = 0, $args = []){

		$output .= "</div>\n";
		$output .= "</ul>\n";
		$output .= "</div>\n";
		$output .= "</div>\n";
	}

}

class Walker_Nav_Menu_Top extends Walker_Nav_Menu{

	function start_el(&$output, $item, $depth = 0, $args = [], $id = 0){

		global $wp_query;

		$indent = ($depth) ? str_repeat("\t", $depth) : '';

		$output .= $indent . '<li id="menu-item-' . $item->ID . '">';

		$attributes = !empty($item->attr_title) ? ' title="' . esc_attr($item->attr_title) . '"' : '';
		$attributes .= !empty($item->target) ? ' target="' . esc_attr($item->target) . '"' : '';
		$attributes .= !empty($item->xfn) ? ' rel="' . esc_attr($item->xfn) . '"' : '';
		$attributes .= !empty($item->url) ? ' href="' . esc_attr($item->url) . '"' : '';

		$item_output = $args->before;

		$itemIcon = get_field('top_menu_icon', $item);
		if($itemIcon){
			$item_output .= apply_filters("dlbi_image", $itemIcon['url'], "", "", "", 20, 20);
		}

		$item_output .= '<a' . $attributes . '>';
		$item_output .= '<span>' . $item->title . '</span>';
		$item_output .= '</a>';
		$item_output .= $args->after;

		$output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
	}

}

// Fix the login url issue
add_filter('login_url', 'sodexo_login_page', 10, 2);

function sodexo_login_page($login_url, $redirect){
	return str_replace("wp-login.php", "wp/wp-login.php", $login_url);
}

add_action('login_form', 'replace_login_submit_form', 1);

function replace_login_submit_form(){
	$form_content = ob_get_contents();
	$form_content = str_replace("wp-login.php", "wp/wp-login.php", $form_content);
	ob_get_clean();
	echo $form_content;
}

/**
 * Customize WordPress Toolbar
 *
 * @param obj $wp_admin_bar An instance of the global object WP_Admin_Bar
 */
add_action('admin_bar_menu', 'sodexo_customize_toolbar', 999);

function sodexo_customize_toolbar($wp_admin_bar){

	$logout = $wp_admin_bar->get_node('logout');

	$wp_admin_bar->remove_node('logout');

	$nonce = wp_create_nonce('logout-nonce');
	$wp_admin_bar->add_node([
		'parent' => 'user-actions',
		'id'     => 'logout',
		'title'  => __('Log Out'),
		'href'   => get_site_url() . '?action=logout',
	]);
}

add_filter('template_redirect', 'sodexo_logout_page', 10);

function sodexo_logout_page($logout_url){
	if(isset($_GET['action']) && $_GET['action'] == 'logout'){
		wp_logout();
	}
}

/**
 * [list_searcheable_acf list all the custom fields we want to include in our search query]
 *
 * @return [array] [list of custom fields]
 *
 */
function sodexo_list_searcheable_acf(){
	$searcheable_acf = [
		"faq_question",
		"faq_answer",
		"testimonial_quote",
		"testimonial_author",
	];

	return $searcheable_acf;
}

/**
 * [advanced_custom_search search that encompasses ACF/advanced custom fields and taxonomies and split expression before request]
 *
 * @param  [query-part/string]      $where    [the initial "where" part of the search query]
 * @param  [object]                 $wp_query []
 *
 * @return [query-part/string]      $where    [the "where" part of the search query as we customized]
 */
function sodexo_advanced_custom_search($where, $wp_query){

	global $wpdb;

	if(empty($where)){
		return $where;
	}

	// get search expression
	$terms       = $wp_query->query_vars['s'];
	$wpdb_prefix = $wpdb->prefix;

	// explode search expression to get search terms
	$exploded = explode(' ', $terms);
	if($exploded === false || count($exploded) == 0){
		$exploded = [0 => $terms];
	}

	// reset search in order to rebuilt it as we whish
	$where = '';

	// get searcheable_acf, a list of advanced custom fields you want to search content in
	$list_searcheable_acf = sodexo_list_searcheable_acf();


	foreach($exploded as $tag) :
		$where .= "
          AND (
            (" . $wpdb_prefix . "posts.post_title LIKE '%" . $tag . "%')
            OR (" . $wpdb_prefix . "posts.post_content LIKE '%" . $tag . "%')
            OR EXISTS (
              SELECT * FROM " . $wpdb_prefix . "postmeta
                  WHERE post_id = " . $wpdb_prefix . "posts.ID
                    AND (";
		foreach($list_searcheable_acf as $searcheable_acf) :

			if($searcheable_acf == $list_searcheable_acf[0]):
				$where .= " (meta_key LIKE '%" . $searcheable_acf . "%' AND meta_value LIKE '%" . $tag . "%') ";
			else :
				$where .= " OR (meta_key LIKE '%" . $searcheable_acf . "%' AND meta_value LIKE '%" . $tag . "%') ";
			endif;

		endforeach;
		$where .= ")
            )
            OR EXISTS (
              SELECT * FROM " . $wpdb_prefix . "terms
              INNER JOIN " . $wpdb_prefix . "term_taxonomy
                ON " . $wpdb_prefix . "term_taxonomy.term_id = " . $wpdb_prefix . "terms.term_id
              INNER JOIN " . $wpdb_prefix . "term_relationships
                ON " . $wpdb_prefix . "term_relationships.term_taxonomy_id = " . $wpdb_prefix . "term_taxonomy.term_taxonomy_id
              WHERE (
                       taxonomy = 'post_tag'
                    OR taxonomy = 'category'
                    OR taxonomy = 'topic'
                    OR taxonomy = 'profile'
                )
                AND object_id = " . $wpdb_prefix . "posts.ID
                AND " . $wpdb_prefix . "terms.name LIKE '%" . $tag . "%'
            )
        )";
	endforeach;

	return $where;
}

//add_filter('posts_search', 'sodexo_advanced_custom_search', 500, 2);
// Change search function globally to search only post, page and portfolio post types
function prefix_limit_post_types_in_search($query){

	if($query->is_post_type_archive('faq') && !is_admin() && !is_search()){
		$query->set('posts_per_page', 6);
	}

	return $query;
}

add_filter('pre_get_posts', 'prefix_limit_post_types_in_search', 10);

// Display the last viewed product in homepage
function sodexo_display_product_in_header(){
	if(function_exists('cn_cookies_accepted') && cn_cookies_accepted()){
		if(is_singular(SOD_PRO_PTYPE)){
			setcookie('product_homepage', get_the_ID(), time() + 2592000, '/');
		}elseif(isset($_COOKIE['product_homepage']) && is_singular(SOD_PRO_PTYPE)){
			unset($_COOKIE['product_homepage']);
			setcookie('product_homepage', get_the_ID(), time() + 2592000, '/');
		}
	}
}

add_action('template_redirect', 'sodexo_display_product_in_header');


/**
 * Theme View
 *
 * Include a file and(optionally) pass arguments to it.
 *
 * @param string $file The file path, relative to theme root
 * @param array  $args The arguments to pass to this file. Optional.
 * Default empty array.
 *
 * @return object Use render() method to display the content.
 */
if(!class_exists('Sodexo_ThemeView')){

	class Sodexo_ThemeView{

		private $args;
		private $file;

		public function __get($name){
			return $this->args[$name];
		}

		public function __construct($file, $args = []){
			$this->file = $file;
			$this->args = $args;
		}

		public function __isset($name){
			return isset($this->args[$name]);
		}

		public function render(){
			if(locate_template($this->file)){
				include(locate_template($this->file)); //Theme Check free. Child themes support.
			}
		}

	}

}

/**
 * Sodexo Get Template Part
 *
 * An alternative to the native WP function `get_template_part`
 *
 * @see PHP class Sodexo_ThemeView
 *
 * @param string $file The file path, relative to theme root
 * @param array  $args The arguments to pass to this file. Optional.
 * Default empty array.
 *
 * @return string The HTML from $file
 */
if(!function_exists('sodexo_get_template_part')){

	function sodexo_get_template_part($file, $args = []){
		$template = new Sodexo_ThemeView($file, $args);
		$template->render();
	}

}

/**
 * Add styles and js : ACF Simulator
 *
 * @param  none
 */
function sodexo_enqueue_stylesheets(){
	global $post;
	if(isset($post->post_type)){
		$show_block = get_field('simulator_show_block', $post->ID);
		if(($post->post_type == 'product') && $show_block){
			$country      = '';
			$widgetscript = 'widgets-script-';
			if(get_field('select_a_country')){
				$country = get_field('select_a_country');
			}
			switch($country){
				case'french':
				case'chile':
					{
						$simulator = $country;
						break;
					}
				case'romania':
					{
						$simulator = get_field('select_a_simulator_romania');
						break;
					}
				case'italy':
					{
						$simulator = get_field('select_a_simulator_italy');
						break;
					}
				default:
					{
						$simulator = get_field('select_a_simulator');
						break;
					}
			}
			if(!apply_filters("simulator-settings", [])){
				// $uri seems broken
				//todo: fix it
				$uri    = get_stylesheet_directory_uri() . '/../../plugins/dlbi-sodexo-' . $country . '-simulator/';
				$uriapp = $uri . 'app-' . $simulator . '/dist/';
				$styles = $uriapp . 'styles/main.css';
				// Styles
				wp_enqueue_style('widgets-style', $styles);

				// Javascript
				wp_enqueue_script($widgetscript . '1', $uriapp . 'manifest.js', '', '', true);
				wp_enqueue_script($widgetscript . '2', $uriapp . 'normalize.js', '', '', true);
				wp_enqueue_script($widgetscript . '3', $uriapp . 'vendor.js', '', '', true);
				wp_enqueue_script($widgetscript . '4', $uriapp . 'main.js', '', '', true);
			}
		}
	}
}

add_action('wp_enqueue_scripts', 'sodexo_enqueue_stylesheets', 18);

function blog_action(){

	$posts_per_page  = (isset($_POST["posts_per_page"])) ? $_POST["posts_per_page"] : 16;
	$page            = (isset($_POST['pageNumber'])) ? $_POST['pageNumber'] : 0;
	$cat             = (isset($_POST['cat'])) ? $_POST['cat'] : '';
	$plugin          = 'dlbi-sodexo-testimonials/dlbi-sodexo-testimonials.php';
	$display_wrapper = true;
	if(isset($_POST['not_display_wrapper'])){
		$display_wrapper = false;
	}
	$array_posts = ['post'];

	header("Content-Type: text/html");
	// Check if plugin is enabled
	if(is_plugin_active($plugin)) :
		$array_posts[] = 'testimonial';
	endif;
	$args = [
		'post_type'      => $array_posts,
		'posts_per_page' => $posts_per_page,
		'cat'            => $cat,
		'paged'          => $page,
	];
	if(isset($_REQUEST["not_in"])){
		$args["post__not_in"] = explode(",", $_REQUEST["not_in"]);
	}
	if(isset($_REQUEST["type"])){
		$args["post_type"] = explode(",", $_REQUEST["type"]);
	}

	$the_query = new WP_Query($args);

	//Reste ÃƒÆ’  faire : voir avec harold pour passer en variable is_page_template('homepage-blog.php')
	// et faire un test dessus
	if($the_query->have_posts()) :
		$count_posts = 0;
		if($display_wrapper){
			?>
            <ul class="row" id="blog-articles">
			<?php
		}
		while($the_query->have_posts()) : $the_query->the_post();
			$count_posts = $the_query->current_post + 1; //counts posts in loop
			// At position 3 you will see the newsletter
			if($count_posts == 3 && ($page <= 1)):
				// Get template widget
				echo '<li class="blog-item col-sm-6 col-lg-3 blog-item-wn">';
				get_template_part('templates/content', 'widget-newsletter');
				echo '</li>';
			endif;
			if($count_posts == 9 && ($page <= 1)):
				// Display product
				echo '<li class="blog-item col-sm-6 col-lg-3 blog-item-pbh">';
				get_template_part('templates/content', 'product-blog-hp');
				echo '</li>';
			endif;
			// Display posts
			echo '<li class="blog-item col-sm-6 col-lg-3 blog-item-default">';
			get_template_part('loop-templates/content-post');
			echo '</li>';
		endwhile;
		if($display_wrapper){
			?>
            </ul>
			<?php
		}
		wp_reset_postdata();
    elseif(have_posts()) :
		$count_posts = 0; //counts posts in loop
		while(have_posts()) : the_post();
			echo '<li class="col-sm-6 col-lg-3">';
			get_template_part('loop-templates/content-post');
			echo '</li>';
			++ $count_posts;
		endwhile;
	endif;
}

add_action('wp_ajax_blog_action', 'blog_action');
add_action('wp_ajax_nopriv_blog_action', 'blog_action');

// Configure SMPT servor
add_action('phpmailer_init', 'sodexo_phpmailer_init');

function sodexo_phpmailer_init(PHPMailer $phpmailer){
	$host                  = get_field('smtp_host', 'option');
	$smtp_port             = get_field('smtp_port', 'option');
	$smtp_username         = get_field('smtp_username', 'option');
	$smtp_password         = get_field('smtp_password', 'option');
	$smtp_authentification = get_field('smtp_authentification', 'option');
	$smtp_secure           = get_field('smtp_secure', 'option');

	$phpmailer->Host = $host;
	$phpmailer->Port = $smtp_port; // could be different
	if($smtp_username){
		$phpmailer->Username = $smtp_username; // if required
	}
	if($smtp_password){
		$phpmailer->Password = $smtp_password; // if required
	}

	$phpmailer->SMTPAuth = $smtp_authentification; // if required
	if($smtp_secure){
		$phpmailer->SMTPSecure = $smtp_secure; // enable if required, 'tls' is another possible value
	}


	$phpmailer->IsSMTP();
}

// Newsletter
add_action('wp_footer', 'sodexo_display_newsletter_modal');

function sodexo_display_newsletter_modal(){
	if((isset($_POST['newsletter_email']) && !empty($_POST['newsletter_email']))){
		$content = '<p>This is inserted at the bottom</p>';
		echo $content;
	}
}

// Redirect 404 for wrong faq page
add_action('template_redirect', 'sodexo_archive_redirect', 100);

function sodexo_archive_redirect(){
	$redirects = get_field('archives_redirection', 'option');
	if($redirects){
		if(sizeof($redirects) > 0){
			foreach($redirects as $redirect){
				if(is_post_type_archive($redirect['archive_post_type'])){
					wp_redirect($redirect['archive_url']);
				}
			}
		}
	}
}

// Remove redirect for non caninocal link
add_filter('redirect_canonical', 'no_redirect_on_404');

function no_redirect_on_404($redirect_url){
	if(is_404()){
		return false;
	}

	return $redirect_url;
}

// Import posts from rss feed
function sodexo_import_feed_items(){
	$feed = fetch_feed(get_field('feed_url', 'option'));

	if(!is_wp_error($feed)){

		if($last_import = get_option('last_import')){
			$last_import_time = $last_import;
		}else{
			$last_import_time = false;
		}

		$items = $feed->get_items();

		$latest_item_time = false;

		foreach($items as $item){
			$item_date = $item->get_date('Y-m-d H:i:s');
			if($last_import_time && ($last_import_time >= strtotime($item_date))){
				continue;
			}
			$content = $item->get_content();

			$post = [
				'post_content' => $content,
				'post_date'    => $item_date,
				'post_title'   => $item->get_title(),
				'post_status'  => 'publish',
				'post_type'    => 'post',
			];

			// Insert post
			$post_id = wp_insert_post($post);
			update_post_meta($post_id, 'post_feed', $item->get_link());

			//Add thumbnail
			$enclosure = $item->get_enclosure();
			if($enclosure){
				sodexo_insert_attachment_from_url($enclosure->get_link(), $post_id);
			}

			// Add categories
			$post_categories = get_field('feed_post_categories', 'option');
			if($post_categories){
				wp_set_post_categories($post_id, $post_categories);
			}

			if(strtotime($item_date) > $latest_item_time){
				$latest_item_time = strtotime($item_date);
			}
		}

		if(false !== $latest_item_time){
			update_option('last_import', $latest_item_time);
		}
	}
}

// add_action('wp', 'sodexo_import_feed_items');

/**
 * Insert an attachment from an URL address.
 *
 * @param  String $url
 * @param  Int    $post_id
 * @param  Array  $meta_data
 *
 * @return Int    Attachment ID
 */
function sodexo_insert_attachment_from_url($url, $post_id = null){

	if(!class_exists('WP_Http')){
		include_once(ABSPATH . WPINC . '/class-http.php');
	}

	$http     = new WP_Http();
	$response = $http->request($url);
	if($response['response']['code'] != 200){
		return false;
	}

	$upload = wp_upload_bits(basename($url), null, $response['body']);
	if(!empty($upload['error'])){
		return false;
	}

	$file_path        = $upload['file'];
	$file_name        = basename($file_path);
	$file_type        = wp_check_filetype($file_name, null);
	$attachment_title = sanitize_file_name(pathinfo($file_name, PATHINFO_FILENAME));
	$wp_upload_dir    = wp_upload_dir();

	$post_info = [
		'guid'           => $wp_upload_dir['url'] . '/' . $file_name,
		'post_mime_type' => $file_type['type'],
		'post_title'     => $attachment_title,
		'post_content'   => '',
		'post_status'    => 'inherit',
	];

	// Create the attachment
	$attach_id = wp_insert_attachment($post_info, $file_path, $post_id);
	update_post_meta($post_id, '_thumbnail_id', $attach_id);

	// Include image.php
	require_once(ABSPATH . 'wp-admin/includes/image.php');

	// Define attachment metadata
	$attach_data = wp_generate_attachment_metadata($attach_id, $file_path);

	// Assign metadata to attachment
	wp_update_attachment_metadata($attach_id, $attach_data);

	return $attach_id;
}

add_action('after_setup_theme', 'sodexo_remove_admin_bar');

// Hide admin bar
function sodexo_remove_admin_bar(){
	if(defined(WP_ENV)){
		if(WP_ENV == 'production'){
			show_admin_bar(false);
		}
	}
}

// Deactivate plugins.
//
// ! Deactivated for now, as disabling recaptcha here breaks BO login !
//
//$plugins = array('w3-total-cache/w3-total-cache.php', 'wordfence/wordfence.php', 'no-captcha-recaptcha/no-captcha-recaptcha.php');
//if(WP_ENV == 'development'){
//  deactivate_plugins($plugins, true);
//}

// Add place an order button in simulator
function sodexo_add_simu_order_button(){
	if(is_singular(SOD_PRO_PTYPE)):
		$order = get_field('simulator_place_an_order_link', get_the_ID());
		if($order):
			?>
            <script>
                jQuery(window).scroll(function () {
                    jQuery('.callToAction-link-order').text('<?php echo $order['title'] ?>');
                    jQuery('.callToAction-link-order').attr('href', '<?php echo $order['url'] ?>');
                });
            </script>
		<?php else: ?>
            <script>
                jQuery(window).scroll(function () {
                    jQuery('.callToAction-link-order').css('display', 'none');
                });
            </script>
		<?php
		endif;
	endif;
}

add_action('wp_footer', 'sodexo_add_simu_order_button');

function sodexo_curl($url, $data, $post, $auth){
	$ch = curl_init($url);
	if($auth){
		curl_setopt($ch, CURLOPT_HTTPHEADER, ["Authorization: Bearer " . $auth]);
	}else{
		curl_setopt($ch, CURLOPT_HEADER, false);
	}
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POST, $post);
	if($post){
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
	}else{
		curl_setopt($ch, CURLOPT_URL, $url);
	}
	$contents = curl_exec($ch);
	curl_close($ch);

	return json_decode($contents);
}

// Add to cart
function sodexo_add_to_cart(){
	if(isset($_GET['add_item']) && $_GET['add_item'] == '1'){

		// Create token
		$data = [
			"client_id"     => 'demo_client',
			"client_secret" => 'secret_demo_client',
			"grant_type"    => 'password',
			"username"      => 'api@example.com',
			"password"      => 'sylius-api',
		];
		$auth = sodexo_curl('https://advantage.pocsf.dev.soxodlbi.xyz/api/oauth/v2/token', $data, true, false)->access_token;

		// Create cart
		$data = [
			"customer"   => 'khouildi.salah@gmail.com',
			"channel"    => 'US_WEB',
			"localeCode" => 'en_US',
		];

		$car_id_transient = get_transient('cart_id');

		if(!$car_id_transient){
			$cart             = sodexo_curl('https://advantage.pocsf.dev.soxodlbi.xyz/api/v1/carts/', $data, true, $auth);
			$car_id_transient = set_transient('cart_id', $cart->id, 12 * 3600);
			$cart_id          = $cart->id;
		}else{
			$cart_id = $car_id_transient;
			$cart    = sodexo_curl('https://advantage.pocsf.dev.soxodlbi.xyz/api/v1/carts/' . $cart_id, [], false, $auth);
			if($cart->code == 404){
				$cart             = sodexo_curl('https://advantage.pocsf.dev.soxodlbi.xyz/api/v1/carts/', $data, true, $auth);
				$car_id_transient = set_transient('cart_id', $cart->id, 12 * 3600);
				$cart_id          = $cart->id;
			}
		}

		// Add an item to the cart
		$data     = [
			"variant"  => '7e00f7cf-0c51-3c09-9704-3dc63c6ef2d0-variant-0',
			"quantity" => $_GET['nombreticketsresto'],
		];
		$add_item = sodexo_curl('https://advantage.pocsf.dev.soxodlbi.xyz/api/v1/carts/' . $cart_id . '/items/', $data, true, $auth);

		wp_redirect('https://advantage.pocsf.dev.soxodlbi.xyz/en_US/');
	}
}

add_action('template_redirect', 'sodexo_add_to_cart');

function sodexo_display_cart(){

	// Create token
	$data = [
		"client_id"     => 'demo_client',
		"client_secret" => 'secret_demo_client',
		"grant_type"    => 'password',
		"username"      => 'api@example.com',
		"password"      => 'sylius-api',
	];
	$auth = sodexo_curl('https://advantage.pocsf.dev.soxodlbi.xyz/api/oauth/v2/token', $data, true, false)->access_token;

	$cart_id = get_transient('cart_id');


	$cart = sodexo_curl('https://advantage.pocsf.dev.soxodlbi.xyz/api/v1/carts/' . $cart_id, [], false, $auth);
	if($cart->id) :
		?>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3>CART</h3>
					<?php
					foreach($cart->items as $item){
						echo $item->quantity . ' X ' . substr_replace($item->unitPrice, '.', - 2, 0) . ' = ' . substr_replace($item->total, '.', - 2, 0) . '<br />';
					}

					echo 'TOTAL : ' . substr_replace($cart->total, '.', - 2, 0);
					?>

                </div>
            </div>
        </div>
	<?php
	endif;
}

function custom_rtrim($txt){
	$no_trim = ['pass'];
	if(!in_array(strtolower($txt), $no_trim)){
		return rtrim($txt, 's');
	}

	return $txt;
}

function custom_soundex($elem){
	return metaphone(custom_rtrim($elem));
}

function size_word($elem){
	return strlen(custom_rtrim($elem)) >= 3;
}

function contain_word_sound($target, $string, $strict = true){
	$string = strtolower(strip_tags($string));
	$words  = preg_split("/[\s,-]+/", $string);
	$words  = array_unique($words);
	$words  = array_filter($words, 'size_word');
	$words  = array_map('custom_soundex', $words);

	$target_words = explode(' ', strtolower($target));
	$part_founded = 0;
	foreach($target_words as $t_word){
		if(in_array(custom_soundex($t_word), $words)){
			$part_founded ++;
		}
	}
	if($strict){
		return $part_founded == count($target_words);
	}

	return $part_founded > 0;
}

// Alter search query
function search_filter($query){
	if(get_field('phonetic_search', 'option')){
		if(!is_admin() && $query->is_main_query()){
			if($query->is_search){
				if(isset($_GET['search'])){
					$query->set('post_type', [$_GET['search']]);
				}

				$the_query = new WP_Query(['posts_per_page' => - 1, 'post_type' => ['post', 'page', 'faq', 'testimonial', 'need', 'product']]);
				global $ids_allowed_search; //Make the IDS available for the search-page-filters template
				$ids_allowed_search = [0]; //If nothing found, the search will display nothing, instead of displaying everything
				if($the_query->have_posts()){
					while($the_query->have_posts()){
						$the_query->the_post();

						$title   = get_the_title();
						$content = get_the_content();
						$data    = $title . ' ' . $content;
						if(get_post_type(get_the_ID()) == 'faq'){
							$faq = get_field('faq_questions', get_the_ID());
							foreach($faq as $question):
								$data .= ' ' . $question['faq_question'];
								$data .= ' ' . $question['faq_answer'];
							endforeach;
						}

						if(contain_word_sound(get_query_var('s'), $data)){
							$ids_allowed_search[] = get_the_ID();
						}
					}
				}

				$query->set('post__in', $ids_allowed_search);
				$query->set('s', '');
			}
		}
	}
}

add_action('pre_get_posts', 'search_filter');

//GRAVITY FORM
add_action('gform_enqueue_scripts', 'enqueue_custom_script');

function enqueue_custom_script(){
	wp_enqueue_style('sodexo-styles', get_stylesheet_directory_uri() . '/dist/styles/main.min.css', []);
}

$gform_save_data = get_field('gform_save_entries_in_database', 'option');
if(!$gform_save_data){
	add_action('gform_after_submission', 'remove_form_entry');
}

function remove_form_entry($entry){
	GFAPI::delete_entry($entry['id']);
}

add_filter('gform_form_settings', 'my_custom_form_setting', 10, 2);

function my_custom_form_setting($settings, $form){

	$title_pos                                                  = rgar($form, 'title_align');
	$settings[__('Form Basics', 'gravityforms')]['title_align'] = '
        <tr>
            <th><label for="title_align">' . __('Title position', 'lbi-sodexo-theme') . '</label></th>
            <td>
              <select name="title_align">
                <option value="center" ' . ($title_pos == 'center' ? 'selected' : '') . '>' . __('Center', 'lbi-sodexo-theme') . '</option>
                <option value="left" ' . ($title_pos == 'left' ? 'selected' : '') . '>' . __('Left', 'lbi-sodexo-theme') . '</option>
                <option value="right" ' . ($title_pos == 'right' ? 'selected' : '') . '>' . __('Right', 'lbi-sodexo-theme') . '</option>
              </select>
            </td>
        </tr>';

	$description_pos                                                  = rgar($form, 'description_align');
	$settings[__('Form Basics', 'gravityforms')]['description_align'] = '
        <tr>
            <th><label for="description_align">' . __('Description position', 'lbi-sodexo-theme') . '</label></th>
            <td>
              <select name="description_align">
                <option value="center" ' . ($description_pos == 'center' ? 'selected' : '') . '>' . __('Center', 'lbi-sodexo-theme') . '</option>
                <option value="left" ' . ($description_pos == 'left' ? 'selected' : '') . '>' . __('Left', 'lbi-sodexo-theme') . '</option>
                <option value="right" ' . ($description_pos == 'right' ? 'selected' : '') . '>' . __('Right', 'lbi-sodexo-theme') . '</option>
              </select>
            </td>
        </tr>';

	$title_color                                                = rgar($form, 'title_color');
	$settings[__('Form Basics', 'gravityforms')]['title_color'] = '
        <tr>
            <th><label for="title_color">' . __('Title color', 'lbi-sodexo-theme') . '</label></th>
            <td><input type="text" name="title_color" value="' . $title_color . '" class="gf-color-picker" ></td>
        </tr>';

	$description_color                                                = rgar($form, 'description_color');
	$settings[__('Form Basics', 'gravityforms')]['description_color'] = '
        <tr>
            <th><label for="description_color">' . __('Description color', 'lbi-sodexo-theme') . '</label></th>
            <td><input type="text" name="description_color" value="' . $description_color . '" class="gf-color-picker" ></td>
        </tr>';

	$popin_style                                              = rgar($form, 'cta_style') == '1' ? 'checked' : '';
	$settings[__('Form Basics', 'gravityforms')]['cta_style'] = '
        <tr>
            <th><label for="cta_style">' . __('Disable CTA style', 'lbi-sodexo-theme') . '</label></th>
            <td><input type="checkbox" name="cta_style" value="1" ' . $popin_style . ' ></td>
        </tr>';

	$settings[__('Form Button', 'gravityforms')]['my_custom_setting'] = '
        <tr>
            <th><label for="my_custom_setting">My Custom Label</label></th>
            <td><input value="' . rgar($form, 'my_custom_setting') . '" name="my_custom_setting"></td>
        </tr>';

	return $settings;
}

// save your custom form setting
add_filter('gform_pre_form_settings_save', 'save_my_custom_form_setting');

function save_my_custom_form_setting($form){
	$form['title_align']       = rgpost('title_align');
	$form['description_align'] = rgpost('description_align');
	$form['title_color']       = rgpost('title_color');
	$form['description_color'] = rgpost('description_color');
	$form['cta_style']         = rgpost('cta_style');
	$form['my_custom_setting'] = rgpost('my_custom_setting');

	return $form;
}

add_filter('gform_pre_render', 'styling_gf_header');

function styling_gf_header($form){
	$style_title = '';
	if(array_key_exists('title_align', $form) && !empty($form['title_align'])){
		$style_title .= 'text-align:' . $form['title_align'] . ';';
	}
	if(array_key_exists('title_color', $form) && !empty($form['title_color'])){
		$style_title .= 'color:' . $form['title_color'] . ';';
	}

	if(!empty($style_title)){
		$form['title'] = '<div style="' . $style_title . '">' . $form['title'] . '</div>';
	}


	$style_description = '';
	if(array_key_exists('description_align', $form) && !empty($form['description_align'])){
		$style_description .= 'text-align:' . $form['description_align'] . ';';
	}
	if(array_key_exists('description_color', $form) && !empty($form['description_color'])){
		$style_description .= 'color:' . $form['description_color'] . ';';
	}

	if(!empty($style_description)){
		$form['description'] = '<div style="' . $style_description . '">' . $form['description'] . '</div>';
	}

	if(array_key_exists('cta_style', $form) && !empty($form['cta_style'])){
		$form['cssClass'] .= ' no-cta-style ';
	}

	return $form;
}

add_action('admin_enqueue_scripts', 'scripts_add_color_picker');

function scripts_add_color_picker($hook){
	if(is_admin() && !empty($_GET['page']) && $_GET['page'] == 'gf_edit_forms'){
		// Add the color picker css file
		wp_enqueue_style('wp-color-picker');
		wp_enqueue_script('sodexo-scripts', get_stylesheet_directory_uri() . '/dist/scripts/main.min.js', ['wp-color-picker'], '1.1.1', true);
	}
}

// Is rp content add specific class
$is_rp_content = get_field('is_rp_content', 'option');
if($is_rp_content){
	add_filter('body_class', 'soxo_rp_content_class');

	function soxo_rp_content_class($classes){
		$classes[] = 'rp-content';

		return $classes;
	}

}

//Workaround for localized slugs (http://msls.co/hooks-filters-and-actions/#msls_options_get_permalink)
function sodexo_msls_options_get_permalink($url, $language){

	global $wp;

	try{
		$post_type   = get_post()->post_type;
		$post_object = get_post_type_object($post_type);
		$slug_src    = $post_object->rewrite['slug'];
		if(is_archive() && !empty(get_queried_object())){
			$post_type = get_queried_object()->name;
			$slug_src  = get_queried_object()->rewrite['slug'];
		}

		$slug_universe = '';

		if(get_field('is_belgium', 'option')){
			$current_url = parse_url('//' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']) ['path'];
			if(strpos($current_url, get_field('fr_b2p_slug', 'option')) !== false){
				$slug_universe = get_field('nl_b2p_slug', 'option');
			}
			if(strpos($current_url, get_field('fr_b2b_slug', 'option')) !== false){
				$slug_universe = get_field('nl_b2b_slug', 'option');
			}
			if(strpos($current_url, get_field('fr__b2m_slug', 'option')) !== false){
				$slug_universe = get_field('nl_b2m_slug', 'option');
			}
			if(strpos($current_url, get_field('nl_b2p_slug', 'option')) !== false){
				$slug_universe = get_field('fr_b2p_slug', 'option');
			}
			if(strpos($current_url, get_field('nl_b2b_slug', 'option')) !== false){
				$slug_universe = get_field('fr_b2b_slug', 'option');
			}
			if(strpos($current_url, get_field('nl_b2m_slug', 'option')) !== false){
				$slug_universe = get_field('fr__b2m_slug', 'option');
			}
		}


		$slug_target = '';
		switch($post_type){
			case 'need' :
				$slug_target = get_field('url_needs', 'option');
				break;
			case 'faq' :
				$slug_target = get_field('url_faq', 'option');
				break;
			case 'product' :
				$slug_target = get_field('url_product', 'option');
				break;
			case 'testimonial' :
				$slug_target = get_field('url_testimonials', 'option');
				break;
		}

		if(!empty($slug_src) && !empty($slug_target)){
			$url = str_replace('/' . $slug_src . '/', '/' . $slug_target . '/', $url);
		}

		return $url;
	}catch(Exception $e){

	}

	return $url;
}

add_filter('msls_options_get_permalink', 'sodexo_msls_options_get_permalink', 10, 2);

// Gravity form GTM
add_action('gform_after_submission', 'access_entry_via_field', 10, 2);

function access_entry_via_field($entry, $form){

	$datas = [];
	foreach($form['fields'] as $field){
		foreach($entry as $key => $value){
			if($field['encryptField'] && $field['id'] == $key && $field['type'] != 'page' && $field['type'] != 'section'){
				@$datas[sanitize_title($field['label'])] = $value;
			}elseif($field['encryptField'] && $field['id'] == intval($key) && $value != "" && $field['type'] != 'page' && $field['type'] != 'section'){
				@$datas[sanitize_title($field['label'])] .= $value . " / ";
			}
		}
	}

	echo "<script>",
		"var fields = Object.entries(" . json_encode($datas) . ");",
		// "console.log(Object.entries(". json_encode($form['fields']) ."));",
	"var dataLayer = window.parent.dataLayer || [];",
		"var datas = {
            'event':'form',
            'form-name': '" . $form['title'] . "',
            'location': window.parent.location.href
        };",
	"fields.forEach(function(item, index) {
            datas[item['0']] = item['1'];
        });",
	"dataLayer.push(datas);",
	"</script>";
}

;
//src code : http://adambalee.com/search-wordpress-by-custom-fields-without-a-plugin/
//remove    ".$wpdb->postmeta.".meta_key LIKE '%_faq_answer' AND  to make it search in the entire postmeta
function cf_search_join($join){
	global $wpdb;

	if(is_search()){
		$join .= ' LEFT JOIN ' . $wpdb->postmeta . ' ON ' . $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
	}

	return $join;
}

function cf_search_where($where){
	global $pagenow, $wpdb;

	if(is_search()){
		$where = preg_replace(
			"/\(\s*" . $wpdb->posts . ".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/", "(" . $wpdb->posts . ".post_title LIKE $1) OR (" . $wpdb->postmeta . ".meta_key LIKE '%_faq_answer' AND " . $wpdb->postmeta . ".meta_value LIKE $1)", $where);
	}

	return $where;
}

function cf_search_distinct($where){
	global $wpdb;

	if(is_search()){
		return "DISTINCT";
	}

	return $where;
}

if(!get_field('phonetic_search', 'option')){
	add_filter('posts_join', 'cf_search_join');
	add_filter('posts_where', 'cf_search_where');
	add_filter('posts_distinct', 'cf_search_distinct');
}


// Add checkbox in gravity forms for send datas or not to GTM
add_action('gform_field_standard_settings', 'my_standard_settings', 10, 2);

function my_standard_settings($position, $form_id){
	//create settings on position 25 (right after Field Label)
	if($position == 25){
		?>
        <li class="encrypt_setting field_setting">
            <label for="field_admin_label" class="section_label">
				<?php esc_html_e('Send field value', 'gravityforms'); ?>
				<?php gform_tooltip('form_field_encrypt_value') ?>
            </label>
            <input type="checkbox" id="field_encrypt_value" onclick="SetFieldProperty('encryptField', this.checked);"/> Send field value
        </li>
		<?php
	}
}

add_action('gform_editor_js', 'editor_script');
function editor_script(){
	?>
    <script type='text/javascript'>
        //adding setting to fields of type "text"
        fieldSettings.text += ', .encrypt_setting';

        //binding to the load field settings event to initialize the checkbox
        jQuery(document).bind('gform_load_field_settings', function (event, field, form) {
            jQuery('#field_encrypt_value').attr('checked', field.encryptField == true);
            // jQuery('#field_encrypt_value').css('display', 'block !important');
        });
    </script>
	<?php
}

add_action('admin_head', 'custom_encrypt_css');

function custom_encrypt_css(){
	echo '<style>
    .encrypt_setting {
      display:block !important;
    }
  </style>';
}

add_filter('msls_options_get_permalink', 'sodexo_msls_options_get_permalink', 10, 2);


function update_acf_display_products($title, $post, $field, $post_id){
	if($post->post_type == 'product'){
		$terms = get_the_terms($post->ID, 'category-product');
		if(count($terms) > 0){
			$title .= ' (' . $terms[0]->name . ')';
		}
	}

	return $title;
}

add_filter('acf/fields/relationship/result', 'update_acf_display_products', 10, 4);
add_filter('acf/fields/post_object/result', 'update_acf_display_products', 10, 4);


function filter_nav_menu_items($posts, $args, $post_type){
	foreach($posts as $key => $post){
		$terms = get_the_terms($posts[$key]->ID, 'category-product');
		if(count($terms) > 0){
			$posts[$key]->post_title .= ' (' . $terms[0]->name . ')';
		}
	}

	return $posts;
}

add_filter('nav_menu_items_product', 'filter_nav_menu_items', 10, 3);
add_filter('nav_menu_items_product_recent', 'filter_nav_menu_items', 10, 3);


if(!function_exists("dlbi_test_mode_active")){
	//Checks if HTTP_TESTMODE is set to true
	function dlbi_test_mode_active(){
		if(isset($_SERVER['HTTP_TESTMODE']) && $_SERVER['HTTP_TESTMODE'] === 'true'){
			return true;
		}

		return false;
	}
}

if(!function_exists("dlbi_display_debug")){
	//display var_export($item) in <pre> when dlbi_test_mode_active(), we can choose if method echo its output, color & background-color
	function dlbi_display_debug($item, $return = true, $color = "#000", $bgColor = "transparent"){
		$pre = "";
		if(dlbi_test_mode_active()){
			$debug = var_export($item, 1);
			$pre   = "<pre style='color:$color;background-color:$bgColor'>$debug</pre>";
		}
		if(!$return){
			echo $pre;
		}

		return $pre;
	}
}

if(!function_exists("dlbi_echo_debug")){
	//echoes something only if test mode is active
	function dlbi_echo_debug($html){
		if(dlbi_test_mode_active()){
			echo $html;
		}
	}
}

//retuns wordpress post_id of an item by its $src
function dlbi_id_by_src($src){
	global $wpdb;
	$src = str_replace(get_bloginfo('url'), "", $src);
	$src = str_replace("//app", "", $src);

	$col = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid like '%s';", "%" . $src));
	if(isset($col[0])){
		return $col[0];
	}

	return false;
}

//if $src is a wordpress item witch has height and width, returns array with height and width, else return false
function dlbi_size_by_src($src, $sizeAttr = "full"){
	$id  = dlbi_id_by_src($src);
	$ret = false;
	if($id){
		$size = wp_get_attachment_image_src($id, $sizeAttr);
		//  dlbi_displayDebug( $size, 0, "green", "white" );
		if($size && isset($size[3])){
			$ret = ["width" => $size[1], "height" => $size[2]];
		}
	}

	return $ret;
}

//check an $src to retreive informations on it, first check it it is a resisez image wordpress url, if not try to get it form database, and if not request it with getimagesize to get height and width
function dlbi_get_image_dimensions($src, $useRequests = true){
	$isWpResized    = false;
	$wpResizedRegex = '/[a-zA-Z0-9.:\/-]*\/uploads\/[a-zA-Z0-9\/]*\/([a-zA-Z0-9-_]+)[-_](\d*)x(\d*)[a-zA-z1-9_]*([a-z.]*)/m';
	$matches        = [];
	$ret            = ['width' => false, 'height' => false, 'filename' => false];
	if(preg_match($wpResizedRegex, $src, $matches)){
		//      echo dlbi_displayDebug( $matches, 1, "green" );
		if(isset($matches[3])){
			$ret["width"]    = $matches[2];
			$ret["height"]   = $matches[3];
			$ret["filename"] = $matches[1];
		}
	}

	if(!$ret["width"]){
		$sizes = dlbi_size_by_src($src);
		//      dlbi_displayDebug( $sizes, 0, "red", "white" );
		if($sizes && is_array($sizes)){
			$ret["width"]  = $sizes["width"];
			$ret["height"] = $sizes["height"];
		}
	}


	if(!$isWpResized && $useRequests && !$ret["width"]){
		$test = getimagesize($src);
		if($test){
			$ret["width"]  = $test[0];
			$ret["height"] = $test[1];
		}
	}

	return $ret;
}


function dlbi_image_request($src, $class = "", $alt = '', $id = "", $width = false, $height = false){
	return dlbi_image($src, $class, $alt, $id, $width, $height, true);
}


//function for filter dlbi_image, uses dlbi_getImageDimentions
function dlbi_image($src, $class = "", $alt = '', $id = "", $width = false, $height = false, $use_request = false){
	if(is_array($src)){
		$arr = $src;
		$src = "";
		if(isset($arr["url"])){
			$src = $arr["url"];
		}
		extract($arr);
	}

	if(!$width || !$height){
		$arr = dlbi_get_image_dimensions($src, $use_request);
		if(!$width && $arr["width"]){
			$width = $arr["width"];
		}
		if(!$height && $arr["height"]){
			$height = $arr["height"];
		}
	}

	$lazyload_settings = apply_filters("lazyload-settings", []);
	$src_html          = 'src="' . $src . '" ';
	if(isset($lazyload_settings["active"]) && $lazyload_settings["active"]){
		$src_html = 'src="' . LAZYSRC . '" data-lazy-src="' . $src . '" ';
	}

	$class_html = 'class="di ' . $class . '" ';
	$alt_html   = 'alt="' . $alt . '" ';
	$id_html    = $width_html = $height_html = '';
	if($id){
		$id_html = 'id="' . $id . '" ';
	}
	if($width){
		$width_html = 'width="' . $width . '" ';
	}
	if($height){
		$height_html = 'height="' . $height . '" ';
	}

	return "<img " . $id_html . $class_html . $src_html . $alt_html . $width_html . $height_html . "/>";
}

add_filter('dlbi_image', 'dlbi_image', 10, 6);
add_filter('dlbi_image_request', 'dlbi_image_request', 10, 6);

//function for filter deferred_script_list
function deferred_script_list($list = []){
	return [
		'sodexo-script-vendor',
		'layerslider',
		'sodexo-scripts-vendors',
		'layerslider-transitions',
		'layerslider-greensock',
		//SOXINT-968
		'widgets-script-1',
		'widgets-script-2',
		'widgets-script-3',
		'widgets-script-4',
		'widgets-style',
		//end SOXINT-968
	];
}

add_filter('deferred_script_list', 'deferred_script_list', 10, 1);

//function for filter async_script_list
function async_script_list($list = []){
	return [
		//      'widgets-script-4',
		//      'sodexo-scripts',
		//      'jquery-sonar',
		//      'wpcom-lazy-load-images',
		//      'wp-embed',
		//
		//      "comment-reply",
		//      'popper-scripts',

	];
}

// add_filter('async_script_list', 'async_script_list', 10, 1);

//function for filter script_loader_tag, displays handle and defer/async attributes if in async_script_list or deferred_script_list
function dlbi_add_script_attr($tag, $handle){
	$asynced  = $deffered = [];
	$asynced  = apply_filters('async_script_list', $asynced);
	$deffered = apply_filters('deferred_script_list', $deffered);
	$attrs    = ' data-handle="' . $handle . '"';
	if(in_array($handle, $asynced)){
		$attrs .= ' async';
	}
	if(in_array($handle, $deffered)){
		$attrs .= ' defer';
	}

	return str_replace(' src', $attrs . ' src', $tag);
}

add_filter('script_loader_tag', 'dlbi_add_script_attr', 10, 2);

//<editor-fold name="SOXINT-794">
function dlbi_minify_layer_slider(){
	//this function will call the minified version of layerslider style
	if(get_field('minify_layerslider', 'option')){
		wp_deregister_style("layerslider");
		wp_enqueue_style("layerslider", get_stylesheet_directory_uri() . "/plugins/layerslider/layerslider.min.css");
	}
}

add_action('wp_enqueue_scripts', 'dlbi_minify_layer_slider', 19);
add_action('wp_enqueue_scripts', 'dlbi_auto_refresh', 18);
//</editor-fold>
function dlbi_filter_https($link){
	$ret = $link;
	$url = get_bloginfo("url");
	if(strpos($url, "https:") !== false){
		$ret = str_replace("http://", "https://", $ret);
	}


	return $ret;
}

add_filter("dlbi_filter_https", "dlbi_filter_https", 10, 1);

function dlbi_file_version($value){
	return "1015";
}

add_filter("dlbi_file_version", "dlbi_file_version");

//<editor-fold name="SOXINT-740">
function dlbi_more_posts(){
	global $wp_query;

	return $wp_query->found_posts > $wp_query->post_count;
}

//</editor-fold>

//<editor-fold desc="Debug functions">
/**
 * functions in this block are for debug purposes only on Thibault's envy
 * right now, they are NOT useful since they rely on several scripts under dlbi-sodexo-theme which are not committed
 **/
if(!function_exists("dlbi_auto_reload_mode_active")){
	function dlbi_auto_reload_mode_active(){
		if(isset($_SERVER['HTTP_AUTORELOAD']) && $_SERVER['HTTP_AUTORELOAD'] === 'true'){
			return true;
		}

		return false;
	}
}

if(!function_exists("dlbi_auto_refresh")){
	//enqueues /autorefresh/autorefresh.js if file exists (not committed !) and dlbi_auto_reload_mode_active for debug and autoF5 purposes
	function dlbi_auto_refresh(){
		if(file_exists(get_stylesheet_directory() . "/autorefresh/hashversion.php") && dlbi_auto_reload_mode_active()){
			$locScript = ['ajaxurl' => admin_url('admin-ajax.php'), "autorefresh" => true];
			global $wp_query;
			$post_id   = $wp_query->get_queried_object_id();
			$post      = $wp_query->get_queried_object();
			$post_type = $post->post_type;
			if(is_front_page()){
				$post_type = 'homepage';
			}
			$locScript['autoRefreshArgs'] = [
				"testMode"           => dlbi_test_mode_active(),
				"refreshTime"        => 4000,
				"checkmode"          => true,
				"autoRefreshVerbose" => false,
				'url'                => get_stylesheet_directory_uri() . "/autorefresh/hashversion.php",
				'caller'             => $post_type . '---' . get_page_template_slug($post_id) . "---" . $post_id,
			];

			wp_enqueue_script('dlbi-autoreload', get_stylesheet_directory_uri() . '/autorefresh/autorefresh.js', ["jquery", "sodexo-scripts"], "3", true);
			wp_localize_script('dlbi-autoreload', 'locScript', $locScript);
		}
	}
}

add_action('wp_enqueue_scripts', 'dlbi_auto_refresh');

if(!function_exists("write_build_json")){
	function write_build_json($key, $file = "core"){
		$file_name = get_stylesheet_directory() . "/autorefresh/build/$file.json";
		if(is_file($file_name)){
			$content = file_get_contents($file_name);
			$array   = json_decode($content, true);
			$val     = 0;
			if(isset($array[$key])){
				$val = $array[$key] * 1;
			}
			++ $val;
			$array[$key] = $val;
			$new_value   = json_encode($array);
			file_put_contents($file_name, $new_value);
		}
	}
}

if(!function_exists("dlbi_update_post_build")){
	function dlbi_update_post_build($post_ID, $post_after, $post_before){
		write_build_json($post_ID, "posts");
	}
}

//after post is updated in database
add_action("post_updated", "dlbi_update_post_build", 100, 3);

if(!function_exists("dlbi_after_rewrite_rules")){
	function dlbi_after_rewrite_rules(){
		write_build_json("rewriterules");
	}
}

add_action("generate_rewrite_rules", "dlbi_after_rewrite_rules");
if(!function_exists("dlbi_after_deactivated_plugin")){
	function dlbi_after_deactivated_plugin(){
		write_build_json("plugins");
	}
}

add_action("deactivated_plugin", "dlbi_after_deactivated_plugin ");
if(!function_exists("dlbi_after_update_menu")){
	function dlbi_after_update_menu(){
		write_build_json("menus");
	}
}

add_action("wp_update_nav_menu", "dlbi_after_update_menu ");
//</editor-fold> End Debug functions

//<editor-fold name="SOXINT-769">
function msls_head_hreflang($lang){
	// dlbi_display_debug( $lang, 0, "orange" );

	return $lang;
}

function dlbi_hreflang(){
	$hreflang = get_field('hreflang', 'option');
	$suffix   = "";
	$explode  = explode("-", $hreflang);
	if(isset($explode[1])){
		$suffix = "-" . $explode[1];
	}
	ob_start();
	do_action("dlbi_msls_head");
	$content = ob_get_contents();
	ob_end_clean();
	$newContent = "";
	$doc        = new DOMDocument();
	if($content){
		$doc->loadHTML($content);
		$items = $doc->getElementsByTagName("link");

		foreach($items as $link){
			$hreflang   = strtolower($link->getAttribute("title")) . $suffix;
			$href       = $link->getAttribute("href");
			$newContent .= "<link rel=\"alternate\" href=\"$href\" hreflang=\"$hreflang\" />
";
		}
	}
	echo $newContent;
}

add_action("dlbi_msls_head", "msls_head");
//
remove_action("wp_head", "msls_head");
add_action("wp_head", "dlbi_hreflang", 1);

//</editor-fold>
//<editor-fold name="SOXINT-771">
function dlbi_can_be_phone_number($value){
	$regex = "/^(\(?\+){0,1}([0-9(). -]){9,15}$/";

	return preg_match($regex, $value);
}

function dlbi_make_it_phone_callable($value){
	return str_replace([" ", '-', ".", "(", ")"], ["", "", "", "", ""], $value);
}

function dlbi_add_tel_link($value){
	if(dlbi_can_be_phone_number($value)){
		if(strpos($value, '<a') === false){
			$callable = dlbi_make_it_phone_callable($value);
			$link     = "<a href='tel:$callable'>";
			$endlink  = "</a>";

			return $link . $value . $endlink;
		}
	}

	return $value;
}

add_filter("dlbi_tel_link", "dlbi_add_tel_link", 1, 1);

//</editor-fold>
//<editor-fold name="SOXO-916">
function featuredtoRSS($content){

	global $post;
	if(has_post_thumbnail($post->ID)){
		$content = '<div>' . get_the_post_thumbnail($post->ID, 'full', ['style' => 'margin-bottom: 15px;']) . '</div>' . $content;
	}

	return $content;
}

if(get_field('activate_featured_rss', 'option')){
	add_filter('the_excerpt_rss', 'featuredtoRSS');
	add_filter('the_content_feed', 'featuredtoRSS');
}
//</editor-fold>

//<editor-fold name="SOXINT-824">

function dlbi_close_modal_button(){
	$text = get_field("gf_return_text", 'option');
	if(!$text){
		$text = __('Return', 'lbi-sodexo-theme');
	}
	return "<button class='close-modal-button gform_button' data-dismiss='modal' aria-label='close'>$text</button>";
}

//callback function called in themes/dlbi-sodexo-theme/gravity-forms-iframe.php to display a close modal button before the submit button
function add_close_modal_button($button, $form){
	return dlbi_close_modal_button() . "<div class='placeholder'></div>" . $button;
}

function add_close_modal_button_before_button($button, $form){
	return add_close_modal_button($button, $form);
}

function add_placeholder_before_button($button, $form){
	return "<div class='placeholder'></div>" . $button;
}

function add_placeholder_after_button($button, $form){
	return $button . "<div class='placeholder'></div>";
}

function add_close_modal_button_after_button($button, $form){
	return $button . dlbi_close_modal_button();
}

function add_button_after_confirmation($confirmation, $form, $entry, $ajax){
	return $confirmation . dlbi_close_modal_button();
}

//</editor-fold>

//<editor-fold desc="SOXINT-810">
add_action('pre_get_posts', 'dlbi_set_posts_per_page', 12); //must be 12 to bypass what was set before (sodexo_alter_query_main)
function dlbi_set_posts_per_page($query){
	global $wp_the_query;
	if((!is_admin()) && ($query === $wp_the_query)){
		if($query->is_archive()){
			$query->set('posts_per_page', 8);
		}
		$cat_name = $query->query_vars["category_name"];
		//      $color    = "darkorchid";
		if(is_numeric($cat_name)){
			$query->query('pagename=blog&paged=' . $cat_name);
			//          $color = "orangered";
		}
		//      dlbi_display_debug($query, 0, $color);
	}
}

//</editor-fold>

//<editor-fold name="SOXINT-853">

add_action('admin_enqueue_scripts', 'dlbi_theme_admin_scripts');

function dlbi_theme_admin_scripts(){
	if(is_admin()){
		wp_enqueue_style('sodexo-styles-admin', get_stylesheet_directory_uri() . '/dist/styles/admin.min.css', [], time());
		wp_enqueue_script('sodexo-scripts-admin', get_stylesheet_directory_uri() . '/dist/scripts/main-bo.min.js', [], time(), true);
	}
}

//</editor-fold>

//<editor-fold desc="SOXINT-883">
function dlbi_file_version_cached($value){
	if(dlbi_auto_reload_mode_active()){
		return time();
	}
	return $value;
}

add_filter("dlbi_file_version", "dlbi_file_version_cached");
//</editor-fold>
//<editor-fold desc="SOXINT-900">

function dlbi_override_yoast_robots_txt_generation(){
	//rewrite of plugins/wordpress-seo/admin/views/tool-file-editor.php 's robots.txt part
	$ret = ["success" => false];
	if(!defined('WPSEO_VERSION')){
		header('Status: 403 Forbidden');
		header('HTTP/1.1 403 Forbidden');
		$ret['error'] = "forbidden";
		echo json_decode($ret);
	}else{//
		//Here is the trick ; with bedrock, robots.txt is misplaced, we must put it in where it is supposed to be, at document root
		$robots_file = $_SERVER["DOCUMENT_ROOT"] . '/robots.txt';
		$ret["path"] = $robots_file;
		if(isset($_POST['create_robots'])){
			$ret['mode'] = "create";
			if(!current_user_can('edit_files')){
				$die_msg      = sprintf(__('You cannot create a %s file.', 'wordpress-seo'), 'robots.txt');
				$ret["error"] = $die_msg;
			}else{
				$robotsnew      = stripslashes($_POST['robotsnew']);
				$ret["content"] = $robotsnew;
				if(file_put_contents($robots_file, $robotsnew)){
					$ret["success"] = true;
					$ret["msg"]     = __("File created", 'lbi-sodexo-theme');
				}else{
					$ret["error"] = sprintf(__('You cannot create a %s file.', 'wordpress-seo'), 'robots.txt');
				}

			}
		}elseif(isset($_POST['submitrobots'])){
			$ret['mode'] = "update";
			if(!current_user_can('edit_files')){
				$die_msg      = sprintf(__('You cannot edit the %s file.', 'wordpress-seo'), 'robots.txt');
				$ret["error"] = $die_msg;
			}else{
				$robotsnew = stripslashes($_POST['robotsnew']);
				if(file_put_contents($robots_file, $robotsnew)){
					$ret["success"] = true;
					$ret["msg"]     = __("File updated", 'lbi-sodexo-theme');
				}else{
					$ret["error"] = sprintf(__('You cannot edit the %s file.', 'wordpress-seo'), 'robots.txt');
				}
			}
		}else{
			$ret["error"] = __("An unhandled error occurred", 'lbi-sodexo-theme');
			//          echo 0;
		}
	}
	echo json_encode($ret);
	die();
}

add_action("wp_ajax_gen_robots_txt", "dlbi_override_yoast_robots_txt_generation");
//</editor-fold>
//<editor-fold desc="SOXINT-723">
function getCookieValue($key){
	if(isset($_COOKIE[$key])){
		//      dlbi_display_debug("getCookieValue:" . $key . " = " . $_COOKIE[$key], 0, "darkPurple");
		return $_COOKIE[$key];
	}
	return false;
}

function get_hp_image(){
	$hp_image        = get_field('homepage_banner_image', 'option');
	$hp_image_cookie = get_field('homepage_banner_image_seen_video', 'option');
	if(getCookieValue("homepage_banner_video_shown") && $hp_image_cookie){
		return $hp_image_cookie;
	}
	return $hp_image;
}

//</editor-fold>
/**
 * Optimise queries for caroussel widget
 */
add_action( 'wp_update_nav_menu', 'soxo_update_nav_menu' );

function soxo_update_nav_menu(){
	$slugs_menu = array('top-menu-affiliate-bar','top-menu-customers-bar', 'top-menu-bar');
	foreach ($slugs_menu as $slug) {
		$menu = wp_get_nav_menu_items($slug);
		update_option($slug, $menu);
	}
}
/**
 * Deactivate Gutenberg
 */
// disable for posts
add_filter('use_block_editor_for_post', '__return_false', 10);
// disable for post types
add_filter('use_block_editor_for_post_type', '__return_false', 10);

//<editor-fold desc="SOXINT-950">
function get_cta_home(){
	$ret          = "";
	$positions    = [
		"left"   => ["top", "bottom",],
		"center" => ["top", "bottom",],
		"right"  => ["top", "bottom",],
	];
	$at_least_one = false;
	$ret          .= "<div class='banner-home-cta-zone hidden'>
                        <div class='inner-wrapper'>";

    foreach($positions as $position_column => $columns){
        $ret .= "<div class='column $position_column'>";
        foreach($columns as $position_row){
            $position = $position_row . "_" . $position_column;
//          $ret      .= dlbi_display_debug("homepage_banner_cta_$position", 1, "orangered");
            $title    = $url = $target = "";
            $cta      = get_field("homepage_banner_cta_$position", 'option');
            if(is_array($cta)){
              extract($cta);
            }
            $html_CTA = '';
            if($url && $title){
                $target_attr = "";
                if($target){
                    $target_attr = "target='$target'";
                }
                $at_least_one = true;
                $html_CTA     = "<a class='btn-sodexo btn-sodexo-red' href='$url' $target_attr>$title</a>";
            }
            $ret .= "<div class='row $position_row'>$html_CTA</div>";
        }
        $ret .= "</div>";
    }
    $ret .= " </div></div>";
    if($at_least_one){
        echo $ret;
    }
}

//</editor-fold>

//<editor-fold desc="SOXINT-983">
function set_cta_object($cta, $class = 'btn-sodexo btn-sodexo-red'){
	$ret = "";
	if($cta && $cta !== "||"){
		$arr = vc_build_link($cta);
		$url = $title = $title_attr = $target = $target_attr = "";
		extract($arr);
		if($title){
			$title_attr = "title='$title'";
		}
		if($title){
			$target_attr = "target='$target'";
		}
		if($url){
			$ret = "<a href='$url' $title_attr $target_attr class='$class'>$title</a>";
		}
	}
	return $ret;
}


//Remove useless metadata
remove_action( 'wp_head', 'wp_generator' );

//Remove All Meta Generators
ini_set('output_buffering', 'on'); // turns on output_buffering
function remove_meta_generators($html) {
    $pattern = '/<meta name(.*)=(.*)"generator"(.*)>/i';
    $html = preg_replace($pattern, '', $html);
    return $html;
}
function clean_meta_generators($html) {
    ob_start('remove_meta_generators');
}
add_action('get_header', 'clean_meta_generators', 100);
add_action('wp_footer', function(){ ob_end_flush(); }, 100);

//Activate plugins
/*$plugins = array('dlbi-hreflang/dlbi-hreflang.php');
foreach($plugins as $plugin){
    activate_plugin($plugin, null, true);
}*/

//</editor-fold>
//<editor-fold desc="SOXINT-1015">
function lazyload_settings($settings = []){
	if(!is_array($settings)){
		$settings = [];
	}
	if(get_field('activate_lazyload', 'option')){
		$settings["active"]      = true;
		$settings["debug"]       = false; //fallback for test mode
		$settings["marginOuter"] = get_field('lazyload_outer_margin', 'option');
		switch(get_field('lazyload_settings', 'option')){
			case "standard":
			default:
				$settings["mode"]         = "standard";
				$settings["bodyclass"]    = "lazyload-on";
				$settings["checkParents"] = true;
				$settings["lazyStyle"]    = true;
				break;
			case "onlyImages":
				$settings["mode"]         = "onlyImages";
				$settings["bodyclass"]    = "lazyload-off";
				$settings["checkParents"] = false;
				$settings["lazyStyle"]    = false;
				break;
			case "onlyVisible":
				$settings["mode"]         = "onlyVisible";
				$settings["bodyclass"]    = "lazyload-on";
				$settings["checkParents"] = false;
				$settings["lazyStyle"]    = true;
				break;
		}

	}else{
		$settings["active"]       = false;
		$settings["mode"]         = "none";
		$settings["bodyclass"]    = "lazyload-off";
		$settings["checkParents"] = false;
		$settings["lazyStyle"]    = false;
	}

	//	$array["active"] = false;

	return $settings;
}

function lazy_style($value){
	$prop = "style";
	if(lazyload_settings()["lazyStyle"]){
		$prop = "data-lazy-style";
	}
	return "$prop=\"$value\"";
}

function lazy_default_wp_images($attr){
	$settings = lazyload_settings();
	if($settings["active"]){
		if(isset($attr["src"]) && $attr["src"] !== LAZYSRC && !isset($attr["data-lazy-src"])){
			$attr["data-lazy-src"] = $attr["src"];
			$attr["src"]           = LAZYSRC;
		}
		if(isset($attr["srcset"]) && $attr["srcset"] !== LAZYSRC && !isset($attr["data-lazy-srcset"])){
			$attr["data-lazy-srcset"] = $attr["srcset"];
			$attr["srcset"]           = LAZYSRC;
		}
	}
	return $attr;
}

function lazy_body_class($classes = []){
	$settings  = lazyload_settings();
	$classes[] = $settings["bodyclass"];
	return $classes;
}

function remove_emoji_scripts(){
	if(get_field('remove_emoji_support', 'option')){
		// remove all actions related to emojis
		remove_action('admin_print_styles', 'print_emoji_styles');
		remove_action('wp_head', 'print_emoji_detection_script', 7);
		remove_action('admin_print_scripts', 'print_emoji_detection_script');
		remove_action('wp_print_styles', 'print_emoji_styles');
		remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
		remove_filter('the_content_feed', 'wp_staticize_emoji');
		remove_filter('comment_text_rss', 'wp_staticize_emoji');
	}
}

function remove_popper_scripts(){
//	dlbi_display_debug("'remove_popper_support' " . get_field('remove_popper_support', 'option'), 0, "orangered");
	if(get_field('remove_popper_support', 'option')){
		wp_dequeue_script("popper-scripts");
	}
}

add_action('init', 'remove_emoji_scripts');
add_action('wp_enqueue_scripts', 'remove_popper_scripts',100);

add_filter("lazyload-settings", "lazyload_settings", 1, 1);
add_filter("lazyload-style", "lazy_style", 1, 1);
add_filter("wp_get_attachment_image_attributes", "lazy_default_wp_images");
add_filter("body_class", "lazy_body_class");

include_once("theme-functions/shortcodes.php");
include_once("theme-functions/plugin-hooks.php");


// Translate cyrilic chars
function pm_ru2lat($str) {
	$tr = array(
	"А"=>"a", "Б"=>"b", "В"=>"v", "Г"=>"g", "Д"=>"d",
	"Е"=>"e", "Ё"=>"yo", "Ж"=>"zh", "З"=>"z", "И"=>"i",
	"Й"=>"j", "К"=>"k", "Л"=>"l", "М"=>"m", "Н"=>"n",
	"О"=>"o", "П"=>"p", "Р"=>"r", "С"=>"s", "Т"=>"t",
	"У"=>"u", "Ф"=>"f", "Х"=>"kh", "Ц"=>"ts", "Ч"=>"ch",
	"Ш"=>"sh", "Щ"=>"sch", "Ъ"=>"", "Ы"=>"y", "Ь"=>"",
	"Э"=>"e", "Ю"=>"yu", "Я"=>"ya", "а"=>"a", "б"=>"b",
	"в"=>"v", "г"=>"g", "д"=>"d", "е"=>"e", "ё"=>"yo",
	"ж"=>"zh", "з"=>"z", "и"=>"i", "й"=>"j", "к"=>"k",
	"л"=>"l", "м"=>"m", "н"=>"n", "о"=>"o", "п"=>"p",
	"р"=>"r", "с"=>"s", "т"=>"t", "у"=>"u", "ф"=>"f",
	"х"=>"kh", "ц"=>"ts", "ч"=>"ch", "ш"=>"sh", "щ"=>"sch",
	"ъ"=>"", "ы"=>"y", "ь"=>"", "э"=>"e", "ю"=>"yu",
	"я"=>"ya"
	);

	return strtr($str, $tr);
}
function pm_transliterate_russian($slug) {
	return pm_ru2lat($slug);
}
add_filter('permalink_manager_filter_default_post_slug', 'pm_transliterate_russian');
add_filter('permalink_manager_filter_term_slug', 'pm_transliterate_russian');
add_filter('permalink_manager_filter_default_term_slug', 'pm_transliterate_russian');
