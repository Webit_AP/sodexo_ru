<?php
/*
  Template Name: Login page
 */
get_header();

$loginpage_image = get_field('login_page_background');
$idSlider = get_field('id_slider');
?>

<?php if ($loginpage_image || $idSlider = get_field('id_slider')) : ?>
    <div class="page-cover">
	    <div class="page-cover_container">
        <?php if(!empty($idSlider)) : ?>
        	<div class="lay-slider">
        		<?php layerslider($idSlider); ?>
        	</div>
        <?php else :
          echo wp_get_attachment_image( $loginpage_image, 'soxo-hero-header' );
        endif; ?>
        <h1 class="sr-only"><?php echo the_title(); ?></h1>
	    </div>
    </div>
<?php endif; ?>

<?php
// check if the flexible content field has rows of data
if( have_rows('login_page_flexible_blocks') ):

     // loop through the rows of data
    while ( have_rows('login_page_flexible_blocks') ) : the_row();

        if( get_row_layout() == 'block_posts_grid' ):

          get_template_part( 'layouts-acf/block_posts-grid-loginpage' ); // Load post grid layout

        endif;

    endwhile;

endif;
?>

<?php the_content() ?>

<div class="breadcrumb">
  <?php
   if(function_exists('bcn_display')):
            bcn_display();
   endif; ?>
</div>
<?php

get_footer();
