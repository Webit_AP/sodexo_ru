<?php
/*
 * Template Name: Request a quote
 */
get_header();
?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<?php
	$idSlider = get_field('id_slider');
	if(!empty($idSlider)) : ?>
		<div class="lay-slider">
			<?php layerslider($idSlider); ?>
		</div>
	<?php endif; ?>
	
	<div class="container">
	    <div class="row">
		<div class="col-md-12">
		    <?php the_content(); ?>
		</div>
	    </div>
	</div>
    <?php endwhile; ?>
<?php endif; ?>
 <div class="breadcrumb">
  <?php
  if(function_exists('bcn_display')):
     bcn_display();
  endif; ?>
</div>
<?php get_footer();
