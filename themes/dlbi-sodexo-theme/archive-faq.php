<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

if(get_field('hide_faq_page', 'option')) {
    global $wp_query;
    $wp_query->set_404();
    status_header( 404 );
    get_template_part( 404 ); exit();
}

get_header();

$bgimage = get_field('banner_faq_and_404', 'option');
if( !empty($bgimage) ) {
  $bgimgurl = $bgimage['url'];
} else {
  $bgimgurl = get_stylesheet_directory_uri() . '/dist/images/bg-404-search.jpg';
}
$style = apply_filters("lazyload-style", "background-image:url('$bgimgurl');");
?>

<section class="search-block-aside" <?php echo $style; ?>>
    <div class="container">
	<h1 class="tt"><?php echo __('Our FAQ is here to help', 'lbi-sodexo-theme'); ?></h1>
	<?php require get_template_directory_child() . '/inc/banner-search.php'; ?>
    </div><!-- container search end -->
</section>

<section class="wrapper" id="wrapper-faq">
    <div class="container" id="content">
	<div class="row">
	    <div class="col-sm-12">
		<div class="faq_filter" id="filters">
		    <h2 class="sodexo-title"><?php echo get_field('faq_archive_filters_title', 'option'); ?></h2>
		    <p class="text-center faq_filter-description col-md-8"><?php echo get_field('faq_archive_filters_description', 'option') ?></p>
		    <div class="hidden-sm-down">

			<div class="faq_filter-buttons text-center">
			    <?php
			    $classprofile = '';
			    foreach (get_terms(['taxonomy' => 'profile', 'hide_empty' => false]) as $tag) :
				if (isset($_GET['profile'])):
				    if ($tag->slug == $_GET['profile']):
					$classprofile = ' faq-profile-active';
				    endif;
				endif;

				$currentTopic = "";
				if (isset($_GET['topic'])):
				    $currentTopic = $_GET['topic'];
				endif;


				$current_url = home_url(add_query_arg(array(), $wp->request));
				$pos = strpos($current_url, "/page");
				$lenghtUrl = strlen($current_url);

				if ($pos > 0) {
				    $newUrl = substr($current_url, 0, -($lenghtUrl - $pos));
				    $newUrl = $newUrl . "/?profile=" . $tag->slug . "&topic=" . $currentTopic . "#topics";
				} else {
				    $newUrl = $current_url . "/?profile=" . $tag->slug . "&topic=" . $currentTopic . "#topics";
				}
				?>



    			    <a href="<?php echo $newUrl ?>" class="btn-sodexo btn-sodexo-faq btn-sodexo-faq-filter<?php echo $classprofile; ?>"><?php echo $tag->name; ?></a>

    			    <!--
    			    <a href="<?php echo add_query_arg('profile', $tag->slug) ?>#topics" class="btn-sodexo btn-sodexo-faq btn-sodexo-faq-filter<?php echo $classprofile; ?>"><?php echo $tag->name; ?></a>
    			    -->
				<?php
				$classprofile = '';
			    endforeach;
			    ?>
			</div>
		    </div>

		    <div class="fake-select hidden-md-up">
			<p class="text-center faq_filter_topic-title"><strong><?php echo get_field('faq_archive_filters_who_you_are', 'option'); ?></strong></p>
			<div class="fake-select_content">
			    <a href="#" class="fake-select-link" data-toggle="modal" data-target="#FilterModal">
				<?php
				if (isset($_GET['profile'])):
				    $namefilter = '';
				    foreach (get_terms(['taxonomy' => 'profile', 'hide_empty' => false]) as $tag) :
					if (isset($_GET['profile'])):
					    if ($tag->slug == $_GET['profile']):
						$namefilter = $tag->name;
					    endif;
					endif;
				    endforeach
				    ?>
    				<span><?php echo $namefilter; ?></span><i class="icon icon-arrow-down"></i>
				<?php else: ?>
    				<span><?php echo __('Select', 'lbi-sodexo-theme'); ?> </span><i class="icon icon-arrow-down"></i>
				<?php endif;
				?>
			    </a>
			</div>
		    </div>

		    <div class="menu-modal modal" id="FilterModal" tabindex="-1" role="dialog" aria-labelledby="FilterModal" aria-hidden="true">
			<div class="modal-dialog" role="document">
			    <div class="modal-content">
				<div class="modal-header">
				    <h5 class="modal-title"><?php echo __('Filter by:', 'lbi-sodexo-theme'); ?></h5>
				    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="icon icon-arrow-down"></i></span>
				    </button>
				</div>
				<div class="modal-body">
				    <ul>
					<?php $g = 0; ?>

					<?php
					foreach (get_terms(['taxonomy' => 'profile', 'hide_empty' => false]) as $tag) :
					    $topic = get_term($tag);
					    if (isset($_GET['topic'])):
						if ($topic->slug == $_GET['topic']):
						    $classtopic = ' faq-topic-active';
						endif;
					    endif;
					    ?>
    					<li class="nav-item" role="presentation">
    					    <a href="<?php echo add_query_arg('profile', $tag->slug) ?>#topics" class="modal-nav-link nav-link<?php echo $classprofile; ?>"><?php echo $tag->name; ?></a>
    					</li>
					    <?php
					    $g++;
					    $classtopic = '';
					    ?>
					<?php endforeach; ?>
				    </ul>
				</div>
			    </div>
			</div>
		    </div>
		</div>

		<?php
		if (get_query_var('profile')) :
		    $profile = get_term_by('slug', get_query_var('profile'), 'profile');
		    $topics = get_field('profile_topics', 'profile_' . $profile->term_id);
		    ?>
    		<div class="faq_filter_topic" id="topics">
			<?php if ($topics) : ?>
			    <p class="text-center faq_filter_topic-title"><strong><?php echo get_field('faq_archive_filters_topic_title', 'option'); ?></strong></p>
			    <div class="hidden-sm-down">
				<div id="test" class="faq_filter_topic-buttons text-center">
				    <?php
				    foreach ($topics as $topic_id) :
					$topic = get_term($topic_id);


					if (isset($_GET['topic'])):

					    if ($topic->slug == $_GET['topic']):
						$classtopic = ' faq-topic-active';
					    endif;

					endif;


					$currentTopic = "";
					if (isset($_GET['topic'])):
					    $currentTopic = $_GET['topic'];
					endif;

					$profile = "";
					if (isset($_GET['profile'])):
					    $profile = $_GET['profile'];
					endif;


					$current_url = home_url(add_query_arg(array(), $wp->request));
					$pos = strpos($current_url, "/page");
					$lenghtUrl = strlen($current_url);

					if ($pos > 0) {
					    $newUrl = substr($current_url, 0, -($lenghtUrl - $pos));
					    $newUrl = $newUrl . "/?profile=" . $profile . "&topic=" . $topic->slug;
					} else {
					    $newUrl = $current_url . "/?profile=" . $profile . "&topic=" . $topic->slug;
					}
					?>
	    			    <!--
	    	<a href="<?php echo add_query_arg('topic', $topic->slug) ?>#topics" class="btn-sodexo btn-sodexo-faq btn-sodexo-faq-topic <?php echo $classtopic; ?>"><?php echo $topic->name; ?></a>
	    			    -->
	    			    <a onclick="dataLayer.push({'event': 'faq','user-type':'<?php echo addslashes($namefilter); ?>','topic':'<?php echo addslashes($topic->name); ?>' });" href="<?php echo $newUrl ?>#topics" class="btn-sodexo btn-sodexo-faq btn-sodexo-faq-topic <?php echo $classtopic; ?>"><?php echo $topic->name; ?></a>

					<?php
					$classtopic = '';
				    endforeach;
				    ?>
				</div>
			    </div>

			    <div class="fake-select hidden-md-up">
				<div class="fake-select_content">
				    <a  href="#" class="fake-select-link" data-toggle="modal" data-target="#MegaTabMenuModal">
					<?php
					if (isset($_GET['topic'])):
					    $nametopic = '';
					    foreach ($topics as $topic_id) :
						$topic = get_term($topic_id);
						if (isset($_GET['topic'])):
						    if ($topic->slug == $_GET['topic']):
							$nametopic = $topic->name;
						    endif;
						endif;
					    endforeach
					    ?>
	    				<span><?php echo $nametopic ?></span><i class="icon icon-arrow-down"></i>
					<?php else: ?>
	    				<span><?php echo 'Select' //echo $topics[0]->name  ?></span><i class="icon icon-arrow-down"></i>
					<?php endif;
					?>

				    </a>
				</div>
			    </div>

			    <div class="menu-modal modal" id="MegaTabMenuModal" tabindex="-1" role="dialog" aria-labelledby="MegaTabMenuModal" aria-hidden="true">
				<div class="modal-dialog" role="document">
				    <div class="modal-content">
					<div class="modal-header">
					    <h5 class="modal-title"><?php echo __('Filter by', 'lbi-sodexo-theme'); ?></h5>
					    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true"><i class="icon icon-arrow-down"></i></span>
					    </button>
					</div>
					<div class="modal-body">
					    <ul>
						<?php $g = 0; ?>
						<?php
						foreach ($topics as $topic_id) :
						    $topic = get_term($topic_id);
						    if (isset($_GET['topic'])):
							if ($topic->slug == $_GET['topic']):
							    $classtopic = ' faq-topic-active';
							endif;
						    endif;
						    ?>
	    					<li class="nav-item" role="presentation">
	    					    <a onclick="dataLayer.push({'event': 'faq','user-type':'<?php echo addslashes($namefilter); ?>','topic':'<?php echo addslashes($topic->name); ?>' });" href="<?php echo add_query_arg('topic', $topic->slug) ?>#topics" class="modal-nav-link nav-link <?php echo $classtopic; ?>"><?php echo $topic->name; ?></a>
	    					</li>
						    <?php $g++; ?>
						    <?php
						    $classtopic = '';
						endforeach;
						?>
					    </ul>
					</div>
				    </div>
				</div>
			    </div>



			<?php endif; ?>
    		</div>
		<?php endif; ?>
	    </div>
	</div>
    </div>
</section>




<section class="wrapper" id="wrapper-search">
    <div class="container" id="content">
	<div class="row justify-content-md-center">
	    <div class="col-sm-12 col-md-8 search-items">




		<?php
		if (get_query_var('profile')) :
		    $profile = get_term_by('slug', get_query_var('profile'), 'profile');
		    $topics = get_field('profile_topics', 'profile_' . $profile->term_id);
		    if (get_query_var('topic')) :
			?>
			<h2 class="text-center sodexo-title"><?php echo sprintf(get_field('faq_archive_results_title', 'option'), ucfirst(get_query_var('topic'))); ?></h2>

			<?php
			if (have_posts()) :
			    $z = 0;
			    $pass = 0;
			    ?>
	    		<div class="panel-group" id="accordion-faq<?php echo $pass; ?>" role="tablist" aria-multiselectable="true">
				<?php
				while (have_posts()) : the_post();
				    // Get list FAQ
				    $faq = get_field('faq_questions', get_the_ID());
				    ?>
				    <?php
				    $y = 0;
				    foreach ($faq as $question):
					if ($question['faq_question'] != ''):
					    ?>

					    <div class="panel panel-default">
						<div class="panel-heading" role="tab" id="heading<?php echo $pass; ?>">
						    <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion-faq" href="#collapse<?php echo $z . $y; ?>" aria-expanded="false"  aria-controls="collapse<?php echo $z . $y; ?>" class="faq-title collapsed" role="button">
							    <?php echo $question['faq_question']; ?>
							</a>
						    </h4>
						</div>
						<div id="collapse<?php echo $z . $y; ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading<?php echo $pass; ?>">
						    <div class="panel-body">
							<?php echo $question['faq_answer']; ?>
						    </div>
						</div>
					    </div>


					    <?php /* echo ($pass == 0) ? 'true': 'false'; */ ?>
					    <?php
					    $y++;
					endif;
					$pass++;
				    endforeach;
				    ?>
				    <?php
				    $z++;
				endwhile;
				?>
	    		</div>
			    <?php
			endif;
		    endif;
		endif;
		?>
		<?php
		/**
		 * @todo
		 * Add button link after Q/A
		 */
		?>
	    </div>
	</div>
    </div>




    <?php if (get_query_var('profile') && get_query_var('topic')) : ?>
        <div class="search-paginate text-center"><?php echo paginate_links(array('add_fragment' => '#topics')); ?></div>
    <?php endif; ?>
</section>

<?php get_footer(); ?>
