<?php
// Filter to replace default Visual Composer css class names for vc_row shortcode and vc_column with Bootstrap css class row and col-*-*
/*
  add_filter('vc_shortcodes_css_class', 'custom_css_classes_for_vc_row_and_vc_column', 10, 2);

  function custom_css_classes_for_vc_row_and_vc_column($class_string, $tag) {
  if ($tag == 'vc_row' || $tag == 'vc_row_inner') {
  $class_string = str_replace('vc_row-fluid', 'row', $class_string); // This will replace "vc_row-fluid" with "row"
  }
  if ($tag == 'vc_column' || $tag == 'vc_column_inner') {
  $class_string = preg_replace('/vc_col-sm-(\d{1,2})/', 'col-sm-$1', $class_string); // This will replace "vc_col-sm-%" with "col-sm-%"
  }
  return $class_string; // Important: you should always return modified or original $class_string
  }


  // Filter to delete default Visual Composer css class names
  /*
  add_filter('vc_shortcodes_css_class', 'custom_css_classes_for_vc_row_and_vc_column', 10, 2);

  function custom_css_classes_for_vc_row_and_vc_column($class_string, $tag) {
  if ($tag == 'vc_row' || $tag == 'vc_row_inner') {
  $class_string = str_replace(['vc_row-fluid','vc_row_inner','vc_row'], '', $class_string); // This will replace VC rows with nothing
  }
  if ($tag == 'vc_column' || $tag == 'vc_column_inner') {
  $class_string = preg_replace('/vc_col-sm-(\d{1,2})/', '', $class_string); // This will replace VC columns with nothing
  }
  // return $class_string; // Important: you should always return modified or original $class_string
  }
 */

function SC_main_menu_shortcode($attrs = [], $content = ''){
	$imagepath     = get_stylesheet_directory_uri() . '/dist/images';
	$loginurl      = get_field('url_login', 'option');
	$customlogo    = get_field('custom_logo', 'option');
	$is_rp_content = get_field('is_rp_content', 'option');
	$noh1          = false;
	$inSticky      = false;
	$container_id  = "top-nav";

	if(is_array($attrs)){
		extract($attrs);
	}
	$displayH1   = (is_front_page() || $is_rp_content) && !$noh1;
	$logoDefault = apply_filters("dlbi_image", $imagepath . "/logo-sodexo-header.svg", "", get_bloginfo('name'), "", 200, 70);
	if($inSticky){
		$logoDefault = apply_filters("dlbi_image", $imagepath . "/logo-sodexo-sticky.svg", "", get_bloginfo('name'), "", 185, 65);
	}
	$logo = $logoDefault;
	if($customlogo){
		$logoCustom = apply_filters("dlbi_image", $customlogo['url'], "", get_bloginfo('name'), "", 185, 65);
		if(!$inSticky){
			$logo = $logoCustom;
		}
	}
	ob_start();
	if($displayH1){ ?>
        <h1>
	<?php } ?>
    <a class="navbar-brand" rel="home" href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>">

		<?php echo $logo; ?>

    </a>
	<?php if($displayH1){ ?>
        </h1>
	<?php } ?>

    <div class="ml-auto menu-top-container">
		<?php
		wp_nav_menu(
			[
				'theme_location'  => 'top-nav',
				'container_class' => '',
				'container_id'    => $container_id,
				'menu_class'      => 'navbar-nav',
				'fallback_cb'     => '',
				'menu_id'         => '',
				'walker'          => new Walker_Nav_Menu_Top(),
			]
		);
		if(!$is_rp_content){
			if(class_exists("Sxode_Login_Dropdown")){
				Sxode_Login_Dropdown::render_for_top_menu();
			}elseif($loginurl){
				$target = "";
				if(get_field("login_button_have_blank_target", 'option')){
					$target = 'target="_blank"';
				}
				?>
                <div class="top-menu-login-item hidden-md-down">
                    <a title="<?php echo __('Login', 'lbi-sodexo-theme'); ?>" <?php echo $target; ?> href="<?php echo $loginurl; ?>" onclick="dataLayer.push({'event': 'click-login'});" class="btn-sodexo btn-sodexo-white icon-person"><?php echo __('Login', 'lbi-sodexo-theme'); ?></a>
                </div>
				<?php
			}
		}
		?>
        <div class="burger">
            <button class="soxo-hamburger collapsed" type="button" data-toggle="collapse" data-target="#sideNav" aria-controls="sideNav" aria-expanded="false" aria-label="Toggle navigation">
                <span><?php echo __('toggle menu', 'lbi-sodexo-theme'); ?></span>
            </button>
            <div><?= get_field('burger_title', 'option'); ?></div>
        </div>
		<?php if(is_multisite() && get_blog_count() > 1): ?>
            <div>
                <div class='multilingue'>
					<?php
					if(function_exists('the_msls')){
						the_msls();
					}
					?>
                </div>
            </div>
		<?php endif; ?>

    </div>
	<?php


	$content = ob_get_contents();
	ob_end_clean();
	return $content;
}


function SC_secondary_menu_shortcode($attrs = [], $content = ''){
	$imagepath = get_stylesheet_directory_uri() . '/dist/images';
	$loginurl  = get_field('url_login', 'option');
	//	$customlogo    = get_field('custom_logo', 'option');
	$is_rp_content = get_field('is_rp_content', 'option');

	$affiliate_menu           = [];
	$customers_menu           = [];
	$pages_for_affiliate_menu = get_field('pages_for_affiliate_menu', 'option');
	$pages_for_customers_menu = get_field('pages_for_customers_menu', 'option');
	if($pages_for_affiliate_menu){
		$affiliate_menu = wp_list_pluck($pages_for_affiliate_menu, 'ID');
	}
	if($pages_for_customers_menu){
		$customers_menu = wp_list_pluck($pages_for_customers_menu, 'ID');
	}
	$custom_nav = 'Top Menu Bar';
	if(in_array(get_the_ID(), $affiliate_menu)){
		$custom_nav = 'Top Menu affiliate Bar';
	}elseif(in_array(get_the_ID(), $customers_menu)){
		$custom_nav = 'Top Menu Bar customers';
	}
	if(is_array($attrs)){
		extract($attrs);
	}
	ob_start();
	/* if ( !$customlogo ) { ?>

                              <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                              <img src="<?php echo $imagepath; ?>/logo-sodexo-header.svg" width="200" height="70" alt="<?php bloginfo( 'name' ); ?>" />
                              </a>

                              <?php } else { ?>

                              <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                              <img src="<?php echo $customlogo['url']; ?>" width="200" height="70" alt="<?php bloginfo( 'name' ); ?>" />
                              </a>

                              <?php } */ ?>

    <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>">
		<?php echo apply_filters("dlbi_image", $imagepath . "/logo-sodexo-sticky.svg", "", get_bloginfo('name'), "", 185, 65); ?>
    </a>

	<?php
	wp_nav_menu(
		[
			'menu'            => $custom_nav,
			// 'container' => false,
			'container_class' => '',
			'container_id'    => '',
			'menu_class'      => 'top-menu-main',
			'fallback_cb'     => '',
			'menu_id'         => '',
			'before'          => '',
			'after'           => '',
			'walker'          => new Walker_Nav_Top_Menu(),
		]
	);


	if(!$is_rp_content){
		if(class_exists("Sxode_Login_Dropdown")){
			Sxode_Login_Dropdown::render_for_top_menu();
		}elseif($loginurl){
			$target = "";
			if(get_field("login_button_have_blank_target", 'option')){
				$target = 'target="_blank"';
			}
			?>
            <div class="combined-menu-login-item">
                <a title="<?php echo __('Login', 'lbi-sodexo-theme'); ?>" href="<?php echo $loginurl; ?>" <?php echo $target; ?> onclick="dataLayer.push({'event': 'click-login'});" class="icon-person"><span class="sr-only"><?php echo __('Login', 'lbi-sodexo-theme'); ?></span></a>
            </div>
			<?php
		}
	}
	?>

    <div class="burger">
        <button class="soxo-hamburger collapsed" type="button" data-toggle="collapse" data-target="#sideNav" aria-controls="sideNav" aria-expanded="false" aria-label="Toggle navigation">
            <span><?php echo __('toggle menu', 'lbi-sodexo-theme'); ?></span>
        </button>
        <div><?= get_field('burger_title', 'option'); ?></div>
    </div>

	<?php if(is_multisite() && get_blog_count() > 1): ?>
        <div class='multilingue'>
			<?php
			if(function_exists('the_msls')){
				the_msls();
			}
			?>
        </div>
	<?php endif;

	$content = ob_get_contents();
	ob_end_clean();
	return $content;
}


add_shortcode("main-menu-shortcode", "SC_main_menu_shortcode");
add_shortcode("secondary-menu-shortcode", "SC_secondary_menu_shortcode");