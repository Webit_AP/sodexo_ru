<?php
/*
Template Name: Our leisure offers
*/
get_header();

?>


<div class="page-cover">
  <div class="container-fluid">
    <div class="row">
      <div class="page-cover_container">
        <?php
        $leisuresheadimg = get_field('leisure_background');
        $idSlider = get_field('id_slider');
        $size = 'soxo-hero-header';
        if(!empty($idSlider)) : ?>
          <div class="lay-slider">
            <?php layerslider($idSlider); ?>
          </div>
          <?php
        elseif($leisuresheadimg) :
          echo wp_get_attachment_image( $leisuresheadimg, $size ); ?>
        <?php endif; ?>
        <span class="sr-only"><?php echo the_title(); ?></span>
      </div>
    </div>
  </div>
</div>


<?php get_template_part( 'layouts-acf/block_col-offers' ); // Load columns offers layout ?>

<?php get_template_part( 'layouts-acf/block_image-list' ); // Load image and list layout ?>

<?php get_template_part( 'layouts-acf/block_posts-grid' ); // Load post grid layout ?>

<?php the_content() ?>

<div class="breadcrumb">
  <?php
  if(function_exists('bcn_display')):
     bcn_display();
  endif; ?>
</div>
<?php
get_footer();
