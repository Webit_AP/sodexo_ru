<?php
/**
 * The right sidebar containing the main widget area.
 *
 * @package understrap
 */

$is_rp_content = get_field( 'is_rp_content', 'option' );

?>

<?php if ( ! is_page_template( 'homepage-blog.php' ) ) : ?>
<div class="col-lg-4 widget-area" id="right-sidebar" role="complementary">
		<div class="social">
		<span><?php echo __( 'Share', 'lbi-sodexo-theme' ); ?> : </span>
			<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>" target="_blank">
				<?php echo apply_filters( 'dlbi_image', get_stylesheet_directory_uri() . '/src/assets/images/icon-facebook.svg', 'facebook', 'Facebook', '', 30, 30 ); ?>
			</a>
			<a href="https://twitter.com/share?url=<?php echo get_permalink(); ?>&text=<?php echo get_the_title(); ?>" target="_blank">
				<?php echo apply_filters( 'dlbi_image', get_stylesheet_directory_uri() . '/src/assets/images/icon-twitter.svg', 'twitter', 'Twitter', '', 30, 30 ); ?>
			</a>
			<?php if ( $is_rp_content ) : ?>
				<a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo get_permalink(); ?>&title=<?php echo get_the_title(); ?>&summary=<?php echo get_the_title(); ?>&source=<?php echo get_the_title(); ?>" target="_blank">
					<?php echo apply_filters( 'dlbi_image', get_stylesheet_directory_uri() . '/src/assets/images/icon-linkedin.svg', 'linkedin', 'Linkedin', '', 30, 30 ); ?>
				</a>
				<a href="mailto:?subject=<?php echo get_the_title(); ?>&body=<?php echo get_the_title(); ?>%0D%0A %0D%0A<?php echo get_permalink(); ?>%0D%0A %0D%0ARead more on sodexobenefits.com">
					<?php echo apply_filters( 'dlbi_image', get_stylesheet_directory_uri() . '/src/assets/images/icon-mail.svg', 'mail', 'Mail', '', 30, 30 ); ?>
				</a>
			<?php endif; ?>
		</div>
		<div class="row">
				<div class="blog-item col-md-6 col-lg-12">
			<?php endif; ?>

		<!-- Display sidebar -->
		<?php dynamic_sidebar( 'right-single' ); ?>

			<?php if ( ! is_page_template( 'homepage-blog.php' ) ) : ?>
				</div>

		<!-- Display related articles -->
				<?php
				$orig_post  = get_the_ID();
				$categories = get_the_category( $orig_post );

				if ( $categories ) :

					$category_ids = array();
					foreach ( $categories as $individual_category ) {
						$category_ids[] = $individual_category->term_id;
					}

					$args = array(
						'category__in'   => $category_ids,
						'post__not_in'   => array( $orig_post ),
						'posts_per_page' => 4, // Number of related posts that will be shown.
					);

					$query = new WP_Query( $args );
					if ( $query->have_posts() ) :
						?>

				<div id="related_posts" class="posts-section col-md-6 col-lg-12">
				<h3 class="tt"><?php echo __( 'RELATED ARTICLES', 'lbi-sodexo-theme' ); ?></h3>
				<ul>
						<?php
						while ( $query->have_posts() ) :
							$query->the_post();
							if ( $orig_post != get_the_ID() ) :
								?>
						<li>
						<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>">
							<div class="relatedthumb">
								<?php
								if ( has_post_thumbnail( get_the_ID() ) ) :
									echo apply_filters( 'dlbi_image', get_the_post_thumbnail_url( get_the_ID(), 'thumbnail' ), 'blog-component--container_image', '', '', 100, 100 );
									?>
									<?php // echo get_the_post_thumbnail( get_the_ID(), 'medium', array( 'class' => 'blog-component--container_image' ) ); ?>
							<?php endif; ?>
							</div>
							<div class="relatedcontent">
							<span class="date"><?php echo get_the_date( 'd.m.Y' ); ?></span>
							<div><?php echo strip_shortcodes( wp_trim_words( get_post_field( 'post_content', get_the_ID() ), 10 ) ); ?></div>
							</div>
						</a>
						</li>
							<?php endif; ?>
						<?php endwhile; ?>
				</ul>
				</div>
					<?php else : ?>
				<p><?php echo __( 'Sorry, no posts matched your criteria', 'lbi-sodexo-theme' ); ?></p>
						<?php
					endif;
		endif;
				$post = $orig_post;
				wp_reset_query();
				?>

			</div>
		</div>
		<?php endif ?>
