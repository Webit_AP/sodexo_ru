<?php

// Load WordPress
$wp_load = $_SERVER['DOCUMENT_ROOT'] . '/wp/wp-load.php';
//$wp_load = $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php';
require_once( $wp_load );


if ((isset($_POST['userEmail']) && !empty($_POST['userEmail']))) {

  $hubspot_api_key = get_field('hubspot_api_key', 'option');

  $recipients = wp_list_pluck(get_field('newsletter_mails', 'option'), 'newsletter_mail');

  if ($recipients) {
    $clients = '';
    foreach ($recipients as $recipient) {
      $clients .= $recipient . ',';
    }
    $confirmation = get_field('newsletter_confirmation_text', 'option');
    $subject = get_field('newsletter_subject', 'option');
    $message = get_field('newsletter_email_body', 'option') . ' : ' . $_POST['userEmail'];
    wp_mail($clients, $subject, $message);


    if($hubspot_api_key){
      $data = array (
        'properties' =>
        array (
          0 =>
          array (
            'property' => 'email',
            'value' => $_POST['userEmail'],
          ),
        ),
      );
      $hubspot_curl = sodexo_curl('https://api.hubapi.com/contacts/v1/contact/?hapikey=' . $hubspot_api_key, $data, true, false);
    }
    echo get_field('newsletter_confirmation_text', 'option');
  }
}
