<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */
get_header();

// $container = get_theme_mod('understrap_container_type');
?>

<div id="content" tabindex="-1">

    <?php
    if (get_field('is_rp_content', 'option')):

        require_once(get_template_directory_child() . '/templates/template-blog-category.php');

    elseif (have_posts()) :
        ?>
        <?php while (have_posts()) : the_post(); ?>

            <?php the_content(); ?>

        <?php endwhile; ?>
    <?php else : ?>

        <?php get_template_part('loop-templates/content', 'none'); ?>

    <?php endif; ?>

</div><!-- #content -->


<?php get_footer(); ?>
