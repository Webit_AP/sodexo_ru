# Sodexo an understrap child theme
* Based on understrap-child : https://github.com/holger1411/understrap-child/
* Documentation understrap: https://github.com/holger1411/understrap 


## Installation
1. Go to the sodexo child theme directory
2. Run ```npm run post-install```

Installation tested with Node v7 : https://nodejs.org/download/release/v7.10.1/


## Watch changes
Run ```gulp watch```

You can change the proxy URL of the browser-sync options in ```/src/config.json```
* -> http://localhost:3000/front-wp-dev/


## How it works
It shares with the parent theme all PHP files and adds its own functions.php on top of the UnderStrap parent themes functions.php.

IT DID NOT LOAD THE PARENT THEMES CSS FILE(S)!
Instead it uses the UnderStrap Parent Theme as dependency via Bower and compiles its own CSS file from it.


## Editing
Add your own CSS styles to ```/src/assets/styles/*/_*.scss```
import you own files into ```/src/assets/styles/index.scss```

To overwrite Bootstrap or UnderStraps base variables just add your own value to: ```/src/assets/styles/common/_variables.scss```

It will be outputted into: ```/dist/styles/styles.min.css```

## Wordpress configurations
In ```/wp-admin/options-reading.php``` use static home page and compose your page with Visual Composer