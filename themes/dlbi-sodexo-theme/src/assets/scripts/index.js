/*global responsiveCarousel: true*/
/*global loadMore: true*/

/*global storeIframe: true*/
/**
 * Main scripts
 **/

/**
 * allows to calculate time elapsed since initialization of genTime
 * <- ! place genTime init at top of file to be of any use
 */
var genTime = (new Date()).getTime();

function timelapse() {
    return (new Date()).getTime() - genTime;
}

/** end init genTime **/

// jQuery(function ($) {
// console.log("loaded", timelapse());
// });


//SOXINT-745
function is_iPhone_or_iPod() {
    var isipad = navigator.userAgent.match(/iPad/i) != null;
    var isiphone = (navigator.userAgent.match(/iPhone/i) != null) || (navigator.userAgent.match(/iPod/i) != null);
    return isiphone || isipad;
}

function is_android() {
    return navigator.userAgent.match(/android/i) != null;
}

function fakesubmit(form) {
    var ser = form.serialize();
    var action = form.attr("action");
    if (!action) {
        action = form.attr("data-action");
    }
    var $modalBody = form.parents('.modal-body');
    jQuery.ajax({
        url: action,
        data: ser,
        type: "POST",
        success: function(data) {
            // console.log(data);
            $modalBody.html(data);
        }
    })
}

//END SOXINT-745

jQuery(document).ready(function($) {
    // console.log("ready", timelapse());
    //Display the color picker for the gravityForms
    // <editor-fold desc="SOXINT-1015: Lazyload initialisation">
    // console.log("SOXINT-1015", LazyloadSettings);
    var $window = $(window);
    //window position item
    var windowPosition = {top: $(window).scrollTop(), bottom: $(window).scrollTop() + $(window).height()};

    //lazyload selector
    var lazySelector = ".lazy:not(.loaded), [data-lazy-src]:not(.loaded), [data-lazy-style]:not(.loaded), .lazy-container";

    //checkvisible selector
    var checkVisibleSelector = ".check-visible";

    //margin outer for lazyload, in px
    var marginOuter = LazyloadSettings.marginOuter * 1 || 20;

    var debugmode = LazyloadSettings.debug || false;
    var checkParentsLazyload = LazyloadSettings.checkParents || false;
    var $body = $("body");
    //</editor-fold/>


    if ($('.gf-color-picker').length >= 1) {
        $('.gf-color-picker').wpColorPicker();
    }

    // rp content show dropdown in view
    $('#show-dropdown').click(function() {
        if ($('.blog-header-dropdown').hasClass('show')) {
            $('.blog-header-dropdown').removeClass('show');
            $('#show-dropdown').removeClass('show-dropdown-open');
        }
        else {
            $('.blog-header-dropdown').addClass('show');
            $('#show-dropdown').addClass('show-dropdown-open');
        }
    });

    var $altImage = $("#banner-alt-image");
    if (localStorage.getItem("homepage_banner_video_shown") && $altImage.length) {
        $altImage.attr("src", $altImage.attr("data-src"));
        var parent = $altImage.closest(".banner-home-type-image-alt");
        var uncle = parent.siblings(".banner-home-type-image");
        parent.addClass("banner-home-type-image").removeClass('banner-home-type-image-alt');
        uncle.addClass("banner-home-type-image-alt").removeClass('banner-home-type-image');
    }


    //<editor-fold name="SOXINT-957">
    function ctaZonePosition() {
        var BHCZ = $(".banner-home-cta-zone");
        if (BHCZ.length) {
            var homeText = $(".banner-home-text");
            if (homeText.length) {
                // console.log(homeText);
                var hometextbottom = homeText.position().top + homeText.height();
                var totalHeight = BHCZ.parent().height();
                BHCZ.height(totalHeight - hometextbottom - 10);
                console.log(totalHeight, hometextbottom, totalHeight - hometextbottom);
                BHCZ.removeClass("hidden");
            }
        }
    }

    $(document).ready(function() {
        ctaZonePosition();
    });
    $(window).on("resize", function() {
        ctaZonePosition();
    });

    // </editor-fold>
    //must be set early, to avoid flash of not completely styled content
    $('.top-menu-carousel-titles-slick').slick({
        swipe: false,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
        fade: true,
        speed: 200,
    });

    var $topMenuCarouselSlick = $('.top-menu-carousel-slick');

    var $slider = $topMenuCarouselSlick.slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true,
        asNavFor: '.top-menu-carousel-titles-slick',
        responsive: [
            {
                breakpoint: 767,
                settings: {
                    dots: false,
                }
            }
        ]
    });
    // <editor-fold desc="SOXINT-1015: Lazyload triggers">
    lazyloadInitialLoad();

    $(lazySelector).on("lazyload", function () {
        lazyLoad($(this));
    });

    $(checkVisibleSelector).on("check-visible", function () {
        var bottomToCheck = windowPosition.bottom + marginOuter;
        var topToCheck = windowPosition.top - marginOuter;
        checkVisible($(this), topToCheck, bottomToCheck);
    });

    $(window).on("scroll resize manualLazyload", function (e) {
        // console.log("trigger", e.type);
        //triggers on scroll & resize, updates windowPosition. Adds triggers with scroll direction
        var currentTop = $(window).scrollTop();
        var currentBottom = currentTop + $(window).height();
        // var scrolled = false;
        if (currentTop < windowPosition.top) {
            // scrolled = true;
            $(window).trigger("scrollUp", currentTop - marginOuter);
        }
        if (currentTop > windowPosition.top) {
            // scrolled = true;
            $(window).trigger("scrollDown", currentBottom + marginOuter);
        }


        windowPosition = {top: currentTop, bottom: currentBottom};
        $(checkVisibleSelector).trigger("check-visible", currentTop, currentBottom);
        // console.log(windowPosition);
    }).on("scrollDown", function (e, sight) {
        scrollDown(sight);
    }).on("scrollUp", function (e, sight) {
        scrollUp(sight);
    }).on("inititialLoad", function () {
        lazyloadInitialLoad();
    });

    $(".top-menu-main").on("mouseenter", "li", function () {
        // console.log("enterli");
        $(this).addClass("lazy-container");
        lazyLoad($(this));
    })
    // </editor_fold>

    // On before slide change add .active class on the menu
    $topMenuCarouselSlick.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
        // console.log("beforeChange");
        var topMenuTabPopup = $('.top-menu--tabs-popup li a');
        var topMenuTabMenu = $('.top-menu--tabs-menu li a');

        topMenuTabPopup.removeClass('active');
        topMenuTabMenu.removeClass('active');
        var mySlideNumber = nextSlide;
        topMenuTabPopup.eq(mySlideNumber).addClass('active');
        topMenuTabMenu.eq(mySlideNumber).addClass('active');
    });

    var once = true;
    $topMenuCarouselSlick.on("setPosition", function() {
        // var lapse = timelapse();
        // if (!once) {
        //     console.log("setPosition", lapse);
        // console.trace();
        // StackTrace.get().then(callback).catch(errback);
        // }
        once = false;
    });

    //rp content dropdown in archives
    $(document).click(function() {
        $("#blog-categories").removeClass('active');
    });

    $("#blog-categories").click(function(e) {
        e.stopPropagation();
        $(this).toggleClass('active');
    });

    // Menu top Tag click
    $('#menu-top-nav a').click(function() {
        var menutopitemname = $(this).text();
        dataLayer.push({
            'event': 'click-header-menu',
            'name': menutopitemname
        });
    });

    // Burger menu
    $('#menu-burger-menu li.ask-for-a-quote a').click(function() {
        dataLayer.push({
            'event': 'click-get-quote'
        });
    });

    // Burger menu nav link bold on click
    $('#menu-burger-menu a.nav-link').click(function() {
        $(this).css('font-weight', 'bold');
    });

    // Download .pdf tag click
    $('a[href$=".pdf"]').click(function() {
        var pdfname = $(this).text();
        dataLayer.push({
            'event': 'download',
            'name': pdfname
        });
    });

    // RP-CONTENT
    //Menu-top
    $('#menu-top-nav a').click(function() {
        var itemname = $(this).text();
        dataLayer.push({
            'event': 'click-header',
            'text-name': itemname
        });
    });

    // Dropdown
    $('.blog-header-dropdown > li > a').click(function() {
        var itemname = $(this).text();
        dataLayer.push({
            'event': 'click-language',
            'text-name': itemname
        });
    });

    // Social networks
    $('.social > a').click(function() {
        var itemname = $(this).children().attr('Alt');
        dataLayer.push({
            'event': 'click-share',
            'sharing-element': itemname
        });
    });

    //NEWSLETTERS
    $('.form_insc_news').submit(function() {
        dataLayer.push({
            'event': 'form-newsletter',
        });
    });

    //CTAS
    //cta header
    $('.blog-header-link').click(function() {
        var itemname = $(this).text();
        dataLayer.push({
            'event': 'click-cta',
            'text-name': itemname
        });
    });


    $('.prod-tabs--content a.btn-show-more-advantages').click(function(e) {
        e.preventDefault();
        $('.prod-tabs--content .see-more-advantages').hide();
        $('.prod-tabs--content .advantage-list li').show();
    });

    // prevent carousel from jumping if clicking while sliding
    // $('.carousel-control-next, .carousel-control-prev').on('click', function(e) {
    //   e.preventDefault();
    // });

    // $(window).on('resize load', $.throttle( 250, function(){
    //   responsiveCarousel();
    // }));

    // Match height elements
    $('.sameheight').matchHeight();

    // Slick sliders init

    $('.otherneeds-carousel-slick').slick({
        dots: true,
        infinite: false,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true,
    });

    $('.choose-sodexo-slick').slick({
        dots: true,
        infinite: false,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true,
        autoplay: true,
    });

    // product page steps mobile
    $('.getting-pass-steps-slick').slick({
        dots: true,
        infinite: false,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true,
    });

    // Product page cross sell mobile
    $('.cross-sell-product-slick').slick({
        dots: true,
        infinite: false,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true,
    });

    // Product page cross sell mobile
    $('.become-affiliate-slick').slick({
        dots: true,
        infinite: false,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true,
    });


    // Go to the right slide when hovering the menu
    if (direct_link != 1) {
        $('.top-menu--tabs li a').click(function(e) {
            e.preventDefault();
            var slideId = $(this).data('slide-to');
            $slider.slick('slickGoTo', slideId);
            $('html, body').animate({
                scrollTop: $(".top_menu ").offset().top - 100
            }, 300);
        });
    }

    // Go to the wanted slide on click
    $('.top-menu--header_top-solution').click(function(e) {
        e.preventDefault();
        var slideId = $(this).data('slide-to');
        $slider.slick('slickGoTo', slideId);
    });

    $('.single-carousel-slick').slick({
        autoplay: true,
        autoplaySpeed: 6000,
        dots: true,
        infinite: true,
        speed: 600,
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true
    });

    $('.carousel-theytrustus-slick').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 5,
        slidesToScroll: 5,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    dots: false,
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });


    // Add class to navbar when scrolling
    var header = $("body.home #navbar-combined");
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if (scroll > 700) {
            header.addClass("sticky-nav");
        } else {
            header.removeClass("sticky-nav");
        }
    });

    $(document).on('click', '[data-load-more]', function(e) {
        // load more
        e.preventDefault();
        loadMore(e);
    });


    // detect swipe
    var xDown = null;

    function handleStart(e) {
        xDown = e.touches[0].clientX;
    }

    function handleTouchMove(e) {
        if (!xDown) {
            return;
        }

        var xUp = e.touches[0].clientX,
            xDiff = xUp - xDown;

        if (xDiff > 5) {
            $('#sideNav').collapse('hide');
        }
        /* reset values */
        xDown = null;
    }

    // prevent body from scrolling if layer is open
    $('#sideNav').bind('show.bs.collapse', function(e) {
        if (e.currentTarget !== e.target) {
            return;
        }
        $('body').css({
            'top': -window.pageYOffset + 'px'
        }).addClass('prevent-scroll');

        // Focus disabled (ES ticket : https://tools.publicis.sapient.com/jira/browse/SOXINT-261)
        // window.requestAnimationFrame(function(){
        //   $('#sideNav input:text:visible:first').focus();
        // });

        // bind swipe listener to close nav on mobiles
        document.addEventListener('touchstart', handleStart, false);
        document.addEventListener('touchmove', handleTouchMove, false);

    });
    $('#sideNav').bind('hidden.bs.collapse', function(e) {
        if (e.currentTarget !== e.target) {
            return;
        }
        var top = parseInt($('body').css('top'), 10) * -1;
        $('body').removeClass('prevent-scroll').css({
            'top': ''
        });
        window.scrollTo(0, top);

        // unbind swipe listener to close nav on mobiles
        document.removeEventListener('touchstart', handleStart);
        document.removeEventListener('touchmove', handleTouchMove);
    });


    // Toggle class on element
    $(document).on('click', "[data-toggle-class]", function(e) {
        e.preventDefault();
        $('#' + $(this).data('toggle-class')).toggleClass('active');
    });

    // When a tab link is clicked
    $("a.tab-nav-link").click(function() {
        // Get the text of the clicked link
        var navlink = $(this);
        var htmlString = navlink.text();
        // Put it in the fake select
        $("a.fake-select-link span").text(htmlString);

        // Get his href attributes
        var hreflink = navlink.attr("href");

        // Sync the tab with the modal menu by toggling the active class on tabs
        $("a.modal-nav-link").removeClass('active');
        $("a.modal-nav-link[href$=" + hreflink + "]").addClass('active');

        // Hide the modale
        $('#MegaTabMenuModal').modal('hide');
    });

    // When link in modal is clicked (product page)
    $("a.modal-nav-link").click(function() {
        // Get the text of the clicked link
        var navlink = $(this);
        var htmlString = navlink.text();
        // Put it in the fake select
        $("a.fake-select-link span").text(htmlString);

        // Get his href attributes
        var hreflink = navlink.attr("href");

        // Sync the tab with the modal menu by toggling the active class on tabs
        $("a.tab-nav-link").removeClass('active');
        $("a.tab-nav-link[href$='" + hreflink + "']").addClass('active');

        // Hide the modale
        $('#MegaTabMenuModal').modal('hide');
    });

    // When link in modal is clicked (homepage top carousel)
    $("a.top_menu-nav-link").click(function(e) {
        e.preventDefault();
        // Go to the wanted slide on click
        var slideId = $(this).data('slide-to');
        $slider.slick('slickGoTo', slideId);

        // Hide the modale
        $('#CarouselMenuModal').modal('hide');
    });


    // Open tabs with URL Hash on product page
    var url = document.location.toString();
    if (url.match('#tab')) {
        $('#wrapper-product-megatab .nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
        $('html, body').animate({
            scrollTop: $("#wrapper-product-megatab").offset().top
        }, 300);
    }
    // Change hash in URL for page-reload
    $('#wrapper-product-megatab .nav-tabs a, #MegaTabMenuModal .nav-item a').click(function(e) {
        window.location.hash = e.target.hash;
    })


    // Blog HP masonry grid
    $(window).load(function() {
        $('.blog-component--block ul').masonry({
            itemSelector: '.blog-item',
            columnWidth: 250,
            percentPosition: true
        });
    });

    // detect if is on IE11 and add class ie11 to body element
    /*
    var isIE11 = !!window.MSInputMethodContext && !!document.documentMode;
    if (isIE11) {
    $('body').addClass('ie11');
  }
  */

    $('#document-needed-selector').change(function(event) {
        event.preventDefault();
        $('.type-contents').hide();
        $('#back-document-needed').show();
        $('#select-document-needed').hide();
        $('.info-blocks_pretitle-title').hide();
        $('#' + $(this).val()).show();
    });

    $(document).on('click', '#back-document-needed', function(event) {
        event.preventDefault();
        $('#select-document-needed').show();
        $('#back-document-needed').hide();
        //$('.type-contents').hide();
        $('.info-blocks_pretitle-title').show();
    });

    // -------------------------------------------------------------------------
    // INSCRIPTION NEWSLETTER FOOTER
    //-------------------------------------------------------------------------
    //
    // Fonction ouvre popin
    function openPopinNews($titrePopin, $textPopin) {
        if (!$("#modalNewsletter").length > 0) {
            $('<div id=\"modalNewsletter\" class=\"modal fade \" tabindex=\"-1\"><div class=\"modal-dialog\"><div class=\"modal-content\"><div class=\"modal-header\"><button class=\"close\" type=\"button\" data-dismiss=\"modal\"></button><h4 id=\"modalNewsletterLabel\" class=\"modal-title\"></h4></div><div class=\"modal-body\">' + $textPopin + '</div><div class=\"modal-footer\"><button class=\"btn btn-default\" type=\"button\" data-dismiss=\"modal\">' + closetext + '</button></div></div></div></div>').appendTo("body");
        }
        // Open modal
        $('#modalNewsletter').modal('show');
    }

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    $("form.form_insc_news").submit(function(e) {

        e.preventDefault();

        var newsFormInput = $(this).find('input[type=email]');
        var userEmail = newsFormInput.val();

        if (validateEmail(userEmail)) { // email validation check

            // recuperer data
            var newsFormAction = $(this).attr("action");
            var attrPosition = newsFormInput.attr("data-layer-position");

            // Envoyer data Ajax
            $.ajax({
                method: "POST",
                url: newsFormAction,
                data: {userEmail: userEmail},
                success: function(data) {
                    openPopinNews("titre", data);
                    dataLayer.push({
                        'event': 'form-newsletter',
                        'location': attrPosition
                    });
                },
                error: function(xhr, status, error) {
                    openPopinNews("error", error);
                }

            }) //END AJAX

        }

    }); // END Form submit


    /*
    -------------------------------------------------------------------------------Â¬Ã…â€œ
    ACTIONS SPECIFIQUES A CERTAINES PAGES
    -------------------------------------------------------------------------------Â¬Ã…â€œ
    */
    /*
    ACTIONS HOME
    -------------------------------------------------------------------------Â¬Ã…â€œ */
    if ($body.is(".home")) {
        $("#banner_arrow_down").on('click', function(e) {
            $('html, body').animate({
                scrollTop: $(".top_menu ").offset().top - 100
            }, 1000);
        });

    }
    /*
    ACTIONS ARCHIVE FAQS
    -------------------------------------------------------------------------Â¬Ã…â€œ */
    else if ($body.is(".post-type-archive-faq")) {

        if (location.href.indexOf("#topics") != -1) {
            $('html, body').animate({
                scrollTop: $("#topics").offset().top - $("#MenuTop").height() - 10
            }, 500);
        }

    }


    /*
    -------------------------------------------------------------------------------Â¬Ã…â€œ
    ACTIONS POUR TOUS LES ACCORDIONS
    -------------------------------------------------------------------------------Â¬Ã…â€œ
    */

    $('.panel-group .panel-default').on('click', function() {

        $('.panel-collapse.in').collapse('hide');

    });


    // LMT autoclose & overlay
    var leadManagmentToolsSelector = '#lead-management-tools';
    var lmtOverLay = $('#lmt-overlay');

    $(leadManagmentToolsSelector).on('show.bs.collapse', ".collapse", function() {
        $('#lead-management-tools .collapse.show').collapse('hide');
        lmtOverLay.addClass('show');
    }).on('hide.bs.collapse', ".collapse", function() {
        lmtOverLay.removeClass('show');
    }).on('shown.bs.collapse', '#collapseQuote', function() {
        var collapsedquote = $(this);
        var recipientquote = collapsedquote.data('iframe');
        collapsedquote.find('.iframe-container').html('<iframe src="' + recipientquote + '" border="0"></iframe>')
    });


    lmtOverLay.on("click", function() {
        $('#lead-management-tools .collapse.show').collapse('hide');
    });


    // Footer popin contact-us
    $('#contactPopinFooter .collapse').on('show.bs.collapse', function() {
        $('#contactPopinFooter .collapse.show').collapse('hide');
        // lmtOverLay.addClass('show');
    })

    // Footer popin iframe load
    $('#contactPopinFooter #collapseMessagefooter').on('shown.bs.collapse', function() {
        var collapsedpopfooter = $(this);
        var recipientpopfooter = collapsedpopfooter.data('iframe');
        collapsedpopfooter.find('.iframe-container').html('<iframe src="' + recipientpopfooter + '" border="0"></iframe>');
    })

    /*
    -------------------------------------------------------------------------------Â¬Ã…â€œ
    ACTIONS CLICK LEAD MANAGEMENT
    -------------------------------------------------------------------------------Â¬Ã…â€œ
    */

    // function focusInputSearch(){
    //     $("#collapseSearch").find( "input[type='search']" ).focus();
    // }

    // Focus sur le search
    $('#lead-management-tools #collapseSearch').on('shown.bs.collapse', function() {
        $(this).find("input[type='search']").focus();
    });

    /*
    -------------------------------------------------------------------------------Â¬Ã…â€œ
    ACTIONS CLICK lmt-layer-contact & contact footer popin
    -------------------------------------------------------------------------------Â¬Ã…â€œ
    */
    $('.lmt-layer-contact select.custom-select').on('change', function(e) {

        // Hide all divs
        $(this).closest("form").parent().find(".lmt-contact-info").hide();

        // Show div
        var target = $(this).find("option:selected").attr("value");
        $('#' + target).show();
    });


    if (document.querySelector('[data-iframe-map]')) {
        storeIframe(document.querySelector('[data-iframe-map]'));
    }

    /*Send pdf function*/
    $body.on("submit", "#mail-pdf-form", function(event) {
        var $my = $(this);
        // console.log("sendPDF", "SOXINT-846");
        var formurl = $my.attr("action");
        var usermail = $('#send_pdf').val();
        if (validateEmail(usermail)) { // email validation check
            if (!$my.find(".loader").length) {
                $my.append("<div class='loader'><span class=\"fa fa-spinner fa-pulse fa-3x\" aria-hidden=\"true\"></span></div>");
            }

            $.ajax({
                type: "POST",
                url: formurl,
                data: {send_pdf_mail: usermail},
                cache: false,
                success: function() {
                    $my.find('.loader').hide(0);
                    $("#pdf_modal_success").removeClass("hidden-xs-up");
                    $('#mail-pdf-form .input-group').addClass("hidden-xs-up");
                },
                error: function() {
                    $my.find('.loader').hide(0);
                    $("#pdf_modal_error").removeClass("hidden-xs-up");
                }
            });
        }

        event.preventDefault();
    });


    // Outdatedbrowser event listener: DOM ready
    function addLoadEvent(func) {
        var oldonload = window.onload;
        if (typeof window.onload != 'function') {
            window.onload = func;
        } else {
            window.onload = function() {
                if (oldonload) {
                    oldonload();
                }
                func();
            }
        }
    }

    // Outdatedbrowser call plugin function after DOM ready
    addLoadEvent(function() {
        outdatedBrowser({
            bgColor: '#f25648',
            color: '#ffffff',
            lowerThan: 'borderImage'
        })
    });

    $('#typeformModal').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var recipient = button.attr('data-typeform'); // can not use .data api, because this attribute can be changed
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this);
        var $modalBody = modal.find('.modal-body');
        $modalBody.html('');
        if ($modalBody.hasClass("apple") || $modalBody.hasClass("ajax")) {
            $.ajax({
                url: recipient + "&ajaxIncluded&width=" + window.innerWidth,
                success: function(data) {
                    // console.log(data);
                    $modalBody.html(data);
                }
            })
        } else {
            $modalBody.html('<iframe class="typeformmodal-iframe" src="' + recipient + '" border="0"></iframe>');
            // $modalBody.trigger("receiveIframe");
        }

    }).on("submit", ".ajax .gf-iframe form", function(e) {
        // console.log("fake submit");
        e.preventDefault();
        fakesubmit($(this));
    }).on("fakesubmitChangepage", ".ajax [data-action]", function(e) {
        // console.log("fake no-submit");
        e.preventDefault();
        fakesubmit($(this));
    });


    $("body").on("click", ".callToAction-groupLinks .callToAction-link", function() {
        var $my = $(this);
        if (!$my.attr("data-typeform")) {
            var typeform = $('.iframe-typeform-wrapper iframe').attr("src");
            $my.attr("data-typeform", typeform);
        }
        $("#typeformModal").trigger('show.bs.modal');
    });


    // $("#more_posts").on("click",function(event){ // When btn is pressed.
    //     event.preventDefault();
    //     // alert('toto');
    //     $("#more_posts").attr("disabled",true); // Disable the button, temp.
    //     load_posts();
    // });

    $(".rp-content .blog-item").slice(0, 16).show();
    $(".rp-content #more_posts").on("click", function(event) {
        event.preventDefault();
        $(".rp-content .blog-item:hidden").slice(0, 16).show();
        $('.blog-component--block ul').masonry({
            itemSelector: '.blog-item',
            columnWidth: 250,
            percentPosition: true
        });
        if ($(".rp-content .blog-item:hidden").length == 0) {
            $(".rp-content #more_posts").hide()
        }
    });


    window.mobileAndTabletcheck = function() {
        var check = false;
        (function(a) {
            if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) {
                check = true;
            }
        })(navigator.userAgent || navigator.vendor || window.opera);
        return check;
    };

    if (window.mobileAndTabletcheck() == true) {
        $('.banner-home-type .banner-home-type-video').hide();
    }

    $.fn.isFading = function() {
        return this.css('opacity') < 1;
    };
    //Button to scroll back to top of body
    $(window).scroll(function() {
        if ($('#return-to-top').isFading()) {
            return;
        }

        if ($(this).scrollTop() >= 50) { //Avoid running fadeIn will is already fadeIn
            if (!$('#return-to-top').isFading()) {
                $('#return-to-top').fadeIn(200);
            }    // Fade in the arrow
        }
        else if ($('#return-to-top').css("display") != "none") {
            $('#return-to-top').fadeOut(200);   // Else fade out the arrow
        }
    });
    $('#return-to-top').click(function(e) {
        e.preventDefault();
        $('body,html').animate({
            scrollTop: 0                       // Scroll to top of body
        }, 500);
    });

    //open all pdf on target_blank
    $('a').on('click', function(e) {
        var arr = $(this).attr('href').split('.');
        if (arr[arr.length - 1] === 'pdf') {
            e.preventDefault();
            window.open($(this).attr('href'));
        }
    });

    //SOXINT-745

    if (is_iPhone_or_iPod()) {
        $(".modal-body").addClass("apple").find("iframe").attr("scrolling", "no");

        // SOXINT-796
        // remove focus when scrolling so we can hide the caret
        $('.modal-body.apple').scroll(function() {
            var selected = $('.modal-body.apple input[type="text"]:focus');
            if (selected.length > 0) {
                selected.blur();
            }
        });
        // END SOXINT-796
    }

    if (is_android) {
        // SOXINT-976
        // console.log("check android");
        $('.modal-body').on("scroll touchmove", function() {
            // console.log("scroll touchmove");
            var selected = $('.modal-body input[type="text"]:focus');
            if (selected.length > 0) {
                selected.blur();
            }
        });
        // END SOXINT-976
    }


    //END SOXINT-745
    //Addon SOXINT-740

    // var $morePosts = $("#more_posts");


    $body.on("click", "#more_posts", function(e) {
        e.preventDefault();
        // console.log("click #more_posts");
        load_posts($(this), $("#blog-articles"));
    }).on("receiveData", "#blog-articles", function(e, object) {
        var $receiver = $(this);
        var $data = object["data"];
        // var types = object["types"];

        var posts_per_page = object["posts_per_page"] * 1;
        var callback = object["callback"];
        if ($data.length < posts_per_page) {
            var callbackNoLeft = object["callbackNoLeft"];
            callbackNoLeft();
        }

        var width_first = $receiver.find(".blog-item:first-of-type").width();
        var $newData = [];
        var i = 0;
        $.each($data, function() {
            //we don't put newsletter forms in displayed response, they are broken
            if (!$(this).find(".emailfooternewsletter").length) {
                if (i < posts_per_page) {
                    var $img = $(this).find('img');
                    var $img_wrapper = $(this).find('.image-wrapper');
                    if ($img.length) {
                        var initial_width = $img.attr("width");
                        var initial_height = $img.attr("height");
                        if (initial_width) {
                            var ratio = width_first / initial_width;
                            // console.log(width_first, initial_width, ratio);
                            var min_height = initial_height * ratio;
                            $img_wrapper.css("min-height", min_height);
                        }
                    }
                    $newData.push(this);
                }
                ++i;
            }
        });
        // console.log(types);

        $receiver.find(".blog-item").addClass("old");
        $receiver.addClass("loaded-after").append($newData);

        setTimeout(function() {
            $receiver.masonry('appended', $newData);
            $receiver.find(".image-wrapper").removeClass("initial-load");
            callback();

        }, 400);
    });


    function load_posts($sender, $receiver) {
        if (!$sender.hasClass('disabled')) {
            // Extract info from data-* attributes;
            var cat = $sender.data('category') || 0;
            var posts_per_page = $sender.data('per-page') * 1 || 8;
            //we don't use $sender.data("page") because $sender.data("page") is not refreshed when this function is called more than one time
            // var page_number = $sender.attr('data-page') * 1 || Math.floor($receiver.find("> *").length / posts_per_page); //if there is 17 elements already displayed then we must be on page 4 if there is 4 elements per page
            // ++page_number;
            var list_not_in = "";
            var sep = "";
            var types = [];
            $receiver.find("[data-id]").each(function() {
                list_not_in += sep + $(this).attr("data-id");
                var type = $(this).attr("data-type");
                if (types.indexOf(type) === -1) {
                    types.push(type);
                }
                sep = ",";
            });

            var str = '&cat=' + cat + '&pageNumber=1&not_in=' + list_not_in + '&posts_per_page=-1&action=blog_action&not_display_wrapper=true&use-image-wrapper=true&type=' + types.join(",");

            $sender.addClass("loading");
            $.ajax({
                type: "POST",
                dataType: "html",
                url: ajaxurl,
                data: str,
                success: function(data) {
                    var $data = $(data);
                    if ($data.length) {
                        $receiver.trigger("receiveData", {
                            "data": $data, "callback": function() {
                                $sender.removeClass("loading");
                            }, "posts_per_page": posts_per_page, "callbackNoLeft": function() {
                                $sender.addClass("disabled").hide(0);
                            }, "types": types
                        });
                    } else {
                        $sender.addClass("disabled").hide(0);
                        $sender.removeClass("loading");
                    }

                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $loader.html(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                    $sender.removeClass("loading");
                }
            });

        }
        return false;
    }

    //EndAddon SOXINT-740

    // <editor-fold name="SOXINT-724">
    var client_browser = "";
    navigator.sayswho = (function() {
        var ua = navigator.userAgent, tem,
            M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
        if (/trident/i.test(M[1])) {
            tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
            return 'IE ' + (tem[1] || '');
        }
        if (M[1] === 'Chrome') {
            tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
            if (tem != null) {
                return tem.slice(1).join(' ').replace('OPR', 'Opera');
            }
        }
        M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
        if ((tem = ua.match(/version\/(\d+)/i)) != null) {
            M.splice(1, 1, tem[1]);
        }
        return M;
    })();

    var $singleCaroussel = $(".single-carousel");
    var containersMaxHeight = 0;
    var initial_height = 0;
    var mustResize = true;
    if ($singleCaroussel.length) {
        var who = navigator.sayswho;

        if (who[0]) {
            if (!Array.isArray(who)) {
                who = who.split(" ");
            }
            client_browser = who[0].toLowerCase().trim();
            $singleCaroussel.addClass("is-" + client_browser);
            switch (client_browser) {
                case "ie": {
                    $singleCaroussel.find(".slick-track").css("height", '100%');
                    // $singleCaroussel.find(".hasbutton .single-carousel--container_text").css("max-width", "40%");
                }
            }
        }
        // console.log(client_browser);

        var minNumberFontSize = 3000;
        // var minUnitFontSize = 3000;
        var windowWidth = $(window).width();
        var formerWidth = windowWidth;

        var $containers = $singleCaroussel.find(".single-carousel--container");

        var $slick_list = $singleCaroussel.find(".slick-list");
        initial_height = $slick_list.css("height");

        $(document).ready(function() {

            $slick_list.css("max-height", initial_height);
            containersMaxHeight = initial_height.replace("px", "") * 1;

            resizeCarouselFonts($containers, windowWidth);
            setMinFontSizeToAll($containers);

        });


        $(window).on("resize", function() {
            windowWidth = $(window).width();
            console.log("resize ", mustResize);
            if (windowWidth !== formerWidth || mustResize) {
                //horizontal
                mustResize = false;
                resizeCarouselFonts($containers, windowWidth);
                setMinFontSizeToAll($containers);
                carouselNeedsResizeImages();
            }
            setTimeout(function() {
                formerWidth = windowWidth;
            }, 300);

        });

    }


    function resizeCarouselFonts($containers, windowWidth) {
        // console.log("resizeCarouselFonts");
        $containers.each(function() {
            var $my = $(this);
            var width = $my.width();
            var $singleCarouselNumber = $my.find(".single-carousel--container_number");
            var $singleCarouselBtnSodexo = $my.find(".btn-sodexo");

            singleCarouselNumberAutoFontSize($singleCarouselNumber, width, windowWidth);
            fixCarouselBtn($singleCarouselBtnSodexo, width, windowWidth);
        });
    }

    function setMinFontSizeToAll($element) {
        var $numberValue = $element.find(".number_value");
        $numberValue.css("font-size", minNumberFontSize);
        var $parent = $element.parents(".slick-slide");
        // console.log("setMinFontSizeToAll", containersMaxHeight)
        $parent.css("min-height", containersMaxHeight).addClass("flex-center-center");
    }

    function fixCarouselBtn($element, parentWidth, windowWidth) {
        var fontSize = numberNoUnit(getDataCss($element, 'font-size'));
        var width = $element.width();
        var maxWidth = parentWidth * (40 / 100);
        if (windowWidth <= 560) {
            maxWidth = parentWidth;
        }

        if (width > maxWidth) {
            var ratio = maxWidth / width;
            var nfs = fontSize * ratio;
            if (nfs < 10) {
                nfs = 10;
            }
            $element.css("font-size", nfs);
            // console.log("fixCarouselBtn", width, maxWidth);
        } else {
            // $element.css('font-size', $element.data("font-size"));
        }
    }

    function getDataCss($element, data, reset) {
        var val = null;
        if ($element.length) {
            if (reset) {
                $element.css(data, '');
            }

            val = $element.data(data);

            if (!val) {
                val = $element.css(data);

                $element.attr("data-" + data, val);
            }
            return val;
        }
    }

    function numberNoUnit(value) {
        if (value) {
            value = value.replace("px", "") * 1;
        }
        return value;
    }

    function setFontSizeToRatio($element, ratio, min) {
        var fontSizeValue = numberNoUnit(getDataCss($element, "font-size", true));
        var fix = 1;
        // console.log(client_browser+"-fix");
        switch (client_browser) {
            case 'ie': {
                //fix for ie... buggy ie
                fix = 0.8;
            }
        }

        var newFontSizeValue = fontSizeValue * ratio * fix;

        if (newFontSizeValue < min) {
            newFontSizeValue = min;
        }

        $element.css("font-size", newFontSizeValue);
        return newFontSizeValue;
    }

    function singleCarouselNumberAutoFontSize($element, parentWidth, windowWidth) {
        var width = $element.width();
        var maxWidth = parentWidth * (60 / 100);
        if (windowWidth <= 560) {
            maxWidth = parentWidth;
        }
        // console.log(parentWidth);
        switch (client_browser) {
            case 'chrome':
            case 'opera': {
                if (parentWidth <= 560) {
                    $('body').find(".hasbutton").addClass("fixed");
                } else {
                    $('body').find(".hasbutton").removeClass("fixed");
                }
            }
        }

        var $numberValue = $element.find(".number_value");
        var $unitValue = $element.find(".number_unit");

        var widths = $numberValue.width() + $unitValue.width();

        var fontSizeNumberValue = numberNoUnit(getDataCss($numberValue, "font-size", true));
        // var fontSizeUnitValue = numberNoUnit(getDataCss($unitValue, "font-size", true));

        if (fontSizeNumberValue < minNumberFontSize) {
            minNumberFontSize = fontSizeNumberValue;
        }
        // if (fontSizeUnitValue < minUnitFontSize) {
        //     minUnitFontSize = fontSizeUnitValue;
        // }
        var newFontSizeNumberValue, newFontSizeUnitValue;
        if (width > maxWidth) { //desktop
            var ratio = maxWidth / width;
            newFontSizeNumberValue = setFontSizeToRatio($numberValue, ratio, 16);
            // newFontSizeUnitValue = setFontSizeToRatio($unitValue, ratio, 9);
            if (newFontSizeNumberValue < minNumberFontSize) {
                minNumberFontSize = newFontSizeNumberValue;
            }
            // if (newFontSizeUnitValue < minUnitFontSize) {
            //     minUnitFontSize = newFontSizeUnitValue;
            // }

            $numberValue.css("font-size", newFontSizeNumberValue);
            // $unitValue.css("font-size", ".37em");
            // console.log("singleCarouselAutoFontSize", "pw =", parentWidth, "vw =", windowWidth, "w =", width, "mw =", maxWidth, 'ratio =', ratio);


        } else if (widths > width) {

            var ratio2 = width / widths;
            newFontSizeNumberValue = setFontSizeToRatio($numberValue, ratio2, 16);
            // newFontSizeUnitValue = setFontSizeToRatio($unitValue, ratio2, 9);


            if (newFontSizeNumberValue < minNumberFontSize) {
                minNumberFontSize = newFontSizeNumberValue;
            }
            // if (newFontSizeUnitValue < minUnitFontSize) {
            //     minUnitFontSize = newFontSizeUnitValue;
            // }

            // console.log("widths > width", widths, width, ratio2);
        }

        var $parent = $element.parents(".slick-slide");
        var height = $parent.height() + 35;
        var maxHeight = initial_height.replace("px", '') * 1;
        if (height > maxHeight) {
            //avoid + 35 px height on each resize event
            height = maxHeight;

        }

        if (height > containersMaxHeight) {
            containersMaxHeight = height;
            // console.log("singleCarouselAutoFontSize", height);
        }


    }

    // </editor-fold> SOXINT-724

    // <editor-fold desc="SOXINT-883">
    // console.log("SOXINT-883");
    // carouselNeedsResizeImages();

    $(window).on("resize", function () {
        $(".new-carousel-needs").removeClass("resized");
        carouselNeedsResizeImages();
    });
    $body.on("click", ".slick-arrow, .slick-dots button", function () {
        carouselNeedsResizeImages();
        setTimeout(function () {
            scrollUpAndDown();
            var slideCurrent = $(".slick-current");
            var media = slideCurrent.find(".media");
            if (media.length) {
                var minHeight = media.height();
                console.log("slick-arrow", minHeight);
                media.parents('a').css({"min-height": minHeight + 20});
                media.parents('.col').css({"min-height": minHeight + 20});
                media.parents('.slick-list').css({"min-height": minHeight + 20});
            }
        }, 300);

    });

    function carouselNeedsResizeImages() {
        console.log("carouselNeedsResizeImages");
        var carNeeds = $(".new-carousel-needs");
        if (carNeeds.length) {
            // carNeeds.addClass(".lazy-container");
            var images = carNeeds.find(".top-menu-carousel_block-image");
            var maxi = 0;
            images.each(function () {
                var height = $(this).height();
                if (height > maxi) {
                    maxi = height;
                }
            });
            // console.log("carouselNeedsResizeImages", mini);
            if (maxi > 1) {
                images.css("height", maxi + "px").addClass("resized");
            }
        }

    }


    // </editor-fold>
    // <editor-fold desc="SOXINT-937">
    if (DQEPluginIsActive && DQEPluginIsActive.length != 0) {
        var DQEFields = ['.social-dqe', '.sirret-dqe', '.zipcode-dqe', '.city-dqe', '.street-dqe'];
        var hasDQE = false;
        var concernedForm = null;
        var b2b;
        $.each(DQEFields, function(key, selector) {
            var item = jQuery(selector);
            if (item.length) {
                item.closest(".ginput_container").addClass("ui-front");
                //the ui-font class is the root that receive autocomplete ui
                hasDQE = true;
                if (!concernedForm) {
                    concernedForm = item.closest("form");
                }
            }
        });
        if (concernedForm && concernedForm.length) {
            $.getScript("https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js", function() {
                $.getScript(DQEJSUrl, function() {
                    $.getScript(DQEJSB2BUrl, function() {
                        concernedForm.dqe({
                            server: DQEPluginURL,
                            // single : '.street-dqe',
                            zip: '.zipcode-dqe',
                            city: '.city-dqe',
                            street: '.street-dqe'
                        });
                        b2b = concernedForm.dqeb2b({
                            server: DQEPluginURL,
                            ac_company: '.social-dqe',
                            ac_callback: 'callback_b2b_autocomplete'
                        });

                    });
                });
            });
        }
    }
    // </editor-fold>
    // <editor-fold desc="SOXINT-968">
    // console.log(SimulatorsSettings);
    if (SimulatorsSettings && SimulatorsSettings["active"] && SimulatorsSettings["showBlock"]) {
        // console.log("SOXINT-968");
        $(window).on("scroll", function() {
            if (!$body.hasClass("simulatorScroll")) {
                // console.log("SOXINT-968", "one scroll", SimulatorsSettings["manifest"]);
                $.getScript(SimulatorsSettings["manifest"], function() {
                    $.getScript(SimulatorsSettings["normalize"], function() {
                        $.getScript(SimulatorsSettings["vendor"], function() {
                            $.getScript(SimulatorsSettings["main"], function() {
                                console.log("SOXINT-968", "loaded");
                            });
                        });
                    });
                });
                $body.addClass("simulatorScroll");
            }
        });
    }
    //</editor-fold/>

    $body.on(
            "click mousedown",
            ".cross-sell--container img, .cross-sell--container " +
            ".cross-sell--container_description",
            function (e) {
                var href = '';
                var parent = $(this).parents(".cross-sell--container");
                var a = parent.find("a");
                if (a.length) {
                    href = a.attr("href");
                }
                if (href) {
                    console.log("click", href, e.which);
                    switch (e.which) {
                        case 3:
                            break;
                        case 2:
                            window.open(href, "_blank");
                            break;
                        case 1:
                        default:
                            document.location.href = href;
                            break;
                    }
                }
            });

    // <editor-fold desc="SOXINT-1015: Lazyload Core">
    function checkParentsVisibility($item, visible) {
        var ret = visible;
        if (!checkParentsLazyload) {
            return ret;
        }
        // var log = false;
        // var src = false;
        // if ($item.attr("data-lazy-src") && $item.attr("data-lazy-src").indexOf("reason_07") !== -1) {
        //     log = true;
        //     src = $item.attr("data-lazy-src");
        //     console.log("before check Parents ", src, visible)
        // }

        if (visible) {
            if ($item.parents(".sub-menu").length) {
                ret = false;
            }
            if ($item.parents(".choose-sodexo--mobile, .choose-sodexo--desktop").length) {
                ret = $item.parents(".choose-sodexo--mobile, .choose-sodexo--desktop").css("display") !== "none";
            }
        }
        // if (log) {
        //     console.log("after check Parents ", src, ret)
        // }
        return ret;
    }

    function visibleInElement($item, coordElement, marginView) {
        if (!marginView) {
            marginView = 0;
        }
        var small = getCoords($item);
        var big = coordElement;
        if (!big['top']) {
            big['top'] = $window.scrollTop() - marginView;
        }
        if (!big['left']) {
            big['left'] = 0;
        }
        if (!big['right']) {
            big['right'] = $window.width();
        }
        if (!big['bottom']) {
            big['bottom'] = $window.scrollTop() + $window.height() + marginView;
        }

        var visibleTop = valueBetween(small.top, big.top, big.bottom) || valueBetween(small.bottom, big.top, big.bottom) || (small.top < big.top && small.bottom > big.bottom);
        var visibleLeft = valueBetween(small.left, big.left, big.right) || valueBetween(small.right, big.left, big.right) || (small.left < big.left && small.right > big.right);

        var ret = visibleTop && visibleLeft;

        ret = checkParentsVisibility($item, ret);

        return ret;
    }

    function fullyVisibleInElement($item, coordElement, marginView) {
        if (!marginView) {
            marginView = 0;
        }
        var ret = false;
        var small = getCoords($item);
        var big = coordElement;
        if (!big['top']) {
            big['top'] = $window.scrollTop() - marginView;
        }
        if (!big['left']) {
            big['left'] = 0;
        }
        if (!big['right']) {
            big['right'] = $window.width();
        }
        if (!big['bottom']) {
            big['bottom'] = $window.scrollTop() + $window.height() + marginView;
        }

        if (visibleInElement($item, big, marginView)) {
            var fullX = valueBetween(small.top, big.top, big.bottom) && valueBetween(small.bottom, big.top, big.bottom);
            var fullY = valueBetween(small.left, big.left, big.right) && valueBetween(small.right, big.left, big.right);
            // console.log("fullyVisibleInElement", fullX, fullY);
            return fullX && fullY;
        }
        return ret;
    }

    function lazyLoad(item) {
        if (item.hasClass("lazy-container")) {
            if (item.css("display") !== "none") {
                item.find(lazySelector).each(function () {
                    lazyLoad($(this));
                });
            }
        } else {
            if (!debugmode) {
                // console.log("lazyload");
            }
            item.addClass("loaded");
            if (item.attr("data-lazy-src")) {
                item.attr("src", item.attr("data-lazy-src"));
                item.attr("data-lazy-src", null);
            }
            if (item.attr("data-lazy-srcset")) {
                item.attr("srcset", item.attr("data-lazy-srcset"));
                item.attr("data-lazy-srcset", null);
            }
            if (item.attr("data-lazy-style")) {
                item.attr("style", item.attr("data-lazy-style"));
                item.attr("data-lazy-style", null);
            }
        }
    }

    function debugLog(message, $image) {
        var top = $image.offset().top;
        var bottom = top + $image.height();
        var srcDisp = "";

        var src = $(this).attr("src");
        if (src) {
            console.log(src);
            var split = src.split("/");
            srcDisp = split[split.length - 1]
        }

        var image = {
            "top": top,
            "bottom": bottom,
            "src": srcDisp,
        };
        var wi = {
            "top": windowPosition.top - marginOuter,
            "bottom": windowPosition.bottom + marginOuter,
        };

        console.log(message, image, wi);
    }

    function lazyloadInitialLoad() {
        var bottomToCheck = windowPosition.bottom + marginOuter;
        var topToCheck = windowPosition.top - marginOuter;
        // console.log("lazyloadInitialLoad", topToCheck, bottomToCheck);

        $(lazySelector).each(function () {
            var visible = visibleInElement($(this), {});

            if (visible) {
                // $(this).trigger("lazyload");
                lazyLoad($(this));
                if (debugmode) {
                    debugLog("lazyload initial", $(this));
                }
            }
        });

        $(checkVisibleSelector).each(function () {
            checkVisible($(this), topToCheck, bottomToCheck);
        });
    }

    function scrollUp(sight) {
        $(lazySelector).each(function () {
            var visible = visibleInElement($(this), {top: sight});
            if (visible) {
                $(this).trigger("lazyload");
                if (debugmode) {
                    debugLog("lazyload scroll up", $(this));
                }
            }
        });
    }

    function scrollDown(sight) {
        $(lazySelector).each(function () {
            var visible = visibleInElement($(this), {bottom: sight});
            if (visible) {
                $(this).trigger("lazyload");
                if (debugmode) {
                    debugLog("lazyload scroll down", $(this));
                }
            }
        });
    }

    function scrollUpAndDown() {
        console.log("scrollUpAndDown");
        var currentTop = $(window).scrollTop();
        var currentBottom = currentTop + $(window).height();
        scrollUp(currentTop - marginOuter);
        scrollDown(currentBottom + marginOuter);
    }

    //</editor-fold/>
    // console.log("Endready", timelapse());
});

//<editor-fold name="TMA-OCT-18-1">
/**
 * optimization function to be used only from browser console
 */
function checkSEO() {
    var ok = 'color: green; font-weight: bold;';
    var $title = jQuery("title");
    var $h1 = jQuery("h1");
    var $description = jQuery("meta[name='description']");
    var $OGdescription = jQuery("meta[name='og:description']");

    if ($title.length) {
        console.log("%c TITLE : " + $title.html(), ok);
    } else {
        console.error("NO title");
    }

    if ($h1.length) {
        var h1text = "";
        var h1Arr = [];
        var i = 0;
        var colst = "";
        var colen = "";
        $h1.each(function() {
            var $my = jQuery(this);
            var text = $my.text();
            h1text += colst + text + colen;
            h1Arr.push({"element": $my, "text": text});
            ++i;

        });
        if (i === 1) {
            console.log("%c H1 : " + h1text, ok);
        } else {
            console.error("Multiple H1 : ", h1Arr);
        }
    } else {
        console.error('NO h1');
    }

    if ($description.length) {
        console.log("%c DESCRIPTION : " + $description.attr("content"), ok);
    } else {
        console.error("NO metadescription");
    }
    if ($OGdescription.length) {
        console.log("%c OG:DESCRIPTION : " + $OGdescription.attr("content"), ok);
    } else {
        console.error("NO meta og:description");
    }


}

//</editor-fold/>

// <editor-fold desc="SOXINT-937">
function callback_b2b_autocomplete(data) {
    jQuery('.social-dqe').val(data.CompanyName);
    jQuery('.sirret-dqe').val(data.CompanyNumber);
    jQuery('.zipcode-dqe').val(data.ZIP_Code);
    jQuery('.city-dqe').val(data.Locality);
    jQuery('.street-dqe').val(data.CompanyAddress2);
}

//</editor-fold/>

// <editor-fold desc="SOXINT-1015: Lazyload Jquery independant functions">
function getCoords($item) {
    // console.log('getCoords', $item);
    var top = $item.offset().top;
    var left = $item.offset().left;
    var height = $item.height();
    var width = $item.width();
    var bottom = top + height;
    var right = left + width;
    return {top: top, left: left, right: right, bottom: bottom};
}

function valueBetween(value, min, max) {
    return value >= min && value <= max;
}

function checkVisible(item, topToCheck, bottomToCheck) {
    var visible = visibleInElement(item, {top: topToCheck, bottom: bottomToCheck});
    if (visible) {
        item.addClass("cv-visible");
        item.trigger("item-visible");
        if (item.hasClass("cv-invisible")) {
            item.removeClass("cv-invisible");
            item.trigger("item-visible-switch");
        }
    } else {
        item.addClass("cv-invisible");
        item.trigger("item-invisible");
        if (item.hasClass("cv-visible")) {
            item.removeClass("cv-visible");
            item.trigger("item-invisible-switch");
        }
    }
}

//</editor-fold/>
