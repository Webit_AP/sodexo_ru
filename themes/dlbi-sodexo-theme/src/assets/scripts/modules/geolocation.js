function geolocate( callback ){

  if( navigator.geolocation ) {
    navigator.geolocation.getCurrentPosition(
      // success
      function( position ){
        // console.log( "User accepted the request for Geolocation and it worked:", position );
        callback( position );
      },
      // error
      function( error ){
        switch(error.code) {
          case error.PERMISSION_DENIED:
            // console.log( "User denied the request for Geolocation." );
            break;
          case error.POSITION_UNAVAILABLE:
            // console.log( "Location information is unavailable." );
            break;
          case error.TIMEOUT:
            // console.log( "The request to get user location timed out." );
            break;
          case error.UNKNOWN_ERROR:
            // console.log( "An unknown error occurred." );
            break;
          default:
            // console.log( "An unspecified error occurred." );
        }
        callback( null );
      }
    );
  }
  else {
    // console.log( "Browser doesn't support geolocation." );
    callback( null );
  }
}

function storeIframe( elt ){

  var callback = function( position ){
    var data = JSON.parse( elt.getAttribute('data-iframe-map') ),
        iframe = document.createElement('iframe');

    if( !position ){
      iframe.src = data.unknown;
    }
    else{
      iframe.src = data.located.replace( "#{position}", position.coords.latitude+","+position.coords.longitude );
    }

    elt.innerHTML = '';
    elt.appendChild( iframe );
    elt.classList.remove('loading');
  };

  elt.classList.add('loading');
  geolocate( callback );

}