// (function lmtTools() {
//
//     var el,
//         els = {},
//         openedItem;
//
//     /**
//      * Attach event handlers
//      */
//     function bind() {
//         el.addEventListener( 'click', route );
//     }
//
//
//     /**
//      * Close active layer
//      */
//     function close() {
//         if ( openedItem ) {
//             openedItem.click();
//             openedItem = null;
//         }
//     }
//
//
//     /**
//      * Register needed DOM elements
//      */
//     function dom() {
//         el         = document.getElementById( 'lead-management-tools' );
//         els.layers = el ? [].slice.call( el.querySelectorAll( '[class*="lmt-layer"]' )) : null;
//     }
//
//
//     /**
//      * Init everything
//      */
//     function init() {
//         dom();
//
//         if ( !el || !els.layers.length ) {
//             return;
//         }
//
//         bind();
//     }
//
//
//     /**
//      * Route events
//      * @param {object} e Click event object
//      */
//     function route( e ) {
//         var closeButton = e.target.matches( '.lmt-close' ),
//             navButton   = e.target.matches( 'button[aria-controls]' ),
//             insideElm   = el.contains( e.target );
//
//         e.stopPropagation();
//
//         if ( closeButton || !insideElm ) {
//             close();
//             document.body.removeEventListener( 'click', route );
//             return;
//         }
//
//         if ( navButton ) {
//             toggleItem( e );
//             document.body.addEventListener( 'click', route );
//         }
//     }
//
//
//     /**
//      * Toggle layer on/off
//      * @param {object} e Click event object
//      */
//     function toggleItem( e ) {
//         var button = e.target.matches( 'button' );
//
//         if ( !button ) {
//             return;
//         }
//
//         var target = document.getElementById( e.target.getAttribute( 'aria-controls' ));
//
//         els.layers.forEach( function( layer ) {
//             var bt = el.querySelector( '[aria-controls="'+ layer.id +'"]' );
//
//             if ( layer !== target || ( layer === target && target.getAttribute( 'aria-hidden' ) === 'false' )) {
//                 bt.setAttribute( 'aria-expanded', 'false' );
//                 layer.setAttribute( 'aria-hidden', 'true' );
//             }
//
//             else {
//                 e.target.setAttribute( 'aria-expanded', 'true' );
//                 target.setAttribute( 'aria-hidden', 'false' );
//                 openedItem = e.target;
//             }
//         });
//     }
//
//
//     // kickstart module
//     document.addEventListener( 'DOMContentLoaded', init );
// })();
