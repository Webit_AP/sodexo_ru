var page = require('webpage').create(),
	system = require('system');

// test des parametres
/*if (system.args.length < 3) {
    console.log('Usage: phantom_generate.js au moins 2 paramètres est attendu | ' + system.args);
    phantom.exit();
}

// récupération des paramètres
var formatPage = "A3";
if (system.args[ 3 ] !== null) {
    formatPage = system.args[ 3 ];
}

var orientationPage = "portrait";
if (system.args[ 4 ] !== null) {
    orientationPage = system.args[ 4 ];
}

var viewportPageWidth = 1080;
var viewportPageHeight = 1024;
if (system.args[ 5 ] !== null) {
    var viewPortGlobal = system.args[ 5 ].split("x");
    viewportPageWidth = viewPortGlobal[0];
    viewportPageHeight = viewPortGlobal[1];
}

var htaccesslogin = "";
var htaccesspassword = "";
if (system.args[ 6 ] !== null && system.args[ 7 ] !== null) {
    htaccesslogin = system.args[ 6 ];
    htaccesspassword = system.args[ 7 ];
}
*/
/*page.paperSize = {
 format      : formatPage,
 orientation : orientationPage,
 margin      : '1cm'
 };*/

// PDF Doc's page height
page.paperSize = {
    format: 'A4',
    orientation: 'landscape',
    // border: '1cm'
    // border: '0'
    margin: {
	top: '1cm',
	bottom: '1cm'
    }
};

page.viewportSize = {
    width: 1080,
    height: 1024
};

page.settings = {
    userAgent: 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36',
    javascriptEnabled: true,
    loadImages: true,
    //userName: htaccesslogin,
    //password: htaccesspassword,
}

page.open(system.args[ 1 ], function (status) {

    if (status === "success") {
	console.log("Status: " + status);
	page.render(system.args[ 2 ]);
	phantom.exit();
    }
});