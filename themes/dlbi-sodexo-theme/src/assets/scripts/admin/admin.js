jQuery(function ($) {
    //<editor-fold desc="version indicator">
    console.log("SOXINT-916");
    //</editor-fold>
    //<editor-fold desc="variables">
    var $body = $("body");
    var loadWatcher = false;
    var addImageWatcher = false;
    var $trustUsWrapper = null;
    //</editor-fold>
    //<editor-fold desc="events">
    var $htform = $("#htaccessform");
    if ($htform.length) {
        $htform.prev("h2").hide(0);
        $htform.remove();
    }

    $body.on("click", ".wpb_carousel_they_trust_widget .vc_control-btn-edit, #carousel_they_trust_widget", setEditTheyTrustUs).on("contentLoaded", ".vc_ui-panel-window", function () {
        var $my = $(this);
        // console.log($my.attr("data-vc-shortcode"));
        switch ($my.attr("data-vc-shortcode")) {
            case "carousel_they_trust_widget": {
                theyTrustContentLoaded($my);
                break;
            }
            default: {
                break;
            }
        }
    }).on("input", ".populate-links", function () {
        // console.log("changed");
        var $line = $(this).closest(".edit_form_line");
        populateLine($line);
    }).on("mouseup", ".edit_form_line, .added", function () {
        var $my = $(this);
        //mouseup is fired after a reorder event, and even after a delete event !
        setTimeout(function () {
            // console.log("mouseup");
            populateLine($my);
        }, 1000);
    }).on("click", ".gallery_widget_add_images", function () {
        var $window = $(this).parents(".vc_ui-panel-window");
        switch ($window.attr("data-vc-shortcode")) {
            case "carousel_they_trust_widget": {
                $window.find(".added").addClass("arelady-here");
                waitingForImages();
                break;
            }
            default: {
                break;
            }
        }
    }).on("submit", "#robotstxtform", function (e) {
        var $my = $(this);
        if (!$my.find(".dropZone").length) {
            $my.find(".submit").append("<span class='dropZone'></span>");
        }
        var $dropZone = $my.find(".dropZone");
        $dropZone.html("<span class='loading'><span class=\"dashicons dashicons-update\"></span></span>");
        e.preventDefault();
        var robotText = $("#robotsnew").val();
        var post = {
            action: "gen_robots_txt",
            robotsnew: robotText,
        };
        var $submit = $("input[name=submitrobots]");
        var $create = $("input[name=create_robots]");
        if ($submit.length) {
            post["submitrobots"] = $submit.val();
        }
        if ($create.length) {
            post["create_robots"] = $create.val();
        }

        $.ajax({
            url: ajaxurl,
            data: post,
            method: "POST",
            success: function (data) {
                var ret = JSON.parse(data);
                if (ret["success"]) {
                    $dropZone.html("<span class='success'><span class=\"dashicons dashicons-yes\"></span> " + ret["msg"] + "</span>");
                } else {
                    $dropZone.html("<span class='failure'><span class=\"dashicons dashicons-no\"></span> " + ret["error"] + "</span>");
                }
            }
        });
    });
    //</editor-fold>
    //<editor-fold desc="functions">
    function populateLine($line) {
        var value = "";
        var sep = "";
        $line.find(".populate-links").each(function () {
            var val = $(this).val();
            value += sep + val;
            sep = ",";
        });
        $("input[name='links']").val(value);

        value = "";
        sep = "";
        $line.find(".populate-targets").each(function () {
            var val = $(this).val();
            if (!$(this).is(":checked")) {
                val = "";
            }
            value += sep + val;
            sep = ",";
        });
        $("input[name='targets']").val(value);//*/
    }

    function setEditTheyTrustUs() {
        loadWatcher = setInterval(function () {
            $trustUsWrapper = $('[data-vc-shortcode="carousel_they_trust_widget"]');
            if ($trustUsWrapper.length && $trustUsWrapper.find(".edit_form_line").length) {
                clearInterval(loadWatcher);
                $trustUsWrapper.trigger("contentLoaded");
            }
        }, 500);
    }

    function waitingForImages() {
        addImageWatcher = setInterval(function () {
            $trustUsWrapper = $('[data-vc-shortcode="carousel_they_trust_widget"]');
            if ($trustUsWrapper.length && $trustUsWrapper.find(".added:not(.arelady-here)").length) {
                clearInterval(addImageWatcher);
                $trustUsWrapper.trigger("contentLoaded");
            }
        }, 500);
    }

    function theyTrustContentLoaded($element) {
        var translation = $element.find("input[name='link_translation']").val();
        var translationTarget = $element.find("input[name='target_translation']").val();
        var i = 0;
        var links = $("input[name='links']").val();
        var targets = $("input[name='targets']").val();
        links = links.split(",");
        targets = targets.split(",");
        $element.find(".added").each(function () {
            var value = links[i];
            var target = targets[i];
            var checked = "";
            if (value === undefined) {
                value = '';
            }
            if (target === "_blank") {
                checked = "checked='checked'"
            }

            if (!$(this).find("input").length) {
                $(this).find("img").wrap("<div class='image-wrapper'></div>");
                $(this).append("<div class='input-wrapper'><div class='input-inner-wrapper text'>" +
                    "<label for='they-trust-link-" + i + "'>" + translation + "</label>" +
                    "<input type='url' id='they-trust-link-" + i + "' class='populate-links' value='" + value + "' />" +
                    "</div><div class='input-inner-wrapper checkbox'>" +
                    "<label for='they-trust-target-" + i + "'>" + translationTarget + "</label>" +
                    "<input type='checkbox' id='they-trust-target-" + i + "' " + checked + " class='populate-targets' value='_blank' />" +
                    "</div></div>");
                ++i;
            }
        });
        $element.find(".edit_form_line").addClass("show");
    }

    //</editor-fold>
});
