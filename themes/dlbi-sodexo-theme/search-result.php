<?php
/**
 * The template for displaying search results pages.
 *
 * @package understrap
 */
get_header();


// Retreive the origin of the search query for the Tagging Plan
$searchOrigin = "";
if ( isset($_GET['origin']) ) {
  $origin = $_GET['origin'];
  switch ($origin) {
    case "LMT" :
      $searchOrigin = "LMT"; // Search made from the FAQ
      break;
    case "Burger" :
      $searchOrigin = "Burger"; // Search made from the search page
      break;
    case "FAQ" :
      $searchOrigin = "FAQ"; // Search made from the search page
      break;
    default :
      $searchOrigin = "générique"; // Search made from the search page
      break;
  }
} else {
  $searchOrigin = "générique";
}

$bgimage = get_field('banner_faq_and_404', 'option');
if( !empty($bgimage) ) {
  $bgimgurl = $bgimage['url'];
} else {
  $bgimgurl = get_stylesheet_directory_uri() . '/dist/images/bg-404-search.jpg';
}
$style = apply_filters("lazyload-style", "background-image:url('$bgimgurl');");
?>

<section class="search-block-aside" <?php echo $style; ?>>
        <div class="container">
            <h1 class="tt"><?php echo __("We're here to help", 'lbi-sodexo-theme'); ?></h1>
            <?php
            // Output a search for that only searches portfolio post types
            $search = get_search_form(false);
            echo $search;
            ?>
        </div><!-- container search end -->
    </section>

    <section class="wrapper" id="wrapper-search">
        <div class="container" id="content">
            <?php if (!get_field('hide_faq_link', 'option')) { ?>
                <div class="row">
                  <div class="col-12">
                    <a href="<?php echo get_post_type_archive_link( SOD_FAQ_PTYPE ); ?>" class="back-link-page">
                      <i class="icon-arrow-left" aria-hidden="true"></i>
                      <span><?php echo __("Back to the FAQ page", 'lbi-sodexo-theme'); ?></span>
                    </a>
                  </div>
                </div>
            <?php } ?>
            <div class="row justify-content-md-center">
                <div class="col-12">
          		    <?php if(!empty($_GET['s']) && isset($_GET['s'])):
                    // Total posts
                    global $wp_query;

                    $query_var = '';
                    if(!empty(get_search_query()))
                      $query_var = get_search_query();
                    elseif(!empty($_GET['s']))
                      $query_var = esc_attr($_GET['s']);
                     ?>
                    <h2 class="sodexo-title"><strong><?php echo $wp_query->found_posts; ?> </strong> <?php echo __('Results for', 'lbi-sodexo-theme') ?> <strong>"<?php echo $query_var; ?>"</strong></h2>
          		    <?php endif; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-9">
                        <div class="search-items">

                            <?php if (have_posts() && !empty($_GET['s'])) :
                                $z=0;?>
                                <div class="panel-group" id="accordion-faq" role="tablist" aria-multiselectable="true">
                                <?php while (have_posts()) : the_post();

                                    /**
                                     * Run the loop for the search to output the results.
                                     * If you want to overload this in a child theme then include a file
                                     * called content-search.php and that will be used instead.
                                     */
                                    sodexo_get_template_part('loop-templates/content-search.php', array(
                                        'z' => $z
                                    ));
                                    $z++;
                                endwhile;

                                // Tag whith results ?>
                                <script>
                                  dataLayer.push({
                                    'event':'search',
                                    'value':'<?php echo addslashes($_GET['s']); ?>',
                                    'moteur-type':'<?php echo $searchOrigin; ?>'
                                  });
                                </script>
                              </div>
                            <?php else :

                                get_template_part('loop-templates/content', 'none');

                                echo get_field('empty_result', 'option');

                                // Tag no results ?>
                                <script>
                                  dataLayer.push({
                                    'event':'search-noresults',
                                    'value':'<?php echo addslashes($_GET['s']); ?>',
                                    'moteur-type':'<?php echo $searchOrigin; ?>'
                                  });
                                </script>

                            <?php endif; ?>

                        </div>
                    </div>
		    <?php if($_GET['s'] || get_query_var('s')): ?>
                    <div class="col-12 col-md-3">
                      <?php get_template_part('templates/content', 'search-page-filters'); ?>
                    </div>
		    <?php endif ?>
                </div>

                    <?php if (have_posts() && !empty($_GET['s'])) : ?>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="text-center search-paginate"><?php
                                    echo paginate_links(); ?></div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>

    </section>

    <div class="breadcrumb">
        <?php
        if(function_exists('bcn_display')):
            if(empty(get_query_var('s')) && !empty($_GET['s']))
              set_query_var('s', $_GET['s']);
            bcn_display();
        endif; ?>
    </div>


<?php get_footer(); ?>
