<?php
/**
 * The template for displaying search forms in Underscores.me
 *
 * @package understrap
 */

?>

<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
	<div class="input-group">

		<input class="field form-control" id="s" name="s" type="text" value="<?php echo $_GET['s'] ?>"
			placeholder="<?php esc_attr_e( 'Search', 'lbi-sodexo-theme' ); ?><?php // esc_attr_e( get_field('faq_archive_search_placeholder', 'option'), 'lbi-sodexo-theme' ); ?>">
		<span class="input-group-btn">
			<input class="submit btn btn-primary" id="searchsubmit" name="submit" type="submit"
			value="<?php esc_attr_e( 'Search', 'lbi-sodexo-theme' ); ?>">
	</span>
	</div>
</form>
